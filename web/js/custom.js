var fee_flag = false;
var totalfee_flag = false;
var edit_fee_flag = false;
var edit_totalfee_flag=false;
$(document).ready(function () {
    var currentUrl  = window.location.href;
    if(currentUrl.indexOf("fee/take_fee") > -1){
        getStudentYear();
    }

    var start_month = $('#start_fee_duration :selected').val();
    var end_month = $('#end_fee_duration :selected').val();
    var flag=$('#flag').val();
    if (start_month) {
        $('#start_fee_duration').find('option').each(function () {
            var selected_month = $(this).val();
            if (parseInt(selected_month) !== parseInt(start_month)) {
                $(this).attr('disabled', true);
            }
        });

        totalFeeget();
        feeMonth();
        if(flag == '3'){
            getOtherFeeDuration();
        }else if(flag == '2'){
            getPreFeeDuration();
        }else if (flag == '1'){
            getSecondaryFeeDuration();
        }
    }

    var edit_fee_start_month = $('#start_duration :selected').val();
    var editflag=$('#edit_flag').val();
    if (edit_fee_start_month) {
        $('#start_duration').find('option').each(function () {
            var selected_month = $(this).val();
            if (parseInt(selected_month) !== parseInt(edit_fee_start_month)) {
                $(this).attr('disabled', true);
            }
        });

        $('#end_duration').find('option').each(function () {
            var selected_month = $(this).val();
            var monthNames = ["June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May"];
            var monthValues = ["6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5"];
            var index_month = monthValues.indexOf(selected_month);
            var edit_index_month = monthValues.indexOf(edit_fee_start_month);
            if (parseInt(index_month) <= parseInt(edit_index_month)) {
                $(this).attr('disabled', true);
            }
            $('#selectmonth').attr('disabled',false);
        });
        edittotalFeeget();
        if(editflag == '3'){
            editgetFeeDuration();
        }else if(editflag == '2'){
            editPrePrimaryFeeDuration();
        }else if (editflag == '1'){
            editSecondaryFeeDuration();
        }
        $('#edit_concession_error').text('');
    }
    var start_installment = $('#start_fee_installment :selected').val();
    var end_installment = $('#end_fee_installment :selected').val();
    if (start_installment) {
        $('#start_fee_installment').find('option').each(function () {
            var selected_installment = $(this).val();
            if (start_installment !== selected_installment) {
                $(this).attr('disabled', true);
            }

        });
        $('#end_fee_installment').find('option').each(function () {
            var selected_month = $(this).val();
            var monthNames = ["June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May"];
            var monthValues = ["first", "second", "third", "fourth"];
            var index_month = monthValues.indexOf(selected_month);
            var edit_index_month = monthValues.indexOf(start_installment);
            if (parseInt(index_month) <= parseInt(edit_index_month)) {
                $(this).attr('disabled', true);
            }
            $('#selectinstallment').attr('disabled',false);
        });
        var tutionfee=$('#tuition_fee').val();
        $('#total_fee').val(tutionfee);
        $('#fee_period').val('first');
        getFirststepFee();
    }
    var editstart_installment = $('#start_editfee_installment :selected').val();
    var editend_installment = $('#end_editfee_installment :selected').val();
    if (editstart_installment) {
        $('#start_editfee_installment').find('option').each(function () {
            var selected_installment = $(this).val();
            if (editstart_installment !== selected_installment) {
                $(this).attr('disabled', true);
            }
        });
        $('#end_editfee_installment').find('option').each(function () {
            var selected_month = $(this).val();
            var monthValues = ["first", "second", "third", "fourth"];
            var index_month = monthValues.indexOf(selected_month);
            var edit_index_month = monthValues.indexOf(editstart_installment);

            if (parseInt(index_month) <= parseInt(edit_index_month)) {
                $(this).attr('disabled', true);
            }
            $('#selecteditinstallment').attr('disabled',false);
        });
        var tutionfee=$('#edit_tuition_fee').val();
        $('#edit_total_fee').val(tutionfee);
        $('#edit_fee_period').val('first');
        editgetFirststepFee();
    }
    var school_student_id = localStorage.getItem('School_id');
    if (school_student_id) {
        $('#add_studentid').val(school_student_id);
    }


    var school_id = localStorage.getItem('School_name');

    if (school_id) {
        $(".selected-language").text(school_id);
    }

    var editstudentdiv=$('#student_std').val();
    var schoolid = $('#edit_studentschoolid').val();

    if(editstudentdiv){
        $.ajax({
            url: "/Students/getDiv_value",
            method: "POST",
            data: {standard: editstudentdiv, school_id: schoolid},
            success: function (data) {
                var std_data = JSON.parse(data);
                var div = std_data.division;
                var division = div.split(",");
                var len = division.length;
                $('#edit_studentstdid').val(std_data.id);
                var selecteddiv=$('#editstudent_div').val();
                $("#editstudent_div").empty();
                $("#editstudent_div").append("<option value=''>Select</option>");
                for (var i = 0; i < len; i++) {
                    if(division[i] == selecteddiv){
                        $("#editstudent_div").append("<option value='" + division[i] + "' selected>" + division[i] + "</option>");
                    }else{
                        $("#editstudent_div").append("<option value='" + division[i] + "'>" + division[i] + "</option>");
                    }

                }
            }
        });
    }
    /************ import csv START ****************/
    $('#import_csv').on('submit', function (event) {
        event.preventDefault();
        $.ajax({
            url: "/Csv_import/import",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $('#import_csv_btn').html('Importing...');
            },
            success: function (data) {
                $('#import_csv')[0].reset();
                $('#csv_file').val('');
                $('#import_csv_btn').attr('disabled', false);
                $('#import_csv_btn').html('Import Done');
            }
        })
    });
    /************ import csv END ****************/

    /************ edit admin profile in nav_bar START ****************/
    $(document).on('submit', '#edit_form', function (event) {
        event.preventDefault();
        $.ajax({
            url: "/edit_profile/edit_profile",
            method: 'POST',
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function (data) {
                $('#manage_profile').modal('hide');
                $('.modal').removeClass('in');
                $('.modal').attr("aria-hidden", "true");
                $('.modal').css("display", "none");
                $('.modal-backdrop').remove();
                $('body').removeClass('modal-open');
                sessionStorage.reloadAfterPageLoad = true;
                window.location.reload();

                $('#edit_form')[0].reset();

            }
        });
    });
    $('#manage_profile').on('hidden.bs.modal', function () {
        $(".help-block").html("");
        $('#edit_form')[0].reset();
        $("#email-check").text("");
        $('#edit_form').find('.error').removeClass('error');
        $('#edit_form').find('.validate').removeClass('validate');
    });
    /************ edit admin profile in nav_bar END ****************/


    /************ check for confiermed old password in nav_bar START ****************/
    var oldpassword_state = false;
    $('#oldpassword').on('blur', function () {
        var oldpassword = $('#oldpassword').val();

        if (oldpassword == '') {
            oldpassword_state = false;
            $("#check-password").text("");
            return;
        }
        $.ajax({
            url: "/edit_profile/check_old_password",
            type: 'post',
            data: {
                'oldpassword_check': 1,
                'oldpassword': oldpassword,
            },
            success: function (response) {
                if (response == 'true') {
                    oldpassword_state = true;
                    $('#oldpassword').parent().removeClass();
                    $('#oldpassword').parent().addClass("check_success");
                    $('#oldpassword').siblings("span").text('Confiermed your old password');
                } else if (response == 'false') {
                    oldpassword_state = false;
                    $('#oldpassword').parent().removeClass();
                    $('#oldpassword').parent().addClass("error");
                    $('#oldpassword').siblings("span").text('Please Enter your correct old password');
                }
            }
        });
    });
    /************ check for confiermed old password in nav_bar END ****************/

    /************ change password in nav_bar START ****************/
    var user_id = $('#user_id').val();
    $('#update_password').on("submit", function (event) {
        event.preventDefault();

        if (oldpassword_state) {
            $.ajax({
                url: "/edit_profile/change_password",
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#change_password').modal('hide');
                    $('.modal').removeClass('in');
                    $('.modal').attr("aria-hidden", "true");
                    $('.modal').css("display", "none");
                    $('.modal-backdrop').remove();
                    $('body').removeClass('modal-open');
                    sessionStorage.changePassword = true;
                    window.location.reload();
                }
            });
        }
    });
    $('#change_password').on('hidden.bs.modal', function () {
        $(".help-block").html("");
        $("#check-password").text("");
        $("#check-new-password").text("");
        $('#update_password')[0].reset();
        $('#update_password').find('.error').removeClass('error');
        $('#update_password').find('.check_success').removeClass('check_success');
        $('#update_password').find('.validate').removeClass('validate');
    });
    /************ change password in nav_bar END ****************/

    /************ admin list START ****************/
    var table = $('#admin_details').DataTable({
        "processing": true,
        "serverSide": true,

        "order": [],
        "ajax": {
            "url": "/admins/fetch_admin_data",
            "type": "POST",
            "data": function (data) {
                data.school_id = $('#school_id').val();
                data.school_name = $('#school_name').val();
                data.firstname = $('#firstname').val();
                data.email = $('#email').val();
                data.contact_no = $('#contact_no').val();
                data.city = $('#city').val();
            }
        },
        "columnDefs": [
            {
                "targets": [0, 2, 3, 4, 5, 6],
                "orderable": false,
            },
        ],
    });

    $('#find').click(function () {
        table.ajax.reload();
    });
    $('#clear').click(function () {
        $('#admin-search')[0].reset();
        table.ajax.reload();
    });
    /************ admin list END ****************/

    /************ add form admin submit button START ****************/
    jQuery(function () {
        $("#add_admin").submit(function () {
            $(this).submit(function () {
                return false;
            });
            return true;
        });
    });
    /************ add form admin submit button END ****************/

    /************ edit form admin submit button START ****************/
    jQuery(function () {
        $("#edit_admin").submit(function () {
            $(this).submit(function () {
                return false;
            });
            return true;
        });
    });
    /************ edit form admin submit button END ****************/

    /************ school list START ****************/
    var school_table = $('#school_details').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "/schools/fetch_school_data",
            "type": "POST",
            "data": function (data) {
                data.school_id = $('#school_id').val();
                data.school_name = $('#school_name').val();
                data.school_email = $('#school_email').val();
                data.school_contact = $('#school_contact').val();
                data.school_city = $('#school_city').val();
                data.school_state = $('#school_state').val();
            }
        },
        "columnDefs": [
            {
                "targets": [2, 3, 6],
                "orderable": false,
            },
        ],
    });

    $('#school_find').click(function () {
        school_table.ajax.reload();
    });
    $('#school_clear').click(function () {
        $('#school-search')[0].reset();
        school_table.ajax.reload();
    });
    /************ school list END ****************/

    /************ add form school submit button START ****************/
    jQuery(function () {
        $("#add_school").submit(function () {
            $(this).submit(function () {
                return false;
            });
            return true;
        });
    });
    /************ add form school submit button END ****************/

    /************ edit form school submit button START ****************/
    jQuery(function () {
        $("#edit_school").submit(function () {
            $(this).submit(function () {
                return false;
            });
            return true;
        });
    });
    /************ edit form school submit button END ****************/


    /************ student list START ****************/
    var student_table = $('#student_data').DataTable({
        "order": [],
        "ajax": {
            "url": "/students/fetch_student_details",
            "type": "POST",
            "data": function (data) {
                data.school_name = localStorage.getItem('School_id');
                data.student_std = $('#student_std').val();
                data.student_div = $('#student_div').val();
                data.student_medium = $('#student_medium').val();
                data.student_school = $('#student_school').val();
                data.student_academic = $('#student_academic').val();
            }
        },
        "columnDefs": [
            {
                "targets": [0, 2, 3, 4, 5, 6],
                "orderable": false,
            },
        ],
    });
    // Check all
    $('#checkall').click(function(){
        if($(this).is(':checked')){
            $('.edit_Student').prop('checked', true);
            var rows = student_table.rows({ 'search': 'applied' }).nodes();
            $('input[type="checkbox"]', rows).prop('checked',true);
        }else{
            $('.edit_Student').prop('checked', false);
            var rows = student_table.rows({ 'search': 'applied' }).nodes();
            $('input[type="checkbox"]', rows).prop('checked',false);
        }
    });

    $('#student_search').click(function () {
        var std = $('#student_std').val();
        var div = $('#student_div').val();
        var medium = $('#student_medium').val();
        var school = $('#student_school').val();
        var academic = $('#student_academic').val();
        $('#hidden_student_std').val(std);
        $('#hidden_student_div').val(div);
        $('#hidden_student_medium').val(medium);
        $('#hidden_student_school').val(school);
        $('#hidden_student_academic').val(academic);
        student_table.ajax.reload();
    });
    $('#student_clear').click(function () {
        $('#student_std').val('').trigger('change');
        $("#student_div").find("option:not(:first)").remove();
        $('#student_medium').val('').trigger('change');
        $('#student_academic').val('').trigger('change');
        $('#student_school').val('').trigger('change');
        $('#hidden_student_std').val('');
        $('#hidden_student_div').val('');
        $('#hidden_student_medium').val('');
        $('#hidden_student_school').val('');
        $('#search_studentdata')[0].reset();
        student_table.ajax.reload();
    });
    /************ student list END ****************/

    /************ open modal for edit standard of all student START******/
    $(document).on('click', '#edit_standard', function () {
        $('#school_edit_all').siblings("span").text('');
        $('#standard_edit_all').siblings("span").text('');
        $('#student_array').text('');
        $('#edit_std_student').modal('show');
    });
    /************ open modal for edit standard of all student START******/


    /************ edit standard of all student START******/
    $(document).on('click', '#edit_all', function () {
        var isSubmit = true;
        var role = $('#session_role').val();
        var add_school = $('#school_edit_all').val();
        var add_std = $('#standard_edit_all').val();
        if (role == 'super_admin') {
            if (add_school == '' || add_school == null) {
                isSubmit = false;
                $('#school_edit_all').parent().removeClass();
                $('#school_edit_all').parent().addClass("error");
                $('#school_edit_all').siblings("span").text('This field is required');
            }
        }
        if (add_std == '' || add_std == null) {
            isSubmit = false;
            $('#standard_edit_all').parent().removeClass();
            $('#standard_edit_all').parent().addClass("error");
            $('#standard_edit_all').siblings("span").text('This field is required');
        }
        var editids_arr = [];
        var rows = student_table.rows({ 'search': 'applied' }).nodes();
        $('input[type="checkbox"]:checked', rows).each(function () {
            editids_arr.push($(this).val());
        });

        if(editids_arr.length < 1){
            isSubmit = false;
            $('#student_array').text('Please select Students');
        }
        if (isSubmit) {
            $.ajax({
                url: "/students/edit_standard_all",
                method: "POST",
                data: {
                    'student_Array': editids_arr,
                    'newStd': add_std,
                    'newSchool' : add_school
                },
                success: function (data) {
                    if (data == 'Record Updated Successfully.') {
                        $('#edit_std_form')[0].reset();
                        $('#edit_std_student').modal('hide');
                        student_table.ajax.reload();
                        if ($('#profile_edit_message').is(':empty')) {
                            $('#profile_edit_message').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        }
                    } else {
                        if ($('#profile_edit_message').is(':empty')) {
                            $('#profile_edit_message').append('<div class="alert alert-danger alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        }
                    }
                }
            });
        }
    });
    /************ edit standard of all student Edit******/

    /************ student list START ****************/
    var student_transfertable = $('#student_transferdata').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "/student_transfer/fetch_student_transferdetails",
            "type": "POST",
            "data": function (data) {
                data.school_name = localStorage.getItem('School_id');
                data.student_std = $('#student_transferstd').val();
                data.student_div = $('#student_transferdiv').val();
                data.student_medium = $('#student_transfermedium').val();
                data.student_school = $('#student_transferschool').val();
            }
        },
        "columnDefs": [
            {
                "targets": [1, 2, 3, 4, 5],
                "orderable": false,
            },
        ],
    });
    $('#student_transfersearch').click(function () {
        $std = $('#student_transferstd').val();
        $div = $('#student_transferdiv').val();
        $medium = $('#student_transfermedium').val();
        $school = $('#student_transferschool').val();
        student_transfertable.ajax.reload();
    });
    $('#student_transferclear').click(function () {
        $('#student_transferstd').val('').trigger('change');
        $("#student_transferdiv").find("option:not(:first)").remove();
        $('#student_transfermedium').val('').trigger('change');
        $('#student_transferschool').val('').trigger('change');
        $('#search_transferstudentdata')[0].reset();
        student_transfertable.ajax.reload();
    });
    /************ student list END ****************/
    /************ add form student submit button START ****************/
    jQuery(function () {
        $("#add_student").submit(function () {
            $(this).submit(function () {
                return false;
            });
            return true;
        });
    });
    /************ add form student submit button END ****************/

    /************ edit form student submit button START ****************/
    jQuery(function () {
        $("#update_student").submit(function () {
            $(this).submit(function () {
                return false;
            });
            return true;
        });
    });
    /************ edit form student submit button END ****************/


    /************ Fee details START ****************/
    var fee_table = $('#student_details_fee').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "/fee/fetch_data_fee",
            "type": "POST",
            "data": function (data) {
                data.school_name = localStorage.getItem('School_id');
                data.search_grnumber = $('#search_grnumber').val();
                data.search_name = $('#search_name').val();
                data.search_email = $('#search_email').val();
                data.search_contact_no = $('#search_contact_no').val();
                data.student_std = $('#student_std').val();
                data.student_div = $('#student_div').val();
                data.student_academic = $('#student_academic').val();
                data.search_student_school = $('#search_student_school').val();
            }
        },
        "columnDefs": [
            {
                "targets": [2, 3, 4],
                "orderable": false,
            },
        ],
    });

    $('#fee_find').click(function () {
        fee_table.ajax.reload();
    });
    $('#fee_clear').click(function () {
        $('#student_std').val('').trigger('change');
        $('#student_academic').val('').trigger('change');
        $("#student_div").find("option:not(:first)").remove();
        $('#search_student_school').val('').trigger('change');
        $('#form-search')[0].reset();
        fee_table.ajax.reload();
    });
    /************ Fee details END ****************/

    /************ take_student_fee submit button START ****************/
    jQuery(function () {
        $("#take_student_fee").submit(function () {
            $(this).submit(function () {
                return false;
            });
            return true;
        });
    });
    /************ take_student_fee submit button END ****************/


    /************get fee duration end month value START ****************/
    $('#start_fee_duration').on('change', function (e) {
        feeMonth();
    });
    /************get fee duration end month value END ****************/



    /************ for get radio button as per remaining fee START ****************/
    var total = $('#total_fee').val();
    var remaining = $('#remaining_fee').val();

    if (parseInt(total) > parseInt(remaining)) {
        $("label[for=12],#12").hide();
    }
    if (parseInt(total) / 2 > parseInt(remaining)) {
        $("label[for=12],#12").hide();
        $("label[for=6],#6").hide();
    }
    if (parseInt(total) / 4 > parseInt(remaining)) {
        $("label[for=12],#12").hide();
        $("label[for=6],#6").hide();
        $("label[for=3],#3").hide();
    }
    if (parseInt(total) / 6 > parseInt(remaining)) {
        $("label[for=12],#12").hide();
        $("label[for=6],#6").hide();
        $("label[for=3],#3").hide();
        $("label[for=2],#2").hide();
    }
    /************ for get radio button as per remaining fee END ****************/

    /************ for get fee period in month as per fee duration START ****************/

    $('.date').change(function () {
        var date = new Date($('#take_date').val());
        var duration = $("input[type='radio'][name='fee_duration']:checked").val();
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        if (duration == 12) {
            $('#fee_period').val('Full Year');
        }
        if (duration == 6) {
            $thisMonth = months[date.getMonth() + 1];
            $endMonth = months[date.getMonth() + 6];
            $fee_period = $thisMonth + ' - ' + $endMonth;
            $('#fee_period').val($fee_period);
        }
        if (duration == 3) {
            $thisMonth = months[date.getMonth() + 1];
            $endMonth = months[date.getMonth() + 3];
            $fee_period = $thisMonth + ' - ' + $endMonth;
            $('#fee_period').val($fee_period);
        }
        if (duration == 2) {
            $thisMonth = months[date.getMonth() + 1];
            $endMonth = months[date.getMonth() + 2];
            $fee_period = $thisMonth + ' - ' + $endMonth;
            $('#fee_period').val($fee_period);
        }
        if (duration == 1) {
            $thisMonth = months[date.getMonth() + 1];
            $('#fee_period').val($thisMonth);
        }
    });
    $("#paid_amount").on('blur', function () {
        checkPaidValue();
    });
    /************ for get fee period in month as per fee duration END ****************/

    /************ student list for daily report START ****************/
    var dailyreport_table = $('#daily_report').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "/daily_report/fetch_daily_data",
            "type": "POST",

            "data": function (data) {
                data.dailyreport_school_name = localStorage.getItem('School_id');
                data.dailyreport_student_std = $('#dailyreport_student_std').val();
                data.dailyreport_student_div = $('#dailyreport_student_div').val();
                data.dailyreport_academic = $('#dailyreport_academic').val();
                data.dailyreport_start_date = $('#dailyreport_start_date').val();
                data.dailyreport_end_date = $('#dailyreport_end_date').val();
                data.dailyreport_receipt_no = $('#dailyreport_receipt_no').val();
                data.dailyreport_student_school = $('#dailyreport_student_school').val();
            }
        },
        "columnDefs": [
            {
                "targets": [2, 3, 4, 5, 6, 7],
                "orderable": false,
            },
        ],
    });

    $('#dailyreport_find').click(function () {
        var std = $('#dailyreport_student_std').val();
        var div = $('#dailyreport_student_div').val();
        var startdate = $('#dailyreport_start_date').val();
        var enddate = $('#dailyreport_end_date').val();
        var school = $('#dailyreport_student_school').val();
        var receipt_no=$('#dailyreport_receipt_no').val();
        var dailyreport_academic=$('#dailyreport_academic').val();
        $('#dailyreport_hidden_std').val(std);
        $('#dailyreport_hidden_div').val(div);
        $('#dailyreport_hidden_startdate').val(startdate);
        $('#dailyreport_hidden_enddate').val(enddate);
        $('#dailyreport_hidden_school').val(school);
        $('#dailyreport_hidden_academic').val(dailyreport_academic);
        $('#dailyreport_hidden_receipt_no').val(receipt_no);
        dailyreport_table.ajax.reload();
    });
    $('#dailyreport_clear').click(function () {
        $('#dailyreport_student_std').val('').trigger('change');
        // $('#student_div').val('').trigger('change');
        $("#dailyreport_student_div").find("option:not(:first)").remove();
        $('#dailyreport_start_date').val('').trigger('change');
        $('#dailyreport_academic').val('').trigger('change');
        $('#dailyreport_end_date').val('').trigger('change');
        $('#dailyreport_student_school').val('').trigger('change');
        $('#dailyreport_receipt_no').val('');
        $('#dailyreport_hidden_std').val('');
        $('#dailyreport_hidden_div').val('');
        $('#dailyreport_hidden_startdate').val('');
        $('#dailyreport_hidden_enddate').val('');
        $('#dailyreport_hidden_school').val('');
        $('#dailyreport_hidden_receipt_no').val('');
        dailyreport_table.ajax.reload();
    });
    /************ student list for daily report END ****************/

    /************ student list for pending fee START ****************/
    var pendingfee_table = $('#pending_list').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "/pending_fee/student_list",
            "type": "POST",

            "data": function (data) {
                data.school_name = localStorage.getItem('School_id');
                data.student_std = $('#student_std').val();
                data.student_div = $('#student_div').val();
                data.pendingfee_medium = $('#pendingfee_medium').val();
                data.pendingfee_school = $('#pendingfee_school').val();
                data.pendingfee_month=$('#pendingfee_month').val();
                data.pendingFeeYear=$('#pendingFeeYear').val();
            }
        },
        "columnDefs": [
            {
                "targets": [2, 3, 4, 5, 6, 7],
                "orderable": false,
            },
        ],
    });

    $('#pending_find').click(function () {
        var std = $('#student_std').val();
        var div = $('#student_div').val();
        var medium = $('#pendingfee_medium').val();
        var pendingfee_month=$('#pendingfee_month').val();
        var school = $('#pendingfee_school').val();
        var pendingFeeYear = $('#pendingFeeYear').val();
        $('#hide_std').val(std);
        $('#hide_div').val(div);
        $('#hide_medium').val(medium);
        $('#hide_school').val(school);
        $('#hide_feemonth').val(pendingfee_month);
        $('#hide_feeyear').val(pendingFeeYear);
        pendingfee_table.ajax.reload();
    });
    $('#pending_clear').click(function () {
        $('#student_std').val('').trigger('change');
        $("#student_div").find("option:not(:first)").remove();
        $('#pendingfee_month').val('').trigger('change');
        $('#pendingfee_medium').val('').trigger('change');
        $('#pendingfee_school').val('').trigger('change');
        $('#pendingFeeYear').val('').trigger('change');
        $('#hide_std').val('');
        $("#hide_div").val('');
        $('#hide_medium').val('');
        $('#hide_feemonth').val('');
        $('#hide_school').val('');
        $('#hide_feeyear').val('');
        $('#pendingsearch_data')[0].reset();
        pendingfee_table.ajax.reload();
    });
    /************ student list for pending fee END ****************/

    /************ alumini student list START ****************/
    var alumini_student_table = $('#alumini_student_data').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "/alumini_student/fetch_student_details",
            "type": "POST",
            "data": function (data) {
                data.school_name = localStorage.getItem('School_id');
                data.alumini_student_std = $('#alumini_student_std').val();
                data.alumini_student_div = $('#alumini_student_div').val();
                data.alumini_student_medium = $('#alumini_student_medium').val();
                data.alumini_student_school = $('#alumini_student_school').val();
            }
        },
        "columnDefs": [
            {
                "targets": [1, 2, 3, 4, 5],
                "orderable": false,
            },
        ],
    });
    $('#alumini_student_search').click(function () {
        $std = $('#alumini_student_std').val();
        $div = $('#alumini_student_div').val();
        $medium = $('#alumini_student_medium').val();
        $school = $('#alumini_student_school').val();
        $('#alumini_hidden_student_std').val($std);
        $('#alumini_hidden_student_div').val($div);
        $('#alumini_hidden_student_medium').val($medium);
        $('#alumini_hidden_student_school').val($school);
        alumini_student_table.ajax.reload();
    });
    $('#alumini_student_clear').click(function () {
        $('#alumini_student_std').val('').trigger('change');
        $("#alumini_student_div").find("option:not(:first)").remove();
        $('#alumini_student_medium').val('').trigger('change');
        $('#alumini_student_school').val('').trigger('change');
        $('#alumini_hidden_student_std').val('');
        $('#alumini_hidden_student_div').val('');
        $('#alumini_hidden_student_medium').val('');
        $('#alumini_hidden_student_school').val('');
        $('#alumini_search_studentdata')[0].reset();
        alumini_student_table.ajax.reload();
    });
    /************alumini student list END ****************/

    /************ standard list for manage START ****************/
    var standardtable = $('#standard_data').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "/standard/fetch_standard_details",
            "type": "POST",
            "data": function (data) {
                data.school_name = localStorage.getItem('School_id');
                data.student_std = $('#student_std').val();
                data.std_school = $('#std_school').val();
            }
        },
        "columnDefs": [
            {
                "targets": [0, 1],
                "orderable": true,
            },
        ],
    });
    $('#search_list').click(function () {
        standardtable.ajax.reload();
    });
    $('#clear_list').click(function () {
        $('#student_std').val('').trigger('change');
        $('#std_school').val('').trigger('change');
        $('#standard_search_data')[0].reset();
        standardtable.ajax.reload();
    });

    $('#dynamic_field').hide();

    $('#close').click(function () {
        // $('#std_form')[0].reset();
        $('#dynamic_field').hide();
    });
    $('#standard_edit_div').click(function () {
        $('#editdivisionerr').text('');
        $('#dynamic_adddiv').show();
    });
    $('#close_edit').click(function () {
        $('#edit_std_form')[0].reset();
        $('#editstdModal').removeData();
        $('#dynamic_adddiv').hide();
    });

    $(document).on('click', '.btn_remove', function () {
        var button_id = $(this).attr("id");
        $('#diverror').html('');
        $('#row' + button_id + '').remove();
    });
    /************ standard list for manage END ****************/

    /************ add standard  for manage standard START ****************/
    $(document).on('click', '#add_button', function () {
        $('.div_row').remove();

        $('#ajax_response').html('');
        $('#error').html('');
        $('#std_form')[0].reset();
        $('#standard_add_std').parent().removeClass("form_error");
        $('#standard_add_std').siblings("span").text(' ');
        $('#standard_add_fee').siblings("span").text(' ');
        $('#standard_add_school').siblings("span").text(' ');
        $('#standard_add_admission_fee').siblings("span").text(' ');
        $('#standard_add_tuition_fee').siblings("span").text(' ');
        $('#standard_add_term_fee').siblings("span").text(' ');
        $('#standard_add_other_fee').siblings("span").text(' ');
        $('#standard_add_smart_fee').siblings("span").text(' ');
        $('#standard_add_computer_fee').siblings("span").text(' ');
        $('#standard_add_sports_fee').siblings("span").text(' ');
        $('#standard_add_insurance_fee').siblings("span").text(' ');
        $('#standard_add_exam_fee').siblings("span").text(' ');
        $('#standard_add_total_fee').siblings("span").text(' ');
        $('#admission_fee').show();
        $('#tution_Fee').show();
        $('#term_Fee').show();
        $('#otherfee').show();
        $('#total_fee').remove();
        $('#sm_fee').remove();
        $('#com_fee').remove();
        $('#sp_fee').remove();
        $('#ins_fee').remove();
        $('#ex_fee').remove();
        $('#stdModal.input').val('');
        $('#divisionerr').text('');
        $('#diverror').text('');
        $('#standard_submit').val("Add");
        $('#stdModal').modal('show');
        var id_School = localStorage.getItem('School_id');
        $('#add_school_standard').val(id_School);
        getFeeStructure();
    });

    $('#standard_submit').click(function (e) {
        e.preventDefault();
        var isSubmit = true;

        $(".div_list").each(function () {
            var divValue = $(this).val();
            var regExDiv = /^[A-Z]*$/;
            var validdiv = regExDiv.test(divValue);
            if (divValue == '' || divValue == null) {
                isSubmit = false;
                $('#diverror').text('This field is required');
            } else if (!validdiv) {
                isSubmit = false;
                $('#diverror').text('Please enter value in A-Z(Capital letters)');
            } else if (divValue.length > 3) {
                isSubmit = false;
                $('#diverror').text('Maximum length is 3');
            }
            var div_arr = jQuery.makeArray(divValue);
            var isValueInArrayOld = div_arr.includes("");
            if (isValueInArrayOld == true) {
                isSubmit = false;
            }
        });
        var add_standard = $.trim($("#standard_add_std").val());
        var add_fee = $.trim($("#standard_add_admission_fee").val());
        var tution_fee = $.trim($("#standard_add_tuition_fee").val());
        var term_fee = $.trim($("#standard_add_term_fee").val());
        var other_fee = $.trim($("#standard_add_other_fee").val());
        var smart_calss_fee=$.trim($("#standard_add_smart_fee").val());
        var computer_fee=$.trim($("#standard_add_computer_fee").val());
        var sports_fee=$.trim($("#standard_add_sports_fee").val());
        var insurance_fee=$.trim($("#standard_add_insurance_fee").val());
        var exam_fee=$.trim($("#standard_add_exam_fee").val());
        var enroll_fee=$.trim($("#standard_add_enroll_fee").val());
        var total_fee=$.trim($('#standard_add_total_fee').val());
        var add_school = $('select[name=standard_add_school]').val()
        // var regExstd=/^([1-9]|1[012])$/;
        var regExstd = /[a-zA-Z0-9_. ,]*/;
        var validstd = regExstd.test($("#standard_add_std").val());
        var regExfee = /^[1-9][0-9]{1,4}$/;
        var validfee = regExfee.test($("#standard_add_admission_fee").val());
        var validtutionfee = regExfee.test($("#standard_add_tuition_fee").val());
        var validtermfee = regExfee.test($("#standard_add_term_fee").val());
        var validotherfee = regExfee.test($("#standard_add_other_fee").val());
        var validsmart_calss_fee=regExfee.test($("#standard_add_smart_fee").val());
        var validcomputer_fee=regExfee.test($("#standard_add_computer_fee").val());
        var validsports_fee=regExfee.test($("#standard_add_sports_fee").val());
        var validinsurance_fee=regExfee.test($("#standard_add_insurance_fee").val());
        var validexam_fee=regExfee.test($("#standard_add_exam_fee").val());
        var validenroll_fee=regExfee.test($("#standard_add_enroll_fee").val());
        var validtotalfee=regExfee.test($("#standard_add_total_fee").val());
        var role = $('#session_role').val();

        if (role == 'super_admin') {
            if (add_school == '' || add_school == null) {
                isSubmit = false;
                $('#standard_add_school').parent().removeClass();
                $('#standard_add_school').parent().addClass("error");
                $('#standard_add_school').siblings("span").text('This field is required');
            }
        }

        if (add_fee == '' || add_fee == null) {
            isSubmit = false;
            $('#standard_add_admission_fee').siblings("span").text('This field is required');
        } else if (!validfee) {
            isSubmit = false;
            $('#standard_add_admission_fee').parent().removeClass();
            $('#standard_add_admission_fee').parent().addClass("error");
            $('#standard_add_admission_fee').siblings("span").text('Please enter between 10 to 99999');
        }

        if (tution_fee == '' || tution_fee == null) {
            isSubmit = false;
            $('#standard_add_tuition_fee').siblings("span").text('This field is required');
        } else if (!validtutionfee) {
            isSubmit = false;
            $('#standard_add_tuition_fee').parent().removeClass();
            $('#standard_add_tuition_fee').parent().addClass("error");
            $('#standard_add_tuition_fee').siblings("span").text('Please enter between 10 to 99999');
        }

        if (term_fee == '' || term_fee == null) {
            isSubmit = false;
            $('#standard_add_term_fee').parent().removeClass();
            $('#standard_add_term_fee').parent().addClass("error");
            $('#standard_add_term_fee').siblings("span").text('This field is required');
        } else if (!validtermfee) {
            isSubmit = false;
            $('#standard_add_term_fee').parent().removeClass();
            $('#standard_add_term_fee').parent().addClass("error");
            $('#standard_add_term_fee').siblings("span").text('Please enter between 10 to 99999');
        }
        if (fee_flag==true){
            if (other_fee == '' || other_fee == null) {
                isSubmit = true;
                $('#standard_add_other_fee').siblings("span").text('');
            }
        }else{
            if (other_fee == '' || other_fee == null) {
                isSubmit = false;
                $('#standard_add_other_fee').parent().removeClass();
                $('#standard_add_other_fee').parent().addClass("error");
                $('#standard_add_other_fee').siblings("span").text('This field is required');
            } else if (!validotherfee) {
                isSubmit = false;
                $('#standard_add_other_fee').parent().removeClass();
                $('#standard_add_other_fee').parent().addClass("error");
                $('#standard_add_other_fee').siblings("span").text('Please enter between 10 to 99999');
            }
        }
        if(totalfee_flag==true){
            if (total_fee == '' || total_fee == null) {
                isSubmit = false;
                $('#standard_add_total_fee').siblings("span").text('This field is required');
            } else if (!validtotalfee) {
                isSubmit = false;
                $('#standard_add_total_fee').parent().removeClass();
                $('#standard_add_total_fee').parent().addClass("error");
                $('#standard_add_total_fee').siblings("span").text('Please enter between 10 to 99999');
            }
        }
        if (fee_flag==true){
            if (smart_calss_fee == '' || smart_calss_fee == null) {
                isSubmit = false;
                $('#standard_add_smart_fee').siblings("span").text('This field is required');
            } else if (!validsmart_calss_fee) {
                isSubmit = false;
                $('#standard_add_smart_fee').parent().removeClass();
                $('#standard_add_smart_fee').parent().addClass("error");
                $('#standard_add_smart_fee').siblings("span").text('Please enter between 10 to 99999');
            }
            if (computer_fee == '' || computer_fee == null) {
                isSubmit = false;
                $('#standard_add_computer_fee').siblings("span").text('This field is required');
            } else if (!validcomputer_fee) {
                isSubmit = false;
                $('#standard_add_computer_fee').parent().removeClass();
                $('#standard_add_computer_fee').parent().addClass("error");
                $('#standard_add_computer_fee').siblings("span").text('Please enter between 10 to 99999');
            }
            if (sports_fee == '' || sports_fee == null) {
                isSubmit = false;
                $('#standard_add_sports_fee').siblings("span").text('This field is required');
            } else if (!validsports_fee) {
                isSubmit = false;
                $('#standard_add_sports_fee').parent().removeClass();
                $('#standard_add_sports_fee').parent().addClass("error");
                $('#standard_add_sports_fee').siblings("span").text('Please enter between 10 to 99999');
            }
            if (insurance_fee == '' || insurance_fee == null) {
                isSubmit = false;
                $('#standard_add_insurance_fee').siblings("span").text('This field is required');
            } else if (!validinsurance_fee) {
                isSubmit = false;
                $('#standard_add_insurance_fee').parent().removeClass();
                $('#standard_add_insurance_fee').parent().addClass("error");
                $('#standard_add_insurance_fee').siblings("span").text('Please enter between 10 to 99999');
            }
            if (exam_fee == '' || exam_fee == null) {
                isSubmit = false;
                $('#standard_add_exam_fee').siblings("span").text('This field is required');
            } else if (!validexam_fee) {
                isSubmit = false;
                $('#standard_add_exam_fee').parent().removeClass();
                $('#standard_add_exam_fee').parent().addClass("error");
                $('#standard_add_exam_fee').siblings("span").text('Please enter between 10 to 99999');
            }
            if (enroll_fee == '' || enroll_fee == null) {
                isSubmit = false;
                $('#standard_add_enroll_fee').siblings("span").text('This field is required');
            } else if (!validenroll_fee) {
                isSubmit = false;
                $('#standard_add_enroll_fee').parent().removeClass();
                $('#standard_add_enroll_fee').parent().addClass("error");
                $('#standard_add_enroll_fee').siblings("span").text('Please enter between 10 to 99999');
            }
        }else{
            isSubmit = true;
            $('#standard_add_smart_fee').siblings("span").text('');
            $('#standard_add_computer_fee').siblings("span").text('');
            $('#standard_add_sports_fee').siblings("span").text('');
            $('#standard_add_insurance_fee').siblings("span").text('');
            $('#standard_add_exam_fee').siblings("span").text('');
        }
        if (add_standard == '' || add_standard == null) {
            isSubmit = false;
            $('#standard_add_std').parent().removeClass();
            $('#standard_add_std').parent().addClass("error");
            $('#standard_add_std').siblings("span").text('This field is required');
        } else if (!validstd) {
            isSubmit = false;
            $('#standard_add_std').parent().removeClass();
            $('#standard_add_std').parent().addClass("error");
            $('#standard_add_std').siblings("span").text('Please enter between 1 to 12');
        } else if (add_standard.length > 8) {
            isSubmit = false;
            $('#standard_add_std').parent().removeClass();
            $('#standard_add_std').parent().addClass("error");
            $('#standard_add_std').siblings("span").text('Please enter maximum eight number');
        }
        var length = $('.div_list').length;
        if (length <= 0) {
            isSubmit = false;
            $('#divisionerr').text('This field is required');
        }
        if ($('#diverror').is(':empty') && $('#divisionerr').is(':empty')) {
            isSubmit = true;
        } else {
            isSubmit = false;
        }
        if (isSubmit) {
            $.ajax({
                url: "/standard/add_standard",
                method: "POST",
                data: $('#std_form').serialize(),
                success: function (data) {
                    if (data == 'Record Inserted Successfully.') {
                        $('#std_form')[0].reset();
                        $('#stdModal').modal('hide');
                        standardtable.ajax.reload();
                        if ($('#ajax_response').is(':empty')) {
                            $('#ajax_response').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        }
                    } else {
                        if ($('#error').is(':empty')) {
                            $('#error').append('<div class="alert alert-danger alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        }
                    }
                }
            });
        }
    });
    $("#standard_add_std").keyup(function () {
        if ($('#standard_add_std').val() !== "") {
            $('#standard_add_std').parent().removeClass();
            $('#standard_add_std').parent().addClass("form_success");
            $('#standard_add_std').siblings("span").text('');
        }
    });
    $("#standard_add_admission_fee").keyup(function () {
        if ($('#standard_add_admission_fee').val() !== "") {
            $('#standard_add_admission_fee').siblings("span").text('');
        }
    });
    $("#standard_add_tuition_fee").keyup(function () {
        if ($('#standard_add_tuition_fee').val() !== "") {
            $('#standard_add_tuition_fee').siblings("span").text('');
        }
    });
    $("#standard_add_term_fee").keyup(function () {
        if ($('#standard_add_term_fee').val() !== "") {
            $('#standard_add_term_fee').siblings("span").text('');
        }
    });
    $("#standard_add_other_fee").keyup(function () {
        if ($('#standard_add_other_fee').val() !== "") {
            $('#standard_add_other_fee').siblings("span").text('');
        }
    });

    $("#standard_add_school").on("change", function () {
        $('#standard_add_school').parent().removeClass();
        $('#standard_add_school').parent().addClass("form_success");
        $('#standard_add_school').siblings("span").text('');
    });

    /************ add standard  for manage standard END ****************/


    /************ edit standard  for manage standard START ****************/
    $('#standard_update').click(function (e) {
        e.preventDefault();
        var isSubmit = true;
        var edit_standard = $.trim($("#standard_edit_std").val());
        var regExstd = /[a-zA-Z0-9_. ,]*/;
        var validstd = regExstd.test($("#standard_edit_std").val());

        var edit_fee = $.trim($("#standard_edit_admission_fee").val());
        var tution_fee = $.trim($("#standard_edit_tuition_fee").val());
        var term_fee = $.trim($("#standard_edit_term_fee").val());
        var other_fee = $.trim($("#standard_edit_other_fee").val());
        var smart_fee = $.trim($("#standard_edit_smart_fee").val());
        var computer_fee = $.trim($("#standard_edit_computer_fee").val());
        var sports_fee = $.trim($("#standard_edit_sports_fee").val());
        var insurance_fee = $.trim($("#standard_edit_insurance_fee").val());
        var exam_fee = $.trim($("#standard_edit_exam_fee").val());
        var total_fee = $.trim($("#standard_edit_total_fee").val());
        var regExfee = /^[1-9][0-9]{1,4}$/;
        var validfee = regExfee.test($("#standard_edit_admission_fee").val());
        var validtutionfee = regExfee.test($("#standard_edit_tuition_fee").val());
        var validtermfee = regExfee.test($("#standard_edit_term_fee").val());
        var validotherfee = regExfee.test($("#standard_edit_other_fee").val());
        var validsmartfee = regExfee.test($("#standard_edit_smart_fee").val());
        var validcomputerfee = regExfee.test($("#standard_edit_computer_fee").val());
        var validsportsfee = regExfee.test($("#standard_edit_sports_fee").val());
        var validinsurancefee = regExfee.test($("#standard_edit_insurance_fee").val());
        var validexamfee = regExfee.test($("#standard_edit_exam_fee").val());
        var validtotalfee = regExfee.test($("#standard_edit_total_fee").val());



        if (edit_standard == '' || edit_standard == null) {
            isSubmit = false;
            $('#standard_edit_std').parent().removeClass();
            $('#standard_edit_std').parent().addClass("error");
            $('#standard_edit_std').siblings("span").text('This field is required');
        } else if (!validstd) {
            isSubmit = false;
            $('#standard_edit_std').parent().removeClass();
            $('#standard_edit_std').parent().addClass("error");
            $('#standard_edit_std').siblings("span").text('Please enter valid value');
        }
        if(edit_fee_flag == true || edit_totalfee_flag == true){
            if (other_fee == '' || other_fee == null) {
                isSubmit = true;
                $('#standard_edit_other_fee').siblings("span").text('');
            }
        }else{
            if (other_fee == '' || other_fee == null) {
                isSubmit = false;
                $('#standard_edit_other_fee').parent().removeClass();
                $('#standard_edit_other_fee').parent().addClass("error");
                $('#standard_edit_other_fee').siblings("span").text('This field is required');
            }
        }
        if(edit_totalfee_flag == true){
            if (edit_fee == '' || edit_fee == null) {
                isSubmit = true;
                $('#standard_edit_admission_fee').siblings("span").text('');
            }
            if (tution_fee == '' || tution_fee == null) {
                isSubmit = true;
                $('#standard_edit_tuition_fee').siblings("span").text('');
            }
            if (term_fee == '' || term_fee == null) {
                isSubmit = true;
                $('#standard_edit_term_fee').siblings("span").text('');
            }
        }else{
            if (edit_fee == '' || edit_fee == null) {
                isSubmit = false;
                $('#standard_edit_admission_fee').parent().removeClass();
                $('#standard_edit_admission_fee').parent().addClass("error");
                $('#standard_edit_admission_fee').siblings("span").text('This field is required');
            }

            if (tution_fee == '' || tution_fee == null) {
                isSubmit = false;
                $('#standard_edit_tuition_fee').parent().removeClass();
                $('#standard_edit_tuition_fee').parent().addClass("error");
                $('#standard_edit_tuition_fee').siblings("span").text('This field is required');
            }

            if (term_fee == '' || term_fee == null) {
                isSubmit = false;
                $('#standard_edit_term_fee').parent().removeClass();
                $('#standard_edit_term_fee').parent().addClass("error");
                $('#standard_edit_term_fee').siblings("span").text('This field is required');
            }
        }
        if(edit_totalfee_flag == true){
            if (total_fee == '' || total_fee == null) {
                isSubmit = false;
                $('#standard_edit_total_fee').parent().removeClass();
                $('#standard_edit_total_fee').parent().addClass("error");
                $('#standard_edit_total_fee').siblings("span").text('This field is required');
            }
        }
        if(edit_fee_flag == true){
            if (smart_fee == '' || smart_fee == null) {
                isSubmit = false;
                $('#standard_edit_smart_fee').parent().removeClass();
                $('#standard_edit_smart_fee').parent().addClass("error");
                $('#standard_edit_smart_fee').siblings("span").text('This field is required');
            }
            if (computer_fee == '' || computer_fee == null) {
                isSubmit = false;
                $('#standard_edit_computer_fee').parent().removeClass();
                $('#standard_edit_computer_fee').parent().addClass("error");
                $('#standard_edit_computer_fee').siblings("span").text('This field is required');
            }
            if (sports_fee == '' || sports_fee == null) {
                isSubmit = false;
                $('#standard_edit_sports_fee').parent().removeClass();
                $('#standard_edit_sports_fee').parent().addClass("error");
                $('#standard_edit_sports_fee').siblings("span").text('This field is required');
            }
            if (insurance_fee == '' || insurance_fee == null) {
                isSubmit = false;
                $('#standard_edit_insurance_fee').parent().removeClass();
                $('#standard_edit_insurance_fee').parent().addClass("error");
                $('#standard_edit_insurance_fee').siblings("span").text('This field is required');
            }
            if (exam_fee == '' || exam_fee == null) {
                isSubmit = false;
                $('#standard_edit_exam_fee').parent().removeClass();
                $('#standard_edit_exam_fee').parent().addClass("error");
                $('#standard_edit_exam_fee').siblings("span").text('This field is required');
            }
        }

        var newValue = "";
        var existArray = [];
        $("input[name='edit_division[]']").each(function () {
            newValue = $(this).val();
            if (newValue) {
                existArray.push(newValue);
            }
        });
        $('input[name^="edit_division"]').each(function () {
            var arr = $(this).val();
            var div_arr = jQuery.makeArray(arr);
            var isValueInArrayOld = div_arr.includes("");
            if (isValueInArrayOld == true) {
                isSubmit = false;
            }
        });
        if (!newValue) {
            isSubmit = false;
        }

        if (isSubmit) {
            $.ajax({
                url: "/standard/update_standard_details",
                method: "POST",
                data: $('#edit_std_form').serialize(),
                success: function (data) {
                    if (data == 'Record Updated Successfully.') {
                        $('#edit_std_form')[0].reset();
                        $('#editstdModal').modal('hide');
                        standardtable.ajax.reload();

                        if ($('#ajax_response').is(':empty')) {
                            $('#ajax_response').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        }
                    } else {
                        if ($("#error_msg").length == 1 && $("#error_msg").length > 0) {
                            $('#error_msg').append('<div class="alert alert-danger alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        }
                    }
                }
            });
        }
    });
    $("#standard_edit_std").keyup(function () {
        if ($('#standard_edit_std').val() !== "") {
            $('#standard_edit_std').parent().removeClass();
            $('#standard_edit_std').parent().addClass("form_success");
            $('#standard_edit_std').siblings("span").text('');
        }
    });
    $("#standard_edit_admission_fee").keyup(function () {
        if ($('#standard_edit_admission_fee').val() !== "") {
            $('#standard_edit_admission_fee').parent().removeClass();
            $('#standard_edit_admission_fee').parent().addClass("form_success");
            $('#standard_edit_admission_fee').siblings("span").text('');
        }
    });
    $("#standard_edit_tuition_fee").keyup(function () {
        if ($('#standard_edit_tuition_fee').val() !== "") {
            $('#standard_edit_tuition_fee').parent().removeClass();
            $('#standard_edit_tuition_fee').parent().addClass("form_success");
            $('#standard_edit_tuition_fee').siblings("span").text('');
        }
    });
    $("#standard_edit_term_fee").keyup(function () {
        if ($('#standard_edit_term_fee').val() !== "") {
            $('#standard_edit_term_fee').parent().removeClass();
            $('#standard_edit_term_fee').parent().addClass("form_success");
            $('#standard_edit_term_fee').siblings("span").text('');
        }
    });
    $("#standard_edit_other_fee").keyup(function () {
        if ($('#standard_edit_other_fee').val() !== "") {
            $('#standard_edit_other_fee').parent().removeClass();
            $('#standard_edit_other_fee').parent().addClass("form_success");
            $('#standard_edit_other_fee').siblings("span").text('');
        }
    });
    $("#standard_edit_smart_fee").keyup(function () {
        if ($('#standard_edit_smart_fee').val() !== "") {
            $('#standard_edit_smart_fee').parent().removeClass();
            $('#standard_edit_smart_fee').parent().addClass("form_success");
            $('#standard_edit_smart_fee').siblings("span").text('');
        }
    });
    $("#standard_edit_computer_fee").keyup(function () {
        if ($('#standard_edit_computer_fee').val() !== "") {
            $('#standard_edit_computer_fee').parent().removeClass();
            $('#standard_edit_computer_fee').parent().addClass("form_success");
            $('#standard_edit_computer_fee').siblings("span").text('');
        }
    });
    $("#standard_edit_sports_fee").keyup(function () {
        if ($('#standard_edit_sports_fee').val() !== "") {
            $('#standard_edit_sports_fee').parent().removeClass();
            $('#standard_edit_sports_fee').parent().addClass("form_success");
            $('#standard_edit_sports_fee').siblings("span").text('');
        }
    });
    $("#standard_edit_insurance_fee").keyup(function () {
        if ($('#standard_edit_insurance_fee').val() !== "") {
            $('#standard_edit_insurance_fee').parent().removeClass();
            $('#standard_edit_insurance_fee').parent().addClass("form_success");
            $('#standard_edit_insurance_fee').siblings("span").text('');
        }
    });
    $("#standard_edit_exam_fee").keyup(function () {
        if ($('#standard_edit_exam_fee').val() !== "") {
            $('#standard_edit_exam_fee').parent().removeClass();
            $('#standard_edit_exam_fee').parent().addClass("form_success");
            $('#standard_edit_exam_fee').siblings("span").text('');
        }
    });
    $("#standard_edit_total_fee").keyup(function () {
        if ($('#standard_edit_total_fee').val() !== "") {
            $('#standard_edit_total_fee').parent().removeClass();
            $('#standard_edit_total_fee').parent().addClass("form_success");
            $('#standard_edit_total_fee').siblings("span").text('');
        }
    });
    /************ edit standard  for manage standard END ****************/
});

/************ check Admin email exists or not for edit START ****************/
function checkAdminusername(event) {
    var enterValue = event.target.value;
    var emailIdValue = enterValue;

    var oldEmail = $('#hidden_email').val();
    document.getElementById("update_admin").disabled = false;
    if (emailIdValue != oldEmail) {
        $.ajax({
            url: "/admins/check_emailId_exists",
            method: "POST",
            data: {firstname: emailIdValue},
            success: function (response) {
                if (response == 'Already exists') {
                    $('#email-check').html('This email is already exists');
                    document.getElementById("update_admin").disabled = true;
                } else if (response == 'Available') {
                    $('#email-check').html('');
                    document.getElementById("update_admin").disabled = false;
                } else {
                    $('#email-check').html('');
                    document.getElementById("update_admin").disabled = false;
                }

            }
        });
    } else {
        $('#email-check').html('');
        document.getElementById("update_admin").disabled = false;
    }

}

/************ check Admin email or not for edit END ****************/

/************ check school email Id exists or not for edit START ****************/
function checkSchoolEmailId(event) {
    var enterValue = event.target.value;
    var emailIdValue = enterValue;

    document.getElementById("school_submit").disabled = false;
    $.ajax({
        url: "/schools/check_emailId_exists",
        method: "POST",
        data: {email: emailIdValue},
        success: function (response) {
            if (response == 'Already exists') {
                $('#school-email-check').html('This email is already exists');
                document.getElementById("school_submit").disabled = true;
            } else if (response == 'Available') {
                $('#school-email-check').html('');
                document.getElementById("school_submit").disabled = false;
            } else {
                $('#school-email-check').html('');
                document.getElementById("school_submit").disabled = false;
            }

        }
    });

}

/************ check school email Id exists or not for edit END ****************/

/************ check School email exists or not in edit for edit START ****************/
function checkSchoolEditEmailId(event) {
    var enterValue = event.target.value;
    var emailIdValue = enterValue;

    var oldEmail = $('#hidden_school_email').val();
    document.getElementById("school_update").disabled = false;
    if (emailIdValue != oldEmail) {
        $.ajax({
            url: "/schools/check_emailId_exists",
            method: "POST",
            data: {email: emailIdValue},
            success: function (response) {
                if (response == 'Already exists') {
                    $('#edit_school_email').html('This email is already exists');
                    document.getElementById("school_update").disabled = true;
                } else if (response == 'Available') {
                    $('#edit_school_email').html('');
                    document.getElementById("school_update").disabled = false;
                } else {
                    $('#edit_school_email').html('');
                    document.getElementById("school_update").disabled = false;
                }

            }
        });
    } else {
        $('#edit_school_email').html('');
        document.getElementById("school_update").disabled = false;
    }

}

/************ check School email exists or not in edit for edit END ****************/

/************ check student email Id exists or not for edit START ****************/
function checkStudentGR(event) {
    var enterValue = event.target.value;
    var GRValue = enterValue;

    document.getElementById("student_add").disabled = false;
    $.ajax({
        url: "/students/checkStudentGRNo",
        method: "POST",
        data: {gr_number: GRValue},
        success: function (response) {
            if (response == 'Already exists') {
                $('#student_gr_check').html('This GR number is already exists');
                document.getElementById("student_add").disabled = true;
            } else if (response == 'Available') {
                $('#student_gr_check').html('');
                document.getElementById("student_add").disabled = false;
            } else {
                $('#student_gr_check').html('');
                document.getElementById("student_add").disabled = false;
            }

        }
    });

}

/************ check school email Id exists or not for edit END ****************/

/************ check student email Id exists or not for edit START ****************/
function checkStudentEmailId(event) {
    var enterValue = event.target.value;
    var emailIdValue = enterValue;

    document.getElementById("student_add").disabled = false;
    if ($.trim(emailIdValue).length > 0) {
        $.ajax({
            url: "/students/check_emailId_exists",
            method: "POST",
            data: {email: emailIdValue},
            success: function (response) {
                if (response == 'Already exists') {
                    $('#student_email_check').html('This email is already exists');
                    document.getElementById("student_add").disabled = true;
                } else if (response == 'Available') {
                    $('#student_email_check').html('');
                    document.getElementById("student_add").disabled = false;
                } else {
                    $('#student_email_check').html('');
                    document.getElementById("student_add").disabled = false;
                }

            }
        });
    }


}

/************ check school email Id exists or not for edit END ****************/

/************ check School email exists or not in edit for edit START ****************/
function checkStudentEditEmailId(event) {
    var enterValue = event.target.value;
    var emailIdValue = enterValue;

    var oldEmail = $('#hidden_student_email').val();
    document.getElementById("student_update").disabled = false;
    if (emailIdValue != oldEmail) {
        $.ajax({
            url: "/students/check_emailId_exists",
            method: "POST",
            data: {email: emailIdValue},
            success: function (response) {
                if (response == 'Already exists') {
                    $('#student_edit_email').html('This email is already exists');
                    document.getElementById("student_update").disabled = true;
                } else if (response == 'Available') {
                    $('#student_edit_email').html('');
                    document.getElementById("student_update").disabled = false;
                } else {
                    $('#student_edit_email').html('');
                    document.getElementById("student_update").disabled = false;
                }

            }
        });
    } else {
        $('#student_edit_email').html('');
        document.getElementById("student_update").disabled = false;
    }

}

/************ check School email exists or not in edit for edit END ****************/

/************ get school name START ****************/
function GetSchoolValueAdmin(event) {
    var enterValue = event.target.value;
    var schoolIdValue = enterValue;
    $.ajax({
        url: "/admins/get_schoolname_value",
        method: "POST",
        data: {school_id: schoolIdValue},
        success: function (data) {
            var fee_data = JSON.parse(data);

            $('#add_schoolname_admin').val(fee_data.name);
            $('#add_id').val(fee_data.id);
        }
    });
}

/************ get school name END ****************/

/************ delete school START ****************/
function delete_school_details(id) {
    if (confirm("Are you sure you want to delete this?")) {
        $.ajax({
            url: "/schools/delete_school_details",
            method: "POST",
            data: {id: id},
            success: function (data) {
                if (data == 'Data Deleted') {
                    $('#profile_edit_message').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">Record Deleted Successfully.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                    $('#school_details').DataTable().ajax.reload();
                } else {
                    $('#profile_edit_message').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                }
            }
        });
    } else {
        return false;
    }
}

/************ delete school END ****************/

/************ check emailId exists or not START ****************/
function checknameId(event) {
    var enterValue = event.target.value;
    var nameIdValue = enterValue;

    document.getElementById("submit").disabled = false;
    $.ajax({
        url: "/admins/check_emailId_exists",
        method: "POST",
        data: {firstname: nameIdValue},
        success: function (response) {
            if (response == 'Already exists') {
                $('#email-error').html('This name is already exists');
                document.getElementById("submit").disabled = true;
            } else if (response == 'Available') {
                $('#email-error').html('');
                document.getElementById("submit").disabled = false;
            } else {
                $('#email-error').html('');
                document.getElementById("submit").disabled = false;
            }

        }
    });
}

/************ check emailId exists or not END ****************/


/************ get school name start ****************/
function getSchoolnameDetails(school_name, school_id) {
    localStorage.setItem('School_id', school_name);
    localStorage.setItem('School_name', school_id);

    var School_id = localStorage.getItem('School_id');
    $.ajax({
        type: "POST",
        url: '/admin/localstorage_val', // change url as your
        data: {School_id: School_id},
        dataType: 'json',
        success: function (data) {
            window.location.reload();
        }

    });
}

/************ get school name start ****************/


/************ fetch admin for edit profile start ****************/
function fetchAdminDetails() {
    var user_id = $('#user_id').val();
    $('#edit_form')[0].reset();
    $.ajax({
        url: "/edit_profile/fetch_admin_details",
        method: "POST",
        data: {user_id: user_id},
        dataType: "json",
        success: function (data) {
            $('#update_uname_admin').val(data.update_uname_admin);
            $('#update_fname_admin').val(data.update_fname_admin);
            $('#update_lname_admin').val(data.update_lname_admin);
            $('#update_email').val(data.update_email_admin);
            $('#update_gender_admin').val(data.update_gender_admin);
            $('#update_admin_email').val(data.update_email_admin);
            $('#update_admin_username').val(data.update_uname_admin);
            var email = $('#update_uname_admin').val();
            $('#hidden_email').val(data.update_uname_admin);

            if (data.update_gender_admin === 'm') {
                $("#update_male_admin").prop("checked", true);
            } else {
                $("#update_female_admin").prop("checked", true);
            }
            $('#user_id').val(user_id);
            $('#btn_action').val("Update");
        }
    });
}

/************ fetch admin for edit profile END ****************/

/************ check schoolId exists or not for edit START ****************/
function checknameIdEdit(event, edit_nameid = null) {
    var enterValue = event.target.value;
    var nameIdValue = enterValue;
    var old_name = edit_nameid;

    document.getElementById("update").disabled = false;
    if (nameIdValue != edit_nameid) {
        $.ajax({
            url: "/admins/check_emailId_exists",
            method: "POST",
            data: {firstname: nameIdValue, old_name: old_name},
            success: function (response) {
                if (response == 'Already exists') {
                    $('#emailid_editerror').html('This name is already exists');
                    document.getElementById("update").disabled = true;
                } else if (response == 'Available') {
                    $('#emailid_editerror').html('');
                    document.getElementById("update").disabled = false;
                } else {
                    $('#emailid_editerror').html('');
                    document.getElementById("update").disabled = false;
                }

            }
        });

    } else {
        $('#emailid_editerror').html('');
        document.getElementById("update").disabled = false;
    }

}

/************ check schoolId exists or not for edit END ****************/

/************ check schoolId exists or not START ****************/
function checkschoolId(event) {
    var enterValue = event.target.value;
    var schoolIdValue = enterValue;
    document.getElementById("school_submit").disabled = false;
    $.ajax({
        url: "/schools/check_schoolId_exists",
        method: "POST",
        data: {school_id: schoolIdValue},
        success: function (response) {
            if (response == 'Already exists') {
                $('#schoolid_error').html('This school id is already exists');
                document.getElementById("school_submit").disabled = true;
            } else if (response == 'Available') {
                $('#schoolid_error').html('');
                document.getElementById("school_submit").disabled = false;
            } else {
                $('#schoolid_error').html('');
                document.getElementById("school_submit").disabled = false;
            }

        }
    });
}

/************ check schoolId exists or not END ****************/

/************ check schoolId exists or not for edit START ****************/
function checkschoolIdEdit(event, edit_schoolid = null) {
    var enterValue = event.target.value;
    var schoolIdValue = enterValue;
    document.getElementById("school_update").disabled = false;
    if (parseInt(schoolIdValue) != parseInt(edit_schoolid)) {
        $.ajax({
            url: "/schools/check_schoolId_exists",
            method: "POST",
            data: {school_id: schoolIdValue},
            success: function (response) {
                if (response == 'Already exists') {
                    $('#schoolid_editerror').html('This school id is already exists');
                    document.getElementById("school_update").disabled = true;
                } else if (response == 'Available') {
                    $('#schoolid_editerror').html('');
                    document.getElementById("school_update").disabled = false;
                } else {
                    $('#schoolid_editerror').html('');
                    document.getElementById("school_update").disabled = false;
                }

            }
        });
    } else {
        $('#schoolid_editerror').html('');
        document.getElementById("school_update").disabled = false;
    }

}

/************ check schoolId exists or not for edit END ****************/


/************ view fee START ****************/
$(document).on('click', '.view', function () {
    var id = $(this).attr("id");
    $.ajax({
        url: "/fee/view_fee",
        method: "POST",
        data: {id: id},
        dataType: "json",
        success: function (data) {
            $('.fee_installement').remove();
            $('#view_firstname').html(data['student_data'][0].firstname + ' ' + data['student_data'][0].father_name + ' ' + data['student_data'][0].surname);
            $('#view_medium').html(data['student_data'][0].medium);
            $('#view_regno').html(data['student_data'][0].gr_number);
            $('#view_std').html(data['student_data'][0].standard);
            $('#view_div').html(data['student_data'][0].divison);
            $('#view_stream').html(data['student_data'][0].stream);
            $('#view_email').html(data['student_data'][0].email);
            $('#view_contact').html(data['student_data'][0].contact_no);
            $('#view_remaining').html(data['student_data'][0].remaining_fee);
            for (var i = 0; i < data['fee_details'].length; i++) {
                $('#paid_fee_details').append('<div class="row fee_installement"><div class="col-sm-3 view_fee_div">' + (i + 1) + ') Paid: ' + data['fee_details'][i].paid_fee + '</div><div class="col-sm-4 view_fee_div">Period: ' + data['fee_details'][i].fee_month + '</div><div class="col-sm-4 view_fee_div">Paid Dt.: ' + data['fee_details'][i].payment_date + '</div></div>');
            }
            $('#feeModal').modal('show');
            $('.modal-title').text("Fee Details");
        }
    })
});
/************ transfer student START ****************/

$(document).on('click', '.transferstudent', function () {
    var id = $(this).attr("id");
    $('#transfer_student_id').val(id);
    $('#transfer_error').html("");
    $("#transstudent_div").empty();
    $("#transstudent_standard").empty();
    $.ajax({
        url: "/student_transfer/get_schoolname",
        method: "POST",
        data: {id: id},
        dataType: "json",
        success: function (data) {
            $('#transfer_school_id').val(data['student_data'][0].school_id);
            $("#transstudent_standard").append("<option value='" + data['student_data'][0].standard_id + "' selected>" + data['standard_data'][0].standard + "</option>");
            $("#transstudent_div").append("<option value='" + data['student_data'][0].divison + "' selected>" + data['student_data'][0].divison + "</option>");
            for (var i = 0; i < data['school_details'].length; i++) {
                if(data['school_details'][i].id ==data['student_data'][0].school_id){
                    $("#transstudent_school").append("<option value='" + data['school_details'][i].id + "' selected>" + data['school_details'][i].name + "</option>");
                }else{
                    $("#transstudent_school").append("<option value='" + data['school_details'][i].id + "'>" + data['school_details'][i].name + "</option>");
                }

            }
        }
    })

});
/************ transfer student END ****************/

/************ pending view fee START ****************/
$(document).on('click', '.pending_view_fee', function () {
    var gr_number = $(this).attr("id");
    $('#paid_fee_details').html('');
    $.ajax({
        url: "/pending_fee/view_fee",
        method: "POST",
        data: {gr_number: gr_number},
        dataType: "json",
        success: function (data) {
            $('.fee_installement').remove();
            $('#view_firstname').html(data['student_data'][0].firstname + ' ' + data['student_data'][0].father_name + ' ' + data['student_data'][0].surname);
            $('#view_medium').html(data['student_data'][0].medium);
            $('#view_regno').html(data['student_data'][0].gr_number);
            $('#view_std').html(data['student_data'][0].standard);
            $('#view_div').html(data['student_data'][0].divison);
            $('#view_stream').html(data['student_data'][0].stream);
            $('#view_email').html(data['student_data'][0].email);
            $('#view_contact').html(data['student_data'][0].contact_no);

            var remaing = parseInt(data['student_data'][0].remaining_fee);
            var remaining_fee = remaing.toLocaleString('en-IN');
            $('#view_remaining').html(remaining_fee);
            if (data['fee_details'].length > 0) {
                $('#paid_fee_details').append('<thead><tr><th scope="col">#</th><th scope="col">Amount Paid</th><th scope="col">Period</th><th scope="col">Date</th></tr></thead>');
            }
            for (var i = 0; i < data['fee_details'].length; i++) {
                $('#paid_fee_details').append('<tr><th scope="row" width="10px">' + (i + 1) + '</th><td width="10%">' + data['fee_details'][i].paid_fee + '</td><td>' + data['fee_details'][i].fee_month + '</td><td>' + data['fee_details'][i].payment_date + '</td></tr>');
            }
            $('#feeModal').modal('show');
            $('.modal-title').text("Fee Details");
        }
    })
});

/************ pending view fee END ****************/


/************ Get DIV student search START ****************/
function searchDivValue(event) {
    var enterValue = event.target.value;
    var stdValue = enterValue;

    $.ajax({
        url: "/students/get_Div_value",
        method: "POST",
        data: {standard: stdValue},
        success: function (data) {
            var std_data = JSON.parse(data);
            var div = std_data.division;
            var uniqueNames = [];
            $.each(div, function(i, el){
                if($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
            });
            if (uniqueNames) {
                var len = uniqueNames.length;
            }
            $("#student_div").empty();
            $("#student_div").append("<option value=''>Select</option>");
            for (var i = 0; i < len; i++) {
                $("#student_div").append("<option value='" + uniqueNames[i] + "'>" + uniqueNames[i] + "</option>");
            }
        }
    });
}

/************ Get DIV student search END ****************/

/************ Get DIV dailyreportstudent search START ****************/
function dailyreportsearchDivValue(event) {
    var enterValue = event.target.value;
    var stdValue = enterValue;

    $.ajax({
        url: "/students/get_Div_value",
        method: "POST",
        data: {standard: stdValue},
        success: function (data) {
            var std_data = JSON.parse(data);
            var div = std_data.division;
            var uniqueNames = [];
            $.each(div, function(i, el){
                if($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
            });
            if (uniqueNames) {
                var len = uniqueNames.length;
            }
            $("#dailyreport_student_div").empty();
            $("#dailyreport_student_div").append("<option value=''>Select</option>");
            for (var i = 0; i < len; i++) {
                $("#dailyreport_student_div").append("<option value='" + uniqueNames[i] + "'>" + uniqueNames[i] + "</option>");
            }
        }
    });
}

/************ Get DIV student search END ****************/
/************ Get DIV student transfer search START ****************/
function studenttransferDivValue(event) {
    var enterValue = event.target.value;
    var stdValue = enterValue;

    $.ajax({
        url: "/students/get_Div_value",
        method: "POST",
        data: {standard: stdValue},
        success: function (data) {

            var std_data = JSON.parse(data);
            var div = std_data.division;
            var uniqueNames = [];
            $.each(div, function(i, el){
                if($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
            });

            if (uniqueNames) {
                var len = uniqueNames.length;
            }

            $("#student_transferdiv").empty();
            $("#student_transferdiv").append("<option value=''>Select</option>");
            for (var i = 0; i < len; i++) {
                $("#student_transferdiv").append("<option value='" + uniqueNames[i] + "'>" +uniqueNames[i] + "</option>");
            }
        }
    });
}

/************ Get DIV student transfer search END ****************/

/************ Get DIV alumini student search START ****************/
function aluminisearchDivValue(event) {
    var enterValue = event.target.value;
    var stdValue = enterValue;

    $.ajax({
        url: "/students/get_Div_value",
        method: "POST",
        data: {standard: stdValue},
        success: function (data) {
            var std_data = JSON.parse(data);
            var div = std_data.division;
            var uniqueNames = [];
            $.each(div, function(i, el){
                if($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
            });
            if (uniqueNames) {
                var len = uniqueNames.length;
            }


            $("#alumini_student_div").empty();
            $("#alumini_student_div").append("<option value=''>Select</option>");
            for (var i = 0; i < len; i++) {
                $("#alumini_student_div").append("<option value='" + uniqueNames[i] + "'>" + uniqueNames[i] + "</option>");
            }
        }
    });
}

/************ Get DIV student search END ****************/

/************ Division for Add student START ****************/
function DivValue(event) {
    var enterValue = event.target.value;
    var stdValue = enterValue;
    var schoolid = $('#add_studentid').val();

    $.ajax({
        url: "/Students/getDiv_value",
        method: "POST",
        data: {standard: stdValue, school_id: schoolid},
        success: function (data) {
            var std_data = JSON.parse(data);
            var div = std_data.division;
            var division = div.split(",");
            var len = division.length;
            $('#add_studentstdid').val(std_data.id);

            $("#student_division").empty();
            $("#student_division").append("<option value=''>Select</option>");
            for (var i = 0; i < len; i++) {
                $("#student_division").append("<option value='" + division[i] + "'>" + division[i] + "</option>");
            }
        }
    });
}

/************ Division for Add student END ****************/

/************ Division for Edit student START ****************/
function EditDivValue(event) {
    var enterValue = event.target.value;
    var stdValue = enterValue;
    var schoolid = $('#edit_studentschoolid').val();

    $.ajax({
        url: "/Students/getDiv_value",
        method: "POST",
        data: {standard: stdValue, school_id: schoolid},
        success: function (data) {
            var std_data = JSON.parse(data);
            var div = std_data.division;
            var division = div.split(",");
            var len = division.length;
            $('#edit_studentstdid').val(std_data.id);

            $("#editstudent_div").empty();
            $("#editstudent_div").append("<option value=''>Select</option>");
            for (var i = 0; i < len; i++) {
                $("#editstudent_div").append("<option value='" + division[i] + "'>" + division[i] + "</option>");
            }
        }
    });
}

/************ Division for Edit student END ****************/

/************ School Vlaue for Add student START ****************/
function GetSchoolValue(event) {
    var enterValue = event.target.value;
    var schoolIdValue = enterValue;
    $.ajax({
        url: "/admins/get_schoolname_value",
        method: "POST",
        data: {school_id: schoolIdValue},
        success: function (data) {
            var fee_data = JSON.parse(data);
            $('#add_studentschoolname').val(fee_data.name);
            $('#add_studentid').val(fee_data.id);
        }
    });
}

/************ School Vlaue for Add student END ****************/

/************ Standard as per school id for Add student START ****************/
function GetStdValue(event) {
    var enterValue = event.target.value;
    var schoolIdValue = enterValue;

    $.ajax({
        url: "/students/get_standard_value",
        method: "POST",
        data: {school_id: schoolIdValue},
        success: function (data) {
            var std_data = JSON.parse(data);
            var len = std_data.length;
            $("#student_standard").empty();
            $("#student_standard").append("<option value=''>Select</option>");
            for (var i = 0; i < len; i++) {
                $("#student_standard").append("<option value='" + std_data[i]['standard'] + "'>" + std_data[i]['standard'] + "</option>");
            }
        }
    });
}

/************ Standard as per school id for Add student END ****************/

/************ for check paid amount is valid or not START ****************/
function checkPaidValue() {

    var fee = $('#total_fee').val();
    var remaining_amount = $('#remaining_amount').val();

    var duration = $("input[type='radio'][name='fee_duration']:checked").val();

    if (duration == 1 && remaining_amount <= 0) {
        var paid_amount = $("#paid_amount").val();
        var regEx = /^[0-9]+$/;
        var validpaid = regEx.test(paid_amount);
        if ((500 > parseInt(paid_amount))) {
            document.getElementById("take_fee").disabled = true;
            $('#paid_error').html('Please enter minimum 500');
        } else if ((parseInt(fee) / 12) < parseInt(paid_amount)) {
            document.getElementById("take_fee").disabled = true;
            $('#paid_error').html('Please enter valid value');
        } else if (!validpaid) {
            document.getElementById("take_fee").disabled = true;
            $('#paid_error').html('Please enter valid value');
        } else {
            document.getElementById("take_fee").disabled = false;
            $('#paid_error').html('');
        }
    } else {
        document.getElementById("take_fee").disabled = false;
        $('#paid_error').html('');
    }

}

/************ for check paid amount is valid or not END ****************/

/************check in standard students exists or not for remove START ****************/
function remove(divId, rowId) {

    $('#diverror').text('');
    $('#divisionerr').text('');
    var standard_id = $('#id').val();
    var div = divId;
    $.ajax({
        url: "/standard/check_for_remove/",
        method: "POST",
        data: {id: standard_id, divId: div},
        success: function (data) {
            if (data == 'Record Deleted Successfully.') {
                $('#row' + rowId).remove();
            } else {
                $('#error_msg').append('<div class="alert alert-danger alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            }
        }
    });
}

/************check in standard students exists or not for remove END ****************/

/************fetch standard details for edit START ****************/
function edit(id) {

    $('#edit_error').text('');
    $('#editdiv_err').text('');
    $('#standard_edit_std').siblings("span").text(' ');
    $('#standard_fee_year').val('');
    $('#standard_edit_admission_fee').siblings("span").text(' ');
    $('#standard_edit_tuition_fee').siblings("span").text(' ');
    $('#standard_edit_term_fee').siblings("span").text(' ');
    $('#standard_edit_other_fee').siblings("span").text(' ');
    $('#standard_edit_total_fee').siblings("span").text(' ');
    var School_Id = localStorage.getItem('School_id');
    $('#hidden_school_std').val(School_Id);
    $('#editstdModal').modal('show');
    $('#standard_id').val(id);
}

/************fetch standard details for edit END ****************/

/************get fee structure year details for edit START ****************/

function getFeeStructureYear(e){

    $('#standard_edit_admission_fee').val('');
    $('#standard_edit_tuition_fee').val('');
    $('#standard_edit_term_fee').val('');
    $('#standard_edit_other_fee').val('');
    $('#standard_edit_smart_fee').val('');
    $('#standard_edit_computer_fee').val('');
    $('#standard_edit_sports_fee').val('');
    $('#standard_edit_insurance_fee').val('');
    $('#standard_edit_exam_fee').val('');
    $('#standard_edit_enroll_fee').val('');
    $('#standard_edit_total_fee').val('');
    var std_id = $('#standard_id').val();
    var fee_year = event.target.value;
    $.ajax({
        url: "/standard/get_data_update",
        method: "POST",
        data: {id: std_id,fee_year:fee_year },
        dataType: "json",
        success: function (data) {
            $('.row_div').remove();
            $('.error_msg').remove();
            $('.success_msg').remove();
            $('#editdivisionerr').remove();
            $('#standard_submit').val("Edit");
            var div = data.division;
            $('#standard_edit_admission_fee').val(data.admission_fee);
            $('#standard_edit_tuition_fee').val(data.tuition_fee);
            $('#standard_edit_term_fee').val(data.term_fee);
            $('#standard_edit_other_fee').val(data.other_fee);
            $('#standard_edit_smart_fee').val(data.smart_class_fee);
            $('#standard_edit_computer_fee').val(data.computer_fee);
            $('#standard_edit_sports_fee').val(data.sports_fee);
            $('#standard_edit_insurance_fee').val(data.insurance_fee);
            $('#standard_edit_exam_fee').val(data.exam_fee);
            $('#standard_edit_enroll_fee').val(data.enroll_fee);
            $('#standard_edit_total_fee').val(data.tuition_fee);
            if(data.feestructure_flag == 1){
                edit_fee_flag = true;
                edit_totalfee_flag = false;
                $('#smart_fee').show();
                $('#sport_fee').show();
                $('#exam_fee').show();
                $('#computer_fee').show();
                $('#insurance_fee').show();
                $('#enroll_fee').show();
                $('#editotherfee').hide();
            }else if(data.feestructure_flag == 4){
                edit_totalfee_flag = true;
                edit_fee_flag = false;
                $('#smart_fee').hide();
                $('#sport_fee').hide();
                $('#exam_fee').hide();
                $('#computer_fee').hide();
                $('#insurance_fee').hide();
                $('#enroll_fee').hide();
                $('#editotherfee').show();
                $('#edit_admission_fee').hide();
                $('#edit_tution_fee').hide();
                $('#edit_term_fee').hide();
                $('#edittotalfee').show();
            }else{
                edit_fee_flag = false;
                edit_totalfee_flag = false;
                $('#smart_fee').hide();
                $('#sport_fee').hide();
                $('#exam_fee').hide();
                $('#computer_fee').hide();
                $('#insurance_fee').hide();
                $('#enroll_fee').hide();
                $('#editotherfee').show();
                $('#edit_admission_fee').show();
                $('#edit_tution_fee').show();
                $('#edit_term_fee').show();
            }
            $('#id').val(data.id);
            $('#standard_edit_std').val(data.standard);
            $('#hidden_school_std').val(data.school_id);
            var std = $('#standard_edit_std').val();
            $('#hidden_standard').val(std);
            if (div) {
                var division = div.split(',');

                for (var i = 0; i < division.length; i++) {
                    $('#std_edit_tbody').append('<tr id="row' + i + '" class="row_div"><td id="tableCellID" colspan="2"><div class="input-group controls"><input type="text" name="edit_division[]" value="' + division[i] + '" placeholder="Enter Division" id="edit_division" class="form-control edit_div_list pull-left" onblur="checkValue(event)"/><button type="button" name="remove" id="' + i + '" class="btn btn-danger edit_remove" onclick="remove(\'' + division[i] + '\',\'' + i + '\')">X</button><span></span></div></td></tr>');
                }
            }

        }
    })
}

/************get fee structure yearstandard details for edit END ****************/

/************for delete standard START ****************/
function delete_details(id) {
    if (confirm("Are you sure you want to delete this?")) {
        $.ajax({
            url: "/standard/delete_standard_details",
            method: "POST",
            data: {id: id},
            success: function (data) {
                alert(data);
                $('#standard_data').DataTable().ajax.reload();
            }
        });
    } else {
        return false;
    }
}

/************for delete standard END ****************/

/************in edit division check already exists or not START ****************/
var i = 1;

function editDiv() {
    $('#editdiv_err').text('');
    i++;
    var length = $('.div_list').length;

    var newValue = "";
    $("input[name='edit_division[]']").each(function () {
        newValue = $(this).val();
    });
    if (length > 0 && !newValue) {
        return true;
    }
    $('#std_edit_tbody').append('<tr id="row' + i + '" class="row_div"><td colspan="2"><div class="input-group controls"><input type="text" name="edit_division[]" placeholder="Enter Division" id="div_add' + i + '" class="form-control div_list pull-left" onblur="checkValue(event)" /><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button><span></span></div></td></tr>');
}

function checkValue(event) {
    $('#edit_error').text('');

    var enterValue = event.target.value;
    var existArray = [];
    var oldVlaue = "";
    $("input[name='edit_division[]']").each(function () {
        oldVlaue = $(this).val();
        if (oldVlaue) {
            existArray.push(oldVlaue);
        }
    });

    var count = {};
    existArray.forEach(function (i) {
        count[i] = (count[i] || 0) + 1;
    });
    var isValueInArrayOld = existArray.includes(enterValue);
    var regExDiv = /^[A-Z]*$/;
    var validdiv = regExDiv.test(enterValue);
    if (!validdiv) {
        event.target.value = "";
        $('#edit_error').html('Please enter value in A-Z(Capital letters)');
    }
    if (enterValue.length > 3) {
        event.target.value = "";
        $('#edit_error').text('Maximum length is 3');
    }
    if (isValueInArrayOld && count[enterValue] > 1) {
        event.target.value = "";
        $('#edit_error').text('Already Inserted');
    }
}

/************in edit division check already exists or not END ****************/

/************in add division check already exists or not START ****************/
var i = 1;

function addDiv() {
    i++;
    var length = $('.div_list').length;

    var newValue = "";
    $("input[name='div[]']").each(function () {
        newValue = $(this).val();
    });
    if (length > 0 && !newValue) {
        return true;
    }
    $('#diverror').text('');
    $('#divisionerr').text('');
    $('#std_add_tbody').append('<tr id="row' + i + '" class="div_row"><td colspan="2"><div class="input-group controls"><input type="text" name="div[]" placeholder="Enter Division" class="form-control div_list"  onblur="checkDiv(event)"/><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></div></td></tr>').fadeIn('slow');
}

function checkDiv(event) {
    $('#diverror').text('');
    var enterValue = event.target.value;
    var existArray = [];
    var oldVlaue = "";
    $("input[name='div[]']").each(function () {
        oldVlaue = $(this).val();
        if (oldVlaue) {
            existArray.push(oldVlaue);
        }
    });

    var count = {};
    existArray.forEach(function (i) {
        count[i] = (count[i] || 0) + 1;
    });
    var isValueInArrayOld = existArray.includes(enterValue);
    var regExDiv = /^[A-Z]*$/;
    var validdiv = regExDiv.test(enterValue);
    if (!validdiv) {
        event.target.value = "";
        $('#diverror').text('Please enter value in A-Z(Capital letters)');
    }
    if (enterValue.length > 3) {
        event.target.value = "";
        $('#diverror').text('Maximum length is 3');
    }
    if (isValueInArrayOld && count[enterValue] > 1) {
        event.target.value = "";
        $('#diverror').text('Already Inserted Division');
    }
}

/************in add division check already exists or not END ****************/

/************in message after update profile START ****************/
$(function () {
        if (sessionStorage.reloadAfterPageLoad == 'true') {
            $('#profile_edit_message').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">Admin Profile Updated Successfully.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            sessionStorage.reloadAfterPageLoad = false;
        }
    }
);
$(function () {
        if (sessionStorage.changePassword == 'true') {
            $('#profile_edit_message').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">Admin Password Changed Successfully.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            sessionStorage.changePassword = false;
        }
    }
);

/************in message after update profile END ****************/

/************check for new password is not same as old password START ****************/
function checkNewPassword(event) {
    var oldpassword = event.target.value;
    document.getElementById("change_password_btn").disabled = false;
    $.ajax({
        url: "/edit_profile/check_old_password",
        type: 'post',
        data: {oldpassword: oldpassword},
        success: function (response) {
            if (response == 'true') {
                $('#new_password').parent().removeClass();
                $('#new_password').parent().addClass("error");
                $('#new_password').siblings("span").text('Please Not Enter Password Same as Old Password');
                document.getElementById("change_password_btn").disabled = true;
            } else if (response == 'false') {
                $('#new_password').parent().removeClass();
                $('#new_password').parent().addClass("error");
                $('#new_password').siblings("span").text('');
                document.getElementById("change_password_btn").disabled = false;
            }
        }
    });
}

/************check for new password is not same as old password END ****************/

/************collect fee for other fee structure START ****************/
function getOtherFeeDuration(isSelectbox = false, isConcession = false) {
    if (isSelectbox) {
        $('#concession').val('');
        $('#discount').val('');
        $('#total_paid_fee').val('');
        $('#paid_fee_error').text('');
    }
    if (isConcession) {
        $('#total_paid_fee').val('');
        $('#paid_fee_error').text('');
    }

    var concession = $('#concession').val();
    var discount = $('#discount').val();
    var pending_fee = $('#pending_fee').val();
    var admission_year = $('#admission_year').val();

    var current_year = new Date().getFullYear();
    var standard =$('#student_feestdandard').val();

    if (Number(admission_year) == Number(current_year) || standard == '1' || standard == 'I' || standard == '9') {
        $('#student_status').val('new');
    } else {
        $('#student_status').val('old');
    }

    if ($('#student_status').val() == 'new') {
        $('#old_student').attr('disabled', true);
    } else {
        $('#new_student').attr('disabled', true);
    }
    var student_status = $('#student_status :selected').val();
    var start_month = $('#start_fee_duration :selected').val();
    var start_month_name = $('#start_fee_duration :selected').text();

    var end_month = $('#end_fee_duration :selected').val();
    var end_month_name = $('#end_fee_duration :selected').text();

    var monthNames = ["June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May"];
    var monthValues = ["6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5"];

    var start = monthNames.indexOf(start_month_name);
    var end = monthNames.indexOf(end_month_name);

    if (start_month && end_month) {
        var duration = (parseInt(end) - parseInt(start)) + 1;
        $('#fee_duration').val(duration);
    } else {
        var duration = '1';
        $('#fee_duration').val(duration);
    }


    var admission_fee = $('#admission_fee').val();
    var tuition_fee = $('#tuition_fee').val();
    var term_fee = $('#term_fee').val();
    var other_fee = $('#other_fee').val();

    if (student_status == 'new') {
        var total = Number(admission_fee) + (Number(tuition_fee) * 12) + (Number(term_fee) * 2) + (Number(other_fee) * 2);
        $('#total_fee').val(total);
    } else {
        var total = (Number(tuition_fee) * 12) + (Number(term_fee) * 2) + (Number(other_fee) * 2);
        $('#total_fee').val(total);
    }

    var total_fee = $('#total_fee').val();


    if (start_month == '6' && !end_month) {
        $('#paid_fee_amount').html('');


        if (student_status == 'new') {
            if (concession > '0' && Number(concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                if(discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)- Number(concession))){
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession) -Number(discount);
                }else{
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession);
                }
                $('#paid_admission_fee').val(admission_fee);
            }else{
                if(discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))){
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) -Number(discount);
                }else{
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee));
                }
                $('#paid_admission_fee').val(admission_fee);
            }

        } else {
            if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                if(discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))- Number(concession)){
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession) - Number(discount);
                }else{
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))- Number(concession);
                }

            }else{
                if(discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))){
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(discount);
                }else{
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee));
                }
            }

        }

        if (student_status == 'new') {
            if ( Number(concession) > (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                $('#concession_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            }else{
                $('#concession_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

        } else {
            if ( Number(concession) > (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                $('#concession_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            }else{
                $('#concession_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

        }

        if (student_status == 'new') {
            if ( Number(discount) > (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee) - Number(concession))) {
                $('#discount_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            }else{
                $('#discount_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

        } else {
            if ( Number(discount) > (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee) - Number(concession))) {
                $('#discount_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            }else{
                $('#discount_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

        }

        if (pending_fee > 0) {
            if (student_status == 'new') {
                if (concession > '0' && Number(concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    if(discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)- Number(concession))){
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }else{
                    if(discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))){
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + discount + '</span><br><span>Pending Fee : ' + pending_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }

            } else {
                if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    if(discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))- Number(concession)){
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    } else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }else{
                    if(discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))){
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + discount + '</span><br><span>Pending Fee : ' + pending_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }
        }else{
            if (student_status == 'new') {
                if (concession > '0' && Number(concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    if(discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)- Number(concession))){
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }else{
                    if(discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))){
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }

            } else {
                if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    if(discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))- Number(concession)){
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }else{
                    if(discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))){
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }
        }


        $('#paid_tution_fee').val(tuition_fee);
        $('#paid_term_fee').val(term_fee);
        $('#paid_other_fee').val(other_fee);
        $('#paid_amount').val(paid_amount);
        $('#fee_period').val(start_month_name);
    } else if (start_month == '12' && !end_month) {
        $('#paid_fee_amount').html('');

        if (concession > '0' && Number(concession) <=(Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
            if ( discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)) {
                var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession) - Number(discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession);
            }

        } else {
            if ( discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)) {
                var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee));
            }
        }

        if ( Number(concession) > (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
            $('#concession_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#concession_error').text('');
            document.getElementById("take_fee").disabled = false;
        }

        if ( Number(discount) > (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)- Number(concession))) {
            $('#discount_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#discount_error').text('');
            document.getElementById("take_fee").disabled = false;
        }
        if (pending_fee > 0) {
            if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))- Number(concession)) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }

            } else {
                if ( discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + discount + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }

            }
        } else {
            if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                if ( discount > '0' && (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))- Number(concession)) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee  + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee  + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if ( discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee  + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        }


        $('#paid_tution_fee').val(tuition_fee);
        $('#paid_term_fee').val(term_fee);
        $('#paid_other_fee').val(other_fee);
        $('#paid_amount').val(paid_amount);
        $('#fee_period').val(start_month_name);
    } else if (start_month && !end_month) {
        $('#paid_fee_amount').html('');
        if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(pending_fee))) {
            if ( discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee)) - Number(concession)) {
                var paid_amount = (Number(tuition_fee) + Number(pending_fee)) - Number(concession) - Number(discount);
            }else{
                var paid_amount =(Number(tuition_fee) + Number(pending_fee)) - Number(concession);
            }
        } else {
            if ( discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee))) {
                var paid_amount = Number(tuition_fee) + Number(pending_fee) - Number(discount);
            }else{
                var paid_amount = Number(tuition_fee) + Number(pending_fee);
            }
        }

        if (Number(concession) > (Number(tuition_fee) + Number(pending_fee))) {
            $('#concession_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#concession_error').text('');
            document.getElementById("take_fee").disabled = false;
        }
        if (Number(discount) > (Number(tuition_fee) + Number(pending_fee)- Number(concession))) {
            $('#concession_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#concession_error').text('');
            document.getElementById("take_fee").disabled = false;
        }
        if (pending_fee > 0) {
            if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(pending_fee))) {
                if ( discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee)) - Number(concession)) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }

            } else {
                if ( discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee))) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount+ '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        } else {
            if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(pending_fee))) {
                if ( discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee)) - Number(concession)) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if ( discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee))) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        }

        $('#paid_tution_fee').val(tuition_fee);
        $('#paid_amount').val(paid_amount);
        $('#fee_period').val(start_month_name);
    }
    if (start_month && end_month) {
        var i;
        var total_month = [];

        for (i = parseInt(start); i <= end; i++) {
            var total = monthValues[i];
            total_month.push(parseInt(total));
        }

        if ($.inArray(6, total_month) > -1 && $.inArray(12, total_month) == -1) {
            $('#paid_fee_amount').html('');

            if (student_status == 'new') {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession) - Number(discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession);
                    }
                    $('#paid_admission_fee').val(admission_fee);
                }else{
                    if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)- Number(discount));
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee));
                    }
                    $('#paid_admission_fee').val(admission_fee);
                }

            } else {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)- Number(discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession);
                    }
                }else{
                    if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee) - Number(discount));
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee));
                    }
                }

            }
            if (student_status == 'new') {
                if ( Number(concession) > (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    $('#concession_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#concession_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            } else {
                if (Number(concession) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    $('#concession_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#concession_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            }
            if (student_status == 'new') {
                if ( Number(discount) > (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)-Number(concession))) {
                    $('#discount_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#discount_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            } else {
                if (Number(discount) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee)-Number(concession))) {
                    $('#discount_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#discount_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            }
            if (pending_fee > 0) {
                if (student_status == 'new') {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + discount + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + discount + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                }
            }else{
                if (student_status == 'new') {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        if ( discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee) - Number(concession))) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + discount +'</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if ( discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        if ( discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee) - Number(concession))) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if ( discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                }
            }

            $('#paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#paid_term_fee').val(term_fee);
            $('#paid_other_fee').val(other_fee);
            $('#paid_amount').val(paid_amount);
            $('#fee_period').val(start_month_name + ' - ' + end_month_name);
        } else if ($.inArray(12, total_month) > -1 && $.inArray(6, total_month) == -1) {
            $('#paid_fee_amount').html('');

            if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession) - Number(discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession);
                }
            } else {
                if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee)- Number(discount));
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee));
                }
            }

            if (Number(concession) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                $('#concession_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            } else {
                $('#concession_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

            if (Number(discount) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee) - Number(concession))) {
                $('#discount_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            } else {
                $('#discount_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

            if (pending_fee > 0) {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession +'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + discount + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if ( discount > '0' && Number(discount) <=(Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }

            $('#paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#paid_term_fee').val(term_fee);
            $('#paid_other_fee').val(other_fee);
            $('#paid_amount').val(paid_amount);
            $('#fee_period').val(start_month_name + ' - ' + end_month_name);
        } else if ($.inArray(6, total_month) > -1 && $.inArray(12, total_month) > -1) {
            $('#paid_fee_amount').html('');

            if (student_status == 'new') {
                if (concession > '0' && Number(concession) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee))) {
                    if ( discount > '0' && Number(discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)) {
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession) - Number(discount);
                    }else{
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession);
                    }
                    $('#paid_admission_fee').val(admission_fee);
                }else{
                    if ( discount > '0' && Number(discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee))) {
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee) - Number(discount));
                    }else{
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee));
                    }
                    $('#paid_admission_fee').val(admission_fee);
                }

            } else {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee))) {
                    if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession) -Number(discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession);
                    }
                }else{
                    if ( discount > '0' && Number(discount) <= Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee)) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee) - Number(discount));
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee));
                    }
                }

            }
            if (student_status == 'new') {
                if (Number(concession) >  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee))) {
                    $('#concession_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#concession_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            } else {
                if (Number(concession) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee))) {
                    $('#concession_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#concession_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            }
            if (student_status == 'new') {
                if (Number(discount) >  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee) - Number(concession))) {
                    $('#discount_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#discount_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            } else {
                if (Number(discount) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee) - Number(concession))) {
                    $('#discount_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#discount_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            }

            if (pending_fee > 0) {
                if (student_status == 'new') {
                    if (concession > '0' && Number(concession) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee))) {
                        if ( discount > '0' && Number(discount) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if ( discount > '0' && Number(discount) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Discount :' + discount +  '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee))) {
                        if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Discount :' + discount + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                }
            }else{
                if (student_status == 'new') {
                    if (concession > '0' && Number(concession) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee))) {
                        if ( discount > '0' && Number(discount) <=(Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) +  '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) +  '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if ( discount > '0' && Number(discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) +  '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee))) {
                        if ( discount > '0' && Number(discount) <=(Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee) - Number(concession))) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) +  '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) +  '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                }
            }

            $('#paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#paid_term_fee').val(Number((term_fee) * 2));
            $('#paid_other_fee').val(Number((other_fee) * 2));
            $('#paid_amount').val(paid_amount);
            $('#fee_period').val(start_month_name + ' - ' + end_month_name);
        } else {
            $('#paid_fee_amount').html('');

            if (concession > '0' && Number(concession) <=(Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee)) - Number(concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee)) - Number(concession) - Number(discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee)) - Number(concession);
                }
            } else {
                if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee) - Number(discount));
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee));
                }
            }

            if (Number(concession) > (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                $('#concession_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            } else {
                $('#concession_error').text('');
                document.getElementById("take_fee").disabled = false;
            }
            if (Number(discount) > (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee) - Number(concession))) {
                $('#discount_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            } else {
                $('#discount_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

            if (pending_fee > 0) {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                    if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee)) - Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession +'</span><br><span>Discount :' + discount +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (concession > '0' && Number(concession) <=(Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                    if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee)) - Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if ( discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }

            $('#paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#paid_amount').val(paid_amount);
            $('#fee_period').val(start_month_name + ' - ' + end_month_name);
        }
    }
    var takedate=$('#take_date').val();
    if ($('#concession_error').is(':empty') && $('#paid_fee_error').is(':empty') && $('#discount_error').is(':empty') ) {
        document.getElementById("take_fee").disabled = false;
    } else {
        document.getElementById("take_fee").disabled = true;
    }
}
/************collect fee for other fee structure END ****************/

/************get fee duration end month value START ****************/
function feeMonth() {
    var start_month = $('#start_fee_duration :selected').val();

    var start_month_name = $('#start_fee_duration :selected').text();

    var monthNames = ["June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May"];
    var monthValues = ["6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5"];
    var index_month = monthNames.indexOf(start_month_name) + 1;
    $('#end_fee_duration').html('');
    $("#end_fee_duration").append("<option value=''>Select</option>");
    for (index_month; index_month < monthNames.length; index_month++) {
        $('#end_fee_duration').append('<option value="' + monthValues[index_month] + '">' + monthNames[index_month] + '</option>');
    }
}
function concession_FeeError() {
    var flagfee=$('#flag').val();
    if(flagfee =='3'){
        getOtherFeeDuration();
    }else if(flagfee =='2'){
        getPreFeeDuration();
    }else if(flagfee =='1'){
        getSecondaryFeeDuration();
    }else if(flagfee =='4'){
        getFirststepFee();
    }
    $('#total_paid_fee').val('');
    $('#paid_fee_error').text('');
    $('#discount').val('');
    $('#discount_error').text('');
}
function discount_FeeError() {
    $('#total_paid_fee').val('');
    $('#paid_fee_error').text('');
    var flagfeestructure=$('#flag').val();
    if(flagfeestructure == '3'){
        getOtherFeeDuration();
    }else if(flagfeestructure =='2'){
        getPreFeeDuration();
    }else if(flagfeestructure =='1'){
        getSecondaryFeeDuration();
    }else if(flagfeestructure =='4'){
        getFirststepFee();
    }
}
/************get fee duration end month value END ****************/
function paid_feeerror() {
    var paid_by_student = $('#total_paid_fee').val();
    var paid_amount = $('#paid_amount').val();
    var fee_year = $('#studentFeeYear').val();
    //console.log('fee_year',fee_year);

    var startfee_paidmonth = $('#start_fee_duration :selected').val();
    var endfee_paidmonth = $('#end_fee_duration :selected').val();
    var startfee_paidinstallment = $('#start_fee_installment :selected').val();
    var endfee_paidinstallment = $('#end_fee_installment :selected').val();

    if(fee_year != '1'){
        if (paid_by_student > '0' && Number(paid_by_student) > Number($('#paid_amount').val())) {
            $('#paid_fee_error').text('Please enter value less than or equal to paid amount');
            document.getElementById("take_fee").disabled = true;
        } else if (startfee_paidmonth == '5' || endfee_paidmonth == '5' || startfee_paidinstallment == 'third' || endfee_paidinstallment == 'third') {
            if (paid_by_student > '0' && Number(paid_by_student) != Number($('#paid_amount').val())) {
                $('#paid_fee_error').text('Please enter paid fee same as total fee');
                document.getElementById("take_fee").disabled = true;
            } else {
                $('#paid_fee_error').text('');
                document.getElementById("take_fee").disabled = false;
            }
        } else {
            $('#paid_fee_error').text('');
            document.getElementById("take_fee").disabled = false;
        }
    }else{
        if (paid_by_student > '0' && Number(paid_by_student) > Number($('#paid_amount').val())) {
            $('#paid_fee_error').text('Please enter value less than or equal to paid amount');
            document.getElementById("take_fee").disabled = true;
        } else if (startfee_paidmonth == '5' || endfee_paidmonth == '5' || startfee_paidinstallment == 'fourth' || endfee_paidinstallment == 'fourth') {
            if (paid_by_student > '0' && Number(paid_by_student) != Number($('#paid_amount').val())) {
                $('#paid_fee_error').text('Please enter paid fee same as total fee');
                document.getElementById("take_fee").disabled = true;
            } else {
                $('#paid_fee_error').text('');
                document.getElementById("take_fee").disabled = false;
            }
        } else {
            $('#paid_fee_error').text('');
            document.getElementById("take_fee").disabled = false;
        }
    }

    if ($('#concession_error').is(':empty') && $('#paid_fee_error').is(':empty')&& $('#discount_error').is(':empty')) {
        document.getElementById("take_fee").disabled = false;
    } else {
        document.getElementById("take_fee").disabled = true;
    }
}
function paid_other_feeerror() {
    var paid_by_student = $('#total_other_fee').val();
    var paid_amount = $('#other_fee').val();
    var fee_year = $('#studentFeeYear').val();
    
}
/************clear all student data from database START ****************/
function clear_all() {
    if (confirm("Are you sure you want to delete this?")) {
        $.ajax({
            url: "/csv_import/clear_all_student",
            method: "POST",
            success: function (data) {
                if (data == 'Clear All Student Data Deleted Successfully.') {
                    $('#success_delete_all').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                } else {
                    $('#success_delete_all').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                }
            }
        });
    } else {
        return false;
    }
}

/************clear all student data from database END  ****************/

/************clear student data standard wise from database START  ****************/
function clear_std_wise() {
    var formData = $('#clear_standard_wise').serialize();
    $std = $('#clear_student_standard').val();
    $school_id = $('#clear_student_school').val();
    if (confirm("Are you sure you want to delete this?")) {
        $.ajax({
            url: "/csv_import/clear_student_standard",
            method: "POST",
            data: formData,
            success: function (data) {
                if (data == 'Clear Student Data Deleted Successfully') {
                    $('#success_delete_data').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                } else {
                    $('#success_delete_data').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                }
            }
        });
    } else {
        return false;
    }
}

/************clear student data standard wise from database END  ****************/

/************Amdin logout START  ****************/
function admin_logout() {
    $.ajax({
        url: "/admin/logout",
        method: "POST",
        success: function (data) {
            window.localStorage.removeItem('School_id');
            window.localStorage.removeItem('School_name');
            window.location.href = "/admin/login";
        }
    });
}

/************Amdin logout END  ****************/


function totalFeeget() {
    var admission = $('#admission_year').val();

    var current = new Date().getFullYear();
    var student_status_old = $('#student_status').val();
    var admission_fee = $('#admission_fee').val();
    var tuition_fee = $('#tuition_fee').val();
    var term_fee = $('#term_fee').val();
    var other_fee = $('#other_fee').val();


    if (Number(admission) == Number(current)) {
        $('#student_status').val('new');
        var fee = Number(admission_fee) + (Number(tuition_fee) * 12) + (Number(term_fee) * 2) + (Number(other_fee) * 2);
        $('#total_fee').val(fee);
    } else {
        $('#student_status').val('old');
        var fee = (Number(tuition_fee) * 12) + (Number(term_fee) * 2) + (Number(other_fee) * 2);
        $('#total_fee').val(fee);
    }

}


function edittotalFeeget() {
    var admission = $('#edit_year').val();

    var current = new Date().getFullYear();
    var student_status_old = $('#edit_student_status').val();
    var admission_fee = $('#edit_admission_fee').val();
    var tuition_fee = $('#edit_tuition_fee').val();
    var term_fee = $('#edit_term_fee').val();
    var other_fee = $('#edit_other_fee').val();

    if (Number(admission) == Number(current)) {
        $('#edit_student_status').val('new');
        var fee = Number(admission_fee) + (Number(tuition_fee) * 12) + (Number(term_fee) * 2) + (Number(other_fee) * 2);
        $('#edit_total_fee').val(fee);
    } else {
        $('#edit_student_status').val('old');
        var fee = (Number(tuition_fee) * 12) + (Number(term_fee) * 2) + (Number(other_fee) * 2);
        $('#edit_total_fee').val(fee);
    }

}

/************ delete fee_collection START ****************/
function delete_fee_collection(id) {
    if (confirm("Are you sure you want to delete this?")) {
        $.ajax({
            url: "/daily_report/fee_collection_details",
            method: "POST",
            data: {id: id},
            success: function (data) {
                if (data == 'Data Deleted') {
                    $('#profile_edit_message').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">Record Deleted Successfully.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                    $('#daily_report').DataTable().ajax.reload();
                } else {
                    $('#profile_edit_message').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                }
            }
        });
    } else {
        return false;
    }
}

/************ delete fee_collection END ****************/

/************get other fee structure for edit fee START ****************/
function editgetFeeDuration(isSelect = false) {
    if (isSelect) {
        $('#edit_concession').val('');
        $('#edit_discount').val('');
        $('#edit_totalpaid_fee').val('');
        $('#edit_paid_fee_error').text('');
    }

    var edit_concession = $('#edit_concession').val();
    var edit_discount=$('#edit_discount').val();
    var edit_pending_fee = $('#edit_pending_fee').val();
    var admission_year = $('#edit_year').val();


    var current_year = new Date().getFullYear();
    document.getElementById("edit_take_fee").disabled = false;
    var editstandard =$('#edit_studentfeestd').val();

    if (Number(admission_year) == Number(current_year) || editstandard == '1' || editstandard == 'I' || editstandard == '9') {
        $('#edit_student_status').val('new');
    } else {
        $('#edit_student_status').val('old');
    }

    if ( $('#edit_student_status').val() == 'new') {
        $('#edit_old_student').attr('disabled', true);
    } else {
        $('#edit_new_student').attr('disabled', true);
    }
    var student_status = $('#edit_student_status :selected').val();
    var start_month = $('#start_duration :selected').val();
    var start_month_name = $('#start_duration :selected').text();

    var end_month = $('#end_duration :selected').val();
    var end_month_name = $('#end_duration :selected').text();

    var monthNames = ["June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May"];
    var monthValues = ["6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5"];

    var start = monthNames.indexOf(start_month_name);
    var end = monthNames.indexOf(end_month_name);

    if (start_month && end_month) {
        var duration = (parseInt(end) - parseInt(start)) + 1;
        $('#edit_fee_duration').val(duration);
    } else {
        var duration = '1';
        $('#edit_fee_duration').val(duration);
    }


    var admission_fee = $('#edit_admission_fee').val();
    var tuition_fee = $('#edit_tuition_fee').val();
    var term_fee = $('#edit_term_fee').val();
    var other_fee = $('#edit_other_fee').val();

    if (student_status == 'new') {
        var total = Number(admission_fee) + (Number(tuition_fee) * 12) + (Number(term_fee) * 2) + (Number(other_fee) * 2) + Number(edit_pending_fee);
        $('#edit_total_fee').val(total);
    } else {
        var total = (Number(tuition_fee) * 12) + Number(edit_pending_fee) + (Number(term_fee) * 2) + (Number(other_fee) * 2);
        $('#edit_total_fee').val(total);
    }

    var total_fee = $('#edit_total_fee').val();


    if (start_month == '6' && !end_month) {

        $('#edit_paid_fee_amount').html('');

        if (student_status == 'new') {
            if (Number(edit_concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                if (Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                }else{
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession);
                }
                $('#edit_paid_admission_fee').val(admission_fee);
            } else {
                if (Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_discount);
                }else{
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee));
                }
                $('#edit_paid_admission_fee').val(admission_fee);
            }
        } else {
            if (Number(edit_concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                if (Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                }else{
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession);
                }
            } else {
                if (Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_discount);
                }else{
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee));
                }
            }
        }

        if (student_status == 'new') {
            if (Number(edit_concession) > (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                $('#edit_concession_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_concession_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
        } else {
            if (Number(edit_concession) > (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                $('#edit_concession_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_concession_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
        }
        if (student_status == 'new') {
            if (Number(edit_discount) > (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                $('#edit_discount_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_discount_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
        } else {
            if (Number(edit_discount) > (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                $('#edit_discount_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_discount_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
        }
        if (edit_pending_fee > 0) {
            if (student_status == 'new') {
                if (Number(edit_concession) > 0 && Number(edit_concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (Number(edit_concession) > 0 && Number(edit_concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }
        }else{
            if (student_status == 'new') {
                if (Number(edit_concession) > 0 && Number(edit_concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee))) {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee))) {
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (Number(edit_concession) > 0 && Number(edit_concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }
        }

        $('#edit_paid_tution_fee').val(tuition_fee);
        $('#edit_paid_term_fee').val(term_fee);
        $('#edit_paid_other_fee').val(other_fee);
        $('#edit_paid_amount').val(paid_amount);
        $('#edit_fee_period').val(start_month_name);
    } else if (start_month == '12' && !end_month) {
        $('#edit_paid_fee_amount').html('');

        if (Number(edit_concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
            if (Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession) -Number(edit_discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession);
            }
        } else {
            if (Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee));
            }
        }


            if (Number(edit_concession) >(Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                $('#edit_concession_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_concession_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }

        if (Number(edit_discount) >(Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
            $('#edit_discount_error').text('Please enter value less than paid amount');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_discount_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }

        if (edit_pending_fee > 0) {
            if (edit_concession > '0' && Number(edit_concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        } else {
            if (edit_concession > '0' && Number(edit_concession) <= Number($('#edit_paid_amount').val())) {
                if (edit_discount > '0' && Number(edit_discount) <= Number($('#edit_paid_amount').val()-Number(edit_concession))) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <= Number($('#edit_paid_amount').val())) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + edit_discount + +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        }
        $('#edit_paid_tution_fee').val(tuition_fee);
        $('#edit_paid_term_fee').val(term_fee);
        $('#edit_paid_other_fee').val(other_fee);
        $('#edit_paid_amount').val(paid_amount);
        $('#edit_fee_period').val(start_month_name);
    } else if (start_month && !end_month) {

        $('#edit_paid_fee_amount').html('');
        if (Number(edit_concession) <= (Number(tuition_fee) + Number(edit_pending_fee))) {
            if (Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                var paid_amount = (Number(tuition_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(edit_pending_fee)) - Number(edit_concession);
            }
        } else {
            if (Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))) {
                var paid_amount = Number(tuition_fee) + Number(edit_pending_fee)-Number(edit_discount);
            }else{
                var paid_amount = Number(tuition_fee) + Number(edit_pending_fee);
            }
        }
        if (Number(edit_concession) >(Number(tuition_fee) + Number(edit_pending_fee))) {
            $('#edit_concession_error').text('Please enter value less than paid amount');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_concession_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }
        if (Number(edit_discount) >(Number(tuition_fee) + Number(edit_pending_fee))) {
            $('#edit_discount_error').text('Please enter value less than paid amount');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_discount_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }
        if (edit_pending_fee > 0) {
            if (edit_concession > '0' && Number(edit_concession) <= (Number(tuition_fee) + Number(edit_pending_fee))) {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount+ '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Discount :' + edit_discount+ '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        } else {
            if (edit_concession > '0' && Number(edit_concession) <= (Number(tuition_fee) + Number(edit_pending_fee))) {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount+'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Discount :' + edit_discount+'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        }
        $('#edit_paid_tution_fee').val(tuition_fee);
        $('#edit_paid_amount').val(paid_amount);
        $('#edit_fee_period').val(start_month_name);
    }
    if (start_month && end_month) {
        var i;
        var total_month = [];

        for (i = parseInt(start); i <= end; i++) {
            var total = monthValues[i];
            total_month.push(parseInt(total));
        }

        if ($.inArray(6, total_month) > -1 && $.inArray(12, total_month) == -1) {
            $('#edit_paid_fee_amount').html('');

            if (student_status == 'new') {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))){
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession);
                    }
                    $('#edit_paid_admission_fee').val(admission_fee);
                }else{
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee));
                    }
                    $('#edit_paid_admission_fee').val(admission_fee);
                }

            } else {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))){
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))-Number(edit_concession)){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession);
                    }
                }else{
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) -Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) ;
                    }

                }
            }
            if (student_status == 'new') {
                if (Number(edit_concession) > (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    $('#edit_concession_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_concession_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            } else {
                if (Number(edit_concession) > (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))) {
                    $('#edit_concession_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_concession_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            }
            if (student_status == 'new') {
                if (Number(edit_discount) > (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_discount_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_discount_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            } else {
                if (Number(edit_discount) > (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))-Number(edit_concession)) {
                    $('#edit_discount_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_discount_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            }
            if (edit_pending_fee > 0) {
                if (student_status == 'new') {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))){
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Discount :' + edit_discount +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }
                }
            }else{
                if (student_status == 'new') {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))){
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +'</span><br><span>Discount :' + edit_discount +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }
                }
            }

            $('#edit_paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#edit_paid_term_fee').val(term_fee);
            $('#edit_paid_other_fee').val(other_fee);
            $('#edit_paid_amount').val(paid_amount);
            $('#edit_fee_period').val(start_month_name + ' - ' + end_month_name);
        } else if ($.inArray(12, total_month) > -1 && $.inArray(6, total_month) == -1) {
            $('#edit_paid_fee_amount').html('');

            if (edit_concession > '0' && Number(edit_concession) <=(Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                if (edit_discount > '0' && Number(edit_discount) <=(Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession);
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <=(Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee));
                }
            }
            if (Number(edit_concession) >(Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                $('#edit_concession_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_concession_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
            if (Number(edit_discount) >(Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_discount)) {
                $('#edit_discount_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_discount_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
            if (edit_pending_fee > 0) {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }

            $('#edit_paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#edit_paid_term_fee').val(term_fee);
            $('#edit_paid_other_fee').val(other_fee);
            $('#edit_paid_amount').val(paid_amount);
            $('#edit_fee_period').val(start_month_name + ' - ' + end_month_name);
        } else if ($.inArray(6, total_month) > -1 && $.inArray(12, total_month) > -1) {

            $('#edit_paid_fee_amount').html('');

            if (student_status == 'new') {
                if (edit_concession > '0' && Number(edit_concession) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                    }else{
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession);
                    }
                    $('#edit_paid_admission_fee').val(admission_fee);
                }else{
                    if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_discount);
                    }else{
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee));
                    }
                    $('#edit_paid_admission_fee').val(admission_fee);
                }

            } else {
                if (edit_concession > '0' && Number(edit_concession) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))){
                    if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession);
                    }
                }else{
                    if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee));
                    }
                }
            }

            if (student_status == 'new') {
                if (Number(edit_concession) > (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    $('#edit_concession_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_concession_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            } else {
                if (Number(edit_concession) > ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    $('#edit_concession_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_concession_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            }
            if (student_status == 'new') {
                if (Number(edit_discount) > (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_discount_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_discount_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            } else {
                if (Number(edit_discount) > ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_discount_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_discount_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            }
            if(edit_pending_fee > 0){
                if (student_status == 'new') {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                        if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Discount :' + edit_discount +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (edit_concession > '0' && Number(edit_concession) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }
                }
            }else{
                if (student_status == 'new') {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                        if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (edit_concession > '0' && Number(edit_concession) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(other_fee) + Number(edit_pending_fee))){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) * 2) + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }
                }
            }

            $('#edit_paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#edit_paid_term_fee').val(Number((term_fee) * 2));
            $('#edit_paid_other_fee').val(Number((other_fee) * 2));
            $('#edit_paid_amount').val(paid_amount);
            $('#edit_fee_period').val(start_month_name + ' - ' + end_month_name);
        } else {
            $('#edit_paid_fee_amount').html('');

            if (Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))) {
                if (Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee)) - Number(edit_concession);
                }
            } else {
                if (Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee));
                }
            }
            if (Number(edit_concession) >(Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))) {
                $('#edit_concession_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_concession_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
            if (Number(edit_discount) >(Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                $('#edit_discount_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_discount_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
            if (edit_pending_fee > 0) {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))) {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))) {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }

            $('#edit_paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#edit_paid_amount').val(paid_amount);
            $('#edit_fee_period').val(start_month_name + ' - ' + end_month_name);
        }
    }

    var startedit_paidmonth = $('#start_duration :selected').val();
    var endedit_paidmonth = $('#end_duration :selected').val();

    var paid_edit_student=$('#edit_totalpaid_fee').val();
    if (startedit_paidmonth == '5' || endedit_paidmonth == '5') {
        if (paid_edit_student > '0' && Number(paid_edit_student) != Number($('#edit_paid_amount').val())) {
            $('#edit_paid_fee_error').text('Please enter paid fee same as total fee');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_paid_fee_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }
    }

    if ($('#edit_concession_error').is(':empty') && $('#edit_paid_fee_error').is(':empty')  && $('#edit_discount_error').is(':empty')) {
        document.getElementById("edit_take_fee").disabled = false;
    } else {
        document.getElementById("edit_take_fee").disabled = true;
    }
}
/************get other fee structure for edit fee END**************/


/************concession and paid fee error in edit page START**************/
function editconcession_FeeError() {
    var edit_flag = $('#edit_flag').val();
    if(edit_flag == '3'){
        editgetFeeDuration();
    }else if(edit_flag == '2'){
        editPrePrimaryFeeDuration();
    }else if (edit_flag == '1'){
        editSecondaryFeeDuration();
    }else if(edit_flag == '4'){
        editgetFirststepFee();
    }
    $('#edit_totalpaid_fee').val('');
    $('#edit_paid_fee_error').text('');
    $('#edit_discount').val('');
    $('#edit_discount_error').text('');
}
function editdiscount_FeeError() {
    $('#edit_totalpaid_fee').val('');
    $('#edit_paid_fee_error').text('');
    var edit_feeflag = $('#edit_flag').val();
    if(edit_feeflag == '3'){
        editgetFeeDuration();
    }else if(edit_feeflag == '2'){
        editPrePrimaryFeeDuration();
    }else if (edit_feeflag == '1'){
        editSecondaryFeeDuration();
    }else if(edit_feeflag == '4'){
        editgetFirststepFee();
    }
}
function editpaid_FeeError() {
    var paid_edit_student = $('#edit_totalpaid_fee').val();
    var edit_paid_Amount = $('#edit_paid_amount').val();
    var startedit_paidmonth = $('#start_duration :selected').val();
    var endedit_paidmonth = $('#end_duration :selected').val();
    var startedit_paidinstallment = $('#start_editfee_installment :selected').val();
    var endedit_paidinstallment = $('#end_editfee_installment :selected').val();
    if (paid_edit_student > '0' && Number(paid_edit_student) > Number(edit_paid_Amount)) {
        $('#edit_paid_fee_error').text('Please enter value less than or equal to paid amount');
        document.getElementById("edit_take_fee").disabled = true;
    } else if (startedit_paidmonth == '5' || endedit_paidmonth == '5' || startedit_paidinstallment == 'fourth' || endedit_paidinstallment == 'fourth') {
        if (paid_edit_student > '0' && Number(paid_edit_student) != Number($('#edit_paid_amount').val())) {
            $('#edit_paid_fee_error').text('Please enter paid fee same as total fee');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_paid_fee_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }
    } else {
        $('#edit_paid_fee_error').text('');
        document.getElementById("edit_take_fee").disabled = false;
    }
    if ($('#edit_concession_error').is(':empty') && $('#edit_paid_fee_error').is(':empty')  && $('#edit_discount_error').is(':empty')) {
        document.getElementById("edit_take_fee").disabled = false;
    } else {
        document.getElementById("edit_take_fee").disabled = true;
    }
}
/************concession and paid fee error in edit page END**************/

/************check password when student delete data START ****************/
function confirm_password_modal(id) {
    $('#confirm_password_error').html('');
    $('#password_delete').val('');
    $('#confirm_password_modal').modal('show');
    $('#delete_id').val(id);
}

$('#password_delete').keypress(function (e) {
    if (e.which == '13') {
        $('#student_submit').trigger('click');
        return false;
    }
});

function checkPassword() {
    var formData = $('#delete_student_data').serialize();
    $.ajax({
        url: "/students/delete_student_data",
        method: "POST",
        data: formData,
        success: function (data) {
            if (data == 'Record Deleted Successfully.') {
                $('#confirm_password_modal').modal('hide');
                if($('#profile_edit_message').is(':empty')){
                    $('#profile_edit_message').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                }
                $('#student_data').DataTable().ajax.reload();
            } else {
                if ($('#confirm_password_error').is(':empty')) {
                    $('#confirm_password_error').append('<div class="alert alert-danger alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                }
            }
        }
    });
}
/************check password when student delete data END ****************/


/************check password when fee collection delete data START ****************/
function confirm_password_fee_modal(id) {
    $('#confirm_error').html('');
    $('#password_fee_delete').val('');
    $('#confirm_password_fee_modal').modal('show');
    $('#delete_fee_id').val(id);
}
$('#password_fee_delete').keypress(function (e) {
    if (e.which == '13') {
        $('#fee_submit').trigger('click');
        return false;
    }
});

function checkPasswordforfee() {
    var formData = $('#delete_fee_data').serialize();
    $.ajax({
        url: "/daily_report/fee_collection_details",
        method: "POST",
        data: formData,
        success: function (data) {
            if (data == 'Record Deleted Successfully.') {
                $('#confirm_password_fee_modal').modal('hide');
                $('#confirm_delete_message').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                $('#daily_report').DataTable().ajax.reload();
            } else {
                if ($('#confirm_error').is(':empty')) {
                    $('#confirm_error').append('<div class="alert alert-danger alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                }

            }
        }
    });
}

/************check password when fee collection delete data END ****************/


/************page reload after print fee receipt open in new window,collcet fee data START ****************/
function take_payment() {
    var payment_date = $('#take_date').val();
    if (payment_date) {
        setInterval(function () {
            window.location.href = "/fee/";
        }, 1000);
    }

}
/************page reload after print fee receipt open in new window,collcet fee data END ****************/


/************check password when fee collection delete data START ****************/


/************check for student transfer START ****************/
function checkfortransfer() {
    var formData = $('#transfer_student_dataform').serialize();
    $('#transfer_error').html('');
    $('#school_password').val('');
    $.ajax({
        url: "/student_transfer/confirm_transfer",
        method: "POST",
        data: formData,
        success: function (data) {
            if (data == 'Student Transfer Successfully.') {
                $('#student_transfer_modal').modal('hide');
                $('#transfer_student_message').append('<div class="alert alert-success alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                $('#student_transferdata').DataTable().ajax.reload();
            } else {
                if ($('#transfer_error').is(':empty')) {
                    $('#transfer_error').append('<div class="alert alert-danger alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                }

            }
        }
    });
}
/************check for student transfer END ****************/

/************fetch div standard START ****************/
function getTrannsferstd(event) {
    var transstudent_school= event.target.value;
    $("#transstudent_div").empty();
    $("#transstudent_div").append("<option value=''>Select</option>");
    $.ajax({
        url: "/student_transfer/get_std_value",
        method: "POST",
        data: {transstudent_school:transstudent_school},
        success: function (data) {

            var std_data = JSON.parse(data);

            $("#transstudent_standard").empty();
            $("#transstudent_standard").append("<option value=''>Select</option>");
            for (var i = 0; i < std_data.length; i++) {
                $("#transstudent_standard").append("<option value='" + std_data[i].id + "'>" + std_data[i].standard + "</option>");
            }
        }
    });
}
/************fetch div transfer END  ****************/

/************fetch div transfer START ****************/
function getTransferdiv(event) {
    var enterValue = event.target.value;
    var stdValue = enterValue;

    $.ajax({
        url: "/student_transfer/get_division_value",
        method: "POST",
        data: {standard: stdValue},
        success: function (data) {
            var std_data = JSON.parse(data);
            var div = std_data.division;
            if (div) {
                var len = div.length;
            }
            $("#transstudent_div").empty();
            $("#transstudent_div").append("<option value=''>Select</option>");
            for (var i = 0; i < len; i++) {
                $("#transstudent_div").append("<option value='" + div[i] + "'>" + div[i] + "</option>");
            }
        }
    });
}
/************fetch div transfer END  ****************/

/************fetch fee structure START ****************/
function getFeeStructure(event) {

    var id_School = localStorage.getItem('School_id');
    $('#sm_fee').remove();
    $('#com_fee').remove();
    $('#sp_fee').remove();
    $('#ins_fee').remove();
    $('#ex_fee').remove();
    $('#en_fee').remove();
    var role=$('#session_role').val();
    if(role == 'admin'){
        var enterValue = id_School;
    }else{
        var enterValue = event.target.value;
    }
    $.ajax({
        url: "/standard/get_fee_structure",
        method: "POST",
        data: {school_id: enterValue},
        success: function (data) {
            var fee_data = JSON.parse(data);
            var fee_id=fee_data[0].feestructure_flag;

            if(fee_id=='1'){
                fee_flag=true;
                $('#otherfee').hide();
                $('#total_fee').remove();
                $("#fee_structure").append("<div class='col-md-6' id='sm_fee'><div class='form-group'><label>Smart Class Fee<span class='error'>*</span></label><div class='controls'><input type='text' name='standard_add_smart_fee' id='standard_add_smart_fee' onkeyup='smartfee();' class='form-control' placeholder='Enter Smart Class Fee'/><span class='error'></span></div></div></div><div class='col-md-6' id='com_fee'><div class='form-group'><label>Computer Fee<span class='error'>*</span></label><div class='controls'><input type='text' name='standard_add_computer_fee' id='standard_add_computer_fee' onkeyup='computerfee();' class='form-control' placeholder='Enter Computer Fee'/><span class='error'></span></div></div></div><div class='col-md-6' id='sp_fee'><div class='form-group'><label>Sports Fee<span class='error'>*</span></label><div class='controls'><input type='text' name='standard_add_sports_fee' id='standard_add_sports_fee' onkeyup='sportsfee();' class='form-control' placeholder='Enter Sports Fee'/><span class='error'></span></div></div></div><div class='col-md-6' id='ins_fee'><div class='form-group'><label>Insurance Fee<span class='error'>*</span></label><div class='controls'><input type='text' name='standard_add_insurance_fee' id='standard_add_insurance_fee' onkeyup='insurancefee();' class='form-control' placeholder='Enter Insurance Fee'/><span class='error'></span></div></div></div><div class='col-md-6' id='ex_fee'><div class='form-group'><label>Exam Fee<span class='error'>*</span></label><div class=\"controls\"><input type='text' name='standard_add_exam_fee' id='standard_add_exam_fee' onkeyup='examfee();' class='form-control' placeholder='Enter Exam Fee'/><span class='error'></span></div></div></div><div class='col-md-6' id='en_fee'><div class='form-group'><label>Enroll Fee<span class='error'>*</span></label><div class='controls'><input type='text' name='standard_add_enroll_fee' id='standard_add_enroll_fee' onkeyup='enrollfee();' class='form-control' placeholder='Enter Enroll Fee'/><span class='error'></span></div></div></div>");
            }else if(fee_id=='4'){
                totalfee_flag = true;
                fee_flag=false;
                $('#otherfee').hide();
                $('#admission_fee').hide();
                $('#tution_Fee').hide();
                $('#term_Fee').hide();
                $('#sm_fee').remove();
                $('#com_fee').remove();
                $('#sp_fee').remove();
                $('#ins_fee').remove();
                $('#ex_fee').remove();
                $('#en_fee').remove();
                $("#fee_structure").append("<div class='col-md-6' id='total_fee'><div class='form-group'><label>Total Fee<span class='error'>*</span></label><div class='controls'><input type='text' name='standard_add_total_fee' id='standard_add_total_fee' onkeyup='totalfee();' class='form-control' placeholder='Enter Total Fee'/><span class='error'></span></div></div></div>");
            }else{
                $('#otherfee').show();
                $('#admission_fee').show();
                $('#tution_Fee').show();
                $('#term_Fee').show();
                fee_flag=false;
                totalfee_flag=false;
                $('#total_fee').remove();
                $('#sm_fee').remove();
                $('#com_fee').remove();
                $('#sp_fee').remove();
                $('#ins_fee').remove();
                $('#ex_fee').remove();
                $('#en_fee').remove();
            }
        }
    });
}
/************fetch fee structure START ****************/

/************collect fee for Pre Primary fee structure START ****************/
function getPreFeeDuration(isSelectbox = false, isConcession = false) {
    if (isSelectbox) {
        $('#concession').val('');
        $('#discount').val('');
        $('#total_paid_fee').val('');
        $('#paid_fee_error').text('');
    }
    if (isConcession) {
        $('#total_paid_fee').val('');
        $('#paid_fee_error').text('');
    }

    var concession = $('#concession').val();
    var discount=$('#discount').val();
    var pending_fee = $('#pending_fee').val();
    var admission_year = $('#admission_year').val();


    var current_year = new Date().getFullYear();
    var standard =$('#student_feestdandard').val();

    if (Number(admission_year) == Number(current_year) || standard == '1' || standard == 'I' || standard == '9') {
        $('#student_status').val('new');
    } else {
        $('#student_status').val('old');
    }

    if ($('#student_status').val() == 'new') {
        $('#old_student').attr('disabled', true);
    } else {
        $('#new_student').attr('disabled', true);
    }
    var student_status = $('#student_status :selected').val();
    var start_month = $('#start_fee_duration :selected').val();
    var start_month_name = $('#start_fee_duration :selected').text();

    var end_month = $('#end_fee_duration :selected').val();
    var end_month_name = $('#end_fee_duration :selected').text();

    var monthNames = ["June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May"];
    var monthValues = ["6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5"];

    var start = monthNames.indexOf(start_month_name);
    var end = monthNames.indexOf(end_month_name);

    if (start_month && end_month) {
        var duration = (parseInt(end) - parseInt(start)) + 1;
        $('#fee_duration').val(duration);
    } else {
        var duration = '1';
        $('#fee_duration').val(duration);
    }


    var admission_fee = $('#admission_fee').val();
    var tuition_fee = $('#tuition_fee').val();
    var term_fee = $('#term_fee').val();
    var other_fee = $('#other_fee').val();

    if (student_status == 'new') {
        var total = Number(admission_fee) + (Number(tuition_fee) * 12) + (Number(term_fee) * 2) + (Number(other_fee));
        $('#total_fee').val(total);
    } else {
        var total = (Number(tuition_fee) * 12) + (Number(term_fee) * 2) + (Number(other_fee));
        $('#total_fee').val(total);
    }

    var total_fee = $('#total_fee').val();


    if (start_month == '6' && !end_month) {
        $('#paid_fee_amount').html('');

        if (student_status == 'new') {
            if (concession > '0' && Number(concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                if (discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession) - Number(discount);
                }else{
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession);
                }
                $('#paid_admission_fee').val(admission_fee);
            }else{
                if (discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(discount);
                }else{
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee));
                }
                $('#paid_admission_fee').val(admission_fee);
            }
        } else {
            if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)-Number(discount);
                }else{
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession);
                }
            }else{
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(discount);
                }else{
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee));
                }
            }

        }

        if (student_status == 'new') {
            if ( Number(concession) > (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                $('#concession_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            }else{
                $('#concession_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

        } else {
            if ( Number(concession) > (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                $('#concession_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            }else{
                $('#concession_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

        }

        if (student_status == 'new') {
            if ( Number(discount) > (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                $('#discount_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            }else{
                $('#discount_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

        } else {
            if ( Number(discount) > (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                $('#discount_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            }else{
                $('#discount_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

        }

        if (pending_fee > 0) {
            if (student_status == 'new') {
                if (concession > '0' && Number(concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession +'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }else{
                    if (discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee +'</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }

            } else {
                if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }else{
                    if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee +'</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }
        }else{
            if (student_status == 'new') {
                if (concession > '0' && Number(concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }else{
                    if (discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +'</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }

            } else {
                if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + concession +'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }else{
                    if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +'</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }
        }


        $('#paid_tution_fee').val(tuition_fee);
        $('#paid_term_fee').val(term_fee);
        $('#paid_other_fee').val(other_fee);
        $('#paid_amount').val(paid_amount);
        $('#fee_period').val(start_month_name);
    } else if (start_month == '12' && !end_month) {
        $('#paid_fee_amount').html('');

        if (concession > '0' && Number(concession) <=(Number(tuition_fee) + Number(term_fee)  + Number(pending_fee))) {
            if (discount > '0' && Number(discount) <=(Number(tuition_fee) + Number(term_fee)  + Number(pending_fee))-Number(concession)) {
                var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(pending_fee)) - Number(concession)-Number(discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(pending_fee)) - Number(concession);
            }
        } else {
            if (discount > '0' && Number(discount) <=(Number(tuition_fee) + Number(term_fee)  + Number(pending_fee))) {
                var paid_amount = (Number(tuition_fee) + Number(term_fee)  + Number(pending_fee))-Number(discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(term_fee)  + Number(pending_fee));
            }
        }

        if ( Number(concession) > (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))) {
            $('#concession_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#concession_error').text('');
            document.getElementById("take_fee").disabled = false;
        }
        if ( Number(discount) > (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))-Number(concession)) {
            $('#discount_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#discount_error').text('');
            document.getElementById("take_fee").disabled = false;
        }
        if (pending_fee > 0) {
            if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))) {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))-Number(concession)) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        } else {
            if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))) {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))-Number(concession)) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        }


        $('#paid_tution_fee').val(tuition_fee);
        $('#paid_term_fee').val(term_fee);
        $('#paid_other_fee').val('0');
        $('#paid_amount').val(paid_amount);
        $('#fee_period').val(start_month_name);
    } else if (start_month && !end_month) {
        $('#paid_fee_amount').html('');
        if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(pending_fee))) {
            if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee))-Number(concession)) {
                var paid_amount = (Number(tuition_fee) + Number(pending_fee)) - Number(concession)-Number(discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(pending_fee)) - Number(concession);
            }
        } else {
            if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee))) {
                var paid_amount = Number(tuition_fee) + Number(pending_fee)-Number(discount);
            }else{
                var paid_amount = Number(tuition_fee) + Number(pending_fee);
            }
        }

        if (Number(concession) > (Number(tuition_fee) + Number(pending_fee))) {
            $('#concession_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#concession_error').text('');
            document.getElementById("take_fee").disabled = false;
        }

        if (Number(discount) > (Number(tuition_fee) + Number(pending_fee))-Number(concession)) {
            $('#discount_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#discount_error').text('');
            document.getElementById("take_fee").disabled = false;
        }

        if (pending_fee > 0) {
            if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(pending_fee))) {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee))-Number(concession)) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee))) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + pending_fee +'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        } else {
            if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(pending_fee))) {
                if(discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee))-Number(concession)) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if(discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee))) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        }

        $('#paid_tution_fee').val(tuition_fee);
        $('#paid_amount').val(paid_amount);
        $('#fee_period').val(start_month_name);
    }
    if (start_month && end_month) {
        var i;
        var total_month = [];

        for (i = parseInt(start); i <= end; i++) {
            var total = monthValues[i];
            total_month.push(parseInt(total));
        }

        if ($.inArray(6, total_month) > -1 && $.inArray(12, total_month) == -1) {
            $('#paid_fee_amount').html('');

            if (student_status == 'new') {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)-Number(discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession);
                    }
                    $('#paid_admission_fee').val(admission_fee);
                }else{
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee));
                    }
                    $('#paid_admission_fee').val(admission_fee);
                }

            } else {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession)-Number(discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee)) - Number(concession);
                    }
                }else{
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee));
                    }
                }

            }
            if (student_status == 'new') {
                if ( Number(concession) > (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    $('#concession_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#concession_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            } else {
                if (Number(concession) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    $('#concession_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#concession_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            }

            if (student_status == 'new') {
                if ( Number(discount) > (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                    $('#discount_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#discount_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            } else {
                if (Number(discount) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                    $('#discount_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#discount_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            }
            if (pending_fee > 0) {
                if (student_status == 'new') {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession +'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                }
            }else{
                if (student_status == 'new') {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                }
            }

            $('#paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#paid_term_fee').val(term_fee);
            $('#paid_other_fee').val(other_fee);
            $('#paid_amount').val(paid_amount);
            $('#fee_period').val(start_month_name + ' - ' + end_month_name);
        } else if ($.inArray(12, total_month) > -1 && $.inArray(6, total_month) == -1) {
            $('#paid_fee_amount').html('');

            if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))) {
                if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))-Number(concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(pending_fee)) - Number(concession)-Number(discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(pending_fee)) - Number(concession);
                }
            } else {
                if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))-Number(discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee));
                }

            }

            if (Number(concession) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(pending_fee))) {
                $('#concession_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            } else {
                $('#concession_error').text('');
                document.getElementById("take_fee").disabled = false;
            }
            if (Number(discount) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(pending_fee))-Number(concession)) {
                $('#discount_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            } else {
                $('#discount_error').text('');
                document.getElementById("take_fee").disabled = false;
            }
            if (pending_fee > 0) {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession +'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee  + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee  + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }

            $('#paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#paid_term_fee').val(term_fee);
            $('#paid_other_fee').val('0');
            $('#paid_amount').val(paid_amount);
            $('#fee_period').val(start_month_name + ' - ' + end_month_name);
        } else if ($.inArray(6, total_month) > -1 && $.inArray(12, total_month) > -1) {
            $('#paid_fee_amount').html('');

            if (student_status == 'new') {
                if (concession > '0' && Number(concession) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee)) - Number(concession)-Number(discount);
                    }else{
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee)) - Number(concession);
                    }
                    $('#paid_admission_fee').val(admission_fee);
                }else{
                    if (discount > '0' && Number(discount) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(discount);
                    }else{
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee));
                    }
                    $('#paid_admission_fee').val(admission_fee);
                }

            } else {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee))-Number(concession)) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee)) - Number(concession)-Number(discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee)) - Number(concession);
                    }
                }else{
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee))-Number(concession)) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee))-Number(discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee));
                    }
                }

            }
            if (student_status == 'new') {
                if (Number(concession) >  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(other_fee) + Number(pending_fee))) {
                    $('#concession_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#concession_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            } else {
                if (Number(concession) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee))) {
                    $('#concession_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#concession_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            }
            if (student_status == 'new') {
                if (Number(discount) >  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                    $('#discount_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#discount_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            } else {
                if (Number(discount) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee))-Number(concession)) {
                    $('#discount_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#discount_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            }
            if (pending_fee > 0) {
                if (student_status == 'new') {
                    if (concession > '0' && Number(concession) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Pending Fee : ' + pending_fee +'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                }
            }else{
                if (student_status == 'new') {
                    if (concession > '0' && Number(concession) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee)) +  '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee)) +  '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee)) +  '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee)) +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) +  '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) +  '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee)) + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee)) +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                }
            }

            $('#paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#paid_term_fee').val(Number((term_fee) * 2));
            $('#paid_other_fee').val(Number((other_fee) ));
            $('#paid_amount').val(paid_amount);
            $('#fee_period').val(start_month_name + ' - ' + end_month_name);
        } else {
            $('#paid_fee_amount').html('');

            if (concession > '0' && Number(concession) <=(Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                if (discount > '0' && Number(discount) <=(Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))-Number(concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee)) - Number(concession)-Number(discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee)) - Number(concession);
                }
            } else {
                if (discount > '0' && Number(discount) <=(Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))-Number(discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee));
                }
            }

            if (Number(concession) > (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                $('#concession_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            } else {
                $('#concession_error').text('');
                document.getElementById("take_fee").disabled = false;
            }
            if (Number(discount) > (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))-Number(concession)) {
                $('#discount_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            } else {
                $('#discount_error').text('');
                document.getElementById("take_fee").disabled = false;
            }
            if (pending_fee > 0) {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession +'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (concession > '0' && Number(concession) <=(Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Concession :' + concession +'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }

            $('#paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#paid_amount').val(paid_amount);
            $('#fee_period').val(start_month_name + ' - ' + end_month_name);
        }
    }
    var takedate=$('#take_date').val();
    if ($('#concession_error').is(':empty') && $('#paid_fee_error').is(':empty') && $('#discount_error').is(':empty') ) {
        document.getElementById("take_fee").disabled = false;
    } else {
        document.getElementById("take_fee").disabled = true;
    }
}
/************collect fee for Pre Primary fee structure END ****************/


/************collect fee for  Secondary fee structure START ****************/
function getSecondaryFeeDuration(isSelectbox = false, isConcession = false) {
    if (isSelectbox) {
        $('#concession').val('');
        $('#discount').val('');
        $('#total_paid_fee').val('');
        $('#paid_fee_error').text('');
    }
    if (isConcession) {
        $('#total_paid_fee').val('');
        $('#paid_fee_error').text('');
    }

    var concession = $('#concession').val();
    var discount = $('#discount').val();
    var pending_fee = $('#pending_fee').val();
    var admission_year = $('#admission_year').val();


    var current_year = new Date().getFullYear();
    var standard =$('#student_feestdandard').val();

    if (Number(admission_year) == Number(current_year) || Number(standard) == '1' || standard == 'I' || standard == '9') {
        $('#student_status').val('new');
    } else {
        $('#student_status').val('old');
    }

    if ($('#student_status').val() == 'new') {
        $('#old_student').attr('disabled', true);
    } else {
        $('#new_student').attr('disabled', true);
    }
    var student_status = $('#student_status :selected').val();

    var start_month = $('#start_fee_duration :selected').val();
    var start_month_name = $('#start_fee_duration :selected').text();

    var end_month = $('#end_fee_duration :selected').val();
    var end_month_name = $('#end_fee_duration :selected').text();

    var monthNames = ["June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May"];
    var monthValues = ["6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5"];

    var start = monthNames.indexOf(start_month_name);
    var end = monthNames.indexOf(end_month_name);

    if (start_month && end_month) {
        var duration = (parseInt(end) - parseInt(start)) + 1;
        $('#fee_duration').val(duration);
    } else {
        var duration = '1';
        $('#fee_duration').val(duration);
    }


    var admission_fee = $('#admission_fee').val();
    var tuition_fee = $('#tuition_fee').val();
    var term_fee = $('#term_fee').val();
    $('#other_fee').val('0');
    var smart_fee=$('#smart_fee').val();
    var computer_fee=$('#computer_fee').val();
    var sports_fee=$('#sports_fee').val();
    var insurance_fee=$('#insurance_fee').val();
    var exam_fee=$('#exam_fee').val();
    var enroll_fee=$('#enroll_fee').val();

    if (student_status == 'new') {
        var total = Number(admission_fee) + (Number(tuition_fee) * 12) + (Number(term_fee) * 2) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee);
        $('#total_fee').val(total);
    } else {
        var total = (Number(tuition_fee) * 12) + (Number(term_fee) * 2) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee);
        $('#total_fee').val(total);
    }

    var total_fee = $('#total_fee').val();


    if (start_month == '6' && !end_month) {
        $('#paid_fee_amount').html('');

        if (student_status == 'new') {
            if (concession > '0' && Number(concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                if (discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(pending_fee)) - Number(concession)) {
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(pending_fee)) - Number(concession) - Number(discount);
                }else{
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee)) - Number(concession);
                }
                $('#paid_admission_fee').val(admission_fee);
            }else{
                if (discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(pending_fee))) {
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(pending_fee))-Number(discount);
                }else{
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee));
                }
                $('#paid_admission_fee').val(admission_fee);
            }

        } else {
            if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                if (discount > '0' && Number(discount) <=  (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(pending_fee)) - Number(concession)) {
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee)) - Number(concession) - Number(discount);
                }else{
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee)) - Number(concession);
                }
            }else{
                if (discount > '0' && Number(discount) <=  (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee)) ) {
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee)) - Number(discount);
                }else{
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee));
                }
            }

        }

        if (student_status == 'new') {
            if ( Number(concession) > (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(pending_fee))) {
                $('#concession_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            }else{
                $('#concession_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

        } else {
            if ( Number(concession) > (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                $('#concession_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            }else{
                $('#concession_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

        }
        if (student_status == 'new') {
            if ( Number(discount) > (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(pending_fee))-Number(concession)) {
                $('#discount_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            }else{
                $('#discount_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

        } else {
            if ( Number(discount) > (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))-Number(concession)) {
                $('#discount_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            }else{
                $('#discount_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

        }
        if (pending_fee > 0) {
            if (student_status == 'new') {
                if (concession > '0' && Number(concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee)) -Number(concession)) {
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee  +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +'</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }else{
                    if (concession > '0' && Number(concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Pending Fee : ' + pending_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }

            } else {
                if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee)+ Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee)+ Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession  +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }else{
                    if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee)+ Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee  +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Pending Fee : ' + pending_fee +'</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee  +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Pending Fee : ' + pending_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }
        }else{
            if (student_status == 'new') {
                if (concession > '0' && Number(concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }else{
                    if (discount > '0' && Number(discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee +'</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }

            } else {
                if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee)+ Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee)+ Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Concession :' + concession +'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee  +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }else{
                    if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee)+ Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee  +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +'</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee  +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }
        }


        $('#paid_tution_fee').val(tuition_fee);
        $('#paid_term_fee').val(term_fee);
        $('#paid_smart_fee').val(smart_fee);
        $('#paid_computer_fee').val(computer_fee);
        $('#paid_sports_fee').val(sports_fee);
        $('#paid_exam_fee').val(exam_fee);
        $('#paid_insurance_fee').val(insurance_fee);
        $('#paid_enroll_fee').val(enroll_fee);
        $('#paid_amount').val(paid_amount);
        $('#fee_period').val(start_month_name);
    } else if (start_month == '12' && !end_month) {
        $('#paid_fee_amount').html('');

        if (concession > '0' && Number(concession) <=(Number(tuition_fee) + Number(term_fee)  + Number(pending_fee))) {
            if (discount > '0' && Number(discount) <=(Number(tuition_fee) + Number(term_fee)  + Number(pending_fee)-Number(concession))) {
                var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(pending_fee)) - Number(concession) - Number(discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(pending_fee)) - Number(concession);
            }
        } else {
            if (discount > '0' && Number(discount) <=(Number(tuition_fee) + Number(term_fee)  + Number(pending_fee))) {
                var paid_amount = (Number(tuition_fee) + Number(term_fee)  + Number(pending_fee))- Number(discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(term_fee)  + Number(pending_fee));
            }
        }

        if ( Number(concession) > (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))) {
            $('#concession_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#concession_error').text('');
            document.getElementById("take_fee").disabled = false;
        }

        if ( Number(discount) > (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))) {
            $('#discount_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#discount_error').text('');
            document.getElementById("take_fee").disabled = false;
        }

        if (pending_fee > 0) {
            if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))) {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))-Number(concession)) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession +'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');

                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        } else {
            if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))) {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))-Number(concession)) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(term_fee) + Number(pending_fee))) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        }


        $('#paid_tution_fee').val(tuition_fee);
        $('#paid_term_fee').val(term_fee);
        $('#paid_smart_fee').val('0');
        $('#paid_computer_fee').val('0');
        $('#paid_sports_fee').val('0');
        $('#paid_exam_fee').val('0');
        $('#paid_insurance_fee').val('0');
        $('#paid_amount').val(paid_amount);
        $('#fee_period').val(start_month_name);
    } else if (start_month && !end_month) {
        $('#paid_fee_amount').html('');
        if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(pending_fee))) {
            if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee))-Number(concession)) {
                var paid_amount = (Number(tuition_fee) + Number(pending_fee)) - Number(concession) -Number(discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(pending_fee)) - Number(concession);
            }
        } else {
            if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee))-Number(concession)) {
                var paid_amount = Number(tuition_fee) + Number(pending_fee) -Number(discount);
            }else{
                var paid_amount = Number(tuition_fee) + Number(pending_fee);
            }
        }

        if (Number(concession) > (Number(tuition_fee) + Number(pending_fee))) {
            $('#concession_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#concession_error').text('');
            document.getElementById("take_fee").disabled = false;
        }

        if (Number(discount) > (Number(tuition_fee) + Number(pending_fee))) {
            $('#discount_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#discount_error').text('');
            document.getElementById("take_fee").disabled = false;
        }

        if (pending_fee > 0) {
            if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(pending_fee))) {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee))-Number(concession)) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession+'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee))) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        } else {
            if (concession > '0' && Number(concession) <= (Number(tuition_fee) + Number(pending_fee))) {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee))-Number(concession)) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (discount > '0' && Number(discount) <= (Number(tuition_fee) + Number(pending_fee))) {
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        }

        $('#paid_tution_fee').val(tuition_fee);
        $('#paid_amount').val(paid_amount);
        $('#fee_period').val(start_month_name);
    }
    if (start_month && end_month) {
        var i;
        var total_month = [];

        for (i = parseInt(start); i <= end; i++) {
            var total = monthValues[i];
            total_month.push(parseInt(total));
        }

        if ($.inArray(6, total_month) > -1 && $.inArray(12, total_month) == -1) {
            $('#paid_fee_amount').html('');

            if (student_status == 'new') {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(pending_fee))-Number(concession)) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee)) - Number(concession)- Number(discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee)) - Number(concession);
                    }
                    $('#paid_admission_fee').val(admission_fee);
                }else{
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))- Number(discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee));
                    }
                    $('#paid_admission_fee').val(admission_fee);
                }

            } else {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))-Number(concession)) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee)) - Number(concession) - Number(discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee)) - Number(concession);
                    }
                }else{
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))-Number(discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee));
                    }
                }

            }
            if (student_status == 'new') {
                if ( Number(concession) > (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                    $('#concession_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#concession_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            } else {
                if (Number(concession) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee)+ Number(pending_fee))) {
                    $('#concession_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#concession_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            }
            if (student_status == 'new') {
                if ( Number(discount) > (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))-Number(concession)) {
                    $('#discount_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#discount_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            } else {
                if (Number(discount) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee)+ Number(pending_fee))-Number(concession)) {
                    $('#discount_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#discount_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            }
            if (pending_fee > 0) {
                if (student_status == 'new') {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession+'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee  +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee  +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee  +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                    }

                }
            }else{
                if (student_status == 'new') {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee  +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee +  '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee +  '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee  +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee +  '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(enroll_fee)+Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) + Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee +  '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee +  '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(enroll_fee)+Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) + Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee  +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                }
            }

            $('#paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#paid_term_fee').val(term_fee);
            $('#paid_smart_fee').val(smart_fee);
            $('#paid_computer_fee').val(computer_fee);
            $('#paid_sports_fee').val(sports_fee);
            $('#paid_exam_fee').val(exam_fee);
            $('#paid_insurance_fee').val(insurance_fee);
            $('#paid_enroll_fee').val(enroll_fee);
            $('#paid_amount').val(paid_amount);
            $('#fee_period').val(start_month_name + ' - ' + end_month_name);
        } else if ($.inArray(12, total_month) > -1 && $.inArray(6, total_month) == -1) {
            $('#paid_fee_amount').html('');

            if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))) {
                if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))-Number(concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(pending_fee)) - Number(concession) -Number(discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(pending_fee)) - Number(concession);
                }
            } else {
                if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))-Number(discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee));
                }
            }

            if (Number(concession) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(pending_fee))) {
                $('#concession_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            } else {
                $('#concession_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

            if (Number(discount) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(pending_fee))-Number(concession)) {
                $('#discount_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            } else {
                $('#discount_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

            if (pending_fee > 0) {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee  + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee  + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }

            $('#paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#paid_term_fee').val(term_fee);
            $('#paid_amount').val(paid_amount);
            $('#fee_period').val(start_month_name + ' - ' + end_month_name);
        } else if ($.inArray(6, total_month) > -1 && $.inArray(12, total_month) > -1) {
            $('#paid_fee_amount').html('');

            if (student_status == 'new') {
                if (concession > '0' && Number(concession) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))-Number(concession)) {
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee)) - Number(concession) -Number(discount);
                    }else{
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee)) - Number(concession);
                    }
                    $('#paid_admission_fee').val(admission_fee);
                }else{
                    if (discount > '0' && Number(discount) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(pending_fee))) {
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee)) - Number(discount);
                    }else{
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(pending_fee));
                    }
                    $('#paid_admission_fee').val(admission_fee);
                }

            } else {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)  +Number(enroll_fee)+ Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)  +Number(enroll_fee)+ Number(pending_fee))-Number(concession)) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)  +Number(enroll_fee)+ Number(pending_fee)) - Number(concession) - Number(discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)  +Number(enroll_fee)+ Number(pending_fee)) - Number(concession);
                    }
                }else{
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)  +Number(enroll_fee)+ Number(pending_fee))) {
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(pending_fee))-Number(discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(pending_fee));
                    }
                }

            }
            if (student_status == 'new') {
                if (Number(concession) >  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                    $('#concession_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#concession_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            } else {
                if (Number(concession) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)  +Number(enroll_fee)+ Number(pending_fee))) {
                    $('#concession_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#concession_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            }

            if (student_status == 'new') {
                if (Number(discount) >  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee)) - Number(concession)) {
                    $('#discount_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#discount_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            } else {
                if (Number(discount) > (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(pending_fee))-Number(concession)) {
                    $('#discount_error').text('Please enter value less than paid amount');
                    document.getElementById("take_fee").disabled = true;
                }else{
                    $('#discount_error').text('');
                    document.getElementById("take_fee").disabled = false;
                }

            }
            if (pending_fee > 0) {
                if (student_status == 'new') {
                    if (concession > '0' && Number(concession) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Enroll Fee : ' + enroll_fee  +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession +'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Enroll Fee : ' + enroll_fee  +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Enroll Fee : ' + enroll_fee  +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +'</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Enroll Fee : ' + enroll_fee  +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +'</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Enroll Fee : ' + enroll_fee  +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee +'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Enroll Fee : ' + enroll_fee  +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                }
            }else{
                if (student_status == 'new') {
                    if (concession > '0' && Number(concession) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)  +Number(enroll_fee)+ Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +'</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee +  '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +'</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee +  '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <=  (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)  +Number(enroll_fee)+ Number(pending_fee))) {
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +'</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee +  '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +'</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))) {
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Enroll Fee : ' + enroll_fee  +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee +  '</span><br><span>Concession :' + concession +'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Enroll Fee : ' + enroll_fee  +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee +  '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(pending_fee))-Number(concession)) {
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +'</span><br><span>Enroll Fee : ' + enroll_fee  + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee+ '</span><br><span>Discount :' + discount +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Enroll Fee : ' + enroll_fee  +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee+  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                }
            }

            $('#paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#paid_term_fee').val(Number((term_fee) * 2));
            $('#paid_smart_fee').val(smart_fee);
            $('#paid_computer_fee').val(computer_fee);
            $('#paid_sports_fee').val(sports_fee);
            $('#paid_exam_fee').val(exam_fee);
            $('#paid_insurance_fee').val(insurance_fee);
            $('#paid_enroll_fee').val(enroll_fee);
            $('#paid_amount').val(paid_amount);
            $('#fee_period').val(start_month_name + ' - ' + end_month_name);
        } else {
            $('#paid_fee_amount').html('');

            if (concession > '0' && Number(concession) <=(Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                if (discount > '0' && Number(discount) <=(Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))-Number(concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee)) - Number(concession)-Number(discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee)) - Number(concession);
                }
            } else {
                if (discount > '0' && Number(discount) <=(Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))-Number(discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee));
                }
            }

            if (Number(concession) > (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                $('#concession_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            } else {
                $('#concession_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

            if (Number(discount) > (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                $('#discount_error').text('Please enter value less than paid amount');
                document.getElementById("take_fee").disabled = true;
            } else {
                $('#discount_error').text('');
                document.getElementById("take_fee").disabled = false;
            }

            if (pending_fee > 0) {
                if (concession > '0' && Number(concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))-Number(concession)) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (discount > '0' && Number(discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (concession > '0' && Number(concession) <=(Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                    if (discount > '0' && Number(discount) <=(Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Concession :' + concession +'</span><br><span>Discount :' + discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Concession :' + concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (discount > '0' && Number(discount) <=(Number((tuition_fee) * parseInt(duration)) + Number(pending_fee))) {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Discount :' + discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else {
                        $('#paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }

            $('#paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#paid_amount').val(paid_amount);
            $('#fee_period').val(start_month_name + ' - ' + end_month_name);
        }
    }
    var takedate=$('#take_date').val();
    if ($('#concession_error').is(':empty') && $('#paid_fee_error').is(':empty')&& $('#discount_error').is(':empty') ) {
        document.getElementById("take_fee").disabled = false;
    } else {
        document.getElementById("take_fee").disabled = true;
    }
}
/************collect fee for secondary fee structure END ****************/


/************get pre primary fee structure for edit fee START ****************/
function editPrePrimaryFeeDuration(isSelect = false) {
    if (isSelect) {
        $('#edit_concession').val('');
        $('#edit_discount').val('');
        $('#edit_totalpaid_fee').val('');
        $('#edit_paid_fee_error').text('');
    }

    var edit_concession = $('#edit_concession').val();
    var edit_discount=$('#edit_discount').val();
    var edit_pending_fee = $('#edit_pending_fee').val();
    var admission_year = $('#edit_year').val();


    var current_year = new Date().getFullYear();
    document.getElementById("edit_take_fee").disabled = false;
    var editstandard =$('#edit_studentfeestd').val();

    if (Number(admission_year) == Number(current_year) || editstandard == '1' || editstandard == 'I' || editstandard == '9') {
        $('#edit_student_status').val('new');
    } else {
        $('#edit_student_status').val('old');
    }


    if ($('#edit_student_status').val() == 'new') {
        $('#edit_old_student').attr('disabled', true);
    } else {
        $('#edit_new_student').attr('disabled', true);
    }
    var student_status = $('#edit_student_status :selected').val();
    var start_month = $('#start_duration :selected').val();
    var start_month_name = $('#start_duration :selected').text();

    var end_month = $('#end_duration :selected').val();
    var end_month_name = $('#end_duration :selected').text();

    var monthNames = ["June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May"];
    var monthValues = ["6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5"];

    var start = monthNames.indexOf(start_month_name);
    var end = monthNames.indexOf(end_month_name);

    if (start_month && end_month) {
        var duration = (parseInt(end) - parseInt(start)) + 1;
        $('#edit_fee_duration').val(duration);
    } else {
        var duration = '1';
        $('#edit_fee_duration').val(duration);
    }


    var admission_fee = $('#edit_admission_fee').val();
    var tuition_fee = $('#edit_tuition_fee').val();
    var term_fee = $('#edit_term_fee').val();
    var other_fee = $('#edit_other_fee').val();

    if (student_status == 'new') {
        var total = Number(admission_fee) + (Number(tuition_fee) * 12) + (Number(term_fee) * 2) + (Number(other_fee)) + Number(edit_pending_fee);
        $('#edit_total_fee').val(total);
    } else {
        var total = (Number(tuition_fee) * 12) + Number(edit_pending_fee) + (Number(term_fee) * 2) + (Number(other_fee));
        $('#edit_total_fee').val(total);
    }

    var total_fee = $('#edit_total_fee').val();


    if (start_month == '6' && !end_month) {

        $('#edit_paid_fee_amount').html('');

        if (student_status == 'new') {
            if (Number(edit_concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                if (Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                }else{
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession);
                }
                $('#edit_paid_admission_fee').val(admission_fee);
            } else {
                if (Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_discount);
                }else{
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee));
                }
                $('#edit_paid_admission_fee').val(admission_fee);
            }
        } else {
            if (Number(edit_concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                if (Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                }else{
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession);
                }
            } else {
                if (Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_discount);
                }else{
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee));
                }
            }
        }

        if (student_status == 'new') {
            if (Number(edit_concession) > (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                $('#edit_concession_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_concession_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
        } else {
            if (Number(edit_concession) > (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                $('#edit_concession_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_concession_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
        }
        if (student_status == 'new') {
            if (Number(edit_discount) > (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                $('#edit_discount_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_discount_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
        } else {
            if (Number(edit_discount) > (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                $('#edit_discount_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_discount_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
        }
        if (edit_pending_fee > 0) {
            if (student_status == 'new') {
                if (Number(edit_concession) > 0 && Number(edit_concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (Number(edit_concession) > 0 && Number(edit_concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }
        }else{
            if (student_status == 'new') {
                if (Number(edit_concession) > 0 && Number(edit_concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee))) {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + Number(other_fee))) {
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (Number(edit_concession) > 0 && Number(edit_concession) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }
        }

        $('#edit_paid_tution_fee').val(tuition_fee);
        $('#edit_paid_term_fee').val(term_fee);
        $('#edit_paid_other_fee').val(other_fee);
        $('#edit_paid_amount').val(paid_amount);
        $('#edit_fee_period').val(start_month_name);
    } else if (start_month == '12' && !end_month) {
        $('#edit_paid_fee_amount').html('');

        if (Number(edit_concession) <= (Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee))) {
            if (Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                var paid_amount = (Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee)) - Number(edit_concession) -Number(edit_discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee)) - Number(edit_concession);
            }
        } else {
            if (Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee))) {
                var paid_amount = (Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee))-Number(edit_discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee));
            }
        }


        if (Number(edit_concession) >(Number(tuition_fee) + Number(term_fee) + Number(edit_pending_fee))) {
            $('#edit_concession_error').text('Please enter value less than paid amount');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_concession_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }

        if (Number(edit_discount) >(Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee))) {
            $('#edit_discount_error').text('Please enter value less than paid amount');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_discount_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }

        if (edit_pending_fee > 0) {
            if (edit_concession > '0' && Number(edit_concession) <= (Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee))) {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) +  Number(edit_pending_fee))) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        } else {
            if (edit_concession > '0' && Number(edit_concession) <= Number($('#edit_paid_amount').val())) {
                if (edit_discount > '0' && Number(edit_discount) <= Number($('#edit_paid_amount').val()-Number(edit_concession))) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee  + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <= Number($('#edit_paid_amount').val())) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span>Discount :' + edit_discount + +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        }
        $('#edit_paid_tution_fee').val(tuition_fee);
        $('#edit_paid_term_fee').val(term_fee);
        $('#edit_paid_other_fee').val('0');
        $('#edit_paid_amount').val(paid_amount);
        $('#edit_fee_period').val(start_month_name);
    } else if (start_month && !end_month) {

        $('#edit_paid_fee_amount').html('');
        if (Number(edit_concession) <= (Number(tuition_fee) + Number(edit_pending_fee))) {
            if (Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                var paid_amount = (Number(tuition_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(edit_pending_fee)) - Number(edit_concession);
            }
        } else {
            if (Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))) {
                var paid_amount = Number(tuition_fee) + Number(edit_pending_fee)-Number(edit_discount);
            }else{
                var paid_amount = Number(tuition_fee) + Number(edit_pending_fee);
            }
        }
        if (Number(edit_concession) >(Number(tuition_fee) + Number(edit_pending_fee))) {
            $('#edit_concession_error').text('Please enter value less than paid amount');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_concession_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }
        if (Number(edit_discount) >(Number(tuition_fee) + Number(edit_pending_fee))) {
            $('#edit_discount_error').text('Please enter value less than paid amount');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_discount_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }
        if (edit_pending_fee > 0) {
            if (edit_concession > '0' && Number(edit_concession) <= (Number(tuition_fee) + Number(edit_pending_fee))) {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount+ '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Discount :' + edit_discount+ '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        } else {
            if (edit_concession > '0' && Number(edit_concession) <= (Number(tuition_fee) + Number(edit_pending_fee))) {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount+'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Discount :' + edit_discount+'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        }
        $('#edit_paid_tution_fee').val(tuition_fee);
        $('#edit_paid_amount').val(paid_amount);
        $('#edit_fee_period').val(start_month_name);
    }
    if (start_month && end_month) {
        var i;
        var total_month = [];

        for (i = parseInt(start); i <= end; i++) {
            var total = monthValues[i];
            total_month.push(parseInt(total));
        }

        if ($.inArray(6, total_month) > -1 && $.inArray(12, total_month) == -1) {
            $('#edit_paid_fee_amount').html('');

            if (student_status == 'new') {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))){
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession);
                    }
                    $('#edit_paid_admission_fee').val(admission_fee);
                }else{
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee));
                    }
                    $('#edit_paid_admission_fee').val(admission_fee);
                }

            } else {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))){
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))-Number(edit_concession)){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) - Number(edit_concession);
                    }
                }else{
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) -Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee)) ;
                    }

                }
            }
            if (student_status == 'new') {
                if (Number(edit_concession) > (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    $('#edit_concession_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_concession_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            } else {
                if (Number(edit_concession) > (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))) {
                    $('#edit_concession_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_concession_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            }
            if (student_status == 'new') {
                if (Number(edit_discount) > (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_discount_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_discount_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            } else {
                if (Number(edit_discount) > (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))-Number(edit_concession)) {
                    $('#edit_discount_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_discount_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            }
            if (edit_pending_fee > 0) {
                if (student_status == 'new') {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))){
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Discount :' + edit_discount +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }
                }
            }else{
                if (student_status == 'new') {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))){
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) + Number(other_fee))){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +'</span><br><span>Discount :' + edit_discount +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Term Fee :  ' + other_fee +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }
                }
            }

            $('#edit_paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#edit_paid_term_fee').val(term_fee);
            $('#edit_paid_other_fee').val(other_fee);
            $('#edit_paid_amount').val(paid_amount);
            $('#edit_fee_period').val(start_month_name + ' - ' + end_month_name);
        } else if ($.inArray(12, total_month) > -1 && $.inArray(6, total_month) == -1) {
            $('#edit_paid_fee_amount').html('');

            if (edit_concession > '0' && Number(edit_concession) <=(Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(edit_pending_fee))) {
                if (edit_discount > '0' && Number(edit_discount) <=(Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +  Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +  Number(edit_pending_fee)) - Number(edit_concession);
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <=(Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +  Number(edit_pending_fee))-Number(edit_discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +  Number(edit_pending_fee));
                }
            }
            if (Number(edit_concession) >(Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +  Number(edit_pending_fee))) {
                $('#edit_concession_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_concession_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
            if (Number(edit_discount) >(Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +  Number(edit_pending_fee))-Number(edit_discount)) {
                $('#edit_discount_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_discount_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
            if (edit_pending_fee > 0) {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(edit_pending_fee))) {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(edit_pending_fee))) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(edit_pending_fee))) {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(edit_pending_fee))) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }

            $('#edit_paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#edit_paid_term_fee').val(term_fee);
            $('#edit_paid_other_fee').val('0');
            $('#edit_paid_amount').val(paid_amount);
            $('#edit_fee_period').val(start_month_name + ' - ' + end_month_name);
        } else if ($.inArray(6, total_month) > -1 && $.inArray(12, total_month) > -1) {

            $('#edit_paid_fee_amount').html('');

            if (student_status == 'new') {
                if (edit_concession > '0' && Number(edit_concession) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(other_fee) + Number(edit_pending_fee))) {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)+ Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                    }else{
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(edit_pending_fee)) - Number(edit_concession);
                    }
                    $('#edit_paid_admission_fee').val(admission_fee);
                }else{
                    if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) +  Number(edit_pending_fee))) {
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(other_fee) + Number(edit_pending_fee))-Number(edit_discount);
                    }else{
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(other_fee) + Number(edit_pending_fee));
                    }
                    $('#edit_paid_admission_fee').val(admission_fee);
                }

            } else {
                if (edit_concession > '0' && Number(edit_concession) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(other_fee) + Number(edit_pending_fee))){
                    if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(edit_pending_fee)) - Number(edit_concession);
                    }
                }else{
                    if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(edit_pending_fee))-Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(edit_pending_fee));
                    }
                }
            }

            if (student_status == 'new') {
                if (Number(edit_concession) > (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) + Number(edit_pending_fee))) {
                    $('#edit_concession_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_concession_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            } else {
                if (Number(edit_concession) > ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(edit_pending_fee))) {
                    $('#edit_concession_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_concession_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            }
            if (student_status == 'new') {
                if (Number(edit_discount) > (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_discount_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_discount_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            } else {
                if (Number(edit_discount) > ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_discount_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_discount_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            }
            if(edit_pending_fee > 0){
                if (student_status == 'new') {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(other_fee) + Number(edit_pending_fee))) {
                        if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(other_fee) + Number(edit_pending_fee))) {
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Discount :' + edit_discount +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee)) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (edit_concession > '0' && Number(edit_concession) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(edit_pending_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(edit_pending_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee)) + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(edit_pending_fee))){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }
                }
            }else{
                if (student_status == 'new') {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(other_fee) + Number(edit_pending_fee))) {
                        if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(other_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee)) + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  + Number(other_fee) + Number(edit_pending_fee))) {
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (edit_concession > '0' && Number(edit_concession) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(edit_pending_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee))){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee)  + Number(edit_pending_fee))){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Term Fee :  ' + Number((other_fee) ) + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }
                }
            }

            $('#edit_paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#edit_paid_term_fee').val(Number((term_fee) * 2));
            $('#edit_paid_other_fee').val(Number((other_fee) ));
            $('#edit_paid_amount').val(paid_amount);
            $('#edit_fee_period').val(start_month_name + ' - ' + end_month_name);
        } else {
            $('#edit_paid_fee_amount').html('');

            if (Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))) {
                if (Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee)) - Number(edit_concession);
                }
            } else {
                if (Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee));
                }
            }
            if (Number(edit_concession) >(Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))) {
                $('#edit_concession_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_concession_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
            if (Number(edit_discount) >(Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                $('#edit_discount_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_discount_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
            if (edit_pending_fee > 0) {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))) {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))) {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }

            $('#edit_paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#edit_paid_amount').val(paid_amount);
            $('#edit_fee_period').val(start_month_name + ' - ' + end_month_name);
        }
    }

    var startedit_paidmonth = $('#start_duration :selected').val();
    var endedit_paidmonth = $('#end_duration :selected').val();
    var paid_edit_student=$('#edit_totalpaid_fee').val();
    if (startedit_paidmonth == '5' || endedit_paidmonth == '5') {
        if (paid_edit_student > '0' && Number(paid_edit_student) != Number($('#edit_paid_amount').val())) {
            $('#edit_paid_fee_error').text('Please enter paid fee same as total fee');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_paid_fee_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }
    }

    if ($('#edit_concession_error').is(':empty') && $('#edit_paid_fee_error').is(':empty')  && $('#edit_discount_error').is(':empty')) {
        document.getElementById("edit_take_fee").disabled = false;
    } else {
        document.getElementById("edit_take_fee").disabled = true;
    }
}
/************get pre primary fee structure for edit fee END**************/

/************get secondary fee structure for edit fee START ****************/
function editSecondaryFeeDuration(isSelect = false) {
    if (isSelect) {
        $('#edit_concession').val('');
        $('#edit_discount').val('');
        $('#edit_totalpaid_fee').val('');
        $('#edit_paid_fee_error').text('');
    }

    var edit_concession = $('#edit_concession').val();
    var edit_discount=$('#edit_discount').val();
    var edit_pending_fee = $('#edit_pending_fee').val();
    var admission_year = $('#edit_year').val();


    var current_year = new Date().getFullYear();
    document.getElementById("edit_take_fee").disabled = false;
    var editstandard =$('#edit_studentfeestd').val();
    if (Number(admission_year) == Number(current_year) || editstandard == '1' || editstandard == 'I') {
        $('#edit_student_status').val('new');
    } else {
        $('#edit_student_status').val('old');
    }

    if ($('#edit_student_status').val() == 'new') {
        $('#edit_old_student').attr('disabled', true);
    } else {
        $('#edit_new_student').attr('disabled', true);
    }
    var student_status = $('#edit_student_status :selected').val();
    var start_month = $('#start_duration :selected').val();
    var start_month_name = $('#start_duration :selected').text();

    var end_month = $('#end_duration :selected').val();
    var end_month_name = $('#end_duration :selected').text();

    var monthNames = ["June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May"];
    var monthValues = ["6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5"];

    var start = monthNames.indexOf(start_month_name);
    var end = monthNames.indexOf(end_month_name);

    if (start_month && end_month) {
        var duration = (parseInt(end) - parseInt(start)) + 1;
        $('#edit_fee_duration').val(duration);
    } else {
        var duration = '1';
        $('#edit_fee_duration').val(duration);
    }


    var admission_fee = $('#edit_admission_fee').val();
    var tuition_fee = $('#edit_tuition_fee').val();
    var term_fee = $('#edit_term_fee').val();
    $('#edit_other_fee').val('0');
    var smart_fee = $('#edit_smart_fee').val();
    var computer_fee = $('#edit_computer_fee').val();
    var sports_fee = $('#edit_sports_fee').val();
    var insurance_fee = $('#edit_insurance_fee').val();
    var exam_fee = $('#edit_exam_fee').val();
    var enroll_fee = $('#edit_enroll_fee').val();

    if (student_status == 'new') {
        var total = Number(admission_fee) + (Number(tuition_fee) * 12) + (Number(term_fee) * 2) + Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee);
        $('#edit_total_fee').val(total);
    } else {
        var total = (Number(tuition_fee) * 12) + Number(edit_pending_fee) + (Number(term_fee) * 2)+Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee);
        $('#edit_total_fee').val(total);
    }

    var total_fee = $('#edit_total_fee').val();


    if (start_month == '6' && !end_month) {

        $('#edit_paid_fee_amount').html('');

        if (student_status == 'new') {
            if (Number(edit_concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(edit_pending_fee))) {
                if (Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) + +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+ Number(enroll_fee) +Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+ Number(enroll_fee) +Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                }else{
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee)) - Number(edit_concession);
                }
                $('#edit_paid_admission_fee').val(admission_fee);
            } else {
                if (Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee)+Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))) {
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) + Number(enroll_fee) +Number(edit_pending_fee))-Number(edit_discount);
                }else{
                    var paid_amount = (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee));
                }
                $('#edit_paid_admission_fee').val(admission_fee);
            }
        } else {
            if (Number(edit_concession) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))) {
                if (Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                }else{
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee)) - Number(edit_concession);
                }
            } else {
                if (Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))) {
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))-Number(edit_discount);
                }else{
                    var paid_amount = (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) + Number(edit_pending_fee));
                }
            }
        }

        if (student_status == 'new') {
            if (Number(edit_concession) > (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))) {
                $('#edit_concession_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_concession_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
        } else {
            if (Number(edit_concession) > (Number(tuition_fee) + Number(term_fee)+Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))) {
                $('#edit_concession_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_concession_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
        }
        if (student_status == 'new') {
            if (Number(edit_discount) > (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                $('#edit_discount_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_discount_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
        } else {
            if (Number(edit_discount) > (Number(tuition_fee) + Number(term_fee)+Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                $('#edit_discount_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_discount_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
        }
        if (edit_pending_fee > 0) {
            if (student_status == 'new') {
                if (Number(edit_concession) > 0 && Number(edit_concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))) {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))) {
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (Number(edit_concession) > 0 && Number(edit_concession) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))) {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))) {
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }
        }else{
            if (student_status == 'new') {
                if (Number(edit_concession) > 0 && Number(edit_concession) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) )) {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) )-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +   '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(admission_fee) + Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee))) {
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (Number(edit_concession) > 0 && Number(edit_concession) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))) {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) + Number(enroll_fee) +Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (Number(edit_discount) > 0 && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))) {
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append( '<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }
        }

        $('#edit_paid_tution_fee').val(tuition_fee);
        $('#edit_paid_term_fee').val(term_fee);
        $('#edit_paid_smart_fee').val(smart_fee);
        $('#edit_paid_computer_fee').val(computer_fee);
        $('#edit_paid_sports_fee').val(sports_fee);
        $('#edit_paid_exam_fee').val(exam_fee);
        $('#edit_paid_insurance_fee').val(insurance_fee);
        $('#edit_paid_enroll_fee').val(enroll_fee);
        $('#edit_paid_amount').val(paid_amount);
        $('#edit_fee_period').val(start_month_name);
    } else if (start_month == '12' && !end_month) {
        $('#edit_paid_fee_amount').html('');

        if (Number(edit_concession) <= (Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee))) {
            if (Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                var paid_amount = (Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee)) - Number(edit_concession) -Number(edit_discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee)) - Number(edit_concession);
            }
        } else {
            if (Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee))) {
                var paid_amount = (Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee))-Number(edit_discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee));
            }
        }


        if (Number(edit_concession) >(Number(tuition_fee) + Number(term_fee) + Number(edit_pending_fee))) {
            $('#edit_concession_error').text('Please enter value less than paid amount');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_concession_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }

        if (Number(edit_discount) >(Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee))) {
            $('#edit_discount_error').text('Please enter value less than paid amount');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_discount_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }

        if (edit_pending_fee > 0) {
            if (edit_concession > '0' && Number(edit_concession) <= (Number(tuition_fee) + Number(term_fee)  + Number(edit_pending_fee))) {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(term_fee) +  Number(edit_pending_fee))) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        } else {
            if (edit_concession > '0' && Number(edit_concession) <= Number($('#edit_paid_amount').val())) {
                if (edit_discount > '0' && Number(edit_discount) <= Number($('#edit_paid_amount').val()-Number(edit_concession))) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee  + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <= Number($('#edit_paid_amount').val())) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span>Discount :' + edit_discount + +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        }
        $('#edit_paid_tution_fee').val(tuition_fee);
        $('#edit_paid_term_fee').val(term_fee);
        $('#edit_paid_other_fee').val('0');
        $('#edit_paid_amount').val(paid_amount);
        $('#edit_fee_period').val(start_month_name);
    } else if (start_month && !end_month) {

        $('#edit_paid_fee_amount').html('');
        if (Number(edit_concession) <= (Number(tuition_fee) + Number(edit_pending_fee))) {
            if (Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                var paid_amount = (Number(tuition_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
            }else{
                var paid_amount = (Number(tuition_fee) + Number(edit_pending_fee)) - Number(edit_concession);
            }
        } else {
            if (Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))) {
                var paid_amount = Number(tuition_fee) + Number(edit_pending_fee)-Number(edit_discount);
            }else{
                var paid_amount = Number(tuition_fee) + Number(edit_pending_fee);
            }
        }
        if (Number(edit_concession) >(Number(tuition_fee) + Number(edit_pending_fee))) {
            $('#edit_concession_error').text('Please enter value less than paid amount');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_concession_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }
        if (Number(edit_discount) >(Number(tuition_fee) + Number(edit_pending_fee))) {
            $('#edit_discount_error').text('Please enter value less than paid amount');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_discount_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }
        if (edit_pending_fee > 0) {
            if (edit_concession > '0' && Number(edit_concession) <= (Number(tuition_fee) + Number(edit_pending_fee))) {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount+ '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Discount :' + edit_discount+ '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        } else {
            if (edit_concession > '0' && Number(edit_concession) <= (Number(tuition_fee) + Number(edit_pending_fee))) {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount+'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <= (Number(tuition_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span>Discount :' + edit_discount+'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + tuition_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        }
        $('#edit_paid_tution_fee').val(tuition_fee);
        $('#edit_paid_amount').val(paid_amount);
        $('#edit_fee_period').val(start_month_name);
    }
    if (start_month && end_month) {
        var i;
        var total_month = [];

        for (i = parseInt(start); i <= end; i++) {
            var total = monthValues[i];
            total_month.push(parseInt(total));
        }

        if ($.inArray(6, total_month) > -1 && $.inArray(12, total_month) == -1) {
            $('#edit_paid_fee_amount').html('');

            if (student_status == 'new') {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee))){
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee)) - Number(edit_concession);
                    }
                    $('#edit_paid_admission_fee').val(admission_fee);
                }else{
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee))){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee)+Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee))-Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee));
                    }
                    $('#edit_paid_admission_fee').val(admission_fee);
                }

            } else {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee))){
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee))-Number(edit_concession)){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee)) - Number(edit_concession);
                    }
                }else{
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee))){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)+Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee)) -Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)+Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee)) ;
                    }

                }
            }
            if (student_status == 'new') {
                if (Number(edit_concession) > (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(edit_pending_fee))) {
                    $('#edit_concession_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_concession_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            } else {
                if (Number(edit_concession) > (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee))) {
                    $('#edit_concession_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_concession_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            }
            if (student_status == 'new') {
                if (Number(edit_discount) > (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_discount_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_discount_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            } else {
                if (Number(edit_discount) > (Number((tuition_fee) * parseInt(duration))+ Number(term_fee)+Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee))-Number(edit_concession)) {
                    $('#edit_discount_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_discount_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            }
            if (edit_pending_fee > 0) {
                if (student_status == 'new') {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(edit_pending_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee +  '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee)+Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee))){
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Discount :' + edit_discount +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee))){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }
                }
            }else{
                if (student_status == 'new') {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +   '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(admission_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee))){
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +   '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +   '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration))+ Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) )){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Discount :' + edit_discount +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }
                }
            }

            $('#edit_paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#edit_paid_term_fee').val(term_fee);
            $('#edit_paid_smart_fee').val(smart_fee);
            $('#edit_paid_computer_fee').val(computer_fee);
            $('#edit_paid_sports_fee').val(sports_fee);
            $('#edit_paid_exam_fee').val(exam_fee);
            $('#edit_paid_insurance_fee').val(insurance_fee);
            $('#edit_paid_enroll_fee').val(enroll_fee);
            $('#edit_paid_amount').val(paid_amount);
            $('#edit_fee_period').val(start_month_name + ' - ' + end_month_name);
        } else if ($.inArray(12, total_month) > -1 && $.inArray(6, total_month) == -1) {
            $('#edit_paid_fee_amount').html('');

            if (edit_concession > '0' && Number(edit_concession) <=(Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(edit_pending_fee))) {
                if (edit_discount > '0' && Number(edit_discount) <=(Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +  Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +  Number(edit_pending_fee)) - Number(edit_concession);
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <=(Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(edit_pending_fee))-Number(edit_concession)) {
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +  Number(edit_pending_fee))-Number(edit_discount);
                }else{
                    var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +  Number(edit_pending_fee));
                }
            }
            if (Number(edit_concession) >(Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +  Number(edit_pending_fee))) {
                $('#edit_concession_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_concession_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
            if (Number(edit_discount) >(Number((tuition_fee) * parseInt(duration)) + Number(term_fee) +  Number(edit_pending_fee))-Number(edit_discount)) {
                $('#edit_discount_error').text('Please enter value less than paid amount');
                document.getElementById("edit_take_fee").disabled = true;
            } else {
                $('#edit_discount_error').text('');
                document.getElementById("edit_take_fee").disabled = false;
            }
            if (edit_pending_fee > 0) {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(edit_pending_fee))) {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(edit_pending_fee))) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            } else {
                if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(edit_pending_fee))) {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(edit_pending_fee))-Number(edit_concession)) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +'</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee +  '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                } else {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(term_fee)  + Number(edit_pending_fee))) {
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + term_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }

            $('#edit_paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
            $('#edit_paid_term_fee').val(term_fee);
            $('#edit_paid_other_fee').val('0');
            $('#edit_paid_amount').val(paid_amount);
            $('#edit_fee_period').val(start_month_name + ' - ' + end_month_name);
        } else if ($.inArray(6, total_month) > -1 && $.inArray(12, total_month) > -1) {

            $('#edit_paid_fee_amount').html('');

            if (student_status == 'new') {
                if (edit_concession > '0' && Number(edit_concession) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)  +Number(enroll_fee)+ Number(edit_pending_fee))) {
                    if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)+Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)  +Number(enroll_fee)+ Number(edit_pending_fee))-Number(edit_concession)) {
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)  +Number(enroll_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                    }else{
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)   +Number(enroll_fee)+ Number(edit_pending_fee)) - Number(edit_concession);
                    }
                    $('#edit_paid_admission_fee').val(admission_fee);
                }else{
                    if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+  Number(edit_pending_fee))) {
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee))-Number(edit_discount);
                    }else{
                        var paid_amount = (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee));
                    }
                    $('#edit_paid_admission_fee').val(admission_fee);
                }

            } else {
                if (edit_concession > '0' && Number(edit_concession) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))){
                    if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))-Number(edit_concession)){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee)) - Number(edit_concession);
                    }
                }else{
                    if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee))-Number(edit_concession)){
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)+Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))-Number(edit_discount);
                    }else{
                        var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)  +Number(enroll_fee)+ Number(edit_pending_fee));
                    }
                }
            }

            if (student_status == 'new') {
                if (Number(edit_concession) > (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee))) {
                    $('#edit_concession_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_concession_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            } else {
                if (Number(edit_concession) > ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))) {
                    $('#edit_concession_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_concession_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            }
            if (student_status == 'new') {
                if (Number(edit_discount) > (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_discount_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_discount_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            } else {
                if (Number(edit_discount) > ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_discount_error').text('Please enter value less than paid amount');
                    document.getElementById("edit_take_fee").disabled = true;
                } else {
                    $('#edit_discount_error').text('');
                    document.getElementById("edit_take_fee").disabled = false;
                }
            }
            if(edit_pending_fee > 0){
                if (student_status == 'new') {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee))) {
                        if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))-Number(edit_concession)) {
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Enroll Fee : ' + enroll_fee + '</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))) {
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +  '</span><br><span>Enroll Fee : ' + enroll_fee +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Discount :' + edit_discount +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +  '</span><br><span>Enroll Fee : ' + enroll_fee +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (edit_concession > '0' && Number(edit_concession) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee)+ Number(edit_pending_fee))-Number(edit_concession)){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +  '</span><br><span>Enroll Fee : ' + enroll_fee +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Enroll Fee : ' + enroll_fee +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee))){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +  '</span><br><span>Enroll Fee : ' + enroll_fee +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) + '</span><br><span>Enroll Fee : ' + enroll_fee +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Pending Fee : ' + edit_pending_fee +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }
                }
            }else{
                if (student_status == 'new') {
                    if (edit_concession > '0' && Number(edit_concession) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee)+ Number(edit_pending_fee))) {
                        if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)  + +Number(enroll_fee)+Number(edit_pending_fee))-Number(edit_concession)) {
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +  '</span><br><span>Enroll Fee : ' + enroll_fee +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +  '</span><br><span>Enroll Fee : ' + enroll_fee +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }else{
                        if (edit_discount > '0' && Number(edit_discount) <= (Number(admission_fee) + Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee)  +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee) + Number(edit_pending_fee))) {
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +  '</span><br><span>Enroll Fee : ' + enroll_fee +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }else{
                            $('#edit_paid_fee_amount').append('<span>Admission Fee : ' + admission_fee + '</span><br><span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +  '</span><br><span>Enroll Fee : ' + enroll_fee +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                        }
                    }

                } else {
                    if (edit_concession > '0' && Number(edit_concession) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee) +Number(enroll_fee)+ Number(edit_pending_fee))){
                        if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) +Number(smart_fee) +Number(computer_fee) +Number(sports_fee) +Number(insurance_fee) +Number(exam_fee)+Number(enroll_fee) )){
                            $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +  '</span><br><span>Enroll Fee : ' + enroll_fee +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +  '</span><br><span>Enroll Fee : ' + enroll_fee +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }else{
                    if (edit_discount > '0' && Number(edit_discount) <= ( Number((tuition_fee) * parseInt(duration)) + Number(term_fee) + Number(term_fee) + Number(other_fee) +Number(enroll_fee)+ Number(edit_pending_fee))){
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +  '</span><br><span>Enroll Fee : ' + enroll_fee +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }else{
                        $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Term Fee : ' + Number((term_fee) * 2) +  '</span><br><span>Enroll Fee : ' + enroll_fee +'</span><br><span>Smart Fee : ' + smart_fee + '</span><br><span>Computer Fee : ' + computer_fee + '</span><br><span>Sports Fee : ' + sports_fee + '</span><br><span>Insurance Fee : ' + insurance_fee + '</span><br><span>Exam Fee : ' + exam_fee  +  '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                    }
                }
            }
        }

        $('#edit_paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
        $('#edit_paid_term_fee').val(Number((term_fee) * 2));
        $('#edit_paid_smart_fee').val(smart_fee);
        $('#edit_paid_computer_fee').val(computer_fee);
        $('#edit_paid_sports_fee').val(sports_fee);
        $('#edit_paid_exam_fee').val(exam_fee);
        $('#edit_paid_insurance_fee').val(insurance_fee);
        $('#edit_paid_enroll_fee').val(enroll_fee);
        $('#edit_paid_amount').val(paid_amount);
        $('#edit_fee_period').val(start_month_name + ' - ' + end_month_name);
    } else {
        $('#edit_paid_fee_amount').html('');

        if (Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))) {
            if (Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee)) - Number(edit_concession)-Number(edit_discount);
            }else{
                var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee)) - Number(edit_concession);
            }
        } else {
            if (Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_discount);
            }else{
                var paid_amount = (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee));
            }
        }
        if (Number(edit_concession) >(Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))) {
            $('#edit_concession_error').text('Please enter value less than paid amount');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_concession_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }
        if (Number(edit_discount) >(Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
            $('#edit_discount_error').text('Please enter value less than paid amount');
            document.getElementById("edit_take_fee").disabled = true;
        } else {
            $('#edit_discount_error').text('');
            document.getElementById("edit_take_fee").disabled = false;
        }
        if (edit_pending_fee > 0) {
            if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))) {
                if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Pending Fee : ' + edit_pending_fee + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        } else {
            if (edit_concession > '0' && Number(edit_concession) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))) {
                if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Concession :' + edit_concession +'</span><br><span>Discount :' + edit_discount + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Concession :' + edit_concession + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            } else {
                if (edit_discount > '0' && Number(edit_discount) <= (Number((tuition_fee) * parseInt(duration)) + Number(edit_pending_fee))-Number(edit_concession)) {
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span>Discount :' + edit_discount +'</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }else{
                    $('#edit_paid_fee_amount').append('<span>Tution Fee : ' + Number((tuition_fee) * parseInt(duration)) + '</span><br><span><b>Total Fee : ' + paid_amount + '</b></span>');
                }
            }
        }

        $('#edit_paid_tution_fee').val(Number((tuition_fee) * parseInt(duration)));
        $('#edit_paid_amount').val(paid_amount);
        $('#edit_fee_period').val(start_month_name + ' - ' + end_month_name);
    }
}

var startedit_paidmonth = $('#start_duration :selected').val();
var endedit_paidmonth = $('#end_duration :selected').val();
    var paid_edit_student=$('#edit_totalpaid_fee').val();
if (startedit_paidmonth == '5' || endedit_paidmonth == '5') {
    if (paid_edit_student > '0' && Number(paid_edit_student) != Number($('#edit_paid_amount').val())) {
        $('#edit_paid_fee_error').text('Please enter paid fee same as total fee');
        document.getElementById("edit_take_fee").disabled = true;
    } else {
        $('#edit_paid_fee_error').text('');
        document.getElementById("edit_take_fee").disabled = false;
    }
}

if ($('#edit_concession_error').is(':empty') && $('#edit_paid_fee_error').is(':empty')  && $('#edit_discount_error').is(':empty')) {
    document.getElementById("edit_take_fee").disabled = false;
} else {
    document.getElementById("edit_take_fee").disabled = true;
}
}
/************get scondary fee structure for edit fee END**************/

function smartfee() {
    if ($('#standard_add_smart_fee').val() !== "") {
        $('#standard_add_smart_fee').siblings("span").text('');
    }
}
function insurancefee() {
    if ($('#standard_add_insurance_fee').val() !== "") {
        $('#standard_add_insurance_fee').siblings("span").text('');
    }
}
function computerfee() {
    if ($('#standard_add_computer_fee').val() !== "") {
        $('#standard_add_computer_fee').siblings("span").text('');
    }
}
function sportsfee() {
    if ($('#standard_add_sportst_fee').val() !== "") {
        $('#standard_add_sports_fee').siblings("span").text('');
    }
}
function examfee() {
    if ($('#standard_add_exam_fee').val() !== "") {
        $('#standard_add_exam_fee').siblings("span").text('');
    }
}
function enrollfee() {
    if ($('#standard_add_enroll_fee').val() !== "") {
        $('#standard_add_enroll_fee').siblings("span").text('');
    }
}
function totalfee() {
    if ($('#standard_add_total_fee').val() !== "") {
        $('#standard_add_total_fee').siblings("span").text('');
    }
}



/************collect fee for other fee structure -FIrststep START ****************/
function getFirststepFee() {

    $('#paid_fee_amount').html('');
    var studentFeeYear = $('#studentFeeYear').val();
    var tuition_fee1 = $('#tuition_fee').val();
    var concession1 = $('#concession').val();
    var discount1 = $('#discount').val();
    var pending_fee1 = $('#pending_fee').val();
    var paid_other_fee = $('#paid_other_fee').val();

    var start_month1 = $('#start_fee_installment :selected').val();
    var start_month_name1 = $('#start_fee_installment :selected').text();

    var end_month1 = $('#end_fee_installment :selected').val();
    var end_month_name1 = $('#end_fee_installment :selected').text();

    var monthValues1 = ["first", "second", "third", "fourth"];

    var start1 = monthValues1.indexOf(start_month1);
    var end1 = monthValues1.indexOf(end_month1);

    if (start_month1 && end_month1) {
        var duration1 = (parseInt(end1) - parseInt(start1)) + 1;
        $('#fee_duration').val(duration1);
    } else {
        var duration1 = '1';
        $('#fee_duration').val(duration1);
    }
    $('#total_fee').val(tuition_fee1);
    if(start_month1 && end_month1){
        var fee_period1 = start_month1 + ' - ' + end_month1;
    }else{
        var fee_period1 = start_month1;
    }

    $('#fee_period').val(fee_period1);

    if(studentFeeYear != '1'){

        var paid_feeamount1=(tuition_fee1/3)*duration1;

        if (Number(concession1) >(Number(paid_feeamount1)  + Number(pending_fee1))) {
            $('#concession_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#concession_error').text('');
            document.getElementById("take_fee").disabled = false;
        }

        if (Number(discount1) >(Number(paid_feeamount1)  + Number(pending_fee1))) {
            $('#discount_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#discount_error').text('');
            document.getElementById("take_fee").disabled = false;
        }
        if (concession1 > '0' && Number(concession1) <= (Number(paid_feeamount1) + Number(pending_fee1))) {

            if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1)  + Number(pending_fee1))- Number(concession1)){
                var paid_amount1 = (Number(paid_feeamount1) + Number(pending_fee1) + Number(paid_other_fee)) - Number(concession1) - Number(discount1);
            }else{
                var paid_amount1 = (Number(paid_feeamount1) + Number(pending_fee1) + Number(paid_other_fee))- Number(concession1);
            }

        }else{
            if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1) + Number(pending_fee1))){
                var paid_amount1 = (Number(paid_feeamount1)  + Number(pending_fee1) + Number(paid_other_fee)) - Number(discount1);
            }else{
                var paid_amount1 = (Number(paid_feeamount1) + Number(pending_fee1) + Number(paid_other_fee));
            }
        }
        if (concession1 > '0' && Number(concession1) <= (Number(paid_feeamount1) + Number(pending_fee1))) {
            if(pending_fee1 > '0'){
                if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1)  + Number(pending_fee1))- Number(concession1)){
                    $('#paid_fee_amount').append('<span>Fee : ' + paid_feeamount1 + '</span><br><span>Concession :' + concession1 +'</span><br><span>Discount :' + discount1 +'</span><br><span>Pending Fee : ' + pending_fee1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Fee : ' + paid_feeamount1 + '</span><br><span>Concession :' + concession1 +'</span><br><span>Pending Fee : ' + pending_fee1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }
            }else{
                if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1)  + Number(pending_fee1))- Number(concession1)){
                    $('#paid_fee_amount').append('<span>Fee : ' + paid_feeamount1 + '</span><br><span>Concession :' + concession1 +'</span><br><span>Discount :' + discount1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Fee : ' + paid_feeamount1 + '</span><br><span>Concession :' + concession1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }
            }
        }else{
            if(pending_fee1 > '0'){
                if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1) + Number(pending_fee1))){
                    $('#paid_fee_amount').append('<span>Fee : ' + paid_feeamount1 +'</span><br><span>Discount :' + discount1 +'</span><br><span>Pending Fee : ' + pending_fee1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Fee : ' + paid_feeamount1 + '</span><br><span>Pending Fee : ' + pending_fee1 +'</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }
            }else{
                if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1) + Number(pending_fee1))){
                    $('#paid_fee_amount').append('<span>Fee : ' + paid_feeamount1 +'</span><br><span>Discount :' + discount1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Fee : ' + paid_feeamount1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }
            }

        }
        var takedate=$('#take_date').val();

        if ($('#concession_error').is(':empty') && $('#paid_fee_error').is(':empty')  && $('#discount_error').is(':empty')) {
            document.getElementById("take_fee").disabled = false;
        } else {
            document.getElementById("take_fee").disabled = true;
        }
        $('#paid_tution_fee').val(paid_feeamount1);
        $('#paid_amount').val(paid_amount1);

    }else{
        var paid_feeamount1=(tuition_fee1/4)*duration1;
        if (Number(concession1) >(Number(paid_feeamount1)  + Number(pending_fee1))) {
            $('#concession_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#concession_error').text('');
            document.getElementById("take_fee").disabled = false;
        }

        if (Number(discount1) >(Number(paid_feeamount1)  + Number(pending_fee1))) {
            $('#discount_error').text('Please enter value less than paid amount');
            document.getElementById("take_fee").disabled = true;
        } else {
            $('#discount_error').text('');
            document.getElementById("take_fee").disabled = false;
        }
        if (concession1 > '0' && Number(concession1) <= (Number(paid_feeamount1) + Number(pending_fee1))) {
            if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1)  + Number(pending_fee1))- Number(concession1)){
                var paid_amount1 = (Number(paid_feeamount1) + Number(pending_fee1)) - Number(concession1) - Number(discount1);
            }else{
                var paid_amount1 = (Number(paid_feeamount1) + Number(pending_fee1))- Number(concession1);
            }

        }else{
            if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1) + Number(pending_fee1))){
                var paid_amount1 = (Number(paid_feeamount1)  + Number(pending_fee1)) - Number(discount1);
            }else{
                var paid_amount1 = (Number(paid_feeamount1) + Number(pending_fee1));
            }
        }
        if (concession1 > '0' && Number(concession1) <= (Number(paid_feeamount1) + Number(pending_fee1))) {
            if(pending_fee1 > '0'){
                if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1)  + Number(pending_fee1))- Number(concession1)){
                    $('#paid_fee_amount').append('<span>Fee :' + paid_feeamount1 + '</span><br><span>Concession :' + concession1 +'</span><br><span>Discount :' + discount1 +'</span><br><span>Pending Fee : ' + pending_fee1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Fee :' + paid_feeamount1 + '</span><br><span>Concession :' + concession1 +'</span><br><span>Pending Fee : ' + pending_fee1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }
            }else{
                if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1)  + Number(pending_fee1))- Number(concession1)){
                    $('#paid_fee_amount').append('<span>Fee :' + paid_feeamount1 + '</span><br><span>Concession :' + concession1 +'</span><br><span>Discount :' + discount1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Fee :' + paid_feeamount1 + '</span><br><span>Concession :' + concession1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }
            }
        }else{
            if(pending_fee1 > '0'){
                if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1) + Number(pending_fee1))){
                    $('#paid_fee_amount').append('<span>Fee :' + paid_feeamount1 +'</span><br><span>Discount :' + discount1 +'</span><br><span>Pending Fee : ' + pending_fee1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Fee :' + paid_feeamount1 + '</span><br><span>Pending Fee : ' + pending_fee1 +'</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }
            }else{
                if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1) + Number(pending_fee1))){
                    $('#paid_fee_amount').append('<span>Fee :' + paid_feeamount1 +'</span><br><span>Discount :' + discount1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }else{
                    $('#paid_fee_amount').append('<span>Fee :' + paid_feeamount1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
                }
            }

        }
        var takedate=$('#take_date').val();

        if ($('#concession_error').is(':empty') && $('#paid_fee_error').is(':empty')  && $('#discount_error').is(':empty')) {
            document.getElementById("take_fee").disabled = false;
        } else {
            document.getElementById("take_fee").disabled = true;
        }
        $('#paid_tution_fee').val(paid_feeamount1);
        $('#paid_amount').val(paid_amount1);
    }



}
/************collect fee for other fee structure -FIrststep END ****************/


/************editcollect fee for other fee structure -FIrststep START ****************/
function editgetFirststepFee() {

    $('#edit_paid_fee_amount').html('');
    var tuition_fee1 = $('#edit_tuition_fee').val();
    var concession1 = $('#edit_concession').val();
    var discount1 = $('#edit_discount').val();
    var pending_fee1 = $('#edit_pending_fee').val();

    var start_month1 = $('#start_editfee_installment :selected').val();
    var start_month_name1 = $('#start_editfee_installment :selected').text();

    var end_month1 = $('#end_editfee_installment :selected').val();
    var end_month_name1 = $('#end_editfee_installment :selected').text();

    var monthValues1 = ["first", "second", "third", "fourth"];

    var start1 = monthValues1.indexOf(start_month1);
    var end1 = monthValues1.indexOf(end_month1);

    if (start_month1 && end_month1) {
        var duration1 = (parseInt(end1) - parseInt(start1)) + 1;
        $('#edit_fee_duration').val(duration1);
    } else {
        var duration1 = '1';
        $('#edit_fee_duration').val(duration1);
    }
    $('#edit_total_fee').val(tuition_fee1);
    if(start_month1 && end_month1){
        var fee_period1 = start_month1 + ' - ' + end_month1;
    }else{
        var fee_period1 = start_month1;
    }

    $('#edit_fee_period').val(fee_period1);

    var paid_feeamount1=(tuition_fee1/4)*duration1;
    if (Number(concession1) >(Number(paid_feeamount1)  + Number(pending_fee1))) {
        $('#edit_concession_error').text('Please enter value less than paid amount');
        document.getElementById("edit_take_fee").disabled = true;
    } else {
        $('#edit_concession_error').text('');
        document.getElementById("edit_take_fee").disabled = false;
    }

    if (Number(discount1) >(Number(paid_feeamount1)  + Number(pending_fee1))) {
        $('#edit_discount_error').text('Please enter value less than paid amount');
        document.getElementById("edit_take_fee").disabled = true;
    } else {
        $('#edit_discount_error').text('');
        document.getElementById("edit_take_fee").disabled = false;
    }
    if (concession1 > '0' && Number(concession1) <= (Number(paid_feeamount1) + Number(pending_fee1))) {
        if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1)  + Number(pending_fee1))- Number(concession1)){
            var paid_amount1 = (Number(paid_feeamount1) + Number(pending_fee1)) - Number(concession1) - Number(discount1);
        }else{
            var paid_amount1 = (Number(paid_feeamount1) + Number(pending_fee1))- Number(concession1);
        }

    }else{
        if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1) + Number(pending_fee1))){
            var paid_amount1 = (Number(paid_feeamount1)  + Number(pending_fee1)) - Number(discount1);
        }else{
            var paid_amount1 = (Number(paid_feeamount1) + Number(pending_fee1));
        }
    }
    if (concession1 > '0' && Number(concession1) <= (Number(paid_feeamount1) + Number(pending_fee1))) {
        if(pending_fee1 > '0'){
            if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1)  + Number(pending_fee1))- Number(concession1)){
                $('#edit_paid_fee_amount').append('<span>Fee :' + paid_feeamount1 + '</span><br><span>Concession :' + concession1 +'</span><br><span>Discount :' + discount1 +'</span><br><span>Pending Fee : ' + pending_fee1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
            }else{
                $('#edit_paid_fee_amount').append('<span>Fee :' + paid_feeamount1 + '</span><br><span>Concession :' + concession1 +'</span><br><span>Pending Fee : ' + pending_fee1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
            }
        }else{
            if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1)  + Number(pending_fee1))- Number(concession1)){
                $('#edit_paid_fee_amount').append('<span>Fee :' + paid_feeamount1 + '</span><br><span>Concession :' + concession1 +'</span><br><span>Discount :' + discount1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
            }else{
                $('#edit_paid_fee_amount').append('<span>Fee :' + paid_feeamount1 + '</span><br><span>Concession :' + concession1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
            }
        }
    }else{
        if(pending_fee1 > '0'){
            if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1) + Number(pending_fee1))){
                $('#edit_paid_fee_amount').append('<span>Fee :' + paid_feeamount1 +'</span><br><span>Discount :' + discount1 +'</span><br><span>Pending Fee : ' + pending_fee1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
            }else{
                $('#edit_paid_fee_amount').append('<span>Fee :' + paid_feeamount1 + '</span><br><span>Pending Fee : ' + pending_fee1 +'</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
            }
        }else{
            if(discount1 > '0' && Number(discount1) <= (Number(paid_feeamount1) + Number(pending_fee1))){
                $('#edit_paid_fee_amount').append('<span>Fee :' + paid_feeamount1 +'</span><br><span>Discount :' + discount1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
            }else{
                $('#edit_paid_fee_amount').append('<span>Fee :' + paid_feeamount1 + '</span><br><span><b>Total Fee : ' + paid_amount1 + '</b></span>');
            }
        }

    }
    if ($('#edit_concession_error').is(':empty') && $('#edit_paid_fee_error').is(':empty')  && $('#edit_discount_error').is(':empty')) {
        document.getElementById("edit_take_fee").disabled = false;
    } else {
        document.getElementById("edit_take_fee").disabled = true;
    }
    $('#edit_paid_tution_fee').val(paid_feeamount1);
    $('#edit_paid_amount').val(paid_amount1);

}
/************edit collect fee for other fee structure -FIrststep END ****************/


/************fetch standard after select school START ****************/
function getSchoolStandard(event) {
    var transstudent_school= event.target.value;
    $.ajax({
        url: "/student_transfer/get_std_value",
        method: "POST",
        data: {transstudent_school:transstudent_school},
        success: function (data) {
            var std_data = JSON.parse(data);
            $("#standard_edit_all").empty();
            $("#standard_edit_all").append("<option value=''>Select</option>");
            for (var i = 0; i < std_data.length; i++) {
                $("#standard_edit_all").append("<option value='" + std_data[i].id + "'>" + std_data[i].standard + "</option>");
            }
        }
    });
}
/************fetch standard after select school END  ****************/

/************Checked/Unched Checkbox of edit student START ****************/
function checkcheckbox(){

    // Total checkboxes
    var length = $('.edit_Student').length;

    // Total checked checkboxes
    var totalchecked = 0;
    $('.edit_Student').each(function(){
        if($(this).is(':checked')){
            totalchecked+=1;
        }
    });

    // Checked unchecked checkbox
    if(totalchecked == length){
        $("#checkall").prop('checked', true);
    }else{
        $('#checkall').prop('checked', false);
    }
}
/************Checked/Unched Checkbox of edit student END  ****************/


/************get fee structure year for collect fee edit START ****************/
function getStudentYear(){
    $('#fee_collect_message').html('');
    var select_year = $('#studentFeeYear1').val();
    var id = $('#take_id').val();
    
    $.ajax({
        url: "/Fee/get_previous_fee",
        method: "POST",
        data: {fee_year: select_year,id:id},
        success: function (data) {
            //console.log('data',data);
            if(data == 'Not remaining for previous year'){
                if ($('#fee_collect_message').is(':empty')) {
                    $('#fee_collect_message').append('<div class="alert alert-danger alert-dismissible mb-2" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                }
            }else{
                var dataArray = JSON.parse(data);
                //var student_id = dataArray.data[0].id;
                var student_id = id;
                $('#take_id').val(id);

                //$('#take_std').val(dataArray.data[0].standard + '-' + dataArray.data[0].divison);
                //$('#student_feestdandard').val(dataArray.data[0].standard);
                if(dataArray.data[0].standard == '1' || dataArray.data[0].standard == 'I' || dataArray.data[0].standard == '9'){
                    $('#student_status').val('new');
                } else {
                    $('#student_status').val('old');
                }

                var remaining_fee = dataArray.data[0].remaining_fee;
                $('#remaining_fee').val(remaining_fee);

                var pending_Fee = dataArray.pending_fee;
                $('#pending_fee').val(pending_Fee);

                var fee_flag= dataArray.flag[0].feestructure_flag;
                $('#flag').val(fee_flag);
                $('#admission_fee').val(dataArray.data[0].admission_fee);
                $('#tuition_fee').val(dataArray.data[0].tuition_fee);
                $('#term_fee').val(dataArray.data[0].term_fee);
                $('#other_fee').val(dataArray.data[0].other_fee);
                $('#smart_fee').val(dataArray.data[0].smart_class_fee);
                $('#computer_fee').val(dataArray.data[0].computer_fee);
                $('#sports_fee').val(dataArray.data[0].sports_fee);
                $('#insurance_fee').val(dataArray.data[0].insurance_fee);
                $('#exam_fee').val(dataArray.data[0].exam_fee);
                $('#enroll_fee').val(dataArray.data[0].enroll_fee);


                if(fee_flag == '1'){
                    totalFeeget();
                    feeMonth();
                    getSecondaryFeeDuration();
                    edit_fee_flag = false;
                    edit_totalfee_flag = false;
                    $('#standard_edit_total_fee').hide();
                    $('#smart_fee').show();
                    $('#sport_fee').show();
                    $('#exam_fee').show();
                    $('#computer_fee').show();
                    $('#insurance_fee').show();
                    $('#enroll_fee').show();
                    $('#editotherfee').hide();
                }else if(fee_flag == '2'){
                    totalFeeget();
                    feeMonth();
                    getPreFeeDuration();
                    edit_fee_flag = false;
                    edit_totalfee_flag = false;
                    $('#standard_edit_total_fee').hide();
                    $('#smart_fee').hide();
                    $('#sport_fee').hide();
                    $('#exam_fee').hide();
                    $('#computer_fee').hide();
                    $('#insurance_fee').hide();
                    $('#enroll_fee').hide();
                    $('#editotherfee').show();
                    $('#edit_admission_fee').show();
                    $('#edit_tution_fee').show();
                    $('#edit_term_fee').show();
                }else if(fee_flag == '3'){
                    totalFeeget();
                    feeMonth();
                    getOtherFeeDuration();
                    edit_fee_flag = false;
                    edit_totalfee_flag = false;
                    $('#standard_edit_total_fee').hide();
                    $('#smart_fee').hide();
                    $('#sport_fee').hide();
                    $('#exam_fee').hide();
                    $('#computer_fee').hide();
                    $('#insurance_fee').hide();
                    $('#enroll_fee').hide();
                    $('#editotherfee').show();
                    $('#edit_admission_fee').show();
                    $('#edit_tution_fee').show();
                    $('#edit_term_fee').show();
                }else if(fee_flag == '4'){
                    getFirststepFee();
                    edit_totalfee_flag = true;
                    edit_fee_flag = false;
                    $('#smart_fee').hide();
                    $('#sport_fee').hide();
                    $('#exam_fee').hide();
                    $('#computer_fee').hide();
                    $('#insurance_fee').hide();
                    $('#enroll_fee').hide();
                    $('#editotherfee').hide();
                    $('#edit_admission_fee').hide();
                    $('#edit_tution_fee').hide();
                    $('#edit_term_fee').hide();
                    $('#edittotalfee').show();
                }

                if(fee_flag == '1' || fee_flag == '2' || fee_flag == '3'){
                    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                    var feePaidMonth = jQuery.inArray(dataArray.fee_month, months);
                    $('#start_fee_duration').find('option').each(function () {
                        $(this).attr('disabled', false);
                    });
                    $("#start_fee_duration").val((parseInt(feePaidMonth)+1)).trigger("change");
                    var start_month = $('#start_fee_duration :selected').val();
                    $('#start_fee_duration').find('option').each(function () {
                        var selected_month =  $(this).val();
                        if (parseInt(selected_month) !== parseInt(start_month)) {
                            $(this).attr('disabled', true);
                        }
                    });
                }else{
                    $('#start_fee_installment').find('option').each(function () {
                        $(this).attr('disabled', false);
                    });
                    $("#start_fee_installment").val(dataArray.fee_installment).trigger("change");
                }

                if(select_year != '1'){
                    $('#start_fee_installment').find('option').each(function () {
                        var selected_installment = $(this).val();
                        if (selected_installment == 'fourth') {
                            $(this).attr('disabled', true);
                        }

                    });
                    $('#end_fee_installment').find('option').each(function () {
                        var selected_installment = $(this).val();
                        if (selected_installment == 'fourth') {
                            $(this).attr('disabled', true);
                        }
                    });
                }else{
                    $('#start_fee_installment').find('option').each(function () {
                        var selected_installment = $(this).val();
                        if (selected_installment == 'fourth') {
                            $(this).attr('disabled', false);
                        }

                    });
                    $('#end_fee_installment').find('option').each(function () {
                        var selected_installment = $(this).val();
                        if (selected_installment == 'fourth') {
                            $(this).attr('disabled', false);
                        }
                    });
                }
            }

        }
    });

}
/************get fee structure year for collect fee for edit END****************/

/*Select last year collect fee start*/
$('#studentFeeYear option:last-child').attr('selected', 'selected');
/*Select last year collect fee end*/

/*Pending Fee Year Filter* Start For PDF Print Start*/
$(document).on('change','#pendingFeeYear',function(){
    var feeYear = $(this).val();
    $('#hide_feeyear_request').val(feeYear);
});
/*End*/

/*Daily Fee Year Filter* Start For PDF Print Start*/
$(document).on('change','#dailyreport_academic',function(){
    var feeYear = $(this).val();
    $('#hide_feeyear_request').val(feeYear);
});
/*End*/