<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Standard listing
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- HTML (DOM) sourced data -->
            <div id="profile_edit_message"></div>
            <div id="ajax_response"></div>
            <section id="html">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">

                                <form role='search' id="standard_search_data">
                                    <div class="row  py-1 search-container pl-1 pr-1">
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-verified">Standard :</label>
                                            <fieldset class="form-group">
                                                <select name="student_std" onchange="searchDivValue(event)"
                                                        id="student_std"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="">Select</option>
                                                    <?php foreach ($std as $row) { ?>
                                                        <option value="<?php echo $row->standard; ?>"><?php echo $row->standard; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <?php if ($_SESSION['role'] == 'super_admin') { ?>
                                            <div class="col-12 col-sm-6 col-lg-3">
                                                <label for="users-list-status">School (city)</label>
                                                <fieldset class="form-group">
                                                    <select name="std_school" id="std_school"
                                                            class="select2-data-array form-control select2-size-sm">
                                                        <option value="">Select</option>
                                                        <?php foreach ($school as $row) { ?>
                                                            <option value="<?php echo $row->id; ?>"><?php echo $row->name . ' (' . $row->city . ')'; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </fieldset>
                                            </div>
                                        <?php } ?>
                                        <div class="col-12 col-sm-6 col-lg-3 d-flex align-items-center">
                                            <button type="button" class="btn btn-success" name="search_list"
                                                    id="search_list"><i class="feather icon-search"></i> Search
                                            </button>
                                            <button type="button" class="btn btn-danger clear-btn" name="clear_list"
                                                    id="clear_list"><i class="feather icon-x"></i> Clear
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-form">Standard listing
                                    <button class="btn btn-success add-button" type="button" id="add_button"><i
                                                class="feather icon-plus"></i> Add
                                    </button>
                                </h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <div class="card-body card-dashboard">

                                    <table class="table table-striped table-bordered" id="standard_data"
                                           style="width: 100%">
                                        <thead>
                                        <tr>
                                            <?php if ($_SESSION['role'] == 'super_admin') { ?>
                                                <th>School name</th>
                                            <?php } ?>
                                            <th>Standard</th>
                                            <th>Division</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<div class="modal fade text-left modal-fullscreen" id="stdModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600" id="myModalLabel33">Add Standard</label>
            </div>
            <form method="post" id="std_form" name="std_form" class="form" novalidate>
                <div class="modal-body">
                    <div id="error">

                    </div>
                        <div id="std_fee_tbody">
                        <?php if ($_SESSION['role'] == 'super_admin') { ?>
                                    <label>School (city)<span class="error">*</span></label>
                                    <div class="form-group mb-1">
                                        <select name="standard_add_school" id="standard_add_school" class="form-control"
                                                class="required" onchange="getFeeStructure(event)">
                                            <option value="">Select</option>
                                            <?php foreach ($school as $row) { ?>
                                                <option value="<?php echo $row->id; ?>"><?php echo $row->name . ' (' . $row->city . ')'; ?></option>
                                            <?php } ?>
                                        </select>
                                        <span class="error"></span>
                                    </div>
                        <?php } else { ?>
                                    <input type="hidden" name="add_school_standard" id="add_school_standard"
                                           class="form-control"
                                           placeholder="Enter Standard" value=""/>
                        <?php } ?>
                        <input type="hidden" name="session_role" id="session_role" class="form-control"
                               value="<?php echo $_SESSION['role']; ?>"/>
                        </div>

                    <div class="row" style="margin-top: 2%;" id='fee_structure'>
                        <div class="col-md-6" id="admission_fee">
                            <div class="form-group">
                                <label>Admission Fee<span class="error">*</span></label>
                                <div class="controls">
                                    <input type="text" name="standard_add_admission_fee"
                                           id="standard_add_admission_fee" class="form-control"
                                           placeholder="Enter Admission Fee"/><span class="error"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="tution_Fee">
                            <div class="form-group">
                                <label>Tuition Fee (for one month)<span class="error">*</span></label>
                                <div class="controls">
                                    <input type="text" name="standard_add_tuition_fee" id="standard_add_tuition_fee"
                                           class="form-control" placeholder="Enter Tuition Fee"/><span class="error"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="term_Fee">
                            <div class="form-group">
                                <label>Term Fee (for one term)<span class="error">*</span></label>
                                <div class="controls">
                                    <input type="text" name="standard_add_term_fee" id="standard_add_term_fee"
                                           class="form-control" placeholder="Enter Term Fee"/><span class="error"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="otherfee">
                            <div class="form-group">
                                <label>Other Fee (for one term)<span class="error">*</span></label>
                                <div class="controls">
                                    <input type="text" name="standard_add_other_fee" id="standard_add_other_fee"
                                           class="form-control" placeholder="Enter Other Fee"/><span class="error"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="std_add" style="width: 100%;">
                        <tbody id="std_add_tbody">
                            <tr>
                                <td colspan="2">
                                    <div class="form-group mb-1">
                                        <label>Standard<span class="error">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="standard_add_std" id="standard_add_std"
                                                   class="form-control" placeholder="Enter Standard"/><span
                                                    class="error"></span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="dynamicform">
                        <span class="error" id="diverror"></span><br/>
                        <a href="#" role="button" class="btn btn-primary" name="standard_add_div" id="standard_add_div"
                           onclick="addDiv()">Add division</a><br/>
                        <span id="divisionerr" class="error"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="standard" id="standard"/>
                    <button type="submit" name="standard_submit" id="standard_submit" class="btn btn-success"
                            value="submit"/>
                    <i class="fa fa-check-square-o"></i> Submit</button>
                    <button type="reset" class="btn btn-danger" name="close" id="close" data-dismiss="modal"
                            value="reset"><i class="feather icon-x"></i> Close
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade text-left modal-fullscreen" id="editstdModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600" id="myModalLabel33">Edit Standard</label>
            </div>
            <form method="post" id="edit_std_form" name="edit_std_form" class="form" novalidate>
                <div class="modal-body">
                    <div id="error_msg">

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Select Year</label>
                                <select name="standard_fee_year" id="standard_fee_year" class="form-control"
                                        class="required" onchange="getFeeStructureYear(event)">
                                    <option value="" selected>Select</option>
                                    <?php foreach($years as $year) { ?>
                                        <option value="<?php echo $year->id ?>"><?php echo $year->year ?></option>
                                    <?php } ?>
                                </select>
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" id="edit_totalfee">
                            <div class="form-group">
                                <input type="hidden" name="standard_id"
                                       id="standard_id" class="form-control"/>
                                <label>Total Fee<span class="error">*</span></label>
                                <div class="controls">
                                    <input type="text" name="standard_edit_total_fee"
                                           id="standard_edit_total_fee" class="form-control"
                                           placeholder="Enter Total Fee"/><span></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="edit_admission_fee">
                            <div class="form-group">
                                <label>Admission Fee<span class="error">*</span></label>
                                <div class="controls">
                                    <input type="text" name="standard_edit_admission_fee"
                                           id="standard_edit_admission_fee" class="form-control"
                                           placeholder="Enter Admission Fee"/><span></span>
                                    <input type="hidden" name="hidden_std" id="hidden_std">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="edit_tution_fee">
                            <div class="form-group">
                                <label>Tuition Fee (for one month)<span class="error">*</span></label>
                                <div class="controls">
                                    <input type="text" name="standard_edit_tuition_fee"
                                           id="standard_edit_tuition_fee" class="form-control"
                                           placeholder="Enter Tuition Fee"/><span></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="edit_term_fee">
                            <div class="form-group">
                                <label>Term Fee (for one term)<span class="error">*</span></label>
                                <div class="controls">
                                    <input type="text" name="standard_edit_term_fee" id="standard_edit_term_fee"
                                           class="form-control" placeholder="Enter Term Fee"/><span></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="editotherfee">
                            <div class="form-group">
                                <label>Other Fee (for one term)<span class="error">*</span></label>
                                <div class="controls">
                                    <input type="text" name="standard_edit_other_fee" id="standard_edit_other_fee"
                                           class="form-control" placeholder="Enter Other Fee"/><span></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="smart_fee">
                            <div class="form-group">
                                <label>Smart Class Fee<span class="error">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="standard_edit_smart_fee" id="standard_edit_smart_fee"
                                               class="form-control" placeholder="Enter Smart Class Fee"/><span></span>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="computer_fee">
                            <div class="form-group">
                                <label>Computer Fee<span class="error">*</span></label>
                                <div class="controls">
                                    <input type="text" name="standard_edit_computer_fee" id="standard_edit_computer_fee" class="form-control" placeholder="Enter Computer Fee"/><span></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="sport_fee">
                            <div class="form-group">
                                <label>Sports Fee<span class="error">*</span></label>
                                <div class="controls">
                                    <input type="text" name="standard_edit_sports_fee" id="standard_edit_sports_fee"
                                           class="form-control" placeholder="Enter Sports Fee"/><span></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="insurance_fee">
                            <div class="form-group">
                                <label>Insurance Fee<span class="error">*</span></label>
                                <div class="controls">
                                    <input type="text" name="standard_edit_insurance_fee" id="standard_edit_insurance_fee" class="form-control" placeholder="Enter Insurance Fee"/><span></span>
                                    <input type="hidden" name="hidden_school_std" id="hidden_school_std">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="exam_fee">
                            <div class="form-group">
                                <label>Exam Fee<span class="error">*</span></label>
                                <div class="controls">
                                    <input type="text" name="standard_edit_exam_fee" id="standard_edit_exam_fee"
                                           class="form-control" placeholder="Enter Exam Fee"/><span></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="enroll_fee">
                            <div class="form-group">
                                <label>Enroll Fee<span class="error">*</span></label>
                                <div class="controls">
                                    <input type="text" name="standard_edit_enroll_fee" id="standard_edit_enroll_fee"
                                           class="form-control" placeholder="Enter Enroll Fee"/><span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="std_edit" style="width: 100%;">
                        <tbody id="std_edit_tbody">
                        <tr>
                            <td colspan="2">
                                <input type="hidden" name="hidden_school" id="hidden_school">
                                <div class="form-group mb-1">
                                    <label>Standard<span class="error">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="standard_edit_std" id="standard_edit_std"
                                               class="form-control" placeholder="Enter Standard"/><span></span>
                                        <input type="hidden" name="hidden_standard" id="hidden_standard">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="editdynamicform">
                        <span class="error" id="edit_error"></span><br/>
                        <a href="#" role="button" class="btn btn-primary" name="standard_edit_div"
                           id="standard_edit_div" onclick="editDiv()">Add division</a><br/>
                        <span id="editdiv_err" class="error"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" id="id"/>
                    <button type="submit" name="standard_update" id="standard_update" class="btn btn-success"
                            value="Submit"/>
                    <i class="fa fa-check-square-o"></i> Update</button>
                    <button type="reset" class="btn btn-danger" name="close_edit" id="close_edit" data-dismiss="modal"
                            value="reset"><i class="feather icon-x"></i> Close
                    </button>
                </div>
            </form>
        </div>
    </div>
</div> 
