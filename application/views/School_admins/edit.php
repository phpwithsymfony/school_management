<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url('admins/'); ?>">Admins</a>
                            </li>
                            <li class="breadcrumb-item active">Edit Admin
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div id="profile_edit_message"></div>
            <section id="basic-form-layouts">
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-form">Edit Admin</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="post" id="edit_admin" action="<?php echo base_url('admins/edit'); ?>"
                                          novalidate>
                                        <?php echo '<div class="error">' . validation_errors() . '</div>'; ?>
                                        <?php foreach ($data

                                        as $row) { ?>
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>User Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name="edit_uname" id="edit_uname"
                                                                   class="form-control"
                                                                   value="<?php echo $row->username; ?>" onblur="checknameIdEdit(event,'<?php echo $row->username; ?>')"
                                                                   placeholder="Enter Admin User Name"
                                                                   minlength="2" maxlength="20" required
                                                                   data-validation-required-message="This field is required"/><span id="emailid_editerror" class="error"></span>
                                                                   <input type="hidden" name="old_username" id="old_username"
                                                                   class="form-control"
                                                                   value="<?php echo $row->username; ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>First Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name="edit_fname" id="edit_fname"
                                                                   class="form-control"
                                                                   value="<?php echo $row->firstname; ?>" onblur="checknameIdEdit(event,'<?php echo $row->firstname; ?>')"
                                                                   placeholder="Enter Admin First Name"
                                                                   data-validation-regex-regex="([a-zA-Z])*"
                                                                   data-validation-regex-message="Only allow alphabets"
                                                                   minlength="2" maxlength="20" required
                                                                   data-validation-required-message="This field is required"/>
                                                        </div>
                                                        <input type="hidden" name="edit_id" id="edit_id"
                                                               class="form-control" value="<?php echo $row->id; ?>"
                                                               placeholder="Enter School Id" required=""/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Middle Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name="edit_mname" id="edit_mname"
                                                                   class="form-control"
                                                                   value="<?php echo $row->middlename; ?>"
                                                                   placeholder="Enter Admin Middle Name"
                                                                   data-validation-regex-regex="([a-zA-Z])*"
                                                                   data-validation-regex-message="Only allow alphabets"
                                                                   minlength="1" maxlength="20" required
                                                                   data-validation-required-message="This field is required"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Last Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name="edit_lname" id="edit_lname"
                                                                   class="form-control"
                                                                   value="<?php echo $row->lastname; ?>"
                                                                   placeholder="Enter Admin Last Name"
                                                                   data-validation-regex-regex="([a-zA-Z])*"
                                                                   data-validation-regex-message="Only allow alphabets"
                                                                   minlength="2" maxlength="20" required
                                                                   data-validation-required-message="This field is required"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Email<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name="edit_email" id="edit_email"
                                                                   class="form-control"
                                                                   value="<?php echo $row->email; ?>"

                                                                   placeholder="Enter Admin Email"
                                                                   data-validation-regex-regex="^[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"
                                                                   data-validation-regex-message="Enter valid email"
                                                                   required
                                                                   data-validation-required-message="This field is required"/>
                                                            <input type="hidden" name="old_email" id="old_email"
                                                                   class="form-control"
                                                                   value="<?php echo $row->email; ?>"/>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Contact No.<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name="edit_contact" id="edit_contact"
                                                                   value="<?php echo $row->contact_no; ?>"
                                                                   class="form-control phone-inputmask"
                                                                   placeholder="Enter Admin Contact No."
                                                                   data-validation-regex-regex="[6-9][0-9]{2}\s[0-9]{3}\s[0-9]{4}"
                                                                   data-validation-regex-message="Enter valid contact no."
                                                                   required
                                                                   data-validation-required-message="This field is required"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>City<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type='text' name='edit_city' id="edit_city"
                                                                   class="form-control"
                                                                   value="<?php echo $row->city; ?>"
                                                                   placeholder="Enter School City"
                                                                   data-validation-regex-regex="([a-zA-Z])*"
                                                                   data-validation-regex-message="Only allow alphabets"
                                                                   required
                                                                   data-validation-required-message="This field is required"
                                                                   minlength="2" maxlength="20"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Address<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <textarea name="edit_address" id="edit_address"
                                                                      class="form-control"
                                                                      placeholder="Enter Admin Address"
                                                                      required/><?php echo $row->address; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Gender<span class="error">*</span></label>
                                                        <div class="input-group controls">
                                                            <div class="mr-1">
                                                                <input type="radio" id="edit_male" name='edit_gender'
                                                                       value="m"<?php if ($row->gender == 'm') { ?> checked="checked"<?php } ?>
                                                                       required>
                                                                <label for="male">Male</label>
                                                            </div>
                                                            <div class="mr-1">
                                                                <input type="radio" id="edit_female" name="edit_gender"
                                                                       value="f"<?php if ($row->gender == 'f') { ?> checked="checked"<?php } ?> >
                                                                <label for="female">Female</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <button type="submit" class="btn btn-success" name="update" id="update"
                                                    value="submit"><i class="fa fa-check-square-o"></i>Update
                                            </button>
                                            <a href="<?php echo base_url('admins/'); ?>" class="btn btn-danger mr-1"
                                               role="button"><i class="feather icon-x"></i> Cancel</a>
                                        </div>
                                    </form>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>