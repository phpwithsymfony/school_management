<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url('admins/'); ?>">Admins</a>
                            </li>
                            <li class="breadcrumb-item active">Add Admin
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div id="profile_edit_message"></div>
            <section id="basic-form-layouts">
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-form">Add Admin</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                   	<form method="post" id="add_admin" class="form" action="<?php echo base_url('admins/add'); ?>" novalidate>
										<?php echo '<div class="error">' . validation_errors() . '</div>'; ?> 	
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>School Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <fieldset class="checkbox">
                                                            <?php foreach ($school_id as $row) {?>
                                                            <label>
                                                            <input type="checkbox" name='add_schoolname_admin[]' id="add_schoolname_admin" value="<?php echo $row->id;?>" required="" onclick="CheckSchoolList()">&nbsp;<?php echo $row->name;?><br/>
                                                            <?php } ?><span id='admin_check' class="error"></span>
                                                            </label>
                                                            </fieldset>
														</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>User Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name="add_uname" id="add_uname" class="form-control" placeholder="Enter Admin User Name" onblur="checknameId(event);" minlength="2" maxlength="20" required data-validation-required-message="This field is required"/>
                                                            <span id="email-error" class="error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>First Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                        	<input type="text" name="add_fname" id="add_fname" class="form-control" placeholder="Enter Admin First Name"  data-validation-regex-regex="([a-zA-Z])*" data-validation-regex-message="Only allow alphabets" minlength="2" maxlength="20" required data-validation-required-message="This field is required"/>
                                                    	</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Middle Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                        	<input type="text" name="add_mname" id="add_mname" class="form-control" placeholder="Enter Admin Middle Name" data-validation-regex-regex="([a-zA-Z])*" data-validation-regex-message="Only allow alphabets" minlength="1" maxlength="20" required data-validation-required-message="This field is required"/>
                                                        </div> 	
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Last Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                        	<input type="text" name="add_lname" id="add_lname" class="form-control" placeholder="Enter Admin Last Name" data-validation-regex-regex="([a-zA-Z])*" data-validation-regex-message="Only allow alphabets" minlength="2" maxlength="20" required data-validation-required-message="This field is required"/>
                                                       	</div> 	
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Email<span class="error">*</span></label>
                                                        <div class="controls">
                                                        	<input type="text" name="add_email" id="add_email" class="form-control"  placeholder="Enter Admin Email" data-validation-regex-regex="^[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"  data-validation-regex-message="Enter valid email" required data-validation-required-message="This field is required"/>
                                                        </div>	
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Contact No.<span class="error">*</span></label>
                                                        <div class="controls">
                                                        	<input type="text" name="add_contact" id="add_contact" class="form-control phone-inputmask" placeholder="Enter Admin Contact No." data-validation-regex-regex="[6-9][0-9]{2}\s[0-9]{3}\s[0-9]{4}"  data-validation-regex-message="Enter valid contact no." required data-validation-required-message="This field is required"/>
                                                        </div>	
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>City<span class="error">*</span></label>
                                                        <div class="controls">
                                                        	<input type='text' name='add_city' id="add_city" class="form-control" placeholder="Enter School City" data-validation-regex-regex="([a-zA-Z])*" data-validation-regex-message="Only allow alphabets" required data-validation-required-message="This field is required" minlength="2" maxlength="20"/>
                                                        </div> 	
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Password<span class="error">*</span></label>
                                                        <div class="controls">
                                                        	<input type='password' name='add_password' id="add_password" class="form-control" placeholder="Enter Admin Password" data-validation-regex-regex="([A-z0-9])*[^\s]\1*" data-validation-regex-message="Not allow space or special character" minlength="6" maxlength="12" required data-validation-required-message="This field is required"/>
                                                    	</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group controls">
                                                        <label>Confirm Password<span class="error">*</span></label>
                                                        <div class="controls">
                                                        	<input type='password' name='confirm_password' id="confirm_password" class="form-control" placeholder="Re-type Password" data-validation-match-match="add_password" required data-validation-required-message="This field is required"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Address<span class="error">*</span></label>
                                                        <div class="controls">
                                                        	<textarea name='add_address' id="add_address" class="form-control" placeholder="Enter Admin Address" required/></textarea>
                                                       	</div> 	
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Gender<span class="error">*</span></label>
                                                        <div class="input-group controls">
                                                            <div class="mr-1">
                                                                <input type="radio" id="add_male" name='add_gender' value="m" required>
                                                                <label  for="male">Male</label>
                                                            </div>
                                                            <div class="mr-1">
                                                                <input type="radio" id="add_female" name="add_gender" value="f">
                                                                <label  for="female">Female</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                        	<button type="submit" class="btn btn-success" name="submit" id="submit" value="submit" onsubmit="submit.disabled = true; return true;"><i class="fa fa-check-square-o"></i> Submit</button>
                                            <a href="<?php echo base_url('admins/'); ?>" class="btn btn-danger mr-1" role="button"><i class="feather icon-x"></i> Cancel</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>