<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-1">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Admins Listing
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- HTML (DOM) sourced data -->
            <div id="profile_edit_message"></div>
            <?php if ($this->session->flashdata('response')) {
                echo '<div class="alert alert-success alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            }
            if ($this->session->flashdata('response_error')) {
                echo '<div class="alert alert-danger alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response_error') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            } ?>
            <section id="html">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <form role='search' id="admin-search">
                                    <div class="row py-1 pl-1 pr-1 search-container">
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-verified">School Code</label>
                                            <fieldset class="form-group">
                                                <input type="text" name="school_id" id="school_id"
                                                       class='form-control form-control-sm'
                                                       placeholder='Type School Id'/>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">School Name</label>
                                            <fieldset class="form-group">
                                                <input type="text" name="school_name" id="school_name"
                                                       class='form-control form-control-sm'
                                                       placeholder='Type School Name'/>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Admin Name</label>
                                            <fieldset class="form-group">
                                                <input type="text" name="firstname" id="firstname"
                                                       class='form-control form-control-sm'
                                                       placeholder='Type Admin Name'/>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Email</label>
                                            <fieldset class="form-group">
                                                <input type="text" name="email" id="email"
                                                       class="form-control form-control-sm"
                                                       placeholder="Type Email">
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Contact No.</label>
                                            <fieldset class="form-group">
                                                <input type='text' name="contact_no" id="contact_no"
                                                       class='form-control form-control-sm'
                                                       placeholder='Type Contact No.'/>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">City</label>
                                            <fieldset class="form-group">
                                                <input type="text" name="city" id="city"
                                                       class="form-control form-control-sm"
                                                       placeholder="Type City">
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3 d-flex align-items-center">
                                            <button type="button" class="btn btn-success" name="find" id="find"><i
                                                        class="feather icon-search"></i> Search
                                            </button>
                                            <button type="button" class="btn btn-danger clear-btn" name="clear"
                                                    id="clear"><i class="feather icon-x"></i> Clear
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-form">Admins Listing
                                    <a href="<?php echo base_url('admins/add'); ?>"
                                       class="btn btn-success add-button"><i class="feather icon-plus"></i>Add</a></h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered" id="admin_details"
                                           style="width: 100%">
                                        <thead>
                                        <tr>
                                            <th>School Code</th>
                                            <th>School Name</th>
                                            <th>Admin Name</th>
                                            <th>Email</th>
                                            <th>Contact Number</th>
                                            <th>City</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>            