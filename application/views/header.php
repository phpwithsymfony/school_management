<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
          content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords"
          content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>School Fee Management</title>
    <link rel="apple-touch-icon" href="/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
          rel="stylesheet">


    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/icheck.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/toggle/switchery.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/pickers/datetime/bootstrap-datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/pickers/pickadate/pickadate.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/custom.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/selects/select2.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: app-assets CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/components.css">
    <!-- END: app-assets CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/validation/form-validation.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/switch.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/pickers/daterange/daterange.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/checkboxes-radios.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/web/assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu"
      data-col="2-columns">

<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto"><a
                            class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                                class="feather icon-menu font-large-1"></i></a></li>
                <li class="nav-item"><a class="navbar-brand" href="<?php echo base_url('home/') ?>"><img
                                class="brand-logo" alt="stack admin logo"
                                src="/app-assets/images/logo/stack-logo-light.png">
                        <h2 class="brand-text">School</h2>
                    </a></li>
                <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse"
                                                  data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a></li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs"
                                                              href="#"><i class="feather icon-menu"></i></a></li>
                    <?php
                    if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'admin') {
                        ?>
                        <li class="dropdown dropdown-language nav-item"><a class="dropdown-toggle nav-link"
                                                                           id="dropdown-flag" href="#"
                                                                           data-toggle="dropdown" aria-haspopup="true"
                                                                           aria-expanded="false"><i
                                        class="fa fa-university"></i><span class="selected-language"
                                                                           id="selected-school">Schools</span></a>
                            <div class="dropdown-menu"
                                 aria-labelledby="dropdown-flag"><?php foreach ($_SESSION['school_data'] as $row) { ?><a
                                    class="dropdown-item" href="#"
                                    onclick="getSchoolnameDetails('<?php echo $row->id ?>','<?php echo $row->name ?>')"
                                    data-language="en"><i class="fa fa-university"></i><?php echo $row->name; ?>
                                    </a><?php } ?></div>
                        </li>
                    <?php } ?>
                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link"
                                                                   href="#" data-toggle="dropdown">
                            <div class="avatar avatar-online"></div>
                            <span class="user-name"><?php echo $_SESSION['name']; ?></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right"><a href="#" data-toggle="modal"
                                                                          data-target="#manage_profile"
                                                                          id="'.$_SESSION['user_id'].'"
                                                                          class="dropdown-item"
                                                                          onclick="fetchAdminDetails()"><i
                                        class="feather icon-user"></i> Edit Profile</a><a href="#" data-toggle="modal"
                                                                                          data-target="#change_password"
                                                                                          class="dropdown-item"><i
                                        class="fa fa-key"></i>Change Password</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" onclick="admin_logout()"><i
                                        class="feather icon-power"></i> Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->


<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" nav-item <?php if ($url == 'home') {
                echo 'active';
            } ?>"><a href="<?php echo base_url('home/') ?>"><i class="feather icon-home"></i><span class="menu-title">Home</span></a>
            </li>
            <?php
            if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {
                ?>
                <li class=" nav-item <?php if ($url == 'admins') {
                    echo 'active';
                } ?>"><a href="<?php echo base_url('admins/') ?>"><i class="feather icon-user"></i><span
                                class="menu-title">Admins</span></a>
                </li>
                <li class=" nav-item <?php if ($url == 'schools') {
                    echo 'active';
                } ?>"><a href="<?php echo base_url('schools/') ?>"><i class="fa fa-university"></i><span
                                class="menu-title">Schools</span></a>
                </li>
            <?php } ?>
            <li class=" nav-item <?php if ($url == 'students') {
                echo 'active';
            } ?>"><a href="<?php echo base_url('students/') ?>"><i class="feather icon-users"></i><span
                            class="menu-title">Students</span></a>
            </li>
            <li class="nav-item <?php if ($url == 'fee') {
                echo 'open';
            } ?>"><a href="#"><i class="feather icon-briefcase"></i><span class="menu-title">Fee</span></a>
                <ul class="menu-content">
                    <li class="<?php if ($url == 'fee') {
                        echo 'active';
                    } ?>"><a class="menu-item" href="<?php echo base_url('fee/') ?>">Collect Fee</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item <?php if ($url == 'daily_report' || $url == 'pending_fee') {
                echo 'open';
            } ?>"><a href="#"><i class="feather icon-file-text"></i><span class="menu-title">Report</span></a>
                <ul class="menu-content">
                    <li class="<?php if ($url == 'daily_report') {
                        echo 'active';
                    } ?>"><a class="menu-item" href="<?php echo base_url('daily_report/') ?>">Daily Report</a>
                    </li>
                    <li class="<?php if ($url == 'pending_fee') {
                        echo 'active';
                    } ?>"><a class="menu-item" href="<?php echo base_url('pending_fee/') ?>">Pending Fee</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item <?php if ($url == 'standard') {
                echo 'active';
            } ?>"><a href="<?php echo base_url('standard/') ?>"><i class="fa fa-list-ol"></i><span class="menu-title">Standard</span></a>
            </li>
            <li class=" nav-item <?php if ($url == 'alumini_student') {
                echo 'active';
            } ?>"><a href="<?php echo base_url('alumini_student/') ?>"><i class="feather icon-user-minus"></i><span class="menu-title">Alumini Student</span></a>
            </li>
            <li class=" nav-item <?php if ($url == 'student_transfer') {
                echo 'active';
            } ?>"><a href="<?php echo base_url('student_transfer/') ?>"><i class="feather icon-users"></i><span class="menu-title">Student Transfer</span></a>
            </li>
            <?php
            if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {
                ?>
                <li class=" nav-item <?php if ($url == 'settings') {
                    echo 'active';
                } ?>"><a href="<?php echo base_url('settings/') ?>"><i class="fa fa-cog"></i><span
                                class="menu-title">Settings</span></a>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>

<div class="modal fade text-left" id="manage_profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600" id="myModalLabel33">Manage Profile</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" id='edit_form' name='edit_form' class="form" novalidate>
                <div class="modal-body">
                    <div class="success_message mb-1" id="message"></div>
                    <div class="form-group mb-1">
                        <label>User Name<span class="input-require">*</span></label>
                        <div class="controls">
                            <input type='text' name='update_uname_admin' id="update_uname_admin"
                                   placeholder="Enter user name" class='form-control'
                                    minlength="2" maxlength="20" onblur="checkAdminusername(event)"
                                   required data-validation-required-message="This field is required"/><span id="email-check" class="error"></span>
                        </div>
                    </div>

                    <div class="form-group mb-1">
                        <label>First Name<span class="input-require">*</span></label>
                        <div class="controls">
                            <input type='text' name='update_fname_admin' id="update_fname_admin"
                                   placeholder="Enter first name" class='form-control'
                                   data-validation-regex-regex="([a-zA-Z])*"
                                   data-validation-regex-message="Only allow alphabets" minlength="2" maxlength="15"
                                   required data-validation-required-message="This field is required"/><span></span>
                        </div>
                    </div>

                    <div class="form-group mb-1">
                        <label>Last Name<span class="input-require">*</span></label>
                        <div class="controls">
                            <input type='text' name='update_lname_admin' id="update_lname_admin"
                                   placeholder="Enter  Last name" class='form-control'
                                   data-validation-regex-regex="([a-zA-Z])*"
                                   data-validation-regex-message="Only allow alphabets" minlength="2" maxlength="15"
                                   required data-validation-required-message="This field is required"/><span></span>
                        </div>
                    </div>

                    <div class="form-group mb-1">
                        <label>Email<span class="input-require">*</span></label>
                        <div class="controls">
                            <input type='text' name='update_email' id="update_email" placeholder="Enter your email"
                                   class='form-control'
                                   data-validation-regex-regex="([a-z0-9_\+.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})"
                                   data-validation-regex-message="Enter valid email" required
                                   data-validation-required-message="This field is required"
                                   />
                            <input type='hidden' name='update_admin_email' id="update_admin_email"/>
 <input type='hidden' name='update_admin_username' id="update_admin_username"/>
                            <input type="hidden" name="hidden_email" id="hidden_email">
                        </div>
                    </div>

                    <div class="form-group mb-1">
                        <label>Gender</label>
                        <div class="input-group controls">
                            <div class="mr-1">
                                <input type="radio" value="m" name="update_gender_admin" id="update_male_admin">
                                <label for="male">Male</label>
                            </div>
                            <div class="mr-1">
                                <input type="radio" value="f" name="update_gender_admin" id="update_female_admin">
                                <label for="female">Female</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="user_id" id="user_id" value="<?php echo $_SESSION['user_id'];?>">
                    <input type="hidden" name="btn_action" id="btn_action"/>
                    <button type="submit" name="update_admin" id="update_admin" class="btn btn-success" value="submit"/>
                    <i class="fa fa-check-square-o"></i> Update</button>
                    <button class="btn btn-danger" name="close" id="close" data-dismiss="modal"><i
                                class="feather icon-x"></i> Close
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade text-left" id="change_password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600" id="myModalLabel33">Change Password</label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" id='update_password' name='update_password' class="form" novalidate>
                <div class="modal-body">
                    <div class="success_message mb-1" id="success_message"></div>
                    <div class="form-group mb-1">
                        <label>Old Password<span class="input-require">*</span></label>
                        <div class="controls">
                            <input type='password' name='oldpassword' id="oldpassword"
                                   placeholder="Enter your old password" class='form-control' required
                                   data-validation-required-message="This field is required"/><span
                                    id="check-password"></span>
                        </div>
                    </div>

                    <div class="form-group mb-1">
                        <label>New Password<span class="input-require">*</span></label>
                        <div class="controls">
                            <input type='password' name='new_password' id="new_password"
                                   placeholder="Enter your new password" class='form-control'
                                   data-validation-regex-regex="([A-z0-9])*[^\s]\1*" onblur=" checkNewPassword(event)"
                                   data-validation-regex-message="Not allow space or special character" minlength="6"
                                   maxlength="12" required
                                   data-validation-required-message="This field is required"/><span
                                    id="check-new-password"></span>
                        </div>
                    </div>

                    <div class="form-group mb-1">
                        <label>Confirm Password<span class="input-require">*</span></label>
                        <div class="controls">
                            <input type='password' name='change_confirmpassword' id="change_confirmpassword"
                                   placeholder="Re-type your new password" class='form-control'
                                   data-validation-match-match="new_password" required
                                   data-validation-required-message="This field is required"/><span></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="change_id" id="change_id" value="<?php echo $_SESSION['user_id']; ?>"/>
                    <input type="hidden" name="change_email" id="change_email"
                           value="<?php echo $_SESSION['user_name']; ?>"/>
                    <input type="hidden" name="change_btn_password" id="change_btn_password" value="Edit"/>
                    <button type="submit" name="change_password_btn" id="change_password_btn" class="btn btn-success"
                            value="Submit"/>
                    <i class="fa fa-check-square-o"></i> Update</button>
                    <button type="reset" class="btn btn-danger" name="close" id="close" data-dismiss="modal"
                            value="Close"><i class="feather icon-x"></i> Close
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>