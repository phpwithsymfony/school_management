<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Pending Fee Listing
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- HTML (DOM) sourced data -->
            <div id="profile_edit_message"></div>
            <section id="html">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <form role='search' id="pendingsearch_data">
                                    <div class="row  py-1 search-container-2 pl-1 pr-1">
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-verified">Standard :</label>
                                            <fieldset class="form-group">
                                                <select name="student_std" onchange="searchDivValue(event)"
                                                        id="student_std"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="">Select</option>
                                                    <?php foreach ($std as $row) { ?>
                                                        <option value="<?php echo $row->standard; ?>"><?php echo $row->standard; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Division</label>
                                            <fieldset class="form-group">
                                                <select name="student_div" id="student_div"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="" id="division">Select</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Medium</label>
                                            <fieldset class="form-group">
                                                <select name="pendingfee_medium" id="pendingfee_medium"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="">Select</option>
                                                    <option value="English">English</option>
                                                    <option value="Gujarati">Gujarati</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Month</label>
                                            <fieldset class="form-group">
                                                <select name="pendingfee_month" id="pendingfee_month"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="">Select</option>
                                                    <option value="June">June</option>
                                                    <option value="July">July</option>
                                                    <option value="August">August</option>
                                                    <option value="September">September</option>
                                                    <option value="October">October</option>
                                                    <option value="November">November</option>
                                                    <option value="December">December</option>
                                                    <option value="January">January</option>
                                                    <option value="February">February</option>
                                                    <option value="March">March</option>
                                                    <option value="April">April</option>
                                                    <option value="May">May</option>
                                                    <option value="first">First</option>
                                                    <option value="second">Second</option>
                                                    <option value="third">Third</option>
                                                    <option value="fourth">Fourth</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <?php if ($_SESSION['role'] == 'super_admin') { ?>
                                            <div class="col-12 col-sm-6 col-lg-3">
                                                <label for="users-list-status">School (city)</label>
                                                <fieldset class="form-group">
                                                    <select name="pendingfee_school" id="pendingfee_school"
                                                            class="select2-data-array form-control select2-size-sm">
                                                        <option value="">Select</option>
                                                        <?php foreach ($school as $row) { ?>
                                                            <option value="<?php echo $row->id; ?>"><?php echo $row->name . ' (' . $row->city . ')'; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </fieldset>
                                            </div>
                                        <?php } ?>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-verified">Fee Year :</label>
                                            <fieldset class="form-group">
                                                <select name="pendingFeeYear" id="pendingFeeYear" class="select2-data-array form-control select2-size-sm">
                                                    <option value="">Select</option>
                                                    <?php foreach ($year as $row) { ?>
                                                        <option value="<?php echo $row->id; ?>"><?php echo $row->year; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3 d-flex align-items-center">
                                            <button type="button" class="btn btn-success" name="pending_find"
                                                    id="pending_find"><i class="feather icon-search"></i> Search
                                            </button>
                                            <button type="button" class="btn btn-danger clear-btn" name="pending_clear"
                                                    id="pending_clear"><i class="feather icon-x"></i> Clear
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title report-header" id="basic-layout-form"><span
                                            class="report_heading">Pending Fee Listing</span>
                                    <div class="download-pdf">
                                        <form method="post" action="<?php echo base_url('pending_fee/convertpdf') ?>">
                                            <input type="hidden" name="hide_std" id="hide_std"/>
                                            <input type="hidden" name="hide_div" id="hide_div"/>
                                            <input type="hidden" name="hide_medium" id="hide_medium"/>
                                            <input type="hidden" name="hide_feemonth" id="hide_feemonth"/>
                                            <input type="hidden" name="hide_school" id="hide_school"/>
                                            <input type="hidden" name="hide_feeyear" id="hide_feeyear"/>
                                            <input type="hidden" name="hide_request_year" id="hide_feeyear_request">
                                            <button type="submit" name="submit" class="btn btn-success"
                                                    value="Download PDF" formtarget="_blank"><i
                                                        class="fa fa-file-pdf-o"></i>&nbsp;Download PDF
                                            </button>
                                        </form>
                                    </div>
                                </h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered" id="pending_list"
                                           style="width: 100%">
                                        <thead>
                                        <tr>
                                            <th>GR. No.</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Contact No.</th>
                                            <th>Standard</th>
                                            <th>Medium</th>
                                            <th>Remaining Fee</th>
                                            <th>Fee Details</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>            