<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Manage Student Data
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div id="success_delete_all"></div>
            <div id="success_delete_data"></div>
            <?php if ($this->session->flashdata('response')) {
                echo '<div class="alert alert-success alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            } ?>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form">Manage Student Data</h4>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collpase show">
                            <div class="card-body card-dashboard">
                                <div class="row">
                                    <div class="col-md-6">
                                        <form method="post" id="import_csv" enctype="multipart/form-data">
                                            <h4>Upload CSV</h4>
                                            <fieldset class="form-group">
                                                <!-- <label for="basicInputFile">Upload CSV</label> -->
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="csv_file"
                                                           id="csv_file" required accept=".csv">
                                                    <label class="custom-file-label" for="csv_file">Choose file</label>
                                                    <button type="submit" name="import_csv" class="btn btn-info mt-1"
                                                            id="import_csv_btn">Import CSV
                                                    </button>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-content collpase show">
                            <div class="card-body card-dashboard">
                                <div class="row">
                                    <div class="col-md-6">
                                        <form method="post" id="clear_all_student">
                                            <h4>Clear All data of student</h4>
                                            <div class="form-group">
                                                <div class="controls">
                                                    <button type="submit" name="clear_all_student_data"
                                                            class="btn btn-info mt-1" id="clear_all_student_data"
                                                            onclick="clear_all()">Clear All
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-content collpase show">
                            <div class="card-body card-dashboard">
                                <form method="post" id="clear_standard_wise">
                                    <h4>Standard Wise Delete Data</h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>School<span class="error">*</span></label>
                                                <div class="controls">
                                                    <select class="select2-data-array form-control"
                                                            name="clear_student_school" id="clear_student_school"
                                                            required="">
                                                        <option value="">Select</option>
                                                        <?php foreach ($school as $row) { ?>
                                                            <option
                                                            value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option><?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Standard<span class="error">*</span></label>
                                                <div class="controls">
                                                    <select class="select2-data-array form-control"
                                                            name="clear_student_standard" id="clear_student_standard"
                                                            required="">
                                                        <option value="">Select</option>
                                                        <?php foreach ($standard as $row) { ?>
                                                            <option
                                                            value="<?php echo $row->id; ?>"><?php echo $row->standard; ?></option><?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" name="clear_standard_wise_btn" class="btn btn-info mt-1"
                                            onclick="clear_std_wise()" id="clear_standard_wise_btn">Clear
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </section>
        </div>
    </div>
</div>