<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Daily Report Listing
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- HTML (DOM) sourced data -->
            <div id="profile_edit_message"></div>
            <div id="confirm_delete_message"></div>
            <?php if ($this->session->flashdata('response')) {
                echo '<div class="alert alert-success alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            }
            if ($this->session->flashdata('response_error')) {
                echo '<div class="alert alert-danger alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response_error') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            } ?>
            <section id="html">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <form role='search' id="dailysearch_data">
                                    <div class="row  py-1 search-container pl-1 pr-1">
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-verified">Standard :</label>
                                            <fieldset class="form-group">
                                                <select name="dailyreport_student_std" onchange="dailyreportsearchDivValue(event)"
                                                        id="dailyreport_student_std"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="">Select</option>
                                                    <?php foreach ($std as $row) { ?>
                                                        <option value="<?php echo $row->standard; ?>"><?php echo $row->standard; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Division</label>
                                            <fieldset class="form-group">
                                                <select name="dailyreport_student_div" id="dailyreport_student_div"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="" id="division">Select</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Start Dt.</label>
                                            <fieldset class="form-group">
                                                <select name="dailyreport_start_date" id="dailyreport_start_date"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="">Select</option>
                                                    <?php foreach ($date as $row) { ?>
                                                        <option value="<?php echo $row; ?>"><?php echo date("d-m-Y", strtotime($row)) ; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">End Dt.</label>
                                            <fieldset class="form-group">
                                                <select name="dailyreport_end_date" id="dailyreport_end_date"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="">Select</option>
                                                    <?php foreach ($date as $row) { ?>
                                                        <option value="<?php echo $row ?>"><?php echo date("d-m-Y", strtotime($row)); ?></option>
                                                    <?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Receipt No.</label>
                                            <fieldset class="form-group">
                                                <input name="dailyreport_receipt_no" id="dailyreport_receipt_no"
                                                        class="form-control form-control form-control-sm" placeholder="Enter Receipt No.">
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Academic Year</label>
                                            <fieldset class="form-group">
                                                <select name="dailyreport_academic" id="dailyreport_academic"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="" selected>Select</option>
                                                    <?php foreach($years as $year) { ?>
                                                        <option value="<?php echo $year->id ?>"><?php echo $year->year ?></option>
                                                    <?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <?php if ($_SESSION['role'] == 'super_admin') { ?>
                                            <div class="col-12 col-sm-6 col-lg-3">
                                                <label for="users-list-status">School (city)</label>
                                                <fieldset class="form-group">
                                                    <select name="dailyreport_student_school" id="dailyreport_student_school"
                                                            class="select2-data-array form-control select2-size-sm">
                                                        <option value="">Select</option>
                                                        <?php foreach ($school as $row) { ?>
                                                            <option value="<?php echo $row->id; ?>"><?php echo $row->name . ' (' . $row->city . ')'; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </fieldset>
                                            </div>
                                        <?php } ?>
                                        <div class="col-12 col-sm-6 col-lg-3 d-flex align-items-center">
                                            <button type="button" class="btn btn-success" name="dailyreport_find"
                                                    id="dailyreport_find"><i class="feather icon-search"></i> Search
                                            </button>
                                            <button type="button" class="btn btn-danger clear-btn"
                                                    name="dailyreport_clear" id="dailyreport_clear"><i
                                                        class="feather icon-x"></i> Clear
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title report-header" id="basic-layout-form"><span
                                            class="report_heading">Daily Report Listing</span>
                                    <div class="download-pdf">
                                        <form method="post" action="<?php echo base_url('daily_report/paidfeepdf') ?>">
                                            <input type="hidden" name="dailyreport_hidden_std" id="dailyreport_hidden_std"/>
                                            <input type="hidden" name="dailyreport_hidden_div" id="dailyreport_hidden_div"/>
                                            <input type="hidden" name="dailyreport_hidden_startdate" id="dailyreport_hidden_startdate"/>
                                            <input type="hidden" name="dailyreport_hidden_enddate" id="dailyreport_hidden_enddate"/>
                                            <input type="hidden" name="dailyreport_hidden_school" id="dailyreport_hidden_school"/>
                                            <input type="hidden" name="dailyreport_hidden_academic" id="dailyreport_hidden_academic"/>
                                            <input type="hidden" name="dailyreport_hidden_receipt_no" id="dailyreport_hidden_receipt_no"/>
                                            <input type="hidden" name="hide_request_year" id="hide_feeyear_request">
                                            <button type="submit" name="submit" class="btn btn-success"
                                                    value="Download PDF" formtarget="_blank"><span
                                                        class="fa fa-file-pdf-o"></span>&nbsp;Download PDF
                                            </button>
                                        </form>
                                    </div>
                                </h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered" id="daily_report"
                                           style="width: 100%">
                                        <thead>
                                        <tr>
                                            <th width="10%">No.</th>
                                            <th width="10%">G.R. No.</th>
                                            <th>Name</th>
                                            <th>Standard</th>
                                            <th>Medium</th>
                                            <th>Receipt No.</th>
                                            <th>View Fee</th>
                                            <th>Fee Details</th>
                                            <th>Fee Collection</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>            