<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url('schools/'); ?>">Schools</a>
                            </li>
                            <li class="breadcrumb-item active">Edit Schools
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div id="profile_edit_message"></div>
            <section id="basic-form-layouts">
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-form">Edit School</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="post" id="edit_school" action="<?php echo base_url('schools/edit');?>" novalidate>
                                        <?php echo '<div class="error">' . validation_errors() . '</div>'; ?>
                                        <?php foreach ($data as $row) { ?>  
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>School Code<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input name="edit_schoolid" id="edit_schoolid" class="form-control" value="<?php echo $row->school_code; ?>" onblur="checkschoolIdEdit(event,<?php echo $row->school_code; ?>)"  data-validation-regex-regex="^[1-9][0-9]*$" data-validation-regex-message="Only allow numeric value" minlength="2" maxlength="6" required data-validation-required-message="This field is required"/>
                                                            <span id="schoolid_editerror" class="error"></span>
                                                            <input type="hidden" name="edit_id" id="edit_id" class="form-control" value="<?php echo $row->id; ?>" placeholder="Enter School Id" required="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text"  name="edit_schoolname" id="edit_schoolname" class="form-control" value="<?php echo $row->name; ?>" minlength="2" maxlength="100" data-validation-regex-regex="^[^-\s][a-zA-Z0-9_@+\s-.]+$"data-validation-regex-message="Enter valid school name" required data-validation-required-message="This field is required"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Email<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name="edit_schoolemail" id="edit_schoolemail" class="form-control" value="<?php echo $row->email; ?>" onblur="checkSchoolEditEmailId(event);"placeholder="Enter Admin Email" data-validation-regex-regex="^[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"  data-validation-regex-message="Enter valid email" required data-validation-required-message="This field is required"/>
                                                            <input type="hidden" name="hidden_school_email" id="hidden_school_email" value="<?php echo $row->email; ?>">
                                                            <span id="edit_school_email" class="error"></span>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Contact No.<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name="edit_schoolcontact" id="edit_schoolcontact" value="<?php echo $row->contact; ?>" class="form-control phone-inputmask" placeholder="Enter Admin Contact No." data-validation-regex-regex="[6-9][0-9]{2}\s[0-9]{3}\s[0-9]{4}"  data-validation-regex-message="Enter valid contact no." required data-validation-required-message="This field is required"/>
                                                        </div>  
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                 <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Address<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <textarea name="edit_schooladdress" id="edit_schooladdress" class="form-control" placeholder="Enter School Address" required/><?php echo $row->address; ?></textarea>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>City<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type='text' name='edit_schoolcity' id="edit_schoolcity" class="form-control" value="<?php echo $row->city; ?>" placeholder="Enter School City" data-validation-regex-regex="([a-zA-Z])*" data-validation-regex-message="Only allow alphabets" required data-validation-required-message="This field is required" minlength="2" maxlength="20"/>
                                                        </div>  
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                 <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>State<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type='text' name='edit_schoolstate' id="edit_schoolstate" class="form-control" value="<?php echo $row->state; ?>" placeholder="Enter School City" data-validation-regex-regex="([a-zA-Z])*" data-validation-regex-message="Only allow alphabets" required data-validation-required-message="This field is required" minlength="2" maxlength="20"/>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Fee Structure<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="hidden" value="<?php echo $row->feestructure_flag; ?>" name="old_feeflag" id="old_feeflag">
                                                            <select class="select2-data-array form-control" name="edit_feestrcture" id="edit_feestrcture" required="">
                                                                    <option value="" <?php if($row->feestructure_flag == '0') { ?> selected="selected"<?php } ?> >Select</option>
                                                                <?php foreach ($feedata as $rowflag) { ?>
                                                                    <option value="<?php echo $rowflag->flag; ?>" <?php if($row->feestructure_flag == $rowflag->flag) { ?> selected="selected"<?php } ?> ><?php echo $rowflag->name; ?></option><?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="form-actions right">
                                            <button type="submit" class="btn btn-success" name="school_update" id="school_update" value="submit"><i class="fa fa-check-square-o"></i> Update</button>
                                            <a href="<?php echo base_url('schools/');?>" class="btn btn-danger mr-1" role="button"><i class="feather icon-x"></i> Cancel</a>
                                        </div>
                                    </form>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>