<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url('schools/'); ?>">Schools</a>
                            </li>
                            <li class="breadcrumb-item active">Add School
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div id="profile_edit_message"></div>
            <section id="basic-form-layouts">
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-form">Add School</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="post" id="add_school" action="<?php echo base_url('schools/add');?>" novalidate>
                                        <?php echo '<div class="error">' . validation_errors() . '</div>'; ?>   
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>School Code<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name="add_schoolid" id="add_schoolid" class="form-control" placeholder="Enter School Id" onblur="checkschoolId(event);" data-validation-regex-regex="^[1-9][0-9]*$" data-validation-regex-message="Only allow numeric value & not start with 0" minlength="2" maxlength="6" required data-validation-required-message="This field is required"/>
                                                            <span class="error" id="schoolid_error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text"  name="add_schoolname" id="add_schoolname" class="form-control" placeholder="Enter School Name" minlength="2" maxlength="60" data-validation-regex-regex="^[^-\s][a-zA-Z0-9_@+\s-.]+$"data-validation-regex-message="Enter valid school name" required data-validation-required-message="This field is required"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                 <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Email<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text"  name="add_schoolemail" id="add_schoolemail" class="form-control" placeholder="Enter School Email" onblur="checkSchoolEmailId(event);"data-validation-regex-regex="^[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"  data-validation-regex-message="Enter valid email" required data-validation-required-message="This field is required"/>
                                                            <span id="school-email-check" class="error"></span>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Contact No.<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name="add_schoolcontact" id="add_schoolcontact" class="form-control phone-inputmask" placeholder="Enter School Contact No." data-validation-regex-regex="[6-9][0-9]{2}\s[0-9]{3}\s[0-9]{4}"  data-validation-regex-message="Enter valid contact no." required data-validation-required-message="This field is required"/>
                                                        </div>  
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Address<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <textarea name="add_schooladdress" id="add_schooladdress" class="form-control" placeholder="Enter School Address" required/></textarea>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>City<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type='text'  name='add_schoolcity' id="add_schoolcity" class="form-control" placeholder="Enter School City" data-validation-regex-regex="([a-zA-Z])*" data-validation-regex-message="Only allow alphabets" required data-validation-required-message="This field is required" minlength="2" maxlength="20"/>
                                                        </div>  
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>State<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type='text' name='add_schoolstate' id="add_schoolstate" class="form-control" placeholder="Enter School State" data-validation-regex-regex="([a-zA-Z])*" data-validation-regex-message="Only allow alphabets" required data-validation-required-message="This field is required" minlength="2" maxlength="20"/>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Fee Structure<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <select class="select2-data-array form-control" name="school_feestrcture" id="school_feestrcture" required="">
                                                                <option value="">Select</option>
                                                            <?php foreach ($feedata as $row) { ?>
                                                                <option value="<?php echo $row->flag; ?>"><?php echo $row->name; ?></option><?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="form-actions right">
                                            <button type="submit" class="btn btn-success" name="school_submit" id="school_submit" value="submit"><i class="fa fa-check-square-o"></i> Submit</button>
                                            <a href="<?php echo base_url('schools/');?>" class="btn btn-danger mr-1" role="button"><i class="feather icon-x"></i> Cancel</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>