<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->
<footer class="footer footer-light navbar-border fixed-bottom">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span
                class="float-md-left d-block d-md-inline-block">Copyright © 2020 | All Rights Reserved.</span></p>
</footer>
<!-- END: Footer-->

<script src="/web/assets/vendor/components/jquery/jquery.min.js"></script>
<!-- BEGIN: Vendor JS-->
<script src="/app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js"></script>
<script src="/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
<script src="/app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
<script src="/app-assets/vendors/js/forms/toggle/switchery.min.js"></script>
<script src="/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="/app-assets/vendors/js/forms/extended/typeahead/typeahead.bundle.min.js"></script>
<script src="/app-assets/vendors/js/forms/extended/typeahead/bloodhound.min.js"></script>
<script src="/app-assets/vendors/js/forms/extended/typeahead/handlebars.js"></script>
<script src="/app-assets/vendors/js/forms/extended/inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="/app-assets/vendors/js/forms/extended/maxlength/bootstrap-maxlength.js"></script>
<script src="/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
<script src="/app-assets/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js"></script>
<script src="/app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="/app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="/app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="/app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="/app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="/app-assets/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
<script src="/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="/app-assets/js/core/app-menu.js"></script>
<script src="/app-assets/js/core/app.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="/app-assets/js/scripts/forms/validation/form-validation.js"></script>
<!-- <script src="/app-assets/js/scripts/tables/datatables-extensions/datatables-sources.js"></script> -->
<script type="text/javascript" src="/web/js/custom.js"></script>
<script src="/app-assets/js/scripts/forms/extended/form-typeahead.js"></script>
<script src="/app-assets/js/scripts/forms/extended/form-inputmask.js"></script>
<script src="/app-assets/js/scripts/forms/extended/form-maxlength.js"></script>
<script src="/app-assets/js/scripts/pickers/dateTime/bootstrap-datetime.js"></script>
<script src="/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
<script src="/app-assets/js/scripts/forms/checkbox-radio.js"></script>
<script src="/app-assets/js/scripts/forms/form-repeater.js"></script>
<script src="/app-assets/js/scripts/forms/select/form-select2.js"></script>
<script src="/app-assets/js/scripts/forms/custom-file-input.js"></script>
<script src="/app-assets/js/scripts/modal/components-modal.js"></script>
<!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>