<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url('students/'); ?>">Students</a>
                            </li>
                            <li class="breadcrumb-item active">Add Students
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div id="profile_edit_message"></div>
            <section id="basic-form-layouts">
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-form">Add Student</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="post" id="add_student"
                                          action="<?php echo base_url('students/add'); ?>" novalidate>
                                        <?php echo '<div class="error">' . validation_errors() . '</div>'; ?>
                                        <div class="form-body">
                                            <div class="row">
                                                <?php if ($_SESSION['role'] == 'admin') { ?>
                                                    <div class="col-md-6">
                                                        <input type='hidden' name='add_studentschoolid'
                                                               id="add_studentschoolid"
                                                               value="<?php echo $school_data[0]->school_code; ?>"
                                                               class="form-control" required readonly=""/>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type='hidden' name='add_studentschoolname'
                                                               id="add_studentschoolname"
                                                               value="<?php echo $school_data[0]->name; ?>"
                                                               class="form-control" readonly=""/>
                                                        <input type='hidden' name='add_studentid' id="add_studentid"
                                                               class="form-control"
                                                               value="<?php echo $school_data[0]->id; ?>" required
                                                               readonly=""/>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>School Code (name)<span
                                                                        class="error">*</span>:</label>
                                                            <div class="controls">
                                                                <select class="select2-data-array form-control"
                                                                        onchange="GetSchoolValue(event),GetStdValue(event)"
                                                                        name="add_studentschoolid" required="">
                                                                    <option value="">Select</option>
                                                                    <?php foreach ($school_id as $row) { ?>
                                                                        <option value="<?php echo $row->school_code; ?>"><?php echo $row->school_code . ' (' . $row->name . ')'; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>School Name :</label>
                                                            <div class="controls">
                                                                <input type='text' name='add_studentschoolname'
                                                                       id="add_studentschoolname" class="form-control"
                                                                       readonly=""/>
                                                                <input type='hidden' name='add_studentid'
                                                                       id="add_studentid" class="form-control"
                                                                       readonly=""/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Standard<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <select class="select2-data-array form-control"
                                                                    onchange="DivValue(event)" name="student_standard"
                                                                    id="student_standard" required="">
                                                                <?php if ($_SESSION['role'] == 'admin') { ?>
                                                                    <option value="">Select</option>
                                                                    <?php foreach ($standard as $row) { ?>
                                                                        <option value="<?php echo $row->standard; ?>"><?php echo $row->standard; ?></option>
                                                                    <?php } ?>
                                                                <?php } else { ?>
                                                                    <option value="">Select</option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Division<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <select class="select2-data-array form-control"
                                                                    name="student_division" id="student_division"
                                                                    required="">
                                                                <option value="">Select</option>
                                                            </select>
                                                            <input type='hidden' name='add_studentstdid'
                                                                   id='add_studentstdid' class='form-control'
                                                                   placeholder='Enter first name'/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>G.R.Number<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name='add_studentgrno'
                                                                   id='add_studentgrno' class="form-control"
                                                                   placeholder="Enter  G.R. Number"
                                                                   data-validation-regex-regex="([0-9])*"
                                                                   data-validation-regex-message="Only allow numeric"
                                                                   minlength="1" maxlength="7" required
                                                                   data-validation-required-message="This field is required"
                                                                   />
                                                            <span id="student_gr_check" class="error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name='add_studentname'
                                                                   id='add_studentname' class="form-control"
                                                                   placeholder="Enter  First Name"
                                                                   data-validation-regex-regex="([a-zA-Z])*"
                                                                   data-validation-regex-message="Only allow alphabets"
                                                                   minlength="2" maxlength="20" required
                                                                   data-validation-required-message="This field is required"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Surname<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name='add_studentsurname'
                                                                   id='add_studentsurname' class="form-control"
                                                                   placeholder="Enter Surname"
                                                                   data-validation-regex-regex="([a-zA-Z])*"
                                                                   data-validation-regex-message="Only allow alphabets"
                                                                   minlength="2" maxlength="20" required
                                                                   data-validation-required-message="This field is required"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Father Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name='add_studentfname'
                                                                   id='add_studentfname' class="form-control"
                                                                   placeholder="Enter  Father Name"
                                                                   data-validation-regex-regex="([a-zA-Z])*"
                                                                   data-validation-regex-message="Only allow alphabets"
                                                                   minlength="2" maxlength="20" required
                                                                   data-validation-required-message="This field is required"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Mother Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name='add_studentmname'
                                                                   id='add_studentmname' class="form-control"
                                                                   placeholder="Enter Mother Name"
                                                                   data-validation-regex-regex="([a-zA-Z])*"
                                                                   data-validation-regex-message="Only allow alphabets"
                                                                   minlength="2" maxlength="20" required
                                                                   data-validation-required-message="This field is required"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <div class="controls">
                                                            <input type="text" name='add_studentemail'
                                                                   id="add_studentemail" class="form-control"
                                                                   placeholder="Enter Email"
                                                                   onblur="checkStudentEmailId(event)"
                                                                   data-validation-regex-regex="^[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"
                                                                   data-validation-regex-message="Enter valid email"/>
                                                            <span id="student_email_check" class="error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Contact No.<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name='add_studentcontact'
                                                                   id="add_studentcontact"
                                                                   class="form-control phone-inputmask"
                                                                   placeholder="Enter Contact No."
                                                                   data-validation-regex-regex="[6-9][0-9]{2}\s[0-9]{3}\s[0-9]{4}"
                                                                   data-validation-regex-message="Enter valid contact no."
                                                                   required
                                                                   data-validation-required-message="This field is required"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Alter Contact No.</label>
                                                        <div class="controls">
                                                            <input type="text" name='add_studentaltercontact'
                                                                   id="add_studentaltercontact"
                                                                   class="form-control phone-inputmask"
                                                                   placeholder="Enter Alter Contact No."
                                                                   data-validation-regex-regex="[6-9][0-9]{2}\s[0-9]{3}\s[0-9]{4}"
                                                                   data-validation-regex-message="Enter valid alter contact no."/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Birth Date<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" class="form-control pickadate-selectors picker__input picker__input--active" placeholder="Enter Birthdate" readonly="" id="P26319154" name='add_studentbirthdate' id="add_studentbirthdate" aria-haspopup="true" aria-readonly="false" aria-owns="P26319154_root" required data-validation-required-message="This field is required">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Medium<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <select class="select2-data-array form-control" name="add_studentmedium" id="add_studentmedium" required="">
                                                                <option value="">Select</option>
                                                                <option value="English">English</option>
                                                                <option value="Gujarati">Gujarati</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Stream</label>
                                                        <div class="controls">
                                                            <select class="select2-data-array form-control"
                                                                    name="add_studentstream" id="add_studentstream">
                                                                <option value="">Select</option>
                                                                <option value="Science">Science</option>
                                                                <option value="Commerce">Commerce</option>
                                                                <option value="Arts">Arts</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Category<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <select class="select2-data-array form-control"
                                                                    name="add_studentcategory" id="add_studentcategory"
                                                                    required="">
                                                                <option value="">Select</option>
                                                                <option value="General">General</option>
                                                                <option value="RTE">RTE</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Section</label>
                                                        <div class="controls">
                                                            <input type="text" name='add_studentsection'
                                                                   id="add_studentsection" class="form-control"
                                                                   placeholder="Enter Section"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Caste<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <select class="select2-data-array form-control"
                                                                    name="add_studentcaste" id="add_studentcaste"
                                                                    required="">
                                                                <option value="">Select</option>
                                                                <option value="General">General</option>
                                                                <option value="Open">Open</option>
                                                                <option value="Bhill-S.T.">Bhill-S.T.</option>
                                                                <option value="Chhara">Chhara</option>
                                                                <option value="S.C.">S.C.</option>
                                                                <option value="S.E.B.C.">S.E.B.C.</option>
                                                                <option value="S.T.">S.T.</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Admission Date<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" class="form-control pickadate-selectors picker__input picker__input--active" placeholder="Enter Admission Date" readonly="" id="P26319154" name='add_studentadmissiondate' id="add_studentadmissiondate" aria-haspopup="true" aria-readonly="false" aria-owns="P26319154_root" required data-validation-required-message="This field is required">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Birthplace</label>
                                                        <div class="controls">
                                                            <input type="text" name="add_birthplace" id="add_birthplace"
                                                                      class="form-control" placeholder="Enter Birthplace" data-validation-regex-regex="([a-zA-Z])*" data-validation-regex-message="Only allow alphabets" maxlength="20"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Address<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <textarea name="add_studentaddress" id="add_studentaddress"
                                                                      class="form-control" placeholder="Enter Address"
                                                                      required/></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Gender<span class="error">*</span></label>
                                                        <div class="input-group controls">
                                                            <div class="mr-1">
                                                                <input type="radio" id="add_studentmale"
                                                                       name="add_studentgender" value="m" required>
                                                                <label for="male">Male</label>
                                                            </div>
                                                            <div class="mr-1">
                                                                <input type="radio" id="add_studentfemale"
                                                                       name="add_studentgender" value="f">
                                                                <label for="female">Female</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-success" name="add_student"
                                                        id="student_add" value="submit"><i
                                                            class="fa fa-check-square-o"></i> Submit
                                                </button>
                                                <a href="<?php echo base_url('students/'); ?>"
                                                   class="btn btn-danger mr-1" role="button"><i
                                                            class="feather icon-x"></i> Cancel</a>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>