<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url('students/'); ?>">STUDENTS</a>
                            </li>
                            <li class="breadcrumb-item active">Edit Student
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div id="profile_edit_message"></div>
            <section id="basic-form-layouts">
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-form">Edit Student</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="post" id="update_student" action="<?php echo base_url('students/edit');?>" novalidate>
                                        <?php echo '<div class="error">' . validation_errors() . '</div>'; ?>
                                        <?php foreach ($data as $row) { ?>  
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Standard<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <select class="select2-data-array form-control"
                                                                    onchange="EditDivValue(event)" name="student_std"
                                                                    id="student_std" required="">
                                                                <option value="">Select</option>
                                                                    <?php foreach ($stdresult as $std) { ?>
                                                                        <option value="<?php echo $std->standard; ?>" <?php if($student_std == $std->standard) { ?> selected="selected"<?php } ?>><?php echo $std->standard; ?></option><?php }?>
                                                            </select>
                                                            <input type="hidden" name="oldstudentstd" id="oldstudentstd" value="<?php echo $row->standard_id;?>">
                                                            <input type="hidden" name="oldremainingfee" id="oldremainingfee" value="<?php echo $row->remaining_fee;?>">
                                                            <input type="hidden" name="admission_year" id="admission_year" value="<?php echo $row->admission_year;?>">
                                                            <input type="hidden" name="category" id="category" value="<?php echo $row->category;?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Division<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <select class="select2-data-array form-control"
                                                                    name="editstudent_div" id="editstudent_div"
                                                                    required="">
                                                                <option value="">Select</option>
                                                                <option value="<?php echo $row->divison;?>"selected="selected"><?php echo $row->divison; ?></option>
                                                            </select>
                                                            <input type="hidden" name="edit_studentstdid" id="edit_studentstdid" class="form-control" value="<?php echo $row->standard_id; ?>" required=""/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>GR Number</label>
                                                        <div class="controls">
                                                            <input type="text"  name='update_grno' id='update_grno' class='form-control' placeholder="Enter GR Number" value="<?php echo $row->gr_number; ?>" data-validation-regex-regex="([0-9])*" data-validation-regex-message="Only allow numeric" minlength="1" maxlength="7"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>First Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text"  name='update_name' id='update_name' class='form-control' placeholder='Enter First Name' value="<?php echo $row->firstname; ?>" data-validation-regex-regex="([a-zA-Z])*" data-validation-regex-message="Only allow alphabets" minlength="2" maxlength="20" required data-validation-required-message="This field is required"/>
                                                        </div>
                                                        <input type="hidden" name="edit_studentschoolid" id="edit_studentschoolid" class="form-control" value="<?php echo $row->school_id; ?>"  required=""/>
                                                        <input type="hidden" name="edit_id" id="edit_id" class="form-control" value="<?php echo $row->id; ?>"  required=""/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Surname<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text"  name='update_surname' id='update_surname' class='form-control' placeholder="Enter Surname" value="<?php echo $row->surname; ?>" data-validation-regex-regex="([a-zA-Z])*" data-validation-regex-message="Only allow alphabets" minlength="2" maxlength="20" required data-validation-required-message="This field is required"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Father Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name='update_fname' id='update_fname' class='form-control' placeholder='Enter Father Name' value="<?php echo $row->father_name; ?>" data-validation-regex-regex="([a-zA-Z])*" data-validation-regex-message="Only allow alphabets" minlength="2" maxlength="20" required data-validation-required-message="This field is required"/>
                                                        </div>  
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Mother Name<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name='update_mname' id='update_mname' class='form-control' placeholder="Enter Mother Name" value="<?php echo $row->mother_name; ?>" data-validation-regex-regex="([a-zA-Z])*" data-validation-regex-message="Only allow alphabets" minlength="2" maxlength="20" required data-validation-required-message="This field is required"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <div class="controls">
                                                            <input type="text"  name='update_email' id="update_email" class='form-control' onblur="checkStudentEditEmailId(event)" placeholder="Enter Email" value="<?php echo $row->email; ?>" data-validation-regex-regex="^[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"  data-validation-regex-message="Enter valid email" />
                                                            <input type="hidden" name="hidden_student_email" id="hidden_student_email" value="<?php echo $row->email; ?>">
                                                            <span id="student_edit_email" class="error"></span>
                                                        </div>  
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Contact No.<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name='update_contact' id="update_contact" value="<?php echo $row->contact_no; ?>" class="form-control phone-inputmask" placeholder="Enter Contact No." data-validation-regex-regex="[6-9][0-9]{2}\s[0-9]{3}\s[0-9]{4}"  data-validation-regex-message="Enter valid contact no." required data-validation-required-message="This field is required"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Medium</label>
                                                        <div class="controls">
                                                            <select name="update_medium" id="update_medium" class="select2-data-array form-control">
                                                                <option value="English"<?php if($row->medium == 'English') { ?> selected="selected"<?php } ?>>English</option>
                                                                <option value="Gujarati"<?php if($row->medium == 'Gujarati') { ?> selected="selected"<?php } ?>>Gujarati</option>
                                                            </select>
                                                        </div>  
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Stream</label>
                                                        <div class="controls">
                                                            <select name="update_stream" id="update_stream" class="select2-data-array form-control">
                                                                <option value="Science"<?php if($row->stream == '') { ?> selected="selected"<?php } ?>>Select</option>
                                                                <option value="Science"<?php if($row->stream == 'Science') { ?> selected="selected"<?php } ?>>Science</option>
                                                                <option value="Commerce"<?php if($row->stream == 'Commerce') { ?> selected="selected"<?php } ?>>Commerce</option>
                                                                <option value="Arts"<?php if($row->stream == 'Arts') { ?> selected="selected"<?php } ?>>Arts</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Fees Category</label>
                                                        <div class="controls">
                                                            <select name="update_category" id="update_category" class="select2-data-array form-control">
                                                                <option value="General"<?php if($row->category == 'General') { ?> selected="selected"<?php } ?>>General</option>
                                                                <option value="RTE"<?php if($row->category == 'RTE') { ?> selected="selected"<?php } ?>>RTE</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Admission Date</label>
                                                        <div class="controls">
                                                            <input type="text" class="form-control pickadate-selectors picker__input picker__input--active" placeholder="Enter Admission Date" readonly="" id="P26319154" name='edit_studentadmissiondate' id="edit_studentadmissiondate" aria-haspopup="true" aria-readonly="false" aria-owns="P26319154_root" value="<?php if($row->admission_date != '0000-00-00'){echo $row->admission_date;}?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Address<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <textarea name="update_address" id="update_address" class="form-control" placeholder="Enter Address" required/><?php echo $row->address;?></textarea>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Gender<span class="error">*</span></label>
                                                        <div class="input-group controls">
                                                            <div class="mr-1">
                                                                <input type="radio" id="edit_male" name='update_gender'value="m"<?php if ($row->gender == 'm') { ?> checked="checked"<?php } ?> required>
                                                                <label  for="male">Male</label>
                                                            </div>
                                                            <div class="mr-1">
                                                                <input type="radio" id="edit_female" name="update_gender" value="f"<?php if ($row->gender == 'f') { ?> checked="checked"<?php } ?> >
                                                                <label  for="female">Female</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="form-actions right">
                                            <input type="hidden" value="<?php echo $row->id; ?>" name="student_id" id="student_id">
                                            <button type="submit" class="btn btn-success" name="update_student" id="student_update" value="submit"><i class="fa fa-check-square-o"></i> Update</button>
                                            <a href="<?php echo base_url('students/');?>" class="btn btn-danger mr-1" role="button"><i class="feather icon-x"></i> Cancel</a>
                                        </div>
                                    </form>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>