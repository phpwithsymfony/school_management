<div class="modal fade text-left modal-fullscreen" id="feeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Fee Details</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="fee_modal">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th scope="row">GR No</th>
                            <td><span id="view_regno"></span></td>
                        </tr>
                        <tr>
                            <th scope="row">Standard</th>
                            <td><span id="view_std"></span>&nbsp;<span id="view_div"></span></td>
                        </tr>
                        <tr>
                            <th scope="row">Name</th>
                            <td><span id="view_firstname"></span></td>
                        </tr>
                        <tr>
                            <th scope="row">Medium</th>
                            <td><span id="view_medium"></span></td>
                        </tr>
                        <tr>
                            <th scope="row">Remaining Fee</th>
                            <td><span id="view_remaining"></span></td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-striped table-bordered view_fee_table" id="paid_fee_details">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left modal-fullscreen" id="confirm_password_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Confirm Password</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="confirm_password_error"></div>
                <form method="post" id="delete_student_data" class="form" novalidate>
                    <div class="form-group">
                        <label>Password<span class="error">*</span></label>
                        <div class="controls">
                            <input type='password' name='password_delete' id="password_delete" class="form-control" placeholder="Enter Admin Password" data-validation-regex-regex="([A-z0-9])*[^\s]\1*" data-validation-regex-message="Not allow space or special character" minlength="6" maxlength="12" required data-validation-required-message="This field is required"/>
                        </div>
                    </div>
                    <input type="hidden" name="delete_id" id="delete_id" value="">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="student_submit" onclick="checkPassword()">Submit</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left modal-fullscreen" id="confirm_password_fee_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Confirm Password</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="confirm_error"></div>
                <form method="post" id="delete_fee_data" class="form" novalidate>
                    <div class="form-group">
                        <label>Password<span class="error">*</span></label>
                        <div class="controls">
                            <input type='password' name='password_fee_delete' id="password_fee_delete" class="form-control" placeholder="Enter Admin Password" data-validation-regex-regex="([A-z0-9])*[^\s]\1*" data-validation-regex-message="Not allow space or special character" minlength="6" maxlength="12" required data-validation-required-message="This field is required"/>
                        </div>
                    </div>
                    <input type="hidden" name="delete_fee_id" id="delete_fee_id" value="">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="fee_submit" onclick="checkPasswordforfee()">Submit</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left modal-fullscreen" id="student_transfer_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Student Transfer</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="transfer_error"></div>
                <form method="post" id="transfer_student_dataform" class="form" novalidate>
                    <div class="form-group">
                        <label>School<span class="error">*</span></label>
                        <div class="controls">
                            <select class="form-control" name="transstudent_school" id="transstudent_school" onchange="getTrannsferstd(event)" required="">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Standard<span class="error">*</span></label>
                        <div class="controls">
                            <select class="form-control" name="transstudent_standard" id="transstudent_standard" onchange="getTransferdiv(event)" required=""><span class="error"></span>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Division<span class="error">*</span></label>
                        <div class="controls">
                            <select class="form-control" name="transstudent_div" id="transstudent_div" required="">
                            </select><span class="error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>School Password<span class="error">*</span></label>
                        <div class="controls">
                            <input type='password' name='school_password' id="school_password" class="form-control" placeholder="Enter Admin Password" data-validation-regex-regex="([A-z0-9])*[^\s]\1*" data-validation-regex-message="Not allow space or special character" minlength="6" maxlength="12" required data-validation-required-message="This field is required"/><span class="error"></span>
                        </div>
                    </div>
                    <input type="hidden" name="transfer_school_id" id="transfer_school_id" value="">
                    <input type="hidden" name="transfer_student_id" id="transfer_student_id" value="">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="fee_submit" onclick="checkfortransfer()">Submit</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>