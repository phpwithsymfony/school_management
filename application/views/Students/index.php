<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Students listing
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- HTML (DOM) sourced data -->
            <div id="profile_edit_message"></div>
            <?php if ($this->session->flashdata('response')) {
                echo '<div class="alert alert-success alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            }
            if ($this->session->flashdata('response_error')) {
                echo '<div class="alert alert-danger alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response_error') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            } ?>
            <section id="html">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <form role='search' id="search_studentdata">
                                    <div class="row  py-1 search-container-2 pl-1 pr-1">
                                        <div class="col-12 col-sm-6 col-lg-3 id_100">
                                            <label for="users-list-verified">Standard :</label>
                                            <fieldset class="form-group">
                                                <select name="student_std" onchange="searchDivValue(event)"
                                                        id="student_std"
                                                        class="form-control select2-size-sm student_std">
                                                    <option value="" selected>Select</option>
                                                    <?php foreach ($std as $row) { ?>
                                                        <option value="<?php echo $row->standard; ?>"><?php echo $row->standard; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Division</label>
                                            <fieldset class="form-group">
                                                <select name="student_div" id="student_div"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="" id="division" selected>Select</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Medium</label>
                                            <fieldset class="form-group">
                                                <select name="student_medium" id="student_medium"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="" selected>Select</option>
                                                    <option value="English">English</option>
                                                    <option value="Gujarati">Gujarati</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Academic Year</label>
                                            <fieldset class="form-group">
                                                <select name="student_academic" id="student_academic"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="" selected>Select</option>
                                                     <?php foreach($years as $year) { ?>
                                                        <option value="<?php echo $year->id ?>"><?php echo $year->year ?></option>
                                                    <?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <?php if ($_SESSION['role'] == 'super_admin') { ?>
                                            <div class="col-12 col-sm-6 col-lg-3">
                                                <label for="users-list-status">School (city)</label>
                                                <fieldset class="form-group">
                                                    <select name="student_school" id="student_school"
                                                            class="select2-data-array form-control select2-size-sm">
                                                        <option value="">Select</option>
                                                        <?php foreach ($school as $row) { ?>
                                                            <option value="<?php echo $row->id; ?>"><?php echo $row->name . ' (' . $row->city . ')'; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </fieldset>
                                            </div>
                                        <?php } ?>
                                        <div class="col-12 col-sm-6 col-lg-3 d-flex align-items-center">
                                            <button type="button" class="btn btn-success" name="student_search"
                                                    id="student_search"><i class="feather icon-search"></i> Search
                                            </button>
                                            <button type="button" value="reset" class="btn btn-danger clear-btn"
                                                    name="student_clear" id="student_clear"><i
                                                        class="feather icon-x"></i> Clear
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="students_header">
                                    <h4 class="card-title" id="basic-layout-form">Students Listing</h4>
                                    <a href="<?php echo base_url('students/add'); ?>" class="btn btn-success"><i
                                                class="feather icon-plus"></i> Add</a>
                                    <form method="post" action="<?php echo base_url('students/student_PDF') ?>">
                                        <input type="hidden" name="hidden_student_std" id="hidden_student_std"/>
                                        <input type="hidden" name="hidden_student_div" id="hidden_student_div"/>
                                        <input type="hidden" name="hidden_student_medium" id="hidden_student_medium"/>
                                        <input type="hidden" name="hidden_student_school" id="hidden_student_school"/>
                                        <input type="hidden" name="hidden_student_academic" id="hidden_student_academic"/>
                                        <button type="submit" name="submit" class="btn btn-success" value="Download PDF"
                                                formtarget="_blank"><span class="fa fa-file-pdf-o"></span>&nbsp;Download
                                            PDF
                                        </button>
                                    </form>
                                    <button class="btn btn-success edit-button" type="button" id="edit_standard"><i
                                                class="feather icon-edit"></i> Edit Standard
                                    </button>
                                </div>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered" id="student_data"
                                           style="width: 100%">
                                        <thead>
                                        <tr>
                                            <th><input type="checkbox" class='checkall' id='checkall'></th>
                                            <th>G.R. No.</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Contact</th>
                                            <th>Std</th>
                                            <th>Medium</th>
                                            <?php if ($_SESSION['role'] == 'super_admin') { ?>
                                                <th>School</th><?php } ?>
                                            <th>Fee</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<div class="modal fade text-left modal-fullscreen" id="edit_std_student" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="modal-title text-text-bold-600" id="myModalLabel33">Edit Standard</label>
            </div>
            <form  id="edit_std_form" name="edit_std_form" class="form" method="post" >
                <div class="modal-body">
                    <div id="error">

                    </div>
                    <div id="std_fee_tbody">
                            <label>School (city)<span class="error">*</span></label>
                            <div class="form-group mb-1">
                                <select name="school_edit_all" id="school_edit_all" class="form-control"
                                        class="required" onchange="getSchoolStandard(event)">
                                    <option value="">Select</option>
                                    <?php foreach ($school as $row) { ?>
                                        <option value="<?php echo $row->id; ?>"><?php echo $row->name . ' (' . $row->city . ')'; ?></option>
                                    <?php } ?>
                                </select>
                                <span class="error"></span>
                            </div>
                        <input type="hidden" name="session_role" id="session_role" class="form-control"
                               value="<?php echo $_SESSION['role']; ?>"/>
                    </div>

                    <table id="std_add" style="width: 100%;">
                        <tbody id="std_add_tbody">
                        <tr>
                            <td colspan="2">
                                <div class="form-group mb-1">
                                    <label>Standard<span class="error">*</span></label>
                                    <div class="controls">
                                        <select name="standard_edit_all" id="standard_edit_all" class="form-control"
                                                class="required">
                                        </select><span
                                                class="error"></span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <span class="error" id="student_array"></span>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="standard" id="standard"/>
                    <button  name="edit_all" id="edit_all" class="btn btn-success" type="button"/>
                    <i class="fa fa-check-square-o"></i> Submit</button>
                    <button type="reset" class="btn btn-danger" name="close" id="close" data-dismiss="modal"
                            value="reset"><i class="feather icon-x"></i> Close
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>