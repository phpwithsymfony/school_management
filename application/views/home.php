<div class="app-content content">
    <!-- BEGIN: Content-->

    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- Stats -->
            <div id="profile_edit_message"></div>
            <?php if ($this->session->flashdata('response')) {
                echo '<div class="alert alert-success alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            }
            if ($this->session->flashdata('response_error')) {
                echo '<div class="alert alert-danger alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response_error') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            } ?>
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="media align-items-stretch">
                                <div class="p-2 text-center bg-primary bg-darken-2">
                                    <i class="fa fa-users font-large-2 white home_label"></i>
                                </div>
                                <div class="p-2 bg-gradient-x-primary white media-body">
                                    <a href="<?php echo base_url('students/') ?>">
                                        <h5 class="home_label home_label_name">Students</h5></a>
                                    <h5 class="text-bold-400 mb-0"><b><?php echo $student; ?></b></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($_SESSION['role'] == 'super_admin') { ?>
                    <div class="col-xl-3 col-lg-6 col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="media align-items-stretch">
                                    <div class="p-2 text-center bg-danger bg-darken-2">
                                        <i class="fa fa-university font-large-2 white home_label"></i>
                                    </div>
                                    <div class="p-2 bg-gradient-x-danger white media-body">
                                        <a href="<?php echo base_url('schools/') ?>">
                                            <h5 class="home_label home_label_name">Schools</h5></a>
                                        <h5 class="text-bold-400 mb-0"><?php echo $school; ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="media align-items-stretch">
                                <div class="p-2 text-center bg-warning bg-darken-2">
                                    <i class="fa fa-inr font-large-2 white home_label"></i>
                                </div>
                                <div class="p-2 bg-gradient-x-warning white media-body">
                                    <a href="<?php echo base_url('fee/') ?>">
                                        <h5 class="home_label home_label_name">Paid Fees</h5></a>
                                    <h5 class="text-bold-400 mb-0"><?php echo $paid_fee; ?></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="media align-items-stretch">
                                <div class="p-2 text-center bg-success bg-darken-2">
                                    <i class="fa fa-calendar-check-o font-large-2 white home_label"></i>
                                </div>
                                <div class="p-2 bg-gradient-x-success white media-body">
                                    <a href="<?php echo base_url('daily_report/') ?>">
                                        <h5 class="home_label home_label_name">Today's Paid Fees</h5></a>
                                    <h5 class="text-bold-400 mb-0"><?php echo $daily_paid_fee; ?></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="media align-items-stretch">
                                <div class="p-2 text-center bg-pink bg-darken-2">
                                    <i class="fa fa-hourglass-end font-large-2 white home_label"></i>
                                </div>
                                <div class="p-2 bg-gradient-x-pink white media-body">
                                    <a href="<?php echo base_url('pending_fee/') ?>">
                                        <h5 class="home_label home_label_name">Pending Fees</h5></a>
                                    <h5 class="text-bold-400 mb-0"><?php echo $pending_fee; ?></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Stats -->
            </div>
        </div>
    </div>
    <!-- END: Content-->