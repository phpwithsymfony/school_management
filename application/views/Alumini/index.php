<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Alumini Students listing
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- HTML (DOM) sourced data -->
            <div id="profile_edit_message"></div>
            <?php if ($this->session->flashdata('response')) {
                echo '<div class="alert alert-success alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            }
            if ($this->session->flashdata('response_error')) {
                echo '<div class="alert alert-danger alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response_error') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            } ?>
            <section id="html">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <form role='search' id="alumini_search_studentdata">
                                    <div class="row  py-1 search-container-2 pl-1 pr-1">
                                        <div class="col-12 col-sm-6 col-lg-3 id_100">
                                            <label for="users-list-verified">Standard :</label>
                                            <fieldset class="form-group">
                                                <select name="alumini_student_std" onchange="aluminisearchDivValue(event)"
                                                        id="alumini_student_std"
                                                        class="form-control select2-size-sm student_std">
                                                    <option value="" selected>Select</option>
                                                    <?php foreach ($std as $row) { ?>
                                                        <option value="<?php echo $row->standard; ?>"><?php echo $row->standard; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Division</label>
                                            <fieldset class="form-group">
                                                <select name="alumini_student_div" id="alumini_student_div"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="" id="division" selected>Select</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Medium</label>
                                            <fieldset class="form-group">
                                                <select name="alumini_student_medium" id="alumini_student_medium"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="" selected>Select</option>
                                                    <option value="English">English</option>
                                                    <option value="Gujarati">Gujarati</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <?php if ($_SESSION['role'] == 'super_admin') { ?>
                                            <div class="col-12 col-sm-6 col-lg-3">
                                                <label for="users-list-status">School (city)</label>
                                                <fieldset class="form-group">
                                                    <select name="alumini_student_school" id="alumini_student_school"
                                                            class="select2-data-array form-control select2-size-sm">
                                                        <option value="">Select</option>
                                                        <?php foreach ($school as $row) { ?>
                                                            <option value="<?php echo $row->id; ?>"><?php echo $row->name . ' (' . $row->city . ')'; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </fieldset>
                                            </div>
                                        <?php } ?>
                                        <div class="col-12 col-sm-6 col-lg-3 d-flex align-items-center">
                                            <button type="button" class="btn btn-success" name="alumini_student_search"
                                                    id="alumini_student_search"><i class="feather icon-search"></i> Search
                                            </button>
                                            <button type="button" value="reset" class="btn btn-danger clear-btn"
                                                    name="alumini_student_clear" id="alumini_student_clear"><i
                                                    class="feather icon-x"></i> Clear
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="students_header">
                                    <h4 class="card-title" id="basic-layout-form">Alumini Students Listing</h4>
                                    <form method="post" action="<?php echo base_url('alumini_student/alumini_student_PDF') ?>">
                                        <input type="hidden" name="alumini_hidden_student_std" id="alumini_hidden_student_std"/>
                                        <input type="hidden" name="alumini_hidden_student_div" id="alumini_hidden_student_div"/>
                                        <input type="hidden" name="alumini_hidden_student_medium" id="alumini_hidden_student_medium"/>
                                        <input type="hidden" name="alumini_hidden_student_school" id="alumini_hidden_student_school"/>
                                        <button type="submit" name="submit" class="btn btn-success" value="Download PDF"
                                                formtarget="_blank"><span class="fa fa-file-pdf-o"></span>&nbsp;Download
                                            PDF
                                        </button>
                                    </form>
                                </div>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered" id="alumini_student_data"
                                           style="width: 100%">
                                        <thead>
                                        <tr>
                                            <th>G.R. No.</th>
                                            <th>Name</th>
                                            <th>Std</th>
                                            <th>Medium</th>
                                            <th>Category</th>
                                            <?php if ($_SESSION['role'] == 'super_admin') { ?>
                                                <th>School</th><?php } ?>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>