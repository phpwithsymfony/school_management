<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url('fee/'); ?>">Fee</a>
                            </li>
                            <li class="breadcrumb-item active">Collect Fee
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div id="fee_collect_message"></div>
            <section id="basic-form-layouts">
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-form">Collect Fee</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="post" id="take_student_fee" action="<?php echo base_url('fee/fee_details');?>" novalidate>
                                        <input type='hidden' name='flag' id="flag" value="<?php echo $flag[0]->feestructure_flag; ?>" />
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Fee year</label>
                                                        <select name="studentFeeYear" id="studentFeeYear1" onchange ="getStudentYear()" class="form-control" required="">
                                                            <?php foreach($years as $year) { 
                                                                if($data[0]->academic_year == $year->id){
                                                            ?>
                                                                <option value="<?php echo $year->id ?>" selected><?php echo $year->year ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?php echo $year->id ?>"><?php echo $year->year ?></option>
                                                            <?php } }?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>G.R.No.</label>
                                                        <input type='text' name='take_GRnumber' id='take_GRnumber' value="<?php echo $data[0]->gr_number; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='take_id' id='take_id' value="<?php echo $data[0]->id; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='admission_year' id='admission_year' value="<?php echo $data[0]->admission_year; ?>" class='form-control' readonly/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Name</label>
                                                        <input type='text' name='take_name' id='take_name' value="<?php echo $data[0]->firstname.' '.$data[0]->father_name.' '.$data[0]->surname; ?>" class='form-control' readonly/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Standard</label>
                                                        <input type='text' name='take_std' id="take_std" value="<?php echo $data[0]->standard.' - '.$data[0]->divison; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='student_feestdandard' id="student_feestdandard" value="<?php echo $data[0]->standard; ?>" class='form-control' readonly/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Student Status (New/Old)</label>
                                                        <div class="controls">
                                                            <?php if($flag[0]->feestructure_flag=='3'){ ?>
                                                            <select name="student_status" id="student_status" onchange="getOtherFeeDuration()" class="form-control" required=""><?php } else if($flag[0]->feestructure_flag=='2'){ ?>
                                                                <select name="student_status" id="student_status" onchange="getPreFeeDuration()" class="form-control" required=""><?php } else if($flag[0]->feestructure_flag=='1'){ ?>
                                                                    <select name="student_status" id="student_status" onchange="getSecondaryFeeDuration()" class="form-control" required=""><?php } else if($flag[0]->feestructure_flag=='4'){?><select name="student_status" id="student_status"  class="form-control" required="" readonly=""><?php } ?>
                                                                <option value="new" id="new_student">New</option>
                                                                <option value="old" id="old_student">Old</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <?php if($flag[0]->feestructure_flag=='4'){ ?>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <label>Total Fee</label>
                                                                    <input type='text' name='total_fee' id="total_fee" value="" class='form-control' readonly/>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label>Other Fee</label>
                                                                    <input type='text' name='other_fee' id="other_fee" value="<?php echo $data[0]->other_fee; ?>" class='form-control' readonly/>
                                                                </div>
                                                            </div>

                                                        <?php } else { ?>
                                                            <input type='text' name='total_fee' id="total_fee" value="" class='form-control' readonly/>
                                                        <?php } ?>

                                                        <input type='hidden' name='admission_fee' id="admission_fee" value="<?php echo $data[0]->admission_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='tuition_fee' id="tuition_fee" value="<?php echo $data[0]->tuition_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='term_fee' id="term_fee" value="<?php echo $data[0]->term_fee; ?>" class='form-control' readonly/>
                                                        <?php if($flag[0]->feestructure_flag!='4'){ ?>
                                                        <input type='hidden' name='other_fee' id="other_fee" value="<?php echo $data[0]->other_fee; ?>" class='form-control' readonly/>
                                                        <?php } ?>
                                                        <input type='hidden' name='smart_fee' id="smart_fee" value="<?php echo $data[0]->smart_class_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='computer_fee' id="computer_fee" value="<?php echo $data[0]->computer_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='sports_fee' id="sports_fee" value="<?php echo $data[0]->sports_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='insurance_fee' id="insurance_fee" value="<?php echo $data[0]->insurance_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='exam_fee' id="exam_fee" value="<?php echo $data[0]->exam_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='enroll_fee' id="enroll_fee" value="<?php echo $data[0]->enroll_fee; ?>" class='form-control' readonly/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Remaining Fee</label>
                                                        <input type='text' name='remaining_fee' id="remaining_fee" class='form-control' value="<?php echo $remaining_fee[0]->remaining_fee; ?>" readonly/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <?php if($flag[0]->feestructure_flag=='4'){ ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Fee Installment</label>
                                                        <div class="">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <select name="start_fee_installment" id="start_fee_installment" onchange="getFirststepFee(true)" class="select2-data-array form-control" required="">
                                                                        <option value="">Select</option>
                                                                        <option value="first" <?php if($fee_installment == 'first') { ?> selected="selected"<?php } ?>>1st Installment</option>
                                                                        <option value="second" <?php if($fee_installment == 'second') { ?> selected="selected"<?php } ?>>2nd Installment</option>
                                                                        <option value="third" <?php if($fee_installment == 'third') { ?> selected="selected"<?php } ?>>3rd Installment</option>
                                                                        <option value="fourth" <?php if($fee_installment == 'fourth') { ?> selected="selected"<?php } ?>>4th Installment</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-1 mt-1 take-fee-select"><span class="">To</span></div>
                                                                <div class="col-md-5">
                                                                    <select name="end_fee_installment" id="end_fee_installment" onchange="getFirststepFee(true)" class="select2-data-array form-control">
                                                                        <option value="" id="selectinstallment">Select</option>
                                                                        <option value="first">1st Installment</option>
                                                                        <option value="second">2nd Installment</option>
                                                                        <option value="third">3rd Installment</option>
                                                                        <option value="fourth">4th Installment</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } else{?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Fee Duration</label>
                                                        <div class="">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <?php if($flag[0]->feestructure_flag=='3'){ ?>
                                                                    <select name="start_fee_duration" id="start_fee_duration" onchange="getOtherFeeDuration()" class="select2-data-array form-control" required=""><?php } else if($flag[0]->feestructure_flag=='2'){ ?><select name="start_fee_duration" id="start_fee_duration" onchange="getPreFeeDuration()" class="select2-data-array form-control" required=""><?php } else if($flag[0]->feestructure_flag=='1'){ ?><select name="start_fee_duration" id="start_fee_duration" onchange="getSecondaryFeeDuration()" class="select2-data-array form-control" required=""><?php } ?>
                                                                <option value="">Select</option>
                                                                <option value="6" <?php if($fee_month == 'June') { ?> selected="selected"<?php } ?>>June</option>
                                                                <option value="7" <?php if($fee_month == 'July') { ?> selected="selected"<?php } ?>>July</option>
                                                                <option value="8" <?php if($fee_month == 'August') { ?> selected="selected"<?php } ?>>August</option>
                                                                <option value="9" <?php if($fee_month == 'September') { ?> selected="selected"<?php } ?>>September</option>
                                                                <option value="10" <?php if($fee_month == 'October') { ?> selected="selected"<?php } ?>>October</option>
                                                                <option value="11" <?php if($fee_month == 'November') { ?> selected="selected"<?php } ?>>November</option>
                                                                <option value="12" <?php if($fee_month == 'December') { ?> selected="selected"<?php } ?>>December</option>
                                                                <option value="1" <?php if($fee_month == 'January') { ?> selected="selected"<?php } ?>>January</option>
                                                                <option value="2" <?php if($fee_month == 'February') { ?> selected="selected"<?php } ?>>February</option>
                                                                <option value="3" <?php if($fee_month == 'March') { ?> selected="selected"<?php } ?>>March</option>
                                                                <option value="4" <?php if($fee_month == 'April') { ?> selected="selected"<?php } ?>>April</option>
                                                                <option value="5" <?php if($fee_month == 'May') { ?> selected="selected"<?php } ?>>May</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-1 mt-1 take-fee-select"><span class="">To</span></div>
                                                        <div class="col-md-5">
                                                            <?php if($flag[0]->feestructure_flag=='3'){ ?>
                                                            <select name="end_fee_duration" id="end_fee_duration" onchange="getOtherFeeDuration(true)" class="select2-data-array form-control"><?php } else if($flag[0]->feestructure_flag=='2'){ ?><select name="end_fee_duration" id="end_fee_duration" onchange="getPreFeeDuration(true)" class="select2-data-array form-control"><?php } else if($flag[0]->feestructure_flag=='1'){ ?><select name="end_fee_duration" id="end_fee_duration" onchange="getSecondaryFeeDuration(true)" class="select2-data-array form-control"><?php } ?>
                                                                <option value="">Select</option>
                                                            </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php }?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Fee Period</label>
                                                        <div class="controls">
                                                            <input type='text' name='fee_period' id="fee_period" class='form-control' readonly/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type='hidden' name='fee_duration' id="fee_duration" />
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Concession</label>
                                                        <div class="controls">
                                                            <input type="text" name='concession' id="concession" class="form-control" onkeyup="concession_FeeError()" data-validation-regex-regex="([0-9])*" data-validation-regex-message="Only allow numeric" minlength="2" maxlength="5" placeholder="Enter Concession"/>
                                                            <span id="concession_error" class="error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Discount</label>
                                                        <div class="controls">
                                                            <input type="text" name='discount' id="discount" class="form-control" data-validation-regex-regex="([0-9])*" onkeyup="discount_FeeError()" data-validation-regex-message="Only allow numeric" minlength="2" maxlength="5" placeholder="Enter Discount"/>
                                                            <span id="discount_error" class="error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <?php if($flag[0]->feestructure_flag=='4'){ ?>
                                                        <div class="row">
                                                        <div class="col-md-6">
                                                        <label>Paid Fee</label>
                                                        <div class="controls">
                                                            <input type="text" name='total_paid_fee' id="total_paid_fee" onblur="paid_feeerror()" class="form-control" placeholder="Enter total paid fee" data-validation-regex-regex="([0-9])*" data-validation-regex-message="Only allow numeric"  maxlength="6"/>
                                                            <span id="paid_fee_error" class="error"></span>
                                                            <input type='hidden' name='pending_fee' id="pending_fee" class='form-control' value="<?php echo $pending_fee;?>"/>
                                                        </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                                <label>Paid Other Fee</label>
                                                                <div class="controls">
                                                                    <input type="text" name='paid_other_fee' id="paid_other_fee" onblur="paid_other_feeerror()"  class="form-control" placeholder="Enter other fee" data-validation-regex-regex="([0-9.])*" data-validation-regex-message="Only allow numeric"  maxlength="6"/>
                                                                    <span id="paid_other_fee_error" class="error"></span>                                     </div>
                                                        </div>
                                                        </div>
                                                        <?php } else { ?>
                                                            <label>Paid Fee</label>
                                                            <div class="controls">
                                                                <input type="text" name='total_paid_fee' id="total_paid_fee" onblur="paid_feeerror()" class="form-control" placeholder="Enter total paid fee" data-validation-regex-regex="([0-9])*" data-validation-regex-message="Only allow numeric"  maxlength="6"/>
                                                                <span id="paid_fee_error" class="error"></span>
                                                                <input type='hidden' name='pending_fee' id="pending_fee" class='form-control' value="<?php echo $pending_fee;?>"/>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Date<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input readonly='true' type="text" name='take_date' id="take_date" class="form-control  date" placeholder="Enter paymentdate" required data-validation-required-message="This field is required"/ value="<?php echo date('d-m-Y');?>">
                                                            <span id="take_date_error" class="error"></span>
                                                        </div>  
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Cheque No.</label>
                                                        <div class="controls">
                                                            <input type='text' name='cheque_no' id="cheque_no" class='form-control' placeholder="Enter Cheque No."/>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Cheque Date</label>
                                                        <div class="controls">
                                                            <input type="text" name='cheque_date' id="cheque_date" class="form-control pickadate-format date" placeholder="Enter Cheque Date"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="controls">
                                                    <label>Paid Amount</label><br>
                                                    <div id="paid_fee_amount"></div>
                                                    <input type='hidden' name='paid_admission_fee' id="paid_admission_fee"/>
                                                    <input type='hidden' name='paid_tution_fee' id="paid_tution_fee"/>
                                                    <input type='hidden' name='paid_term_fee' id="paid_term_fee"/>
                                                    <?php if($flag[0]->feestructure_flag!='4'){ ?>
                                                        <input type='hidden' name='paid_other_fee' id="paid_other_fee"/>
                                                    <?php } ?>
                                                    <input type='hidden' name='paid_smart_fee' id="paid_smart_fee"/>
                                                    <input type='hidden' name='paid_computer_fee' id="paid_computer_fee"/>
                                                    <input type='hidden' name='paid_sports_fee' id="paid_sports_fee"/>
                                                    <input type='hidden' name='paid_exam_fee' id="paid_exam_fee"/>
                                                    <input type='hidden' name='paid_insurance_fee' id="paid_insurance_fee"/>
                                                    <input type='hidden' name='paid_enroll_fee' id="paid_enroll_fee"/>
                                                    <input type='hidden' name='paid_amount' id="paid_amount" class='form-control' value="<?php echo $paid_fee;?>"/>

                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Trust</label>
                                                    <div class="controls">
                                                        <input type="text" name='trust_name' id="trust_name" class="form-control"  placeholder="Enter Trust Name" maxlength="50"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <input type='hidden' name='remaining_amount' id="remaining_amount" class='form-control' value="<?php echo $remaining_amount; ?>" readonly/>
                                            <button type="submit" class="btn btn-success" name="take_fee" id="take_fee" value="submit" formtarget="_blank" onclick="take_payment()"><i class="fa fa-check-square-o"></i> Submit</button>
                                            <a href="<?php echo base_url('fee/'); ?>" class="btn btn-danger mr-1" role="button" id="fee_cancel"><i class="feather icon-x"></i> Cancel</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>