<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Fee Listing
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- HTML (DOM) sourced data -->
            <div id="profile_edit_message"></div>
            <?php if ($this->session->flashdata('response')) {
                echo '<div class="alert alert-success alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            }
            if ($this->session->flashdata('response_error')) {
                echo '<div class="alert alert-danger alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response_error') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            } ?>
            <section id="html">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <form role='search' id="form-search">
                                    <div class="row  py-1 search-container pl-1 pr-1">
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-verified">G.R.No.</label>
                                            <fieldset class="form-group">
                                                <input type="text" name="search_grnumber" id="search_grnumber"
                                                       class="form-control form-control-sm"
                                                       placeholder="Type G.R. No."/>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-role">Name</label>
                                            <fieldset class="form-group">
                                                <input type="text" name="search_name" id="search_name"
                                                       class='form-control form-control-sm' placeholder='Type Name'/>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Email</label>
                                            <fieldset class="form-group">
                                                <input type="text" name="search_email" id="search_email"
                                                       class='form-control form-control-sm' placeholder='Type Email'/>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Contact No.</label>
                                            <fieldset class="form-group">
                                                <input type="text" name="search_contact_no" id="search_contact_no"
                                                       class="form-control form-control-sm"
                                                       placeholder="Type Contact No.">
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Standard</label>
                                            <fieldset class="form-group">
                                                <select name="student_std" onchange="searchDivValue(event)"
                                                        id="student_std"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="">Select</option>
                                                    <?php foreach ($std as $row) { ?>
                                                        <option value="<?php echo $row->standard; ?>"><?php echo $row->standard; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Division</label>
                                            <fieldset class="form-group">
                                                <select name="student_div" id="student_div"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="" id="division">Select</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <label for="users-list-status">Academic Year</label>
                                            <fieldset class="form-group">
                                                <select name="student_academic" id="student_academic"
                                                        class="select2-data-array form-control select2-size-sm">
                                                    <option value="" selected>Select</option>
                                                    <?php foreach($years as $year) { ?>
                                                        <option value="<?php echo $year->id ?>"><?php echo $year->year ?></option>
                                                    <?php } ?>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <?php if ($_SESSION['role'] == 'super_admin') { ?>
                                            <div class="col-12 col-sm-6 col-lg-3">
                                                <label for="users-list-status">School (city)</label>
                                                <fieldset class="form-group">
                                                    <select name="search_student_school" id="search_student_school"
                                                            class="select2-data-array form-control select2-size-sm">
                                                        <option value="">Select</option>
                                                        <?php foreach ($school as $row) { ?>
                                                            <option value="<?php echo $row->id; ?>"><?php echo $row->name . ' (' . $row->city . ')'; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </fieldset>
                                            </div>
                                        <?php } ?>
                                        <div class="col-12 col-sm-6 col-lg-3 d-flex align-items-center">
                                            <button type="button" class="btn btn-success" name="fee_find" id="fee_find">
                                                <i class="feather icon-search"></i> Search
                                            </button>
                                            <button type="button" class="btn btn-danger clear-btn" name="fee_clear"
                                                    id="fee_clear"><i class="feather icon-x"></i> Clear
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-form">Fee Listing</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <div class="card-body card-dashboard">
                                    <table class="table table-striped table-bordered" id="student_details_fee"
                                           style="width: 100%">
                                        <thead>
                                        <tr>
                                            <th>G.R. No.</th>
                                            <th>Name</th>
                                            <th>Standard</th>
                                            <th>Category</th>
                                            <th>Fee</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>            