
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url('fee/'); ?>">Fee</a>
                            </li>
                            <li class="breadcrumb-item active">Collect Fee
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div id="profile_edit_message"></div>
            <section id="basic-form-layouts">
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="basic-layout-form">Collect Fee</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="post" id="edit_student_fee" action="<?php echo base_url('daily_report/edit');?>" novalidate>
                                        <input type='hidden' name='edit_flag' id="edit_flag" value="<?php echo $flag[0]->feestructure_flag; ?>" />
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Fee year</label>
                                                        <select name="studentFeeYear" class="form-control" required="">
                                                                <?php foreach($years as $year) { ?>
                                                                <?php if($year->year == $fee_year ){ ?>
                                                                    <option value="<?php echo $year->id ?>" selected="selected"><?php echo $year->year ?></option>
                                                                    <?php } else { ?>
                                                                         <option value="<?php echo $year->id ?>"><?php echo $year->year ?></option>
                                                            <?php } } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>G.R.No.</label>
                                                        <input type='text' name='edit_GRnumber' id='edit_GRnumber' value="<?php echo $data[0]->gr_number; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='edit_fee_id' id='edit_fee_id' value="<?php echo $data[0]->id; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='edit_year' id='edit_year' value="<?php echo $data[0]->admission_year; ?>" class='form-control' readonly/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Name</label>
                                                        <input type='text' name='edit_fee_name' id='edit_fee_name' value="<?php echo $data[0]->firstname.' '.$data[0]->father_name.' '.$data[0]->surname; ?>" class='form-control' readonly/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Standard</label>
                                                        <input type='text' name='edit_fee_std' id="edit_fee_std" value="<?php echo $data[0]->standard.' - '.$data[0]->divison; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='edit_studentfeestd' id="edit_studentfeestd" value="<?php echo $data[0]->standard; ?>" class='form-control' readonly/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Student Status (New/Old)</label>
                                                        <div class="controls">
                                                            <?php if($flag[0]->feestructure_flag=='3'){ ?>
                                                            <select name="edit_student_status" id="edit_student_status" onchange="editgetFeeDuration()" class="form-control" required=""><?php } else if($flag[0]->feestructure_flag=='2'){ ?>
                                                                <select name="edit_student_status" id="edit_student_status" onchange="editPrePrimaryFeeDuration()" class="form-control" required=""><?php } else if($flag[0]->feestructure_flag=='1'){ ?>
                                                                    <select name="edit_student_status" id="edit_student_status" onchange="editSecondaryFeeDuration()" class="form-control" required=""><?php } else if($flag[0]->feestructure_flag=='4'){ ?>
                                                                        <select name="edit_student_status" id="edit_student_status"  class="form-control" required="" readonly=""><?php } ?>
                                                                <option value="new" id="edit_new_student">New</option>
                                                                <option value="old" id="edit_old_student">Old</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Total Fee</label>
                                                        <input type='text' name='edit_total_fee' id="edit_total_fee" value="" class='form-control' readonly/>
                                                        <input type='hidden' name='edit_admission_fee' id="edit_admission_fee" value="<?php echo $data[0]->admission_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='edit_tuition_fee' id="edit_tuition_fee" value="<?php echo $data[0]->tuition_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='edit_term_fee' id="edit_term_fee" value="<?php echo $data[0]->term_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='edit_other_fee' id="edit_other_fee" value="<?php echo $data[0]->other_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='edit_smart_fee' id="edit_smart_fee" value="<?php echo $data[0]->smart_class_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='edit_computer_fee' id="edit_computer_fee" value="<?php echo $data[0]->computer_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='edit_sports_fee' id="edit_sports_fee" value="<?php echo $data[0]->sports_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='edit_insurance_fee' id="edit_insurance_fee" value="<?php echo $data[0]->insurance_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='edit_exam_fee' id="edit_exam_fee" value="<?php echo $data[0]->exam_fee; ?>" class='form-control' readonly/>
                                                        <input type='hidden' name='edit_enroll_fee' id="edit_enroll_fee" value="<?php echo $data[0]->enroll_fee; ?>" class='form-control' readonly/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Remaining Fee</label>
                                                        <input type='text' name='edit_remaining_fee' id="edit_remaining_fee" class='form-control' value="<?php echo $remaining_fee; ?>" readonly/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <?php if($flag[0]->feestructure_flag=='4'){ ?>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Fee Installment</label>
                                                            <div class="">
                                                                <div class="row">
                                                                    <div class="col-md-5">
                                                                        <select name="start_editfee_installment" id="start_editfee_installment" onchange="editgetFirststepFee(true)" class="select2-data-array form-control" required="">
                                                                            <option value="">Select</option>
                                                                            <option value="first" <?php if($fee_installment == 'first') { ?> selected="selected"<?php } ?>>1st Installment</option>
                                                                            <option value="second" <?php if($fee_installment == 'second') { ?> selected="selected"<?php } ?>>2nd Installment</option>
                                                                            <option value="third" <?php if($fee_installment == 'third') { ?> selected="selected"<?php } ?>>3rd Installment</option>
                                                                            <option value="fourth" <?php if($fee_installment == 'fourth') { ?> selected="selected"<?php } ?>>4th Installment</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-1 mt-1 take-fee-select"><span class="">To</span></div>
                                                                    <div class="col-md-5">
                                                                        <select name="end_editfee_installment" id="end_editfee_installment" onchange="editgetFirststepFee(true)" class="select2-data-array form-control" >
                                                                            <option value="" id="selecteditinstallment">Select</option>
                                                                            <option value="first" <?php if($end_installment == 'first') { ?> selected="selected"<?php } ?>>1st Installment</option>
                                                                            <option value="second" <?php if($end_installment == 'second') { ?> selected="selected"<?php } ?>>2nd Installment</option>
                                                                            <option value="third" <?php if($end_installment == 'third') { ?> selected="selected"<?php } ?>>3rd Installment</option>
                                                                            <option value="fourth" <?php if($end_installment == 'fourth') { ?> selected="selected"<?php } ?>>4th Installment</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } else{?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Fee Duration</label>
                                                        <div class="">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <?php if($flag[0]->feestructure_flag=='3'){ ?>
                                                                    <select name="start_duration" id="start_duration" onchange="editgetFeeDuration()" class="select2-data-array form-control" required=""><?php } else if($flag[0]->feestructure_flag=='2'){ ?>
                                                                        <select name="start_duration" id="start_duration" onchange="editPrePrimaryFeeDuration()" class="select2-data-array form-control" required=""><?php } else if($flag[0]->feestructure_flag=='1'){ ?>
                                                                            <select name="start_duration" id="start_duration" onchange="editSecondaryFeeDuration()" class="select2-data-array form-control" required=""><?php } ?>
                                                                        <option value="">Select</option>
                                                                        <option value="6" <?php if($start_month == 'June') { ?> selected="selected"<?php } ?>>June</option>
                                                                        <option value="7" <?php if($start_month == 'July') { ?> selected="selected"<?php } ?>>July</option>
                                                                        <option value="8" <?php if($start_month == 'August') { ?> selected="selected"<?php } ?>>August</option>
                                                                        <option value="9" <?php if($start_month == 'September') { ?> selected="selected"<?php } ?>>September</option>
                                                                        <option value="10" <?php if($start_month == 'October') { ?> selected="selected"<?php } ?>>October</option>
                                                                        <option value="11" <?php if($start_month == 'November') { ?> selected="selected"<?php } ?>>November</option>
                                                                        <option value="12" <?php if($start_month == 'December') { ?> selected="selected"<?php } ?>>December</option>
                                                                        <option value="1" <?php if($start_month == 'January') { ?> selected="selected"<?php } ?>>January</option>
                                                                        <option value="2" <?php if($start_month == 'February') { ?> selected="selected"<?php } ?>>February</option>
                                                                        <option value="3" <?php if($start_month == 'March') { ?> selected="selected"<?php } ?>>March</option>
                                                                        <option value="4" <?php if($start_month == 'April') { ?> selected="selected"<?php } ?>>April</option>
                                                                        <option value="5" <?php if($start_month == 'May') { ?> selected="selected"<?php } ?>>May</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-1 mt-1 take-fee-select"><span class="">To</span></div>
                                                                <div class="col-md-5">
                                                                    <?php if($flag[0]->feestructure_flag=='3'){ ?>
                                                                    <select name="end_duration" id="end_duration"  class="select2-data-array form-control" onchange="editgetFeeDuration(true)"><?php } else if($flag[0]->feestructure_flag=='2'){ ?>
                                                                        <select name="end_duration" id="end_duration"  class="select2-data-array form-control" onchange="editPrePrimaryFeeDuration(true)"><?php } else if($flag[0]->feestructure_flag=='1'){ ?>
                                                                            <select name="end_duration" id="end_duration"  class="select2-data-array form-control" onchange="editSecondaryFeeDuration(true)"><?php } ?>
                                                                        <option value="" id="selectmonth">Select</option>
                                                                        <option value="6" <?php if($end_month == 'June') { ?> selected="selected"<?php } ?>>June</option>
                                                                        <option value="7" <?php if($end_month== 'July') { ?> selected="selected"<?php } ?>>July</option>
                                                                        <option value="8" <?php if($end_month == 'August') { ?> selected="selected"<?php } ?>>August</option>
                                                                        <option value="9" <?php if($end_month == 'September') { ?> selected="selected"<?php } ?>>September</option>
                                                                        <option value="10" <?php if($end_month == 'October') { ?> selected="selected"<?php } ?>>October</option>
                                                                        <option value="11" <?php if($end_month == 'November') { ?> selected="selected"<?php } ?>>November</option>
                                                                        <option value="12" <?php if($end_month == 'December') { ?> selected="selected"<?php } ?>>December</option>
                                                                        <option value="1" <?php if($end_month == 'January') { ?> selected="selected"<?php } ?>>January</option>
                                                                        <option value="2" <?php if($end_month == 'February') { ?> selected="selected"<?php } ?>>February</option>
                                                                        <option value="3" <?php if($end_month == 'March') { ?> selected="selected"<?php } ?>>March</option>
                                                                        <option value="4" <?php if($end_month == 'April') { ?> selected="selected"<?php } ?>>April</option>
                                                                        <option value="5" <?php if($end_month == 'May') { ?> selected="selected"<?php } ?>>May</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php }?>
                                                <input type='hidden' name='edit_fee_duration' id="edit_fee_duration" />
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Fee Period</label>
                                                        <div class="controls">
                                                            <input type='text' name='edit_fee_period' id="edit_fee_period" class='form-control' readonly/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Concession</label>
                                                        <div class="controls">
                                                            <input type="text" name='edit_concession' id="edit_concession" class="form-control" onkeyup="editconcession_FeeError()" data-validation-regex-regex="([0-9])*" data-validation-regex-message="Only allow numeric" minlength="2" maxlength="5" value="" placeholder="Enter Concession"/>
                                                            <span id="edit_concession_error" class="error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Discount</label>
                                                        <div class="controls">
                                                            <input type="text" name='edit_discount' id="edit_discount" class="form-control" onkeyup="editdiscount_FeeError()" data-validation-regex-regex="([0-9])*" data-validation-regex-message="Only allow numeric" minlength="2" maxlength="5" value="" placeholder="Enter Discount"/>
                                                            <span id="edit_discount_error" class="error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Paid Fee</label>
                                                        <div class="controls">
                                                            <input type="text" name='edit_totalpaid_fee' id="edit_totalpaid_fee" class="form-control" placeholder="Enter total paid fee"  onblur="editpaid_FeeError()" data-validation-regex-regex="([0-9])*" data-validation-regex-message="Only allow numeric" minlength="1" maxlength="6"/>
                                                            <span id="edit_paid_fee_error" class="error"></span>
                                                            <input type="hidden" name="edit_pending_fee" id="edit_pending_fee" value="<?php echo $pending_fee;?>" class="form-control">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Date<span class="error">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name='edit_take_date' id="edit_take_date" class="form-control pickadate-format date" placeholder="Enter paymentdate" required data-validation-required-message="This field is required"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Cheque No.</label>
                                                        <div class="controls">
                                                            <input type='text' name='edit_cheque_no' id="edit_cheque_no" class='form-control' placeholder="Enter Cheque No."/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Cheque Date</label>
                                                        <div class="controls">
                                                            <input type="text" name='edit_cheque_date' id="edit_cheque_date" class="form-control pickadate-format date" placeholder="Enter Cheque Date"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="controls">
                                                    <label>Paid Amount</label><br>
                                                    <div id="edit_paid_fee_amount"></div>
                                                    <input type='hidden' name='edit_paid_admission_fee' id="edit_paid_admission_fee"/>
                                                    <input type='hidden' name='edit_paid_tution_fee' id="edit_paid_tution_fee"/>
                                                    <input type='hidden' name='edit_paid_term_fee' id="edit_paid_term_fee"/>
                                                    <input type='hidden' name='edit_paid_other_fee' id="edit_paid_other_fee"/>
                                                    <input type='hidden' name='edit_paid_smart_fee' id="edit_paid_smart_fee"/>
                                                    <input type='hidden' name='edit_paid_computer_fee' id="edit_paid_computer_fee"/>
                                                    <input type='hidden' name='edit_paid_sports_fee' id="edit_paid_sports_fee"/>
                                                    <input type='hidden' name='edit_paid_insurance_fee' id="edit_paid_insurance_fee"/>
                                                    <input type='hidden' name='edit_paid_exam_fee' id="edit_paid_exam_fee"/>
                                                    <input type='hidden' name='edit_paid_enroll_fee' id="edit_paid_enroll_fee"/>
                                                    <div class="controls">
                                                        <input type='hidden' name='edit_paid_amount' id="edit_paid_amount" class='form-control' value="" readonly/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Trust</label>
                                                    <div class="controls">
                                                        <input type="text" name='edit_trust_name' id="edit_trust_name" class="form-control" value="<?php echo $trust_name; ?>"   placeholder="Enter Trust Name" maxlength="50"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <input type='hidden' name='edit_remaining_amount' id="edit_remaining_amount" class='form-control' value="<?php echo $remaining_amount; ?>" readonly/>
                                            <button type="submit" class="btn btn-success" name="edit_take_fee" id="edit_take_fee" value="submit"><i class="fa fa-check-square-o"></i> Submit</button>
                                            <a href="<?php echo base_url('daily_report/'); ?>" class="btn btn-danger mr-1" role="button"><i class="feather icon-x"></i> Cancel</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>