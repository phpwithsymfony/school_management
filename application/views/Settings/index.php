<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('home/'); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Super Admin Settings
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic form layout section start -->
            <div id="profile_edit_message"></div>
            <?php if ($this->session->flashdata('response')) {
                echo '<div class="alert alert-success alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            } ?>
            <section id="basic-form-layouts">
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="post" action="<?= base_url('settings/update_settings') ?>" novalidate>
                                        <?php echo '<div class="error">' . validation_errors() . '</div>'; ?>
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Master Password <span class="error">*</span> : </label>
                                                        <div class="controls">
                                                            <?php foreach ($data as $row){ ?>
                                                            <input name="masterpassword" id="masterpassword" value="<?= $row->masterpassword ?>" class="form-control" minlength="2" maxlength="12" required data-validation-required-message="This field is required"/>
                                                            <input type="hidden" name="master_pass_id" id="master_pass_id" class="form-control" value="<?php echo $row->id; ?>" required="" />
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        <div class="form-actions right">
                                            <button type="submit" class="btn btn-success" name="school_update" id="school_update" value="submit"><i class="fa fa-check-square-o"></i> Update</button>
                                            <a class="btn btn-danger mr-1" role="button"><i class="feather icon-x"></i> Cancel</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>            