<div class="app-content content">
    <!-- BEGIN: Content-->

    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- Stats -->
            <div id="profile_edit_message"></div>
            <?php if ($this->session->flashdata('response')) {
                echo '<div class="alert alert-success alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            }
            if ($this->session->flashdata('response_error')) {
                echo '<div class="alert alert-danger alert-dismissible mb-2" role="alert">' . $this->session->flashdata('response_error') . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            } ?>
            </div>
        </div>
    </div>
    <!-- END: Content-->