    <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fee extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->library('form_validation');
        $this->load->model("Manage_standard_model");
        $this->load->model("Student_model");
        $this->load->model('Fee_model');
        $this->load->model('Take_fee_model');
        $this->load->model("Daily_report_model");
        $this->load->library('pdf');
        $this->load->model('Academic_year_model');
        $this->load->database();
        date_default_timezone_set('Asia/Kolkata');
    }

    /**
     * @Desc : show all student list
     */
    public function index()
    {

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);

            $standard['std'] = $this->Manage_standard_model->fetch_std();
            $standard['years'] = $this->Academic_year_model->getAllYears();
            $standard['school'] = $this->Student_model->fetch_school();
            $this->load->view('Fee/index', $standard);
            $this->load->view('Students/view_fee');
            $this->load->view('footer');
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }

    }

    /**
     * @Desc : for fetch all student details
     */
    public function fetch_data_fee()
    {

        $list = $this->Fee_model->get_datatables();
        $data = array();

        foreach ($list as $rows) {
            $row = array();

            $row[] = $rows->gr_number;
            $row[] = $rows->firstname . ' ' . $rows->father_name . ' ' . $rows->surname;
            $row[] = $rows->standard . ' ' . $rows->divison;
            $row[] = $rows->category;
            if ($rows->category == 'RTE') {
                $row[] = 'No Fee';
            } elseif ($rows->remaining_fee == '0' && $rows->remaining_fee !== NULL) {
                $row[] = 'No Pending Fee';
            } else {
                $row[] = '<a href="take_fee?id=' . $rows->student_id . '" class="btn btn-warning btn-sm" >Collect Fee</a>
                <button  name="view" data-toggle="modal" data-target="#feeModal" id="' . $rows->student_id . '" class="btn btn-sm btn-info pending_view_fee">View Fee</button>';
            }
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Fee_model->count_all(),
            "recordsFiltered" => $this->Fee_model->count_filtered(),
            "data" => $data,
        );

        echo json_encode($output);
    }

    /**
     * @Desc : for take fee
     */
    public function take_fee()
    {

            $id = $this->input->get('id');

            if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'admin') {
                $this->db->select('school_id');
                $array = array('username' => $_SESSION['username']);
                $this->db->where($array);
                $query = $this->db->get('tbl_admin');
                $result_school = $query->result();
                $array=json_decode(json_encode($result_school), true);
                $school_ids = array_column($array, 'school_id');

                $this->db->select('school_id,academic_year');
                $this->db->where_in('school_id', $school_ids);
                $this->db->where('id', $id);
                $query = $this->db->get('tbl_student');
                $result = $query->num_rows();
                $student_year_school = $query->result();
                if(is_array($student_year_school) && count($student_year_school) > 0) {
                    $student_fee_year = $student_year_school[0]->school_id."-".$student_year_school[0]->academic_year;
                } else {
                    $student_fee_year='';
                }

                if ($result > 0) {
                    $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
                    $this->load->view('header', $url_name);
                    $student['data'] = array();
                    $student['years'] = $this->Academic_year_model->getAllYears();
                    $student['remaining_amount'] = '0';

                    $student_data = $this->Take_fee_model->take_fee($id,$student_fee_year);

                    $student['data'] = $student_data;
                
                    $this->db->select('feestructure_flag');
                    $this->db->where('id',$student_data[0]->school_id);
                    $flag_result=$this->db->get('tbl_school');
                    $student['flag']=$flag_result->result();
                    $feeflag=$student['flag'][0]->feestructure_flag;

                    if($feeflag == '4'){
                        $result = $this->Take_fee_model->get_last_month($id);

                        if($result != ''){
                            $student['pending_fee']=$result->pending_fee;
                        }else{
                            $student['pending_fee']='0';
                        }
                        $monthinstallment = array("first", "second", "third", "fourth");
                        $student['fee_installment'] = '';
                        if ($result != '') {
                            $last_fee_period = $result->fee_month;

                            if (strstr($last_fee_period, '- ')) {
                                $last_fee_month = trim(substr($last_fee_period, strrpos($last_fee_period, '- ') + 1));
                            } else {
                                $last_fee_month = $result->fee_month;
                            }

                            $index = array_search($last_fee_month, $monthinstallment) + 1;
                            $get_month = $monthinstallment[$index];
                            $student['fee_installment'] = $get_month;
                            $student['paid_fee'] = " ";
                        } else {
                            $student['fee_installment'] = "first";
                            $student['paid_fee'] = " ";
                        }
                    }else{
                        //for get last fee month
                        $last_fee_period = array();
                        $result = $this->Take_fee_model->get_last_month($id);

                        $monthNames = array("June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May");
                        $monthValues = array("6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5");
                        if($result != ''){
                            $student['pending_fee']=$result->pending_fee;
                        }else{
                            $student['pending_fee']='0';
                        }

                        $student['fee_month'] = '';
                        if ($result != '') {
                            $last_fee_period = $result->fee_month;

                            if (strstr($last_fee_period, '- ')) {
                                $last_fee_month = trim(substr($last_fee_period, strrpos($last_fee_period, '- ') + 1));
                            } else {
                                $last_fee_month = $result->fee_month;
                            }

                            $index = array_search($last_fee_month, $monthNames) + 1;
                            $get_month = $monthNames[$index];
                            $student['fee_month'] = $get_month;
                            $student['paid_fee'] = " ";
                        } else {
                            $student['fee_month'] = "June";
                            $student['paid_fee'] = " ";
                        }
                    }
                    //for get remaining fee
                    $remaining = $this->Take_fee_model->remaining_fee($id);

                    $student['remaining_fee'] = $remaining;
                    if ($student['remaining_fee'][0]->remaining_fee > '0') {
                        $student['years'] = $this->Academic_year_model->getAllYears();
                        $this->load->view('Fee/take_fee', $student);
                        $this->load->view('footer');
                    } else {
                        $this->session->set_flashdata('response_error', "Please First Add Admission-fee,Tution-fee,Term-fee,Other-fee value for this standard.");
                        redirect('fee/');
                    }
                }else{
                    if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
                        $this->session->set_flashdata('response_error', "Please login with this school's id and password to access page");
                        redirect('home/');
                    }else{
                        $response['message'] = "Please login to access page";
                        $this->load->view('Auth/login', $response);
                    }
                }
            }else if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
                $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
                $this->load->view('header', $url_name);
                $student['data'] = array();
                $student['years'] = $this->Academic_year_model->getAllYears();
                $student['remaining_amount'] = '0';

                $student_data = $this->Take_fee_model->take_fee($id);
                $student['data'] = $student_data;

                $this->db->select('feestructure_flag');
                $this->db->where('id',$student_data[0]->school_id);
                $flag_result=$this->db->get('tbl_school');
                $student['flag']=$flag_result->result();
                $feeflag=$student['flag'][0]->feestructure_flag;

                if($feeflag == '4'){
                    $result = $this->Take_fee_model->get_last_month($id);
                    if($result != ''){
                        $student['pending_fee']=$result->pending_fee;
                    }else{
                        $student['pending_fee']='0';
                    }
                    $monthinstallment = array("first", "second", "third", "fourth");
                    $student['fee_installment'] = '';
                    if ($result != '') {
                        $last_fee_period = $result->fee_month;

                        if (strstr($last_fee_period, '- ')) {
                            $last_fee_month = trim(substr($last_fee_period, strrpos($last_fee_period, '- ') + 1));
                        } else {
                            $last_fee_month = $result->fee_month;
                        }

                        $index = array_search($last_fee_month, $monthinstallment) + 1;
                        $get_month = $monthinstallment[$index];
                        $student['fee_installment'] = $get_month;
                        $student['paid_fee'] = " ";
                    } else {
                        $student['fee_installment'] = "first";
                        $student['paid_fee'] = " ";
                    }
                }else{
                    //for get last fee month
                    $last_fee_period = array();
                    $result = $this->Take_fee_model->get_last_month($id);

                    $monthNames = array("June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May");
                    $monthValues = array("6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5");
                    if($result != ''){
                        $student['pending_fee']=$result->pending_fee;
                    }else{
                        $student['pending_fee']='0';
                    }

                    $student['fee_month'] = '';
                    if ($result != '') {
                        $last_fee_period = $result->fee_month;

                        if (strstr($last_fee_period, '- ')) {
                            $last_fee_month = trim(substr($last_fee_period, strrpos($last_fee_period, '- ') + 1));
                        } else {
                            $last_fee_month = $result->fee_month;
                        }

                        $index = array_search($last_fee_month, $monthNames) + 1;
                        $get_month = $monthNames[$index];
                        $student['fee_month'] = $get_month;
                        $student['paid_fee'] = " ";
                    } else {
                        $student['fee_month'] = "June";
                        $student['paid_fee'] = " ";
                    }
                }

                //for get remaining fee
                $remaining = $this->Take_fee_model->remaining_fee($id);
                $student['remaining_fee'] = $remaining;

                if ($student['remaining_fee'][0]->remaining_fee > '0') {
                    $this->load->view('Fee/take_fee', $student);
                    $this->load->view('footer');
                } else {
                    $this->session->set_flashdata('response_error', "Please First Add Admission-fee,Tution-fee,Term-fee,Other-fee value for this standard.");
                    redirect('fee/');
                }
            }else {
                $response['message'] = "Please login to access page";
                $this->load->view('Auth/login', $response);
            }

    }
    /**
     * @Desc : for get previous month fee
     */
    public function get_previous_fee(){

        $id = $this->input->post('id');

        $fee_year = $this->input->post('fee_year');

        /*if($fee_year){
            $this->db->select('*');
            $this->db->where('id', $id);
            $query = $this->db->get('tbl_student');
            $result = $query->num_rows();
            if($result > 0){
                $row = $query->result_array();
                foreach ($row as $rows){
                    $firstname = $rows['firstname'];
                    $fathername = $rows['father_name'];
                    $surname = $rows['surname'];
                    $birthdate = $rows['birthdate'];
                    $school = $rows['school_id'];
                }
                $this->db->select('*');
                $array = array('birthdate' => $birthdate,'firstname' => $firstname,'father_name' => $fathername,'surname' => $surname,'academic_year' => $fee_year);
                $this->db->where($array);
                $student_query = $this->db->get('tbl_student');
                $student_row = $student_query->result_array();
                $result = $student_query->num_rows();
                foreach ($student_row as $student_rows){
                    $id = $student_rows['id'];
                }
            }
        }else{
            $id = $this->input->post('id');
        }*/
        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'admin') {
            $this->db->select('school_id');
            $array = array('username' => $_SESSION['username']);
            $this->db->where($array);
            $query = $this->db->get('tbl_admin');
            $result_school = $query->result();
            $array=json_decode(json_encode($result_school), true);
            $school_ids = array_column($array, 'school_id');

            $this->db->select('school_id');
            $this->db->where_in('school_id', $school_ids);
            $this->db->where('id', $id);
            $query = $this->db->get('tbl_student');
            $result = $query->num_rows();

            if ($result > 0) {
                $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
                $this->load->view('header', $url_name);
                $student['data'] = array();
                $student['remaining_amount'] = '0';

                $this->db->select('school_id');
                $this->db->where('id',$id);
                $this->db->where('academic_year',$fee_year);
                $f_result=$this->db->get('tbl_student');
                $student_f=$f_result->result();
                $student_fee_year = $student_f[0]->school_id.'-'.$fee_year;

                $student_data = $this->Take_fee_model->take_fee($id,$student_fee_year);
                $student['data'] = $student_data;
                $this->db->select('feestructure_flag');
                $this->db->where('id',$student_data[0]->school_id);
                $flag_result=$this->db->get('tbl_school');
                $student['flag']=$flag_result->result();
                $feeflag=$student['flag'][0]->feestructure_flag;
                if($feeflag == '4'){
                    $result = $this->Take_fee_model->get_last_month($id);
                    if($result != ''){
                        $student['pending_fee']=$result->pending_fee;
                    }else{
                        $student['pending_fee']='0';
                    }
                    if($fee_year != '1'){
                        $monthinstallment = array("first", "second", "third");
                    }else{
                        $monthinstallment = array("first", "second", "third", "fourth");
                    }
                    $student['fee_installment'] = '';
                    if ($result != '') {
                        $last_fee_period = $result->fee_month;

                        if (strstr($last_fee_period, '- ')) {
                            $last_fee_month = trim(substr($last_fee_period, strrpos($last_fee_period, '- ') + 1));
                        } else {
                            $last_fee_month = $result->fee_month;
                        }

                        $index = array_search($last_fee_month, $monthinstallment) + 1;
                        $get_month = $monthinstallment[$index];
                        $student['fee_installment'] = $get_month;
                        $student['paid_fee'] = " ";
                    } else {
                        $student['fee_installment'] = "first";
                        $student['paid_fee'] = " ";
                    }
                }else{
                    //for get last fee month
                    $last_fee_period = array();
                    $result = $this->Take_fee_model->get_last_month($id);

                    $monthNames = array("June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May");
                    $monthValues = array("6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5");
                    if($result != ''){
                        $student['pending_fee']=$result->pending_fee;
                    }else{
                        $student['pending_fee']='0';
                    }

                    $student['fee_month'] = '';
                    if ($result != '') {
                        $last_fee_period = $result->fee_month;

                        if (strstr($last_fee_period, '- ')) {
                            $last_fee_month = trim(substr($last_fee_period, strrpos($last_fee_period, '- ') + 1));
                        } else {
                            $last_fee_month = $result->fee_month;
                        }
                        if($last_fee_month == 'May'){
                            echo 'Not remaining for previous year';
                            exit();
                        }
                        $index = array_search($last_fee_month, $monthNames) + 1;
                        $get_month = $monthNames[$index];
                        $student['fee_month'] = $get_month;
                        $student['paid_fee'] = " ";
                    } else {
                        $student['fee_month'] = "June";
                        $student['paid_fee'] = " ";
                    }
                }
                //for get remaining fee
                $remaining = $this->Take_fee_model->remaining_fee($id,$fee_year);
                $student['remaining_fee'] = $remaining;
                $student['fee_year'] = $fee_year;
                if($student){
                    print_r(json_encode($student));
                    exit();
                }else{
                    echo 'Not remaining for previous year';
                    exit();
                }
            }else{
                if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
                    $this->session->set_flashdata('response_error', "Please login with this school's id and password to access page");
                    redirect('home/');
                }else{
                    $response['message'] = "Please login to access page";
                    $this->load->view('Auth/login', $response);
                }
            }
        }else if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);
            $student['data'] = array();
            $student['remaining_amount'] = '0';

            $this->db->select('school_id');
            $this->db->where('id',$id);
            $this->db->where('academic_year',$fee_year);
            $f_result=$this->db->get('tbl_student');
            $student_f=$f_result->result();

            $student_fee_year = $student_f[0]->school_id.'-'.$fee_year;

            $student_data = $this->Take_fee_model->take_fee($id,$student_fee_year);
            $student['data'] = $student_data;

            $this->db->select('feestructure_flag');
            $this->db->where('id',$student_data[0]->school_id);
            $flag_result=$this->db->get('tbl_school');
            $student['flag']=$flag_result->result();
            $feeflag=$student['flag'][0]->feestructure_flag;

            if($feeflag == '4'){
                $result = $this->Take_fee_model->get_last_month($id);
                if($result != ''){
                    $student['pending_fee']=$result->pending_fee;
                }else{
                    $student['pending_fee']='0';
                }
                if($fee_year != '1'){
                    $monthinstallment = array("first", "second", "third");
                }else{
                    $monthinstallment = array("first", "second", "third", "fourth");
                }

                $student['fee_installment'] = '';
                if ($result != '') {
                    $last_fee_period = $result->fee_month;

                    if (strstr($last_fee_period, '- ')) {
                        $last_fee_month = trim(substr($last_fee_period, strrpos($last_fee_period, '- ') + 1));
                    } else {
                        $last_fee_month = $result->fee_month;
                    }

                    $index = array_search($last_fee_month, $monthinstallment) + 1;
                    $get_month = $monthinstallment[$index];
                    $student['fee_installment'] = $get_month;
                    $student['paid_fee'] = " ";
                } else {
                    $student['fee_installment'] = "first";
                    $student['paid_fee'] = " ";
                }
            }else{
                //for get last fee month
                $last_fee_period = array();
                $result = $this->Take_fee_model->get_last_month($id);

                $monthNames = array("June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May");
                $monthValues = array("6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5");
                if($result != ''){
                    $student['pending_fee']=$result->pending_fee;
                }else{
                    $student['pending_fee']='0';
                }

                $student['fee_month'] = '';
                if ($result != '') {
                    $last_fee_period = $result->fee_month;

                    if (strstr($last_fee_period, '- ')) {
                        $last_fee_month = trim(substr($last_fee_period, strrpos($last_fee_period, '- ') + 1));
                    } else {
                        $last_fee_month = $result->fee_month;
                    }
                    if($last_fee_month == 'May'){
                        echo 'Not remaining for previous year';
                        exit();
                    }
                    $index = array_search($last_fee_month, $monthNames) + 1;
                    $get_month = $monthNames[$index];
                    $student['fee_month'] = $get_month;
                    $student['paid_fee'] = " ";
                } else {
                    $student['fee_month'] = "June";
                    $student['paid_fee'] = " ";
                }
            }

            //for get remaining fee
            $remaining = $this->Take_fee_model->remaining_fee($id,$fee_year);
            $student['remaining_fee'] = $remaining;
            $student['fee_year'] = $fee_year;
            if($student){
                print_r(json_encode($student));
                exit();
            }else{
                echo 'Not remaining for previous year';
                exit();
            }

        }else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }
    /**
     * @Desc : for paid fee for one month
     */
    public function paid_for_onemonth()
    {
        $output = array();
        $id = $this->input->post('id');

        $student_data = $this->Take_fee_model->take_fee($id);
        $student['data'] = $student_data;

        $result = $this->Take_fee_model->paid_amount($id);
        if ($result != '') {
            $paid = $result->paid_fee;
        }
        $total_fee = $student['data'][0]->standard_fee;
        $paid_amount = $total_fee / 12;
        if ($result != '' && $result->period == 1 && $paid < $paid_amount) {
            $difference = $paid_amount - $paid;
            $student['paid_fee'] = $paid_amount + $difference;
        } else {
            $difference = 0;
            $student['paid_fee'] = $paid_amount;
        }
        $output['reaming_last'] = $difference;
        echo json_encode($output);
    }

    /**
     * @Desc : for view fee details
     */
    public function view_fee()
    {
        $output = array();

        $id = $this->input->post('id');

        $student_data = $this->Fee_model->fetch_fee_data($id);

        $result = $this->Fee_model->fetch_student_fee($id);

        $fee_details = array();
        for ($i = 0; $i < count($result); $i++) {
            $fee_details[$i]["paid_fee"] = $result[$i]['paid_fee'];
            $fee_details[$i]["fee_month"] = $result[$i]['fee_month'];
            $fee_details[$i]["payment_date"] = $result[$i]['payment_date'];
        }
        $output['student_data'] = $student_data;
        $output['fee_details'] = $fee_details;
        echo json_encode($output);
    }
    /**
     * @Desc : for fee fetails PDF
     */
    public function fee_details()
    {
        $s_id=$this->input->post('take_id');

        if(!isset($s_id) && empty($s_id) && $s_id=="") {
            header("location:/fee");
            exit;
        }

        $fee_year=$this->input->post('studentFeeYear');

        $this->db->select('school_id');
        $this->db->where('id',$s_id);
        $this->db->where('academic_year',$fee_year);
        $s_query = $this->db->get('tbl_student');
        $school = $s_query->result();
        $school_id=$school[0]->school_id;

        $this->db->select('*');
        $this->db->where('school_id',$school_id);
        $this->db->where('fee_year',$fee_year);
        $query = $this->db->get('tbl_fee_details');
        $last = $query->last_row();

        if ($last) {
            $receipt_no = $last->receipt_no + 1;
        } else {
            $receipt_no = 1;
        }
        $paid_amount=$this->input->post('paid_amount');
        $total_paid=$this->input->post('total_paid_fee');
        if($total_paid){
            $paid_fee=$total_paid;
            if($total_paid == $paid_amount){
                $pending_fee=0;
            }else{
                $pending_fee=((int)$paid_amount)-((int)$total_paid);
            }
        }else{
            $paid_fee=$paid_amount;
            $pending_fee=0;
        }

        $previospendingfee=$this->input->post('pending_fee');
        if($pending_fee == 0){
            $paid_pending=$previospendingfee;
        }else{
            $paid_pending=0;
        }
        //for paid amount enter in database

            $data = array(
                'receipt_no' => $receipt_no,
                'student_id' => $this->input->post('take_id'),
                'school_id' =>$school_id,
                'period' => $this->input->post('fee_duration'),
                'paid_fee' => $paid_fee,
                'pending_fee' => $pending_fee,
                'fee_month' => $this->input->post('fee_period'),
                'payment_date' => date('Y-m-d',strtotime($this->input->post('take_date'))),
                'cheque_no' => $this->input->post('cheque_no'),
                'cheque_date' => $this->input->post('cheque_date'),
                'admission_fee' => $this->input->post('paid_admission_fee'),
                'tution_fee' => $this->input->post('paid_tution_fee'),
                'term_fee' => $this->input->post('paid_term_fee'),
                'other_fee' => $this->input->post('paid_other_fee'),
                'smart_class_fee' =>$this->input->post('paid_smart_fee'),
                'computer_fee' => $this->input->post('paid_computer_fee'),
                'sports_fee' => $this->input->post('paid_sports_fee'),
                'exam_fee' => $this->input->post('paid_exam_fee'),
                'insurance_fee' => $this->input->post('paid_insurance_fee'),
                'enroll_fee' => $this->input->post('paid_enroll_fee'),
                'discount' => $this->input->post('discount'),
                'concession' => $this->input->post('concession'),
                'paid_pendingfee' => $paid_pending,
                'trust_name' => $this->input->post('trust_name'),
                'fee_year'=> $this->input->post('studentFeeYear'),
                'created_on' => date("Y-m-d H:i:s")
            );
            
            $insert = $this->db->insert('tbl_fee_details', $data);
            $concession = $this->input->post('concession');
            $discount = $this->input->post('discount');
            $paid_other_fee = $this->input->post('paid_other_fee');
            $total_paid = $paid_fee;

            if($paid_other_fee && $paid_other_fee > 0) {
                $total_paid = $paid_fee + $paid_other_fee;
            }

            if ($concession && $concession > 0) {
                $total_paid = $total_paid - $concession;
            } else if($discount && $discount>0){
                $total_paid = $total_paid - $discount;
            }

            /*if($concession > 0 && $discount > 0) {
                $total_paid = $paid_fee + $discount + $concession;
            }
            if($paid_other_fee){
                $total_paid = $total_paid + $paid_other_fee;
            }*/

            $rem_fee = $this->input->post('remaining_fee');
            $fee_report = array(
                'remaining_fee' => ($rem_fee - $total_paid)
            );
            $this->db->where("id", $this->input->post('take_id'));
            $remaining_fee = $this->db->update("tbl_student", $fee_report);

            $id = $this->input->post('take_id');
            $fee_year=$this->input->post('studentFeeYear');

            if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $student['fee_details'] = $this->Daily_report_model->fee_details($id);

            $student['student_data'] = $this->Daily_report_model->student_details($id);

            $school = array();
            $this->db->select('school_id');
            $this->db->where('id',$id);
            $this->db->where('academic_year',$fee_year);
            $student_query=$this->db->get('tbl_student');
            $student_result=$student_query->result();
            $this->db->select('*');
            $this->db->where('id', $student_result[0]->school_id);
            $school_query = $this->db->get('tbl_school');
            $student['school_name'] = $school_query->result();

            $feeYear = $student['fee_details']->fee_year;
            $student['academic_year'] = $this->Academic_year_model->getYear($feeYear);
            $this->load->view('Fee/FeeReceiptPDF', $student);

            $html = $this->output->get_output();

            $customPaper = array(0,0,425,566);
            $this->pdf->generate($html, 'fee_receipt',$customPaper,TRUE,'potrait');
            exit();


        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }

    }
    /**
     * @Desc : for fee fetails PDF
     */
    public function UpdateFeesStudents()
    {
        //$student_id = 5446;
        //$student_data = $this->Fee_model->getStudentTotalFeeDetails($student_id);
        $student_data = $this->Fee_model->getStudentTotalFeeDetails();
        $current_year = date("Y");

        $updated_count = 0;
        for($i=0;$i<count($student_data);$i++) {
            $remaining_fee = 0;
            $admission_year = $student_data[$i]['admission_year'];
            $standard = $student_data[$i]['standard'];
            $student_id = $student_data[$i]['id'];
            $firstname = $student_data[$i]['firstname'];
            $surname = $student_data[$i]['surname'];
            $school_name = $student_data[$i]['school_name'];
            $division = $student_data[$i]['division'];

            if ($admission_year == $current_year|| $standard == '1' || $standard == 'I' || $standard == '9') {
                $new_addmission = true;
            } else {
                $new_addmission = false;
            }
            if($student_data[$i]['feestructure_flag']=='1') {
                if($new_addmission) {
                    $admission_fee = $student_data[$i]['admission_fee'];
                } else {
                    $admission_fee = 0;
                }
                $tution_fee = ($student_data[$i]['tuition_fee'] * 12);
                $term_fee = ($student_data[$i]['term_fee'] * 2);
                $smart_class_fee = $student_data[$i]['smart_class_fee'];
                $computer_fee = $student_data[$i]['computer_fee'];
                $sports_fee = $student_data[$i]['sports_fee'];
                $insurance_fee = $student_data[$i]['insurance_fee'];
                $exam_fee = $student_data[$i]['exam_fee'];
                $enroll_fee = $student_data[$i]['enroll_fee'];
                $other_fee = 0;
            }
            else if($student_data[$i]['feestructure_flag']=='2') {
                if($new_addmission) {
                    $admission_fee = $student_data[$i]['admission_fee'];
                } else {
                    $admission_fee = 0;
                }
                $tution_fee = ($student_data[$i]['tuition_fee'] * 12);
                $term_fee = ($student_data[$i]['term_fee'] * 2);
                $smart_class_fee = 0;
                $computer_fee = 0;
                $sports_fee = 0;
                $insurance_fee = 0;
                $exam_fee = 0;
                $enroll_fee = 0;
                $other_fee = $student_data[$i]['other_fee'];
            }
            else if($student_data[$i]['feestructure_flag']=='3') {
                if($new_addmission) {
                    $admission_fee = $student_data[$i]['admission_fee'];
                } else {
                    $admission_fee = 0;
                }
                $tution_fee = ($student_data[$i]['tuition_fee'] * 12);
                $term_fee = ($student_data[$i]['term_fee'] * 2);
                $smart_class_fee = 0;
                $computer_fee = 0;
                $sports_fee = 0;
                $insurance_fee = 0;
                $exam_fee = 0;
                $enroll_fee = 0;
                $other_fee = ($student_data[$i]['other_fee'] * 2);
            }
            else
            {
                $admission_fee = 0;
                $tution_fee = $student_data[$i]['tuition_fee'];
                $term_fee = 0;
                $smart_class_fee = 0;
                $computer_fee = 0;
                $sports_fee = 0;
                $insurance_fee = 0;
                $exam_fee = 0;
                $enroll_fee = 0;
                $other_fee = $student_data[$i]['other_fee'];
            }

            $total_fee = $admission_fee + $tution_fee + $term_fee + $smart_class_fee + $computer_fee + $sports_fee +  $insurance_fee + $exam_fee + $enroll_fee + $other_fee;
            $student_paid_fee_data = $this->Fee_model->getStudentPaidFeeDetails($student_id);
            if(is_array($student_paid_fee_data) && count($student_paid_fee_data) > 0) {
                $total_paid_fee = $student_paid_fee_data[0]['total_fee'];
                $total_paid_discount = $student_paid_fee_data[0]['total_discount'];
                $total_paid_concession = $student_paid_fee_data[0]['total_concession'];
            } else {
                $total_paid_fee = 0;
                $total_paid_discount = 0;
                $total_paid_concession = 0;
            }
            if($total_fee > $total_paid_fee) {
                $remaining_fee = $total_fee - $total_paid_fee;
            } else {
                $remaining_fee = 0;
            }

            $total_discount = $total_paid_discount + $total_paid_concession;
            if($total_discount > 0 && $remaining_fee >= $total_discount) {
                $remaining_fee = $remaining_fee - $total_discount;
            }
            echo "=========================================================="."<br/>";
            echo "Student ID : " .$student_id."<br/>";
            echo "Student Name : " .$firstname." ".$surname."<br/>";
            echo "Student STD : " .$standard." ".$division."<br/>";
            echo "Student School : " .$school_name."<br/>";
            echo "Total Fee : " .$total_fee."<br/>";
            echo "Total Paid Fee : " .$total_paid_fee."<br/>";
            echo "Remaining Fee : " .$remaining_fee."<br/>";
            echo "Total Discount : " .$total_paid_discount."<br/>";
            echo "Total Concession : " .$total_paid_concession."<br/><br/>";


            $updated_count = $updated_count + 1;
            $this->Fee_model->updateStudentRemainingFeeDetails($remaining_fee,$student_id);
            echo "=========================================================="."<br/><br/>";
        }

        print "Total $updated_count students remaining fees updated successfully";
        exit;

    }
}