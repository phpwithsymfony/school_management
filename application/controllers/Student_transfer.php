<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_transfer extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->library('form_validation');
        $this->load->model("Student_model");
        $this->load->model("Manage_standard_model");
        $this->load->database();
        $this->load->library('pdf');
        date_default_timezone_set('Asia/Kolkata');
    }

    /**
     * @Desc : show all students list
     */
    public function index()
    {

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);

            $standard['std'] = $this->Manage_standard_model->fetch_std();
            $standard['school'] = $this->Student_model->fetch_school();

            $this->load->view('Student_transfer/index', $standard);
            $this->load->view('Students/view_fee');
            $this->load->view('footer');
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }

    }

    /**
     * @Desc : fetch student list
     */
    public function fetch_student_transferdetails()
    {
        $fetch_data = $this->Student_model->make_datatables();
        $data = array();

        foreach ($fetch_data as $row) {
            $sub_array = array();
            $sub_array[] = $row->gr_number;
            $sub_array[] = $row->firstname . ' ' . $row->father_name . ' ' . $row->surname;
            $sub_array[] = $row->standard . ' ' . $row->divison;
            $sub_array[] = $row->medium;
            $sub_array[] = $row->category;
            if ($_SESSION['role'] == 'super_admin') {
                $sub_array[] = $row->name;
            }
            $sub_array[] ='<a href="" data-toggle="modal" data-target="#student_transfer_modal" type="button" name="transfer" id="' . $row->id . '" class="btn btn-info btn-sm transferstudent">Transfer</a>';

            $data[] = $sub_array;
        }
        $output = array(
            "draw" => intval($_POST["draw"]),
            "recordsTotal" => $this->Student_model->get_all_data(),
            "recordsFiltered" => $this->Student_model->get_filtered_data(),
            "data" => $data
        );
        echo json_encode($output);
    }
    /**
     * @Desc : fetch school list
     */
    public function get_schoolname()
    {
        $id=$this->input->post('id');

        $this->db->select('school_id,standard_id,divison');
        $this->db->where('id',$id);
        $student=$this->db->get('tbl_student');
        $student_data=$student->result();

        $this->db->select('standard');
        $this->db->where('id',$student_data[0]->standard_id);
        $student=$this->db->get('tbl_standard_management');
        $standard_data=$student->result();

        $this->db->select('*');
        $school=$this->db->get('tbl_school');
        $school_details=$school->result();
        $output['student_data'] = $student_data;
        $output['school_details'] = $school_details;
        $output['standard_data'] = $standard_data;
        echo json_encode($output);
    }
    /**
     * @Desc : check password for student transfer
     */
    public function confirm_transfer()
    {
        $school=$this->input->post('transfer_school_id');
        $student=$this->input->post('transfer_student_id');
        $password=md5($this->input->post('school_password'));
        $school_name=$this->input->post('transstudent_school');

        $this->db->select('password');
        $this->db->where('school_id',$school);
        $query=$this->db->get('tbl_admin');
        $result=$query->result();

        $newstd=$this->input->post('transstudent_standard');
        $this->db->select('*');
        $this->db->where('standard_mgt_id',$newstd);
        $getstd=$this->db->get('tbl_standard');
        $getresult=$getstd->result();
        $schoolid=$getresult[0]->school_id;
        $admission_fee=$getresult[0]->admission_fee;
        $tuition_fee=$getresult[0]->tuition_fee;
        $term_fee=$getresult[0]->term_fee;
        $other_fee=$getresult[0]->other_fee;
        $smart_class_fee=$getresult[0]->smart_class_fee;
        $computer_fee=$getresult[0]->computer_fee;
        $sports_fee=$getresult[0]->sports_fee;
        $insurance_fee=$getresult[0]->insurance_fee;
        $exam_fee=$getresult[0]->exam_fee;
        $enroll_fee=$getresult[0]->enroll_fee;

        $this->db->select('feestructure_flag');
        $this->db->where('id',$school_name);
        $getschool=$this->db->get('tbl_school');
        $getschool=$getschool->result();
        $flag=$getschool[0]->feestructure_flag;

        $this->db->select('standard');
        $this->db->where('id',$newstd);
        $standard_query=$this->db->get('tbl_standard_management');
        $standard_result=$standard_query->result();

        $this->db->select('admission_year,category');
        $this->db->where('id',$student);
        $student_query=$this->db->get('tbl_student');
        $student_result=$student_query->result();
        $admission_year=$student_result[0]->admission_year;
        $category=$student_result[0]->category;

        $current_year = date("Y");


        $this->db->where('student_id',$student);
        $this->db->delete('tbl_fee_details');

        if ($category == 'General') {
            if($flag == '1'){
                if (count($getresult) > 0) {
                    if ($admission_year == $current_year || $standard_result[0]->standard == '1' || $standard_result[0]->standard == 'I') {
                        $remaining_fee = $admission_fee + ($tuition_fee) * 12 + $term_fee + $term_fee + $smart_class_fee + $computer_fee + $sports_fee + $insurance_fee + $exam_fee+ $enroll_fee;
                    } else {
                        $remaining_fee = ($tuition_fee) * 12 + $term_fee + $term_fee + $smart_class_fee + $computer_fee + $sports_fee + $insurance_fee + $exam_fee+ $enroll_fee;
                    }
                } else {
                    $remaining_fee = NULL;
                }
            }else if($flag == '2'){
                if (count($getresult) > 0) {
                    if ($admission_year == $current_year || $standard_result[0]->standard == '1' || $standard_result[0]->standard == 'I') {
                        $remaining_fee = $admission_fee + ($tuition_fee) * 12 + $term_fee + $term_fee  + $other_fee;
                    } else {
                        $remaining_fee = ($tuition_fee) * 12 + $term_fee+ $term_fee + $other_fee;
                    }

                } else {
                    $remaining_fee = NULL;
                }
            }else{
                if (count($getresult) > 0) {
                    if ($admission_year == $current_year ||  $standard_result[0]->standard == '1' || $standard_result[0]->standard == 'I') {
                        $remaining_fee = $admission_fee + ($tuition_fee) * 12 + $term_fee + $term_fee + $other_fee + $other_fee;
                    } else {
                        $remaining_fee = ($tuition_fee) * 12 + $term_fee + $term_fee + $other_fee + $other_fee;
                    }

                } else {
                    $remaining_fee = NULL;
                }
            }
        } else {
            $remaining_fee = '0';
        }

        if($password==$result[0]->password){

            $updated_data = array(
                'school_id' => $school_name,
                'standard_id' => $this->input->post('transstudent_standard'),
                'divison' =>$this->input->post('transstudent_div'),
                'remaining_fee' =>$remaining_fee,
                'updated' => date("Y-m-d H:i:s")
            );
            $this->db->where("id", $student);
            $this->db->update("tbl_student", $updated_data);
            echo "Student Transfer Successfully.";
        }else{
            echo "Please Enter Correct Password";
        }
    }
    /**
     * @Desc : fetch standard value
     */
    public function get_std_value()
    {
        $school_id = $this->input->post('transstudent_school');

        $this->db->select('id,standard');
        $this->db->where('school_id',$school_id);
        $query=$this->db->get('tbl_standard_management');
        $result=$query->result();

        echo json_encode($result);

    }
    /**
     * @Desc : fetch division value
     */
    public function get_division_value()
    {
        $stadard_id = $this->input->post('standard');

        $this->db->select('division');
        $this->db->where('id',$stadard_id);
        $query=$this->db->get('tbl_standard_management');
        $result=$query->result();
        $division = array();
        foreach ($result as $row) {
            array_push($division, $row->division);
        }
        $div = implode(",", $division);
        $get_div = explode(',', $div);
        $final_div = array_unique($get_div);
        $output['division'] =$final_div;
        echo json_encode($output);

    }
}