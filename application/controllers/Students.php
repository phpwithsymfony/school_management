<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->library('form_validation');
        $this->load->model("Student_model");
        $this->load->model("Manage_standard_model");
        $this->load->model('Academic_year_model');
        $this->load->database();
        $this->load->library('pdf');
        date_default_timezone_set('Asia/Kolkata');
    }

    /**
     * @Desc : show all students list
     */
    public function index()
    {

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);

            $standard['std'] = $this->Manage_standard_model->fetch_std();
            $standard['school'] = $this->Student_model->fetch_school();
            $standard['years'] = $this->Academic_year_model->getAllYears();

            $this->load->view('Students/index', $standard);
            $this->load->view('Students/view_fee');
            $this->load->view('footer');
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }

    }

    /**
     * @Desc : fetch student list
     */
    public function fetch_student_details()
    {
        $fetch_data = $this->Student_model->make_datatables();
        $data = array();
        $i=0;
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $sub_array[] = '<input type="checkbox" class="edit_Student" id="editStudent_'.$row->id.'" onclick="checkcheckbox();" value="'.$row->id.'""/>';
            $sub_array[] = $row->gr_number;
            $sub_array[] = $row->firstname . ' ' . $row->father_name . ' ' . $row->surname;
            $sub_array[] = $row->email;
            $sub_array[] = $row->contact_no;
            $sub_array[] = $row->standard . ' ' . $row->divison;
            $sub_array[] = $row->medium;
            if ($_SESSION['role'] == 'super_admin') {
                $sub_array[] = $row->name;
            }
            if ($row->category == 'RTE') {
                $sub_array[] = 'No fee';
            } else {
                $sub_array[] = '<a href="" data-toggle="modal" data-target="#feeModal" type="button" name="view" id="' . $row->id . '" class="btn btn-info btn-sm pending_view_fee">View Fee</a>';
            }
            $sub_array[] = '<a  href="edit?id=' . $row->id . '" name="update" class="btn btn-warning btn-sm update" title="Edit Student"><span class="feather icon-edit"></span></a>&nbsp;&nbsp;<button class="btn btn-danger btn-sm delete" type="button" id="' . $row->id . '" onclick="confirm_password_modal(' . $row->id . ')" title="Delete Student"><span class="feather icon-trash-2" ></span></button>';
            $data[] = $sub_array;
        }
        $output = array(
//            "draw" => intval($_POST["draw"]),
//            "recordsTotal" => $this->Student_model->get_all_data(),
//            "recordsFiltered" => $this->Student_model->get_filtered_data(),
            "data" => $data
        );
        echo json_encode($output);
    }

    /**
     * @Desc : fetch standard value
     */
    public function get_standard_value()
    {
        $school_id = $this->input->post('school_id');

        $fetch_data = $this->Student_model->get_std_value($school_id);

        echo json_encode($fetch_data);

    }

    /**
     * @Desc : add student
     */
    public function add()
    {

        if ($this->input->post('add_student')) {
            $this->form_validation->set_rules("add_studentname", "Name", "required|alpha|min_length[2]|max_length[20]|trim");
            $this->form_validation->set_rules("add_studentmname", "Mother name", "required|alpha|min_length[2]|max_length[20]|trim");
            $this->form_validation->set_rules("add_studentfname", "Father name", "required|alpha|min_length[2]|max_length[20]|trim");
            $this->form_validation->set_rules("add_studentsurname", "Surname", "required|alpha|min_length[2]|max_length[20]|trim");
            $this->form_validation->set_rules("add_studentcontact", "Contact No.", "required");
            $this->form_validation->set_rules("add_studentbirthdate", "Birthdate", "required");
            $this->form_validation->set_rules("add_studentmedium", "Medium", "required");
            $this->form_validation->set_rules("add_studentcategory", "Category", "required");
            $this->form_validation->set_rules("add_studentschoolid", "School Id", "required");
            $this->form_validation->set_rules("add_studentaddress", "Address", "required");
            $this->form_validation->set_rules("add_studentcaste", "Caste", "required");
            $this->form_validation->set_rules("add_studentadmissiondate", "Admissiondate", "required");

            if ($this->form_validation->run() === TRUE) {

                if ($this->input->post('add_studentstream')) {
                    $student_stream = $this->input->post('add_studentstream');
                } else {
                    $student_stream = NULL;
                }
                if ($this->input->post('add_studentemail')) {
                    $student_email = $this->input->post('add_studentemail');
                } else {
                    $student_email = NULL;
                }
                if ($this->input->post('add_studentaltercontact')) {
                    $student_alter_contact = $this->input->post('add_studentaltercontact');
                } else {
                    $student_alter_contact = NULL;
                }
                if ($this->input->post('add_studentsection')) {
                    $student_section = $this->input->post('add_studentsection');
                } else {
                    $student_section = NULL;
                }

                $birthdate = $this->input->post('add_studentbirthdate');
                $newbirthdate = str_replace(',', '', $birthdate);
                $old_date_timestamp = strtotime($newbirthdate);
                $new_date = date('Y-m-d', $old_date_timestamp);

                $admission_date = $this->input->post('add_studentadmissiondate');
                $newadmission_date = str_replace(',', '', $admission_date);
                $admission_date_timestamp = strtotime($newadmission_date);
                $new_admission_date = date('Y-m-d', $admission_date_timestamp);

                $parts = explode('-', $new_admission_date);
                $admission_y = $parts[0];

                $currenYear = date("Y");
                $next_year = date('y', strtotime('+1 year'));

                $this->db->select('*');
                $query_academic=$this->db->get('tbl_academic_year');
                $result_academic=$query_academic->last_row();
                $last_fee_year = $result_academic->id;

                $last_grno = $this->input->post('add_studentgrno');
                $name = $this->input->post('add_studentname');
                $surname = $this->input->post('add_studentsurname');
                $father_name = $this->input->post('add_studentfname');
                $mother_name = $this->input->post('add_studentmname');
                $email = $student_email;
                $contact_no = $this->input->post('add_studentcontact');
                $address = $this->input->post('add_studentaddress');
                $gender = $this->input->post('add_studentgender');
                $birthdate = $new_date;
                $standard = $this->input->post('add_studentstdid');
                $division = $this->input->post('student_division');
                $medium = $this->input->post('add_studentmedium');
                $stream = $student_stream;
                $category = $this->input->post('add_studentcategory');
                $section = $student_section;
                $school_id = $this->input->post('add_studentid');
                $add_altercontact = $student_alter_contact;
                $caste = $this->input->post('add_studentcaste');
                $admission_date = $new_admission_date;
                $created = date("Y-m-d H:i:s");
                $created_by = $_SESSION['role'];
                $academic_year = $last_fee_year;
                $birthplace=$this->input->post('add_birthplace');
                $admission_year = $admission_y;

                $current_year = date("Y");

                $rem_fee = array();
                $std = $this->input->post('add_studentstdid');
                $school_id = $this->input->post('add_studentid');
                $rem_fee['fee'] = $this->Student_model->remaining_fee($std, $school_id);

                $this->db->select('feestructure_flag');
                $this->db->where('id',$school_id);
                $fee_query=$this->db->get('tbl_school');
                $fee_result=$fee_query->result();

                $this->db->select('standard');
                $this->db->where('id',$standard);
                $standard_query=$this->db->get('tbl_standard_management');
                $standard_result=$standard_query->result();

                if ($category == 'General') {
                    if($fee_result[0]->feestructure_flag == '1'){
                        if (count($rem_fee['fee']) > 0) {
                            if ($admission_y == $current_year || $standard_result[0]->standard == '1' || $standard_result[0]->standard == 'I' || $standard_result[0]->standard == '9') {
                                $rem = $rem_fee['fee']['0']->admission_fee + ($rem_fee['fee']['0']->tuition_fee) * 12 + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->smart_class_fee + $rem_fee['fee']['0']->	computer_fee + $rem_fee['fee']['0']->sports_fee + $rem_fee['fee']['0']->insurance_fee + $rem_fee['fee']['0']->exam_fee+ $rem_fee['fee']['0']->enroll_fee;
                            } else {
                                $rem = ($rem_fee['fee']['0']->tuition_fee) * 12 + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->smart_class_fee + $rem_fee['fee']['0']->	computer_fee + $rem_fee['fee']['0']->sports_fee + $rem_fee['fee']['0']->insurance_fee + $rem_fee['fee']['0']->exam_fee+ $rem_fee['fee']['0']->enroll_fee;
                            }
                        } else {
                            $rem = NULL;
                        }
                    }else if($fee_result[0]->feestructure_flag == '2'){
                        if (count($rem_fee['fee']) > 0) {
                            if ($admission_y == $current_year || $standard_result[0]->standard == '1' || $standard_result[0]->standard == 'I' || $standard_result[0]->standard == '9') {
                                $rem = $rem_fee['fee']['0']->admission_fee + ($rem_fee['fee']['0']->tuition_fee) * 12 + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->term_fee  + $rem_fee['fee']['0']->other_fee;
                            } else {
                                $rem = ($rem_fee['fee']['0']->tuition_fee) * 12 + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->other_fee;
                            }

                        } else {
                            $rem = NULL;
                        }
                    }else if($fee_result[0]->feestructure_flag == '4'){
                        if (count($rem_fee['fee']) > 0) {
                                $rem = $rem_fee['fee']['0']->tuition_fee;

                        } else {
                            $rem = NULL;
                        }
                    }else{
                        if (count($rem_fee['fee']) > 0) {
                            if ($admission_y == $current_year ||  $standard_result[0]->standard == '1' || $standard_result[0]->standard == 'I' || $standard_result[0]->standard == '9') {
                                $rem = $rem_fee['fee']['0']->admission_fee + ($rem_fee['fee']['0']->tuition_fee) * 12 + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->other_fee + $rem_fee['fee']['0']->other_fee;
                            } else {
                                $rem = ($rem_fee['fee']['0']->tuition_fee) * 12 + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->other_fee + $rem_fee['fee']['0']->other_fee;
                            }

                        } else {
                            $rem = NULL;
                        }
                    }
                } else {
                    $rem = '0';
                }
                try {
                    $this->db->trans_start(FALSE);
                    $this->Student_model->insert($last_grno, $standard, $division, $medium, $stream, $section, $admission_year, $name, $surname, $father_name, $mother_name, $email, $contact_no, $add_altercontact, $address, $gender, $birthdate, $category, $rem, $school_id, $caste, $admission_date, $created, $created_by,$academic_year,$birthplace);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() == FALSE) {
                        throw new Exception('Database error! Please try again..');
                    }
                    $this->session->set_flashdata('response', "Record Inserted Successfully.");
                    redirect('students/');
                } catch (Exception $e) {
                    $this->load->view('error');
                    return false;
                }
            }
        }


        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);
            $query = "SELECT * FROM tbl_school WHERE school_code IS NULL OR
school_code = ' '";
            $data = $this->db->query($query);
            $result = $data->result();
            $std_query = "SELECT * FROM tbl_standard_management";
            $std_data = $this->db->query($std_query);
            $std_result = $std_data->result();
            if ($result || $std_result == null) {
                $this->session->set_flashdata('response_error', "Please First Add school_code for all school or add standard of school");
                redirect('students/');
            } else {

                $student['standard'] = $this->Manage_standard_model->fetch_std();
                $this->load->model('Admin_model');

                $student['school_id'] = $this->Student_model->get_school_id();

                $student['school_data'] = $this->Student_model->fetch_school_data();
                $this->load->view('Students/add', $student);
                $this->load->view('footer');
            }
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }


    /**
     * @Desc : edit student
     */
    public function edit()
    {

        if ($this->input->post('update_student')) {
            $this->form_validation->set_rules("update_name", "Name", "required|alpha|min_length[2]|max_length[20]|trim");
            $this->form_validation->set_rules("update_mname", "Mother name", "required|alpha|min_length[2]|max_length[20]|trim");
            $this->form_validation->set_rules("update_fname", "Father name", "required|alpha|min_length[2]|max_length[20]|trim");
            $this->form_validation->set_rules("update_surname", "Surname", "required|alpha|min_length[2]|max_length[20]|trim");
            $this->form_validation->set_rules("update_contact", "Contact No.", "required");
            $this->form_validation->set_rules("update_address", "Address", "required");

            $oldstd=$this->input->post('oldstudentstd');
            $newstd=$this->input->post('edit_studentstdid');
            $remaining=$this->input->post('oldremainingfee');
//            $newcategory = $this->input->post('update_category');
////            var_dump($newcategory);
////            die();
//            $oldcategory =$this->input->post('category');
            if($oldstd==$newstd && $this->input->post('update_category') != 'General'){
                $remaining_fee=$remaining;
            }elseif($this->input->post('update_category') == 'General'){
                $this->db->select('*');
                $this->db->where('standard_mgt_id',$newstd);
                $getstd=$this->db->get('tbl_standard');
                $getresult=$getstd->result();
                $schoolid=$getresult[0]->school_id;
                $admission_fee=$getresult[0]->admission_fee;
                $tuition_fee=$getresult[0]->tuition_fee;
                $term_fee=$getresult[0]->term_fee;
                $other_fee=$getresult[0]->other_fee;
                $smart_class_fee=$getresult[0]->smart_class_fee;
                $computer_fee=$getresult[0]->computer_fee;
                $sports_fee=$getresult[0]->sports_fee;
                $insurance_fee=$getresult[0]->insurance_fee;
                $exam_fee=$getresult[0]->exam_fee;
                $enroll_fee=$getresult[0]->enroll_fee;

                $this->db->select('feestructure_flag');
                $this->db->where('id',$schoolid);
                $getschool=$this->db->get('tbl_school');
                $getschool=$getschool->result();
                $flag=$getschool[0]->feestructure_flag;

                $this->db->select('standard');
                $this->db->where('id',$newstd);
                $standard_query=$this->db->get('tbl_standard_management');
                $standard_result=$standard_query->result();

                $admission_date = $this->input->post('edit_studentadmissiondate');
                $newadmission_date = str_replace(',', '', $admission_date);
                $admission_date_timestamp = strtotime($newadmission_date);
                $new_admission_date = date('Y-m-d', $admission_date_timestamp);

                $parts = explode('-', $new_admission_date);
                $admission_y = $parts[0];
                if($this->input->post('admission_year') != $admission_y){
                    $admission_year =$admission_y;
                }else{
                    $admission_year =$this->input->post('admission_year');
                }


                $current_year = date("Y");

                $category =$this->input->post('update_category');

                if ($category == 'General') {
                    if($flag == '1'){
                        if (count($getresult) > 0) {
                            if ($admission_year == $current_year || $standard_result[0]->standard == '1' || $standard_result[0]->standard == 'I') {
                                $remaining_fee = $admission_fee + ($tuition_fee) * 12 + $term_fee + $term_fee + $smart_class_fee + $computer_fee + $sports_fee + $insurance_fee + $exam_fee+ $enroll_fee;
                            } else {
                                $remaining_fee = ($tuition_fee) * 12 + $term_fee + $term_fee + $smart_class_fee + $computer_fee + $sports_fee + $insurance_fee + $exam_fee+ $enroll_fee;
                            }
                        } else {
                            $remaining_fee = NULL;
                        }
                    }else if($flag == '2'){
                        if (count($getresult) > 0) {
                            if ($admission_year == $current_year || $standard_result[0]->standard == '1' || $standard_result[0]->standard == 'I') {
                                $remaining_fee = $admission_fee + ($tuition_fee) * 12 + $term_fee + $term_fee  + $other_fee;
                            } else {
                                $remaining_fee = ($tuition_fee) * 12 + $term_fee+ $term_fee + $other_fee;
                            }

                        } else {
                            $remaining_fee = NULL;
                        }
                    }else if($flag == '3'){
                        if (count($getresult) > 0) {
                            if ($admission_year == $current_year ||  $standard_result[0]->standard == '1' || $standard_result[0]->standard == 'I') {
                                $remaining_fee = $admission_fee + ($tuition_fee) * 12 + $term_fee + $term_fee + $other_fee + $other_fee;
                            } else {
                                $remaining_fee = ($tuition_fee) * 12 + $term_fee + $term_fee + $other_fee + $other_fee;
                            }

                        } else {
                            $remaining_fee = NULL;
                        }
                    }else{
                        if (count($getresult) > 0) {
                            $remaining_fee = $tuition_fee;
                        } else {
                            $remaining_fee = NULL;
                        }
                    }
                } else {
                    $remaining_fee = '0';
                }

            }else{
                $remaining_fee = '0';
            }
            $updated_data = array();

            if ($this->form_validation->run() === TRUE) {
                $updated_data = array(
                    'gr_number' => $this->input->post('update_grno'),
                    'firstname' => $this->input->post('update_name'),
                    'surname' => $this->input->post('update_surname'),
                    'father_name' => $this->input->post('update_fname'),
                    'mother_name' => $this->input->post('update_mname'),
                    'email' => $this->input->post('update_email'),
                    'standard_id'=>$this->input->post('edit_studentstdid'),
                    'divison'=>$this->input->post('editstudent_div'),
                    'contact_no' => $this->input->post('update_contact'),
                    'address' => $this->input->post('update_address'),
                    'medium' => $this->input->post('update_medium'),
                    'stream' => $this->input->post('update_stream'),
                    'gender' => $this->input->post('update_gender'),
                    'category' => $this->input->post('update_category'),
                    'remaining_fee' => $remaining_fee,
                    'admission_date' => $new_admission_date,
                    'admission_year' => $admission_year,
                    'updated_by' => $_SESSION['role'],
                    'updated' => date("Y-m-d H:i:s")
                );
                try {
                    $this->db->trans_start(FALSE);
                    $this->db->where("id", $this->input->post("student_id"));
                    $this->db->update("tbl_student", $updated_data);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() == FALSE) {
                        throw new Exception('Database error! Please try again..');
                    }
                    $this->session->set_flashdata('response', "Record Updated Successfully.");
                    redirect('students/');
                } catch (Exception $e) {
                    $this->load->view('error');
                    return false;
                }
            }
        }

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'admin') {

            $id = $this->input->get('id');

            $this->db->select('school_id');
            $array = array('username' => $_SESSION['username']);
            $this->db->where($array);
            $query = $this->db->get('tbl_admin');
            $result_school = $query->result();
            $school_ids = array_column($result_school, 'school_id');

            $this->db->select('school_id');
            $this->db->where_in('school_id', $school_ids);
            $this->db->where('id', $id);
            $query = $this->db->get('tbl_student');
            $result = $query->num_rows();

            if ($result > 0) {

                $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
                $this->load->view('header', $url_name);

                $id = $this->input->get('id');
                if ($id) {
                    $_SESSION['admin_editid'] = $id;
                } else {
                    $id = $_SESSION['admin_editid'];
                }

                $row['data'] = $this->Student_model->update_student($id);
                $student_stdid=$row['data'][0]->standard_id;
                $student_schoolid=$row['data'][0]->school_id;

                $this->db->select('*');
                $this->db->where('id',$student_stdid);
                $std=$this->db->get('tbl_standard_management');
                $result=$std->result();
                $row['student_std']=$result[0]->standard;

                $query="SELECT standard FROM `tbl_standard_management` WHERE school_id=$student_schoolid";
                $stdresult = $this->db->query($query);
                $row['stdresult'] = $stdresult->result();

                $this->load->view('Students/edit', $row);
                $this->load->view('footer');
            } else {
                if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
                    $this->session->set_flashdata('response_error', "Please login with this school's id and password to access page");
                    redirect('home/');
                }else{
                    $response['message'] = "Please login to access page";
                    $this->load->view('Auth/login', $response);
                }
            }

        } else if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);

            $id = $this->input->get('id');
            if ($id) {
                $_SESSION['admin_editid'] = $id;
            } else {
                $id = $_SESSION['admin_editid'];
            }

            $row['data'] = $this->Student_model->update_student($id);
            $student_stdid=$row['data'][0]->standard_id;
            $student_schoolid=$row['data'][0]->school_id;

            $this->db->select('*');
            $this->db->where('id',$student_stdid);
            $std=$this->db->get('tbl_standard_management');
            $result=$std->result();
            $row['student_std']=$result[0]->standard;

            $query="SELECT standard FROM `tbl_standard_management` WHERE school_id=$student_schoolid";
            $stdresult = $this->db->query($query);
            $row['stdresult'] = $stdresult->result();

            $this->load->view('Students/edit', $row);
            $this->load->view('footer');
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }

    /**
     * @Desc : delete student
     */
    public function delete_student_data()
    {
        $id=$this->input->post('delete_id');
        $password=md5($this->input->post('password_delete'));

        $this->db->select('school_id');
        $this->db->where('id',$id);
        $school_query=$this->db->get('tbl_student');
        $school_result=$school_query->result();

        $this->db->select('password');
        $this->db->where_in('school_id',$school_result[0]->school_id);
        $admin_query=$this->db->get('tbl_admin');
        $admin_result=$admin_query->result();
      
        if($password == $admin_result[0]->password){
            if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'admin') {

                $id = $this->input->post('delete_id');

                $this->db->select('school_id');
                $array = array('username' => $_SESSION['username']);
                $this->db->where($array);
                $query = $this->db->get('tbl_admin');
                $result_school = $query->result();
                $school_ids = array_column($result_school, 'school_id');

                $this->db->select('school_id');
                $this->db->where_in('school_id', $school_ids);
                $this->db->where('id', $id);
                $query = $this->db->get('tbl_student');
                $result = $query->num_rows();

                if ($result > 0) {
                    $id =$this->input->post('delete_id');
                    $this->db->where('id', $id);
                    $data=array(
                        'is_deleted' => '1'
                    );
                    $query = $this->db->update("tbl_student", $data);
                    if ($query) {
                        echo "Record Deleted Successfully.";
//                        $this->session->set_flashdata('response', "Record Deleted Successfully.");
//                        redirect('students/');
                    }
                } else {
                    if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
                        $this->session->set_flashdata('response_error', "Please login with this school's id and password to access page");
                        redirect('home/');
                    }else{
                        $response['message'] = "Please login to access page";
                        $this->load->view('Auth/login', $response);
                    }
                }

            } else if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {

                $id = $this->input->post('delete_id');
                $this->db->where('id', $id);
                $data=array(
                    'is_deleted' => '1'
                );
                $query = $this->db->update("tbl_student", $data);
                if ($query) {
                    echo "Record Deleted Successfully.";
//                    $this->session->set_flashdata('response', "Record Deleted Successfully.");
//                    redirect('students/');
                }
            } else {
                $response['message'] = "Please login to access page";
                $this->load->view('Auth/login', $response);
            }
        }else{
           echo 'Please enter correct password';
        }

    }

    /**
     * @Desc : get div value for search student
     */
    public function get_Div_value()
    {
        $output = array();

        $standard = $this->input->post('standard');

        $data = $this->Student_model->fetch_div_data($standard);
        $division = array();
        foreach ($data as $row) {
            array_push($division, $row->division);
        }

        $final_div = array_unique($division);
        $div = implode(",", $final_div);
        $get_div = explode(',', $div);

        foreach ($data as $rows) {
            $output['division'] = $get_div;
            $output['id'] = $rows->id;
        }
        echo json_encode($output);
    }

    /**
     * @Desc : get div value for add student
     */
    public function getDiv_value()
    {
        $output = array();

        $standard = $this->input->post('standard');
        $school_id = $this->input->post('school_id');

        $data = $this->Student_model->fetch_div_add($standard, $school_id);

        foreach ($data as $row) {
            $output['division'] = $row->division;
            $output['id'] = $row->id;
        }
        echo json_encode($output);
    }

    /**
     * @Desc : for check email id exists
     */
    public function check_emailId_exists()
    {
        $data = array();
        $email_id = $this->input->post('email');
        $data = $this->Student_model->email_id_exists($email_id);
        if ($data != null) {
            echo 'Already exists';
        } else {
            echo 'Available';
        }
    }

    /**
     * @Desc : for check gr number exists
     */
    public function checkStudentGRNo()
    {
        $data = array();
        $gr_number = $this->input->post('gr_number');
        $data = $this->Student_model->gr_number_exists($gr_number);
        if ($data != null) {
            echo 'Already exists';
        } else {
            echo 'Available';
        }
    }

    /**
     * @Desc : for download students pdf
     */
    public function student_PDF()
    {
        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $list['data'] = $this->Student_model->get_student_list();

            if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'admin') {
                if (isset($_SESSION['local_schoolid'])) {
                    $this->db->select('*');
                    $this->db->where('id', $_SESSION['local_schoolid']);
                    $school_query = $this->db->get('tbl_school');
                    $result = $school_query->result();
                    if (!empty($result)) {
                        $list['school_name'] = $result;
                    } else {
                        return false;
                    }

                } else {
                    $this->db->select('*');
                    $this->db->where('id', $_SESSION['school_id']);
                    $school_query = $this->db->get('tbl_school');
                    $list['school_name'] = $school_query->result();
                }
            }
            $this->load->view('Students/student_PDF', $list);

            $html = $this->output->get_output();

            $this->pdf->generate($html, 'student_pdf',$paper='A4',$stream=TRUE, $orientation = "landscape");
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }

    /**
     * @Desc : for edit standard details of all student
     */
    public function edit_standard_all()
    {
        $student_array=$_REQUEST['student_Array'];
        $new_std=$_REQUEST['newStd'];
        $new_school=$_REQUEST['newSchool'];

        try {
            $this->db->trans_start(FALSE);
            $this->Student_model->edit_standard_all($student_array,$new_std,$new_school);
            $this->db->trans_complete();

            if ($this->db->trans_status() == FALSE) {
                throw new Exception('Database error! Please try again..');
            }
            echo 'Record Updated Successfully.';
        } catch (Exception $e) {
            echo 'Record Not Updated.';
            return false;
        }
    }

}
