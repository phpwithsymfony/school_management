<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daily_report extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->model("Manage_standard_model");
        $this->load->model("Student_model");
        $this->load->model("Setting_model");
        $this->load->model("Daily_report_model");
        $this->load->model('Take_fee_model');
        $this->load->model('Academic_year_model');
        $this->load->library('pdf');
    }


    /**
     * @Desc : for  student list
     */
    public function index()
    {

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {

            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);
            $student['std'] = $this->Manage_standard_model->fetch_std();

            $student['school'] = $this->Student_model->fetch_school();
            $student['years'] = $this->Academic_year_model->getAllYears();

            $student['date'] = $this->Daily_report_model->fetch_payment_date();
            
            $this->load->view('Daily_report/index', $student);
            $this->load->view('Students/view_fee');
            $this->load->view('footer');
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }

    /**
     * @Desc : for fetch all student details
     */
    public function fetch_daily_data()
    {
        $list = $this->Daily_report_model->get_datatables();
        $data = array();
        $i=0;
        foreach ($list as $rows) {
            $row = array();
            $i++;
            $row[] = $i;
            $row[] = $rows->gr_number;
            $row[] = $rows->firstname . ' ' . $rows->father_name . ' ' . $rows->surname;
            $row[] = $rows->standard . ' ' . $rows->divison;
            $row[] = $rows->medium;
            $row[] = $rows->receipt_no;
            $row[] = '<a href="" data-toggle="modal" data-target="#feeModal" type="button" name="view" id="' . $rows->student_id. '" class="btn btn-info btn-sm pending_view_fee">View Fee</a>';
            $row[] = '<form method="post" action="fee_details">
            <input type="hidden" name="hidden_Grno" id="hidden_Grno" value="' . $rows->id . '"/>
            <input type="hidden" name="hidden_paymentdate" id="hidden_paymentdate" value="' . $rows->payment_date . '"/>
            <button type="submit" class="btn btn-info btn-sm" value="Fee Details" formtarget="_blank"><span class="fa fa-file-pdf-o"></span>&nbsp;Fee Details</button>
            </form>';
            $this->db->select('*');
            $this->db->where('student_id',$rows->student_id);
            $query=$this->db->get('tbl_fee_details');
            $result=$query->last_row();
            if($result->id == $rows->id){
                $row[]='<a  href="edit?id=' . $rows->id . '" name="update" class="btn btn-warning btn-sm update" title="Edit Fee Collection"><span class="feather icon-edit"></span></a>&nbsp;&nbsp;<button class="btn btn-danger btn-sm delete" type="button" id="' . $rows->id . '" onclick="confirm_password_fee_modal(' . $rows->id . ')" title="Delete Fee Collection"><span class="feather icon-trash-2"  ></span></button>';
            }else{
                $row[]='';
            }

            $row[] = '';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Daily_report_model->count_all(),
            "recordsFiltered" => $this->Daily_report_model->count_filtered(),
            "data" => $data,
        );

        echo json_encode($output);
    }
    /**
     * @Desc : for fee fetails PDF
     */
    public function fee_details()
    {
        $fee_period = $this->input->post('hidden_paymentdate');
        $id = $this->input->post('hidden_Grno');

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $student['fee_details'] = $this->Daily_report_model->report_fee_details($id);

            $student['student_data'] = $this->Daily_report_model->report_student_details($id);

            $school = array();
            if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'admin') {
                if (isset($_SESSION['local_schoolid'])) {
                    $this->db->select('*');
                    $this->db->where('id', $_SESSION['local_schoolid']);
                    $school_query = $this->db->get('tbl_school');
                    $student['school_name'] = $school_query->result();
                } else {
                    $this->db->select('*');
                    $this->db->where('id', $_SESSION['school_id']);
                    $school_query = $this->db->get('tbl_school');
                    $student['school_name'] = $school_query->result();
                }
            }
            if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {
                $id = $id = $this->input->post('hidden_Grno');
                $this->db->select('student_id');
                $this->db->where('id',$id);
                $student_query=$this->db->get('tbl_fee_details');
                $student_result=$student_query->result();
                $this->db->select('school_id');
                $this->db->where('id',$student_result[0]->student_id);
                $schoolid_query=$this->db->get('tbl_student');
                $schoolid_result=$schoolid_query->result();
                $this->db->select('*');
                $this->db->where('id', $schoolid_result[0]->school_id);
                $school_query = $this->db->get('tbl_school');
                $student['school_name'] = $school_query->result();
            }
            $student['fee_part'] = $this->Daily_report_model->report_fee_part($id);
            $feeYear = $student['fee_details'][0]->fee_year;
            $student['academic_year'] = $this->Academic_year_model->getYear($feeYear);

            $this->load->view('Daily_report/FeeDetailsPDF', $student);

            $html = $this->output->get_output();
            $customPaper = array(0,0,425,566);
            $this->pdf->generate($html, 'fee_receipt',$customPaper,TRUE,'potrait');
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }

    }

    /**
     * @Desc : for paid fee data
     */
    public function paidfeepdf()
    {

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $fee_year = $this->input->post('dailyreport_hidden_academic');
            if($fee_year){
                $this->db->select('year');
                $this->db->where('id', $fee_year);
                $year_query = $this->db->get('tbl_academic_year');
                $year_result = $year_query->result();
                $list['request_year'] = $year_result[0]->year;
            }
            $list['data'] = $this->Daily_report_model->get_paidfee_list();
            if($this->input->post('dailyreport_hidden_startdate')){
                $startdate=$this->input->post('dailyreport_hidden_startdate');
                $dateTimeStart = new DateTime($startdate);
                $list['start_date']= $dateTimeStart->format('d/m/Y');
            }else{
                $list['start_date']='';
            }
            if($this->input->post('dailyreport_hidden_enddate')){
                $enddate=$this->input->post('dailyreport_hidden_enddate');
                $dateTime = new DateTime($enddate);
                $list['end_date']= $dateTime->format('d/m/Y');
            }else{
                $list['end_date']='';
            }

//            $enddate=$this->input->post('hidden_enddate');
//            $dateTime = new DateTime($enddate);
//            $list['end_date']= $dateTime->format('d/m/Y');

            $paid_feesum=array();
            $cheque_paid=array();
            $concession=array();
            $discount=array();
            foreach ($list['data'] as $row){
                if($row->cheque_no){
                    $che_paid=$row->paid_fee;
                    array_push($cheque_paid, $che_paid);
                }else{
                    $paid=$row->paid_fee;
                    array_push($paid_feesum,$paid);
                }
                $con=$row->concession;
                array_push($concession,$con);
                $dis=$row->discount;
                array_push($discount,$dis);
            }
            $list['total_paid']=array_sum($paid_feesum);
            $list['total_con']=array_sum($concession);
            $list['total_dis']=array_sum($discount);
            $list['total_cheque']=array_sum($cheque_paid);
            if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'admin') {
                if (isset($_SESSION['local_schoolid'])) {
                    $this->db->select('*');
                    $this->db->where('id', $_SESSION['local_schoolid']);
                    $school_query = $this->db->get('tbl_school');
                    $list['school_name'] = $school_query->result();
                } else {
                    $this->db->select('*');
                    $this->db->where('id', $_SESSION['school_id']);
                    $school_query = $this->db->get('tbl_school');
                    $list['school_name'] = $school_query->result();
                }
            }

            /*If receipt id is  not found adding new ids and show ---- value  in pdf start */
            $number = array();
            foreach($list['data'] as $data){
                $number[] = $data->receipt_no;
            }
            $new_arr = range($number[0],max($number)); 
            $missing_number = array_diff($new_arr, $number);
           
            if(!empty($missing_number)){
                $receiptArray = array();
                foreach($missing_number as $key=>$nu){
                    $receiptArray[$key]['receipt_no'] =  $nu ;
                }
                $newData = array_merge($receiptArray,$list['data']);
                $column = array_column($newData, 'receipt_no');
                array_multisort($column,$newData);
                
                $list['newdata'] = $newData;
            }else{
                $list['newdata'] = $list['data'];
            } 
            /*end*/
            
            $this->load->view('Daily_report/paidfee_PDF', $list);

            $html = $this->output->get_output();

            $this->pdf->generate($html, 'paid_fee',$paper='A4',$stream=TRUE, $orientation = "landscape");
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }

    }

    public function fee_collection_details(){

        $id=$this->input->post('delete_fee_id');
        $password=md5($this->input->post('password_fee_delete'));

        $this->db->select('student_id');
        $this->db->where('id',$id);
        $student_query=$this->db->get('tbl_fee_details');
        $student_result=$student_query->result();

        $this->db->select('school_id');
        $this->db->where('id',$student_result[0]->student_id);
        $school_query=$this->db->get('tbl_student');
        $school_result=$school_query->result();

        $this->db->select('password');
        $this->db->where('school_id',$school_result[0]->school_id);
        $admin_query=$this->db->get('tbl_admin');
        $admin_result=$admin_query->result();

        $this->db->select('masterpassword');
        $masterpassword = $this->db->get('tbl_setting');
        $masterPass = $masterpassword->result();

        if($password == md5($masterPass[0]->masterpassword)){
            $id=$this->input->post('delete_fee_id');

            $this->db->select('paid_fee,student_id,concession,discount');
            $this->db->where('id',$id);
            $query=$this->db->get('tbl_fee_details');
            $result=$query->result();

            $paid_fee=$result[0]->paid_fee;
            $concession_fee=$result[0]->concession;
            $discount_fee=$result[0]->discount;
            $student_id=$result[0]->student_id;

            $this->db->select('remaining_fee');
            $this->db->where('id',$student_id);
            $student_query=$this->db->get('tbl_student');
            $student_result=$student_query->result();

            $rem_fee=$student_result[0]->remaining_fee;

            $update_data = array(
                'remaining_fee' => ((int)$rem_fee + (int)$paid_fee + (int)$concession_fee)+(int)$discount_fee);
            $this->db->where("id",$student_id);
            $remaining_fee = $this->db->update("tbl_student",$update_data);
            if($remaining_fee){
                $id=$this->input->post('delete_fee_id');
                $this->db->where('id',$id);
                $query=$this->db->delete('tbl_fee_details');
                if($query){
                    echo 'Record Deleted Successfully.';
                }else{
                    echo 'Data not deleted';
                }
            }
        }else{
            echo "Please Enter Correct Password";
        }

    }

    public function edit(){
        $fee_id = $this->input->get('id');
        if ($fee_id) {
            $_SESSION['admin_fee_id'] = $fee_id;
        } else {
            $fee_id = $_SESSION['admin_fee_id'];
        }

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'admin') {

            $this->db->select('school_id');
            $array = array('username' => $_SESSION['username']);
            $this->db->where($array);
            $query = $this->db->get('tbl_admin');
            $result_school = $query->result();
            $school_ids = array_column($result_school, 'school_id');

            $this->db->select('student_id');
            $this->db->where('id', $fee_id);
            $s_query = $this->db->get('tbl_fee_details');
            $s_result = $s_query->result();
            $this->db->select('school_id');
            $this->db->where('id', $s_result[0]->student_id);
            $this->db->where_in('school_id', $school_ids);
            $query_schoolid = $this->db->get('tbl_student');
            $result_schoolid = $query_schoolid->num_rows();

            if ($result_schoolid > 0) {

                $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
                $this->load->view('header', $url_name);

                //for get student data

                if ($fee_id) {
                    $_SESSION['admin_fee_id'] = $fee_id;
                } else {
                    $fee_id = $_SESSION['admin_fee_id'];
                }
                $student['data'] = array();

                $student['years'] = $this->Academic_year_model->getAllYears();
                $this->db->select('fee_year');
                $this->db->where('id',$fee_id);
                $tblFeeDetail = $this->db->get('tbl_fee_details')->result();
                $student['fee_year'] = $this->Academic_year_model->getYear($tblFeeDetail[0]->fee_year);
                
                $student['remaining_amount'] = '0';
                $this->db->select('*');
                $this->db->where('id',$fee_id);

                $old_query=$this->db->get('tbl_fee_details');
                $old_result=$old_query->result();
                $id=$old_result[0]->student_id;
                $old_paid_fee=$old_result[0]->paid_fee;
                $student_data = $this->Take_fee_model->take_fee($id);
                $student['data'] = $student_data;

                $this->db->select('feestructure_flag');
                $this->db->where('id',$student_data[0]->school_id);
                $flag_result=$this->db->get('tbl_school');
                $student['flag']=$flag_result->result();
                $feeflag=$student['flag'][0]->feestructure_flag;
                
                if($feeflag == '4'){
                $result = $this->Take_fee_model->edit_get_last_month($fee_id);
                $result_pending = $this->Take_fee_model->get_pending_fee($fee_id);
                if($result_pending != ''){
                    $student['pending_fee']=$result_pending[0]->pending_fee;
                }else{
                    $student['pending_fee']='0';
                }
                $monthinstallment = array("first", "second", "third", "fourth");
                $student['fee_installment'] = '';
                if ($result != '') {
                    $last_fee_period = $result->fee_month;

                    $last_fee_period = $result->fee_month;

                    $last_fee_month = explode('-', $last_fee_period);

                    $start_index = array_search($last_fee_month[0], $monthinstallment);
                    $start_month = trim($last_fee_month[0]);
                    $student['fee_installment'] = $start_month;
                    if (count($last_fee_month) > 1) {
                        $end_month = trim($last_fee_month[1]);
                        $student['end_installment'] = $end_month;
                    } else {
                        $student['end_installment'] = '';
                    }
                } else {
                    $student['fee_installment'] = "first";
                    $student['paid_fee'] = " ";
                }
                $paid =  $result->paid_fee;
                $concession_fee =  $result->concession;
                $discount_fee=$result->discount;
                $student['paid_fee']=$result->paid_fee;
                $student['trust_name']=$result->trust_name;
                if($result->concession){
                    $student['concession'] =  $result->concession;
                }else{
                    $student['concession']='';
                }
                if($result->discount){
                    $student['discount'] =  $result->discount;
                }else{
                    $student['discount']='';
                }
                }else {
                    //for get last fee month
                    $last_fee_period = array();
                    $result = $this->Take_fee_model->edit_get_last_month($fee_id);

                    $result_pending = $this->Take_fee_model->get_pending_fee($fee_id);
                    if ($result_pending != '') {
                        $student['pending_fee'] = $result_pending[0]->pending_fee;
                    } else {
                        $student['pending_fee'] = '0';
                    }

                    $monthNames = array("June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May");
                    $monthValues = array("6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5");

                    $student['fee_month'] = '';
                    if ($result != '') {
                        $last_fee_period = $result->fee_month;

                        $last_fee_month = explode('-', $last_fee_period);

                        $start_index = array_search($last_fee_month[0], $monthNames);
                        $start_month = trim($last_fee_month[0]);
                        $student['start_month'] = $start_month;
                        if (count($last_fee_month) > 1) {
                            $end_month = trim($last_fee_month[1]);
                            $student['end_month'] = $end_month;
                        } else {
                            $student['end_month'] = '';
                        }
                        $paid =  $result->paid_fee;
                        $concession_fee =  $result->concession;
                        $discount_fee=$result->discount;
                        $student['paid_fee']=$result->paid_fee;
                        $student['trust_name']=$result->trust_name;
                        if($result->concession){
                            $student['concession'] =  $result->concession;
                        }else{
                            $student['concession']='';
                        }
                        if($result->discount){
                            $student['discount'] =  $result->discount;
                        }else{
                            $student['discount']='';
                        }
                    }

                }

                $remaining = $this->Take_fee_model->remaining_fee($id);
                $remaining_fee= $remaining[0]->remaining_fee;
                $student['remaining_fee']=$remaining_fee + $paid + $concession_fee + $discount_fee;
                //for paid amount enter in database
                if ($this->input->post('edit_take_fee')) {

                    $remaining = $this->Take_fee_model->remaining_fee($id);
                    $remaining_fee= $remaining[0]->remaining_fee;
                    $fee_rem = array(
                        'remaining_fee' => ((int)$remaining_fee + (int)$paid + (int)$concession_fee + (int)$discount_fee)
                    );
                    $this->db->where("id", $this->input->post('edit_fee_id'));
                    $this->db->update("tbl_student", $fee_rem);

                    $total_paid_fee=$this->input->post('edit_totalpaid_fee');
                    $total=$this->input->post('edit_paid_amount');

                    if($total_paid_fee){
                        $paid_fee= $total_paid_fee;
                        if($total_paid_fee == $total){
                            $pending_fee='0';
                        }else{
                            $pending_fee=((int)$total)-((int)$total_paid_fee);
                        }
                    }else{
                        $paid_fee=$total;
                        $pending_fee=0;
                    }
                    $data = array(
                        'student_id' => $this->input->post('edit_fee_id'),
                        'period' => $this->input->post('edit_fee_duration'),
                        'paid_fee' => $paid_fee,
                        'pending_fee' =>$pending_fee,
                        'fee_month' => $this->input->post('edit_fee_period'),
                        'payment_date' => $this->input->post('edit_take_date'),
                        'cheque_no' => $this->input->post('edit_cheque_no'),
                        'cheque_date' => $this->input->post('edit_cheque_date'),
                        'admission_fee' => $this->input->post('edit_paid_admission_fee'),
                        'tution_fee' => $this->input->post('edit_paid_tution_fee'),
                        'term_fee' => $this->input->post('edit_paid_term_fee'),
                        'other_fee' => $this->input->post('edit_paid_other_fee'),
                        'smart_class_fee' => $this->input->post('edit_paid_smart_fee'),
                        'computer_fee' => $this->input->post('edit_paid_computer_fee'),
                        'sports_fee' => $this->input->post('edit_paid_sports_fee'),
                        'exam_fee' => $this->input->post('edit_paid_exam_fee'),
                        'insurance_fee' => $this->input->post('edit_paid_insurance_fee'),
                        'enroll_fee' => $this->input->post('edit_paid_enroll_fee'),
                        'concession' => $this->input->post('edit_concession'),
                        'discount' => $this->input->post('edit_discount'),
                        'created_on' => date("Y-m-d H:i:s")
                    );

                    $this->db->where('id',$fee_id);
                    $update = $this->db->update('tbl_fee_details', $data);

                    $concession = $this->input->post('edit_concession');
                    $discount=$this->input->post('edit_discount');
                    if ($concession) {
                        $total_paid = $paid_fee + $concession;
                    } elseif ($discount){
                        $total_paid = $paid_fee + $discount;
                    }  else{
                        $total_paid = $paid_fee;
                    }
                    if ($discount && $concession){
                        $total_paid = $paid_fee + $discount +$concession;
                    }
                    $rem_fee = $this->input->post('edit_remaining_fee');
                    $fee_report = array(
                        'remaining_fee' => ((int)$rem_fee - (int)$total_paid)
                    );
                    $this->db->where("id", $this->input->post('edit_fee_id'));
                    $remaining_fee = $this->db->update("tbl_student", $fee_report);

                    if ($update && $remaining_fee) {
                        $this->session->set_flashdata('response', "Fee Payement Edit Successfully Done.");
                        redirect('daily_report/');
                    } else {
                        echo "Data not submitted";
                        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
                            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
                            $this->load->view('header', $url_name);
                            //for get remaining fee

                            $this->load->view('Fee/edit_fee', $student);
                            $this->load->view('footer');

                        } else {
                            $response['message'] = "Please login to access page";
                            $this->load->view('Auth/login', $response);
                        }
                    }
                }

                $this->load->view('Fee/edit_fee', $student);
                $this->load->view('footer');
                }else{
                if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
                    $this->session->set_flashdata('response_error', "Please login with this school's id and password to access page");
                    redirect('home/');
                }else{
                    $response['message'] = "Please login to access page";
                    $this->load->view('Auth/login', $response);
                }
                }
            }else if(isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true){
                $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
                $this->load->view('header', $url_name);
                //for get student data

                if ($fee_id) {
                    $_SESSION['admin_fee_id'] = $fee_id;
                } else {
                    $fee_id = $_SESSION['admin_fee_id'];
                }
                $student['data'] = array();

                $student['years'] = $this->Academic_year_model->getAllYears();
                $this->db->select('fee_year');
                $this->db->where('id',$fee_id);
                $tblFeeDetail = $this->db->get('tbl_fee_details')->result();
                $student['fee_year'] = $this->Academic_year_model->getYear($tblFeeDetail[0]->fee_year);

                $student['remaining_amount'] = '0';
                $this->db->select('*');
                $this->db->where('id',$fee_id);

                $old_query=$this->db->get('tbl_fee_details');
                $old_result=$old_query->result();
                $id=$old_result[0]->student_id;
                $old_paid_fee=$old_result[0]->paid_fee;
                $student_data = $this->Take_fee_model->take_fee($id);
                $student['data'] = $student_data;
                $this->db->select('feestructure_flag');
                $this->db->where('id',$student_data[0]->school_id);
                $flag_result=$this->db->get('tbl_school');
                $student['flag']=$flag_result->result();
                $feeflag=$student['flag'][0]->feestructure_flag;

            if($feeflag == '4'){
                $result = $this->Take_fee_model->edit_get_last_month($fee_id);
                $result_pending = $this->Take_fee_model->get_pending_fee($fee_id);
                if($result_pending != ''){
                    $student['pending_fee']=$result_pending[0]->pending_fee;
                }else{
                    $student['pending_fee']='0';
                }
                $monthinstallment = array("first", "second", "third", "fourth");
                $student['fee_installment'] = '';
                if ($result != '') {
                    $last_fee_period = $result->fee_month;

                    $last_fee_period = $result->fee_month;

                    $last_fee_month = explode('-', $last_fee_period);

                    $start_index = array_search($last_fee_month[0], $monthinstallment);
                    $start_month = trim($last_fee_month[0]);
                    $student['fee_installment'] = $start_month;
                    if (count($last_fee_month) > 1) {
                        $end_month = trim($last_fee_month[1]);
                        $student['end_installment'] = $end_month;
                    } else {
                        $student['end_installment'] = '';
                    }
                } else {
                    $student['fee_installment'] = "first";
                    $student['paid_fee'] = " ";
                }
                $paid =  $result->paid_fee;
                $concession_fee =  $result->concession;
                $discount_fee=$result->discount;
                $student['paid_fee']=$result->paid_fee;
                $student['trust_name']=$result->trust_name;
                if($result->trust_name){
                    $student['trust_name'] =  $result->trust_name;
                }else{
                    $student['trust_name']='';
                }
                if($result->concession){
                    $student['concession'] =  $result->concession;
                }else{
                    $student['concession']='';
                }
                if($result->discount){
                    $student['discount'] =  $result->discount;
                }else{
                    $student['discount']='';
                }
            }else {
                //for get last fee month
                $last_fee_period = array();
                $result = $this->Take_fee_model->edit_get_last_month($fee_id);

                $result_pending = $this->Take_fee_model->get_pending_fee($fee_id);
                if ($result_pending != '') {
                    $student['pending_fee'] = $result_pending[0]->pending_fee;
                } else {
                    $student['pending_fee'] = '0';
                }

                $monthNames = array("June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May");
                $monthValues = array("6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5");

                $student['fee_month'] = '';

                if ($result != '') {
                    $last_fee_period = $result->fee_month;

                    $last_fee_month = explode('-', $last_fee_period);

                    $start_index = array_search($last_fee_month[0], $monthNames);
                    $start_month = trim($last_fee_month[0]);
                    $student['start_month'] = $start_month;
                    if (count($last_fee_month) > 1) {
                        $end_month = trim($last_fee_month[1]);
                        $student['end_month'] = $end_month;
                    } else {
                        $student['end_month'] = '';
                    }
                    $paid =  $result->paid_fee;
                    $concession_fee =  $result->concession;
                    $discount_fee=$result->discount;
                    $student['paid_fee']=$result->paid_fee;
                    $student['trust_name']=$result->trust_name;
                    if($result->trust_name){
                        $student['trust_name'] =  $result->trust_name;
                    }else{
                        $student['trust_name']='';
                    }
                    if($result->concession){
                        $student['concession'] =  $result->concession;
                    }else{
                        $student['concession']='';
                    }
                    if($result->discount){
                        $student['discount'] =  $result->discount;
                    }else{
                        $student['discount']='';
                    }
                }

            }

                $remaining = $this->Take_fee_model->remaining_fee($id);
                $remaining_fee= $remaining[0]->remaining_fee;
                $student['remaining_fee']=$remaining_fee + $paid + $concession_fee +$discount_fee;
               
               
                //for paid amount enter in database
                if ($this->input->post('edit_take_fee')) {

                    $remaining = $this->Take_fee_model->remaining_fee($id);
                    $remaining_fee= $remaining[0]->remaining_fee;
                    $fee_rem = array(
                        'remaining_fee' => ((int)$remaining_fee + (int)$paid + (int)$concession_fee + (int)$discount_fee)
                    );
                    $this->db->where("id", $this->input->post('edit_fee_id'));
                    $this->db->update("tbl_student", $fee_rem);

                    $total_paid_fee=$this->input->post('edit_totalpaid_fee');
                    $total=$this->input->post('edit_paid_amount');
                    if($total_paid_fee){
                        $paid_fee= $total_paid_fee;
                        if($total_paid_fee == $total){
                            $pending_fee='0';
                        }else{
                            $pending_fee=((int)$total)-((int)$total_paid_fee);
                        }
                    }else{
                        $paid_fee=$total;
                        $pending_fee=0;
                    }
                    $previospendingfee=$this->input->post('edit_pending_fee');
                    if($pending_fee == 0){
                        $paid_pending=$previospendingfee;
                    }else{
                        $paid_pending=0;
                    }
                    $data = array(
                        'student_id' => $this->input->post('edit_fee_id'),
                        'period' => $this->input->post('edit_fee_duration'),
                        'paid_fee' => $paid_fee,
                        'pending_fee' => $pending_fee,
                        'fee_month' => $this->input->post('edit_fee_period'),
                        'payment_date' => $this->input->post('edit_take_date'),
                        'cheque_no' => $this->input->post('edit_cheque_no'),
                        'cheque_date' => $this->input->post('edit_cheque_date'),
                        'admission_fee' => $this->input->post('edit_paid_admission_fee'),
                        'tution_fee' => $this->input->post('edit_paid_tution_fee'),
                        'term_fee' => $this->input->post('edit_paid_term_fee'),
                        'other_fee' => $this->input->post('edit_paid_other_fee'),
                        'smart_class_fee' => $this->input->post('edit_paid_smart_fee'),
                        'computer_fee' => $this->input->post('edit_paid_computer_fee'),
                        'sports_fee' => $this->input->post('edit_paid_sports_fee'),
                        'exam_fee' => $this->input->post('edit_paid_exam_fee'),
                        'insurance_fee' => $this->input->post('edit_paid_insurance_fee'),
                        'enroll_fee' => $this->input->post('edit_paid_enroll_fee'),
                        'concession' => $this->input->post('edit_concession'),
                        'discount' => $this->input->post('edit_discount'),
                        'paid_pendingfee' => $paid_pending,
                        'trust_name' => $this->input->post('edit_trust_name'),
                        'fee_year' => $this->input->post('studentFeeYear'),
                        'created_on' => date("Y-m-d H:i:s")
                    );
                    $this->db->where('id',$fee_id);
                    $update = $this->db->update('tbl_fee_details', $data);
                    $concession = $this->input->post('edit_concession');
                    $discount =$this->input->post('edit_discount');
                    if ($concession) {
                        $total_paid = $paid_fee + $concession;
                    } elseif ($discount){
                        $total_paid = $paid_fee + $discount;
                    }  else{
                        $total_paid = $paid_fee;
                    }
                    if ($discount && $concession){
                        $total_paid = $paid_fee + $discount +$concession;
                    }
                    $rem_fee = $this->input->post('edit_remaining_fee');
                    $fee_report = array(
                        'remaining_fee' => ((int)$rem_fee - (int)$total_paid)
                    );
                    $this->db->where("id", $this->input->post('edit_fee_id'));
                    $remaining_fee = $this->db->update("tbl_student", $fee_report);

                    if ($update && $remaining_fee) {
                        $this->session->set_flashdata('response', "Fee Payement Edit Successfully Done.");
                        redirect('daily_report/');
                    } else {
                        echo "Data not submitted";
                        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
                            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
                            $this->load->view('header', $url_name);
                            //for get remaining fee

                            $this->load->view('Fee/edit_fee', $student);
                            $this->load->view('footer');

                        } else {
                            $response['message'] = "Please login to access page";
                            $this->load->view('Auth/login', $response);
                        }
                    }
                }
                $this->load->view('Fee/edit_fee', $student);
                $this->load->view('footer');
            }else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }
}
