<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admins extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->library('form_validation');
        $this->load->database();
        date_default_timezone_set('Asia/Kolkata');
        $this->load->model("Admin_model");
    }

    /**
     * @Desc : show all admins list
     */
    public function index()
    {
        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {
            $success['message'] = '';
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);
            $this->load->view('School_admins/index', $success);
            $this->load->view('footer');
        } else if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $this->session->set_flashdata('response_error', "Please login as super admin to access page");
            redirect('home/');
        }else{
                $response['message'] = "Please login to access page";
                $this->load->view('Auth/login', $response);
        }
    }

    /**
     * @Desc : for get all admin data
     */
    public function fetch_admin_data()
    {

        $list = $this->Admin_model->get_datatables();
        $data = array();

        foreach ($list as $rows) {
            $row = array();

            $row[] = $rows->school_code;
            $row[] = $rows->name;
            $row[] = $rows->firstname . ' ' . $rows->middlename . ' ' . $rows->lastname;
            $row[] = $rows->email;
            $row[] = $rows->contact_no;
            $row[] = $rows->city;
            $row[] = '<a  href="edit?id=' . $rows->id . '" name="update" class="btn btn-warning btn-sm update"><span class="feather icon-edit" title="Edit Admin"></span></a>&nbsp;&nbsp;<a  href="delete?id=' . $rows->id . '" name="delete" class="btn btn-danger btn-sm delete" onclick="return confirm(\'Are you sure you want to Remove?\');" title="Delete Admin"><span class="feather icon-trash-2"></span></a>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Admin_model->count_all(),
            "recordsFiltered" => $this->Admin_model->count_filtered(),
            "data" => $data,
        );

        echo json_encode($output);

    }

    /**
     * @Desc : for get school name as per school id
     */
    public function get_schoolname_value()
    {

        $output = array();
        $school_id = $this->input->post('school_id');
        $data = $this->Admin_model->fetch_school_name($school_id);

        foreach ($data as $row) {
            $output['name'] = $row->name;
            $output['id'] = $row->id;
        }
        echo json_encode($output);
    }

    /**
     * @Desc : for email id exists or not
     */
    public function check_emailId_exists()
    {
        $data = array();
        $email_id = $this->input->post('firstname');

        if ($this->input->post('old_name')) {
            $old_email = $this->input->post('old_name');
        } else {
            $old_email = null;
        }
        $data = $this->Admin_model->email_id_exists($email_id, $old_email);
        if ($data != null) {
            echo 'Already exists';
        } else {
            echo 'Available';
        }
    }

    /**
     * @Desc : for add admin
     */
    public function add()
    {

        $data = array();
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules("add_uname", "User Name", "required|min_length[2]|max_length[20]");
            $this->form_validation->set_rules("add_fname", "First Name", "required|alpha|min_length[2]|max_length[20]|trim");
            $this->form_validation->set_rules("add_mname", "Middle Name", "required|alpha|min_length[1]|max_length[20]|trim");
            $this->form_validation->set_rules("add_lname", "Last Name", "required|alpha|min_length[2]|max_length[20]|trim");
            $this->form_validation->set_rules("add_city", "City", "required|min_length[2]|max_length[20]");
            $this->form_validation->set_rules("add_email", "Email", "required|valid_email");
            $this->form_validation->set_rules("add_contact", "Contact No.", "required");
            $this->form_validation->set_rules("add_password", "Password", "required|min_length[6]|max_length[15]");
            $this->form_validation->set_rules("confirm_password", "Confirm Password", "required|min_length[6]|matches[add_password]");

            if ($this->form_validation->run() === TRUE) {

                $str = "0123456789qwertyuiopasdfghjklzxcvbnm";
                $str = str_shuffle($str);
                $str = substr($str, 0, 10);
                $school_id = $this->input->post('add_schoolname_admin[]');
                $i = 0;
                foreach ($school_id as $row) {
                    $i++;
                    $data[] = array(
                        'role' => 'admin',
                        'school_id' => $row,
                        'username' => $this->input->post('add_uname'),
                        'firstname' => $this->input->post('add_fname'),
                        'lastname' => $this->input->post('add_lname'),
                        'middlename' => $this->input->post('add_mname'),
                        'email' => $this->input->post('add_email'),
                        'contact_no' => $this->input->post('add_contact'),
                        'password' => md5($this->input->post('add_password')),
                        'address' => $this->input->post('add_address'),
                        'gender' => $this->input->post('add_gender'),
                        'city' => $this->input->post('add_city'),
                        'token' => $str,
                        'created_at' => date("Y-m-d H:i:s")
                    );

                }
                try {
                    $this->db->trans_start(FALSE);
                    $this->db->insert_batch('tbl_admin', $data);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() == FALSE) {
                        throw new Exception('Database error! Please try again..');
                    }
                    $this->session->set_flashdata('response', "Record Inserted Successfully.");
                    redirect('admins/');
                } catch (Exception $e) {
                    $this->load->view('error');
                    return false;
                }
            }
        }

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);
            $query = "SELECT * FROM tbl_school WHERE school_code IS NULL OR
school_code = ' '";
            $data = $this->db->query($query);
            $result = $data->result();
            if ($result) {
                $this->session->set_flashdata('response_error', "Please First Add school_code for all school");
                redirect('admins/');
            } else {
                $school['school_id'] = $this->Admin_model->get_school_id();
                $this->load->view('School_admins/add', $school);
                $this->load->view('footer');
            }
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }

    /**
     * @Desc : for edit admin
     */
    public function edit()
    {

        if ($this->input->post('update')) {
            $this->form_validation->set_rules("edit_uname", "User Name", "required|min_length[2]|max_length[20]");
            $this->form_validation->set_rules("edit_fname", "First Name", "required|alpha|min_length[2]|max_length[20]|trim");
            $this->form_validation->set_rules("edit_mname", "Middle Name", "required|alpha|min_length[1]|max_length[20]|trim");
            $this->form_validation->set_rules("edit_lname", "Last Name", "required|alpha|min_length[2]|max_length[20]|trim");
            $this->form_validation->set_rules("edit_city", "City", "required|min_length[2]|max_length[20]");
            $this->form_validation->set_rules("edit_email", "Email", "required|valid_email");
            $this->form_validation->set_rules("edit_contact", "Contact No.", "required");

            if ($this->form_validation->run() === TRUE) {

                $updated_data = array(
                    'username' => $this->input->post('edit_uname'),
                    'firstname' => $this->input->post('edit_fname'),
                    'middlename' => $this->input->post('edit_mname'),
                    'lastname' => $this->input->post('edit_lname'),
                    'email' => $this->input->post('edit_email'),
                    'contact_no' => $this->input->post('edit_contact'),
                    'address' => $this->input->post('edit_address'),
                    'city' => $this->input->post('edit_city'),
                    'gender' => $this->input->post('edit_gender'),
                    'updated_at' => date("Y-m-d H:i:s")
                );
                $email = $this->input->post('old_email');
                $username=$this->input->post('old_username');
                $this->db->select('id');
                $this->db->where('username', $username);
                $email_query = $this->db->get('tbl_admin');
                $email_result = $email_query->result();
                $id = array_column($email_result, 'id');
               
                try {
                    $this->db->trans_start(FALSE);
                    $this->db->where_in('id', $id);
                    $this->db->update('tbl_admin', $updated_data);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() == FALSE) {
                        throw new Exception('Database error! Please try again..');
                    }
                    $this->session->set_flashdata('response', "Record Updated Successfully.");
                    redirect('admins/');
                } catch (Exception $e) {
                    $this->load->view('error');
                    return false;
                }
            }
        }

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {

            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);

            $id = $this->input->get('id');
            if ($id) {
                $_SESSION['admin_editid'] = $id;
            } else {
                $id = $_SESSION['admin_editid'];
            }
            $updated_data = $row = array();
            $row['data'] = $this->Admin_model->update_admin($id);
            $this->load->view('School_admins/edit', $row);
            $this->load->view('footer');
        } else if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $this->session->set_flashdata('response_error', "Please login as super admin to access page");
            redirect('home/');
        }else{
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }

    /**
     * @Desc : for delete admin
     */
    public function delete()
    {
        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {

            $id = $this->input->get('id');

            try {
                $this->db->trans_start(FALSE);
                $this->db->where('id', $id);
                $this->db->delete('tbl_admin');
                $this->db->trans_complete();

                if ($this->db->trans_status() == FALSE) {
                    throw new Exception('Database error! Please try again..');
                }
                $this->session->set_flashdata('response', "Record Deleted Successfully");
                redirect('admins/');
            } catch (Exception $e) {
                $this->load->view('error');
                return false;
            }
        } else if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $this->session->set_flashdata('response_error', "Please login as super admin to access page");
            redirect('home/');
        }else{
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }

    public function CheckSchoolList()
    {
        $data = json_decode(stripslashes($_POST['data']));

        $this->db->select('*');
        $this->db->where_in('school_id', $data);
        $query = $this->db->get('tbl_admin');
        $result = $query->result();
        if ($result) {
            echo 'Already exists';
        } else {
            echo 'Available';
        }
    }
}