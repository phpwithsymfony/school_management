<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumini_student extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->library('form_validation');
        $this->load->model("AluminiStudent_model");
        $this->load->model("Student_model");
        $this->load->model("Manage_standard_model");
        $this->load->database();
        $this->load->library('pdf');
        date_default_timezone_set('Asia/Kolkata');
    }

    /**
     * @Desc : show all deleted students list
     */
    public function index()
    {

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);

            $standard['std'] = $this->Manage_standard_model->fetch_std();
            $standard['school'] = $this->Student_model->fetch_school();

            $this->load->view('Alumini/index', $standard);
            $this->load->view('footer');
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }

    }

    /**
     * @Desc : fetch student list
     */
    public function fetch_student_details()
    {
        $fetch_data = $this->AluminiStudent_model->make_datatables();
        $data = array();

        foreach ($fetch_data as $row) {
            $sub_array = array();
            $sub_array[] = $row->gr_number;
            $sub_array[] = $row->firstname . ' ' . $row->father_name . ' ' . $row->surname;
            $sub_array[] = $row->standard . ' ' . $row->divison;
            $sub_array[] = $row->medium;
            $sub_array[] = $row->category;
            if ($_SESSION['role'] == 'super_admin') {
                $sub_array[] = $row->name;
            }
            $sub_array[] = '<a  href="add?id=' . $row->id . '" name="update" class="btn btn-warning btn-sm update" onclick="return confirm(\'Are you sure for revert student?\');" title="Revert Student"><span class="feather icon-user-plus"></span></a>';
            $data[] = $sub_array;
        }
        $output = array(
            "draw" => intval($_POST["draw"]),
            "recordsTotal" => $this->AluminiStudent_model->get_all_data(),
            "recordsFiltered" => $this->AluminiStudent_model->get_filtered_data(),
            "data" => $data
        );
        echo json_encode($output);
    }

    /**
     * @Desc : revert student list
     */
    public function add()
    {
        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'admin') {

            $id = $this->input->get('id');

            $this->db->select('school_id');
            $array = array('email' => $_SESSION['email']);
            $this->db->where($array);
            $query = $this->db->get('tbl_admin');
            $result_school = $query->result();
            $school_ids = array_column($result_school, 'school_id');

            $this->db->select('school_id');
            $this->db->where_in('school_id', $school_ids);
            $this->db->where('id', $id);
            $query = $this->db->get('tbl_student');
            $result = $query->num_rows();

            if ($result > 0) {
                $id = $this->input->get('id');
                $this->db->where('id', $id);
                $data=array(
                    'is_deleted' => '0'
                );
                $query = $this->db->update("tbl_student", $data);
                if ($query) {
                    $this->session->set_flashdata('response', "Record Reverted Successfully.");
                    redirect('students/');
                }
            } else {
                $response['message'] = "Please login to access page";
                $this->load->view('Auth/login', $response);
            }

        } else if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {

            $id = $this->input->get('id');
            $this->db->where('id', $id);
            $data=array(
                'is_deleted' => '0'
            );
            $query = $this->db->update("tbl_student", $data);
            if ($query) {
                $this->session->set_flashdata('response', "Record Reverted Successfully.");
                redirect('students/');
            }
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }

    /**
     * @Desc : for download alumini students pdf
     */
    public function alumini_student_PDF()
    {
        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $list['data'] = $this->AluminiStudent_model->get_student_list();

            if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'admin') {
                if (isset($_SESSION['local_schoolid'])) {
                    $this->db->select('*');
                    $this->db->where('id', $_SESSION['local_schoolid']);
                    $school_query = $this->db->get('tbl_school');
                    $result = $school_query->result();
                    if (!empty($result)) {
                        $list['school_name'] = $result;
                    } else {
                        return false;
                    }

                } else {
                    $this->db->select('*');
                    $this->db->where('id', $_SESSION['school_id']);
                    $school_query = $this->db->get('tbl_school');
                    $list['school_name'] = $school_query->result();
                }
            }
            $this->load->view('Alumini/alumini_student_PDF', $list);

            $html = $this->output->get_output();

            $this->pdf->generate($html, 'student_pdf');
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }
}