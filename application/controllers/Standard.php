<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Standard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->database();
        $this->load->model("Manage_standard_model");
        $this->load->model('Academic_year_model');
    }

    //for manage standard page
    public function index()
    {

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {

            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);
            $standard['std'] = $this->Manage_standard_model->fetch_std();

            $this->load->model("Student_model");
            $standard['school'] = $this->Student_model->fetch_school();
            $standard['years'] = $this->Academic_year_model->getAllYears();
            $this->load->view('Standard/index', $standard);
            $this->load->view('footer');
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }

    //for fetch standard details for manage
    public function fetch_standard_details()
    {
        $fetch_data = $this->Manage_standard_model->get_datatables();
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            if ($_SESSION['role'] == 'super_admin') {
                $sub_array[] = $row->name;
            }
            $sub_array[] = $row->standard;
            $sub_array[] = $row->division;
            $sub_array[] = '<button class="btn btn-warning btn-sm edit" type="button" id="' . $row->id . '" onclick="edit(' . $row->id . ')" title="Edit Standard"><span class="feather icon-edit"></span></button>
            <button class="btn btn-danger btn-sm delete" type="button" id="' . $row->id . '" onclick="delete_details(' . $row->id . ')"><span class="feather icon-trash-2" title="Delete Standard"></span></button>';
            $data[] = $sub_array;
        }
        $output = array(
            "draw" => intval($_POST["draw"]),
            "recordsTotal" => $this->Manage_standard_model->get_all_data(),
            "recordsFiltered" => $this->Manage_standard_model->get_filtered_data(),
            "data" => $data
        );
        echo json_encode($output);
    }

    //for add new standard
    public function add_standard()
    {

        $standard = $this->input->post('standard_add_std');
        if ($_SESSION['role'] == 'admin') {
            $school = $this->input->post('add_school_standard');
        } else {
            $school = $this->input->post('standard_add_school');
        }
        $division = $this->input->post('div');
        $query = null;

        $array = array('standard' => $standard, 'school_id' => $school);
        $query = $this->db->get_where('tbl_standard_management', $array);
        $count = $query->num_rows();

        if ($count == 0) {
            $div = "";
            foreach ($division as $div1) {
                $div .= $div1 . ",";
            }
            $divison = substr($div, 0, strlen($div) - 1);
            $data = array(
                'standard' => $standard,
                'division' => $divison,
                'school_id' => $school,
                'created_at' => date("Y-m-d H:i:s")
            );
            $this->db->insert('tbl_standard_management', $data);

            $id = $this->db->insert_id();
            if ($_SESSION['role'] == 'admin') {
                $school = $this->input->post('add_school_standard');
            } else {
                $school = $this->input->post('standard_add_school');
            }
            $admission_fee = $this->input->post('standard_add_admission_fee');
            $term_fee = $this->input->post('standard_add_term_fee');
            $other_fee = $this->input->post('standard_add_other_fee');

            if($this->input->post('standard_add_tuition_fee')){
                $tuition_fee = $this->input->post('standard_add_tuition_fee');
            }else if($this->input->post('standard_add_total_fee')){
                $tuition_fee = $this->input->post('standard_add_total_fee');
                $admission_fee=0;
                $term_fee=0;
                $other_fee=0;
            }
            if($this->input->post('standard_add_smart_fee')){
                $smart_fee = $this->input->post('standard_add_smart_fee');
            }else{
                $smart_fee = 0;
            }
            if($this->input->post('standard_add_computer_fee')){
                $computer_fee = $this->input->post('standard_add_computer_fee');
            }else{
                $computer_fee = 0;
            }
            if($this->input->post('standard_add_sports_fee')){
                $sports_fee = $this->input->post('standard_add_sports_fee');
            }else{
                $sports_fee = 0;
            }
            if($this->input->post('standard_add_insurance_fee')){
                $insurance_fee = $this->input->post('standard_add_insurance_fee');
            }else{
                $insurance_fee = 0;
            }
            if($this->input->post('standard_add_exam_fee')){
                $exam_fee = $this->input->post('standard_add_exam_fee');
            }else{
                $exam_fee = 0;
            }
            if($this->input->post('standard_add_enroll_fee')){
                $enroll_fee = $this->input->post('standard_add_enroll_fee');
            }else{
                $enroll_fee = 0;
            }
            $fee_data = array();
            $fee_data = array(
                'standard_mgt_id' => $id,
                'admission_fee' => $admission_fee,
                'tuition_fee' => $tuition_fee,
                'term_fee' => $term_fee,
                'other_fee' => $other_fee,
                'smart_class_fee'=>$smart_fee,
                'computer_fee'=>$computer_fee,
                'sports_fee'=>$sports_fee,
                'insurance_fee'=>$insurance_fee,
                'exam_fee'=>$exam_fee,
                'enroll_fee'=>$enroll_fee,
                'school_id' => $school,
                'fee_year' => $school.'-2'
            );
            $this->db->insert('tbl_standard', $fee_data);
            echo 'Record Inserted Successfully.';
        } else {
            echo 'Data is already inserted';
        }
    }

    //for update standard details
    public function update_standard_details()
    {
        $standard = $this->input->post('standard_edit_std');
        $fee_year = $this->input->post('standard_fee_year');
        $edit_division = $this->input->post('edit_division');
        $admission_fee = $this->input->post('standard_edit_admission_fee');
        $term_fee = $this->input->post('standard_edit_term_fee');
        $other_fee = $this->input->post('standard_edit_other_fee');
        $smart_fee = $this->input->post('standard_edit_smart_fee');
        $computer_fee = $this->input->post('standard_edit_computer_fee');
        $sports_fee = $this->input->post('standard_edit_sports_fee');
        $insurance_fee = $this->input->post('standard_edit_insurance_fee');
        $exam_fee = $this->input->post('standard_edit_exam_fee');
        $enroll_fee = $this->input->post('standard_edit_enroll_fee');
        $oldstd = $this->input->post('hidden_standard');
        $school_id = $this->input->post('hidden_school_std');


        $this->db->select('*');
        $this->db->where('id', $school_id );
        $fee_query = $this->db->get('tbl_school');
        $feeresult = $fee_query->result();

        $feestructure_flag = $feeresult[0]->feestructure_flag;

        if($feestructure_flag == '4'){
            $tuition_fee = $this->input->post('standard_edit_total_fee');
            $other_fee = $this->input->post('standard_edit_other_fee');
        }else{
            $tuition_fee = $this->input->post('standard_edit_tuition_fee');
        }
            $id = $this->input->post('id');
            $div = "";
            foreach ($edit_division as $div1) {
                $div .= $div1 . ",";
            }
            $divison = substr($div, 0, strlen($div) - 1);
            $data = array(
                'standard' => $standard,
                'division' => $divison,
                'updated' => date("Y-m-d H:i:s")
            );
            $this->db->where('id', $id);
            $this->db->update('tbl_standard_management', $data);
            $this->db->select('*');
            $this->db->where('standard_mgt_id', $id);
            $this->db->where('fee_year', ($school_id.'-'.$fee_year));
            $check_query = $this->db->get('tbl_standard');
            $exist_std = $check_query->num_rows();

            if ($exist_std == 0) {
                $standard_fee_data = array(
                    'standard_mgt_id' => $id,
                    'school_id' => $school_id,
                    'fee_year' => $school_id.'-'.$fee_year,
                    'admission_fee' => $admission_fee,
                    'tuition_fee' => $tuition_fee,
                    'term_fee' => $term_fee,
                    'other_fee' => $other_fee,
                    'smart_class_fee' => $smart_fee,
                    'computer_fee' => $computer_fee,
                    'sports_fee' => $sports_fee,
                    'insurance_fee' => $insurance_fee,
                    'exam_fee' => $exam_fee,
                    'enroll_fee' => $enroll_fee
                );
                $this->db->insert('tbl_standard', $standard_fee_data);

                if($feestructure_flag == '1'){
                    if($standard == '1' || $standard == 'I' || $standard == '9'){
                        $rem_fee =  $admission_fee + ($tuition_fee * 12) + ($term_fee * 2) + $smart_fee + $sports_fee + $computer_fee + $insurance_fee + $exam_fee + $enroll_fee;
                    }else{
                        $rem_fee = ($tuition_fee * 12) + ($term_fee * 2) + $smart_fee + $sports_fee + $computer_fee + $insurance_fee + $exam_fee + $enroll_fee;
                    }
                    $student = array(
                        'remaining_fee' => $rem_fee
                    );

                    $this->db->where('standard_id', $id);
                    $this->db->where('school_id', $school_id);
                    $this->db->where('remaining_fee', NULL);
                    $this->db->where('academic_year', $fee_year);
                    $this->db->update('tbl_student', $student);
                    $current_year = date("Y");
                    $rem_fee_new = $admission_fee + ($tuition_fee * 12) + ($term_fee * 2) + $smart_fee + $sports_fee + $computer_fee + $insurance_fee + $exam_fee + $enroll_fee;
                    $student_new = array(
                        'remaining_fee' => $rem_fee_new
                    );
                    $this->db->where('standard_id', $id);
                    $this->db->where('school_id', $school_id);
                    $this->db->where('admission_year', $current_year);
                    $this->db->where('academic_year', $fee_year);
                    $this->db->update('tbl_student', $student_new);
                }elseif ($feestructure_flag == '2'){
                    if($standard == '1' || $standard == 'I' || $standard == '9'){
                        $rem_fee =  $admission_fee + ($tuition_fee * 12) + ($term_fee * 2) + ($other_fee);
                    }else{
                        $rem_fee = ($tuition_fee * 12) + ($term_fee * 2) + ($other_fee);
                    }
                    $student = array(
                        'remaining_fee' => $rem_fee
                    );
                    $this->db->where('standard_id', $id);
                    $this->db->where('school_id', $school_id);
                    $this->db->where('remaining_fee', NULL);
                    $this->db->where('academic_year', $fee_year);
                    $this->db->update('tbl_student', $student);
                    $current_year = date("Y");
                    $rem_fee_new = $admission_fee + ($tuition_fee * 12) + ($term_fee * 2) + ($other_fee);
                    $student_new = array(
                        'remaining_fee' => $rem_fee_new
                    );
                    $this->db->where('standard_id', $id);
                    $this->db->where('school_id', $school_id);
                    $this->db->where('admission_year', $current_year);
                    $this->db->where('academic_year', $fee_year);
                    $this->db->update('tbl_student', $student_new);
                }elseif ($feestructure_flag == '3'){
                    if($standard == '1' || $standard == 'I' || $standard == '9'){
                        $rem_fee =  $admission_fee + ($tuition_fee * 12) + ($term_fee * 2) + ($other_fee * 2);
                    }else{
                        $rem_fee = ($tuition_fee * 12) + ($term_fee * 2) + ($other_fee * 2);
                    }
                    $student = array(
                        'remaining_fee' => $rem_fee
                    );
                    $this->db->where('standard_id', $id);
                    $this->db->where('school_id', $school_id);
                    $this->db->where('remaining_fee', NULL);
                    $this->db->where('academic_year', $fee_year);
                    $this->db->update('tbl_student', $student);
                    $current_year = date("Y");
                    $rem_fee_new = $admission_fee + ($tuition_fee * 12) + ($term_fee * 2) + ($other_fee * 2);
                    $student_new = array(
                        'remaining_fee' => $rem_fee_new
                    );
                    $this->db->where('standard_id', $id);
                    $this->db->where('school_id', $school_id);
                    $this->db->where('admission_year', $current_year);
                    $this->db->where('academic_year', $fee_year);
                    $this->db->update('tbl_student', $student_new);
                }elseif ($feestructure_flag == '4'){
                    $rem_fee = $tuition_fee;
                    $student = array(
                        'remaining_fee' => $rem_fee
                    );
                    $this->db->where('standard_id', $id);
                    $this->db->where('school_id', $school_id);
                    $this->db->where('remaining_fee', NULL);
                    $this->db->where('academic_year', $fee_year);
                    $this->db->update('tbl_student', $student);
                }
            } else {

                $fee_data = array(
                    'admission_fee' => $admission_fee,
                    'tuition_fee' => $tuition_fee,
                    'fee_year' => $school_id.'-'.$fee_year,
                    'term_fee' => $term_fee,
                    'other_fee' => $other_fee,
                    'smart_class_fee' => $smart_fee,
                    'computer_fee' => $computer_fee,
                    'sports_fee' => $sports_fee,
                    'insurance_fee' => $insurance_fee,
                    'exam_fee' => $exam_fee,
                    'enroll_fee' => $enroll_fee
                );
                $fee_year_update = $school_id.'-'.$fee_year;

                $this->db->where('standard_mgt_id', $id);
                $this->db->where('fee_year', $fee_year_update);
                //$this->db->where('academic_year', $fee_year);
                $this->db->update('tbl_standard', $fee_data);
                //print($this->db->last_query());exit;
                if($feestructure_flag == '1'){
                    if($standard == '1' || $standard == 'I' || $standard == '9'){
                        $rem_fee =  $admission_fee + ($tuition_fee * 12) + ($term_fee * 2) + $smart_fee + $sports_fee + $computer_fee + $insurance_fee + $exam_fee + $enroll_fee;
                    }else{
                        $rem_fee = ($tuition_fee * 12) + ($term_fee * 2) + $smart_fee + $sports_fee + $computer_fee + $insurance_fee + $exam_fee + $enroll_fee;
                    }
                    $student = array(
                        'remaining_fee' => $rem_fee
                    );

                    $this->db->where('standard_id', $id);
                    $this->db->where('school_id', $school_id);
                    $this->db->where('remaining_fee', NULL);
                    $this->db->where('academic_year', $fee_year);
                    $this->db->update('tbl_student', $student);
                    $current_year = date("Y");
                    $rem_fee_new = $admission_fee + ($tuition_fee * 12) + ($term_fee * 2) + $smart_fee + $sports_fee + $computer_fee + $insurance_fee + $exam_fee + $enroll_fee;
                    $student_new = array(
                        'remaining_fee' => $rem_fee_new
                    );
                    $this->db->where('standard_id', $id);
                    $this->db->where('school_id', $school_id);
                    $this->db->where('admission_year', $current_year);
                    $this->db->update('tbl_student', $student_new);
                }elseif ($feestructure_flag == '2'){
                    if($standard == '1' || $standard == 'I' || $standard == '9'){
                        $rem_fee =  $admission_fee + ($tuition_fee * 12) + ($term_fee * 2) + ($other_fee);
                    }else{
                        $rem_fee = ($tuition_fee * 12) + ($term_fee * 2) + ($other_fee);
                    }
                    $student = array(
                        'remaining_fee' => $rem_fee
                    );
                    $this->db->where('standard_id', $id);
                    $this->db->where('school_id', $school_id);
                    $this->db->where('remaining_fee', NULL);
                    $this->db->where('academic_year', $fee_year);
                    $this->db->update('tbl_student', $student);
                    $current_year = date("Y");
                    $rem_fee_new = $admission_fee + ($tuition_fee * 12) + ($term_fee * 2) + ($other_fee);
                    $student_new = array(
                        'remaining_fee' => $rem_fee_new
                    );
                    $this->db->where('standard_id', $id);
                    $this->db->where('school_id', $school_id);
                    $this->db->where('admission_year', $current_year);
                    $this->db->where('academic_year', $fee_year);
                    $this->db->update('tbl_student', $student_new);
                }elseif ($feestructure_flag == '3'){
                    if($standard == '1' || $standard == 'I' || $standard == '9'){
                        $rem_fee =  $admission_fee + ($tuition_fee * 12) + ($term_fee * 2) + ($other_fee * 2);
                    }else{
                        $rem_fee = ($tuition_fee * 12) + ($term_fee * 2) + ($other_fee * 2);
                    }
                    $student = array(
                        'remaining_fee' => $rem_fee
                    );
                    $this->db->where('standard_id', $id);
                    $this->db->where('school_id', $school_id);
                    $this->db->where('remaining_fee', NULL);
                    $this->db->where('academic_year', $fee_year);
                    $this->db->update('tbl_student', $student);
                    $current_year = date("Y");
                    $rem_fee_new = $admission_fee + ($tuition_fee * 12) + ($term_fee * 2) + ($other_fee * 2);
                    $student_new = array(
                        'remaining_fee' => $rem_fee_new
                    );
                    $this->db->where('standard_id', $id);
                    $this->db->where('school_id', $school_id);
                    $this->db->where('academic_year', $fee_year);
                    $this->db->where('admission_year', $current_year);
                    $this->db->update('tbl_student', $student_new);
                }elseif ($feestructure_flag == '4'){
                    $rem_fee = $tuition_fee;
                    $student = array(
                        'remaining_fee' => $rem_fee
                    );
                    $this->db->where('standard_id', $id);
                    $this->db->where('school_id', $school_id);
                    $this->db->where('academic_year', $fee_year);
                    $this->db->where('remaining_fee', NULL);
                    $this->db->update('tbl_student', $student);
                }
            }
            echo 'Record Updated Successfully.';
        }
//    }

    //for get standard for update
    public function get_data_update()
    {
        $output = array();
        $this->load->model("Fee_model");
        $id = $this->input->post('id');
        $fee_year = $this->input->post('fee_year');


        $this->db->select('*');
        $this->db->where('id', $id);
        $standard_query = $this->db->get('tbl_standard_management');
        $standard_result = $standard_query->result();

        $this->db->select('feestructure_flag');
        $this->db->where('id', $standard_result[0]->school_id);
        $fee_query = $this->db->get('tbl_school');
        $feeresult = $fee_query->result();
        $output['feestructure_flag'] = $feeresult[0]->feestructure_flag;

        $year = $standard_result[0]->school_id.'-'.$fee_year;
        $this->db->select('*');
        $this->db->where('standard_mgt_id', $id);
        $this->db->where('fee_year', $year);
        $std_query = $this->db->get('tbl_standard');
        $std_result = $std_query->result();

        if ($std_result) {
            $data = $this->Manage_standard_model->fetch_std_data($id,$year);

            foreach ($data as $row) {
                $output['id'] = $row->id;
                $output['standard'] = $row->standard;
                $output['division'] = $row->division;
                $output['school_id'] = $row->school_id;
                $output['admission_fee'] = $row->admission_fee;
                $output['tuition_fee'] = $row->tuition_fee;
                $output['term_fee'] = $row->term_fee;
                $output['other_fee'] = $row->other_fee;
                $output['smart_class_fee'] = $row->smart_class_fee;
                $output['computer_fee'] = $row->computer_fee;
                $output['sports_fee'] = $row->sports_fee;
                $output['exam_fee'] = $row->exam_fee;
                $output['insurance_fee'] = $row->insurance_fee;
                $output['enroll_fee'] = $row->enroll_fee;
            }
        } else {
            $data = $this->Manage_standard_model->fetch_std_fee_data($id,$year);
            foreach ($data as $row) {
                $output['id'] = $row->id;
                $output['standard'] = $row->standard;
                $output['division'] = $row->division;
                $output['school_id'] = $row->school_id;
                $output['admission_fee'] = '';
                $output['tuition_fee'] = '';
                $output['term_fee'] = '';
                $output['other_fee'] = '';
                $output['smart_class_fee'] = '';
                $output['computer_fee'] = '';
                $output['sports_fee'] = '';
                $output['exam_fee'] = '';
                $output['insurance_fee'] ='';
                $output['enroll_fee'] = '';
            }
        }

        echo json_encode($output);
    }

    //for delete standard
    public function delete_standard_details()
    {
        $id = $this->input->post('id');

        $result = $this->Manage_standard_model->check_delete($id);
        if ($result > 0) {
            echo 'In this divison no. of students are ' . $result . ' so this divison not be deleted';
        } else {
            $id = $this->input->post('id');

            $this->db->where('standard_mgt_id', $id);
            $this->db->delete('tbl_standard');

            $this->db->where('id', $id);
            $this->db->delete('tbl_standard_management');
            echo 'Record Deleted Successfully.';
        }

    }

    //for check in standard student value is empty or not
    public function check_for_remove()
    {
        $id = $this->input->post('id');
        $divId = $this->input->post('divId');

        $result = $this->Manage_standard_model->check_for_delete($id, $divId);

        if ($result > '0') {
            echo 'In this divison no. of students are ' . $result . ' so this divison not be deleted.';
        } else {
            echo 'Record Deleted Successfully.';
        }
    }

    //for check in division student value is empty or not
    public function check_for_division()
    {
        $id = $this->input->post('id');
        $div_add = $this->input->post('div_add');

        $division = $this->Manage_standard_model->check_for_div($id);
        $div = explode(",", $division);
        if (in_array($div_add, $div, TRUE)) {
            echo "found";
        } else {
            echo "not found";
        }
    }

    //for get fee structure
    public function get_fee_structure(){
        $school_id=$this->input->post('school_id');

        $this->db->select('*');
        $this->db->where('id',$school_id);
        $query=$this->db->get('tbl_school');
        $result=$query->result();

        echo json_encode($result);
    }
}

?>
