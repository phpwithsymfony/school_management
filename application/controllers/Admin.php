<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        error_reporting(E_ALL & ~E_NOTICE);
        $this->load->helper(array('url', 'form'));
        $this->load->library('form_validation');
        $this->load->database();
        date_default_timezone_set('Asia/Kolkata');
    }

    /**
     * @Desc : for login
     */
    public function login()
    {

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            redirect('home/');
        } else {
            $response['message'] = "";

            if ($this->input->post('submit')) {

                $this->form_validation->set_rules("name", "required");
                $this->form_validation->set_rules("password", "Password", "required");

                if ($this->form_validation->run() === TRUE) {
                    $response['message'] = "";
                    $name = $this->input->post('name');
                    $password = md5($this->input->post('password'));

                    $this->db->SELECT('*');
                    $array = array('username' => $name, 'password' => $password);
                    $query = $this->db->get_where('tbl_admin', $array);

                    $user = $query->row();

                    if ($user != null ) {

                        $this->load->library('session');

                        $_SESSION['user_logged'] = TRUE;
                        $_SESSION['user_id'] = $user->id;
                        $_SESSION['name'] = $user->firstname;
                        $_SESSION['email'] = $user->email;
                        $_SESSION['username'] = $user->username;
                        $_SESSION['role'] = $user->role;
                        $_SESSION['school_id'] = $user->school_id;
                        $_SESSION['user_name'] = $user->username;
                        if($_SESSION['user_name'] != 'superadmin'){
                            $this->db->select('school_id');
                            $array = array('username' => $_SESSION['user_name']);
                            $this->db->where($array);
                            $query = $this->db->get('tbl_admin');
                            $result_school = $query->result();
                            $array=json_decode(json_encode($result_school), true);
                            $school_ids = array_column($array, 'school_id');

                            $this->db->select('*');
                            $this->db->where_in('id', $school_ids);
                            $query = $this->db->get('tbl_school');
                            $school_data = $query->result();
                            $_SESSION['school_data'] = $school_data;

                        }



                        $updated_last_login = array(
                            'last_login' => date("Y-m-d H:i:s")
                        );
                        $this->db->where("id", $_SESSION['user_id']);
                        $this->db->update("tbl_admin", $updated_last_login);
                        redirect("/");
                    } else {
                        $response['message'] = "No such account exists in database";
                    }
                }
            } else {
                $response['message'] = "";
            }
            $this->load->view('Auth/login', $response);
        }

    }

    /**
     * @Desc : for login session destroy
     */
    public function logout()
    {
        $this->session->sess_destroy();
        if ($this->session->sess_destroy()) {
            echo "done";
        }
    }

    /**
     * @Desc : for store localstorage value in session
     */
    public function localstorage_val()
    {
        $this->load->library('session');
        if ($this->input->post('School_id')) {
            if (isset($_SESSION['local_schoolid'])) {
                unset($_SESSION['local_schoolid']);
                $_SESSION['local_schoolid'] = $this->input->post('School_id');
            } else {
                $_SESSION['local_schoolid'] = $this->input->post('School_id');
            }
        }
        echo $_SESSION['local_schoolid'];
    }

    public function database_backup()
    {


        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {
            if (!is_dir('app-assets/db_backup/')) {
                $this->session->set_flashdata('response_error', "Please create db_backup folder.");
                redirect("admin/db_backup/");
            } else {
                $this->load->database();
                $this->load->dbutil();
                $key_name2 = $this->db->database . '_';
                $key_name3 = date("dmy");
                $file_name = $key_name2 . $key_name3;
                $prefs = array(
                    'ignore' => array(),
                    'format' => 'txt',
                    'filename' => $file_name,
                    'add_drop' => TRUE,
                    'add_insert' => TRUE,
                    'newline' => "\n"
                );

                $backup = $this->dbutil->backup($prefs);
                $this->load->helper('file');
                write_file('app-assets/db_backup/' . $file_name .'.sql', $backup);

                $path = $_SERVER['DOCUMENT_ROOT'] . 'app-assets/db_backup/';
                $files = glob($path . '*');
                if (count($files) > 7) {
                    unlink($files[0]);
                }
                $this->session->set_flashdata('response', " $file_name.sql Created Successfully");
                $this->load->view('header');
                $this->load->view('db_backup');
                $this->load->view('footer');
            }
        }else{
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }
}