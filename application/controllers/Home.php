<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->model('Home_model');
    }

    /**
     * @Desc : for home page
     */
    public function index()
    {

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {

            $data['school'] = $this->Home_model->school_data();
            $data['student'] = $this->Home_model->student_data();
            $data['paid_fee'] = $this->Home_model->paid_fee_data();
            $data['daily_paid_fee'] = $this->Home_model->daily_paid_fee();
            $data['pending_fee'] = $this->Home_model->pending_fee_data();
           
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);
            $this->load->view('home', $data);
            $this->load->view('footer');

        } else if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {

            $data['student'] = $this->Home_model->student_data();
            $data['paid_fee'] = $this->Home_model->paid_fee_data();
            $data['daily_paid_fee'] = $this->Home_model->daily_paid_fee();
            $data['pending_fee'] = $this->Home_model->pending_fee_data();

            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);
            $this->load->view('home', $data);
            $this->load->view('footer');

        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }
}

?>