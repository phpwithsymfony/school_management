<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schools extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->library('form_validation');
        $this->load->database();
        date_default_timezone_set('Asia/Kolkata');
        $this->load->model('School_model');
    }

    /**
     * @Desc : show all schools list
     */
    public function index()
    {

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);
            $this->load->view('Schools/index');
            $this->load->view('footer');
        }else if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $this->session->set_flashdata('response_error', "Please login as super admin to access page");
            redirect('home/');
        }else{
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }

    /**
     * @Desc : fetch all schools data
     */
    public function fetch_school_data()
    {
        $list = $this->School_model->get_datatables();
        $data = array();
        foreach ($list as $rows) {
            $row = array();

            $row[] = $rows->school_code;
            $row[] = $rows->name;
            $row[] = $rows->email;
            $row[] = $rows->contact;
            $row[] = $rows->city;
            $row[] = $rows->state;
            $row[] = '<a  href="edit?id=' . $rows->id . '" name="update" class="btn btn-warning btn-sm update" title="Edit School"><span class="feather icon-edit"></span></a>&nbsp;&nbsp;<button class="btn btn-danger btn-sm delete" type="button" id="' . $rows->id . '" onclick="delete_school_details(' . $rows->id . ')" title="Delete School"><span class="feather icon-trash-2" ></span></button>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->School_model->count_all(),
            "recordsFiltered" => $this->School_model->count_filtered(),
            "data" => $data,
        );

        echo json_encode($output);
    }

    /**
     * @Desc : for add school
     */
    public function add()
    {

        $data = array();
        if ($this->input->post('school_submit')) {
            $this->form_validation->set_rules("add_schoolid", "School Id", "required");
            $this->form_validation->set_rules("add_schoolname", "School Name", "required|min_length[2]|max_length[200]");
            $this->form_validation->set_rules("add_schoolcity", "City", "required|min_length[2]|max_length[20]");
            $this->form_validation->set_rules("add_schoolstate", "State", "required|min_length[2]|max_length[20]");
            $this->form_validation->set_rules("add_schoolemail", "Email", "required|valid_email");
            $this->form_validation->set_rules("add_schooladdress", "Address", "required");
            $this->form_validation->set_rules("add_schoolcontact", "Contact No.", "required");
            if ($this->form_validation->run() === TRUE) {

                $data = array(
                    'school_code' => $this->input->post('add_schoolid'),
                    'name' => $this->input->post('add_schoolname'),
                    'email' => $this->input->post('add_schoolemail'),
                    'contact' => $this->input->post('add_schoolcontact'),
                    'address' => $this->input->post('add_schooladdress'),
                    'city' => $this->input->post('add_schoolcity'),
                    'state' => $this->input->post('add_schoolstate'),
                    'feestructure_flag' => $this->input->post('school_feestrcture'),
                    'created_at' => date("Y-m-d H:i:s")
                );
                try {
                    $this->db->trans_start(FALSE);
                    $this->db->insert('tbl_school', $data);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() == FALSE) {
                        throw new Exception('Database error! Please try again..');
                    }
                    $this->session->set_flashdata('response', "Record Inserted Successfully.");
                    redirect('schools/');
                } catch (Exception $e) {
                    $this->load->view('error');
                    return false;
                }
            }
        }

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);

            $this->db->select('*');
            $query=$this->db->get('tbl_fee_structure');
            $feedata['feedata']=$query->result();
            $this->load->view('Schools/add',$feedata);
            $this->load->view('footer');
        } else if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $this->session->set_flashdata('response_error', "Please login as super admin to access page");
            redirect('home/');
        }else{
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }

    /**
     * @Desc : for school id exists or not
     */
    public function check_schoolId_exists()
    {
        $data = array();
        $school_id = $this->input->post('school_id');
        $this->load->model('School_model');
        $data = $this->School_model->school_id_exists($school_id);
        if ($data != null) {
            echo 'Already exists';
        } else {
            echo 'Available';
        }
    }

    /**
     * @Desc : for edit school
     */
    public function edit()
    {

        if ($this->input->post('school_update')) {
            $this->form_validation->set_rules("edit_schoolid", "School Id", "required");
            $this->form_validation->set_rules("edit_schoolname", "School Name", "required|min_length[2]|max_length[200]");
            $this->form_validation->set_rules("edit_schoolcity", "City", "required|min_length[2]|max_length[20]");
            $this->form_validation->set_rules("edit_schoolstate", "State", "required|min_length[2]|max_length[20]");
            $this->form_validation->set_rules("edit_schoolemail", "Email", "required|valid_email");
            $this->form_validation->set_rules("edit_schooladdress", "Address", "required");
            $this->form_validation->set_rules("edit_schoolcontact", "Contact No.", "required");
            if ($this->form_validation->run() === TRUE) {

                $updated_data = array(
                    'school_code' => $this->input->post('edit_schoolid'),
                    'name' => $this->input->post('edit_schoolname'),
                    'email' => $this->input->post('edit_schoolemail'),
                    'contact' => $this->input->post('edit_schoolcontact'),
                    'address' => $this->input->post('edit_schooladdress'),
                    'city' => $this->input->post('edit_schoolcity'),
                    'state' => $this->input->post('edit_schoolstate'),
                    'feestructure_flag'=> $this->input->post('edit_feestrcture'),
                    'updated' => date("Y-m-d H:i:s")
                );
                $oldfee=$this->input->post('old_feeflag');
                $newfee=$this->input->post('edit_feestrcture');
                if($oldfee != $newfee){
                    $updatefee_data = array(
                        'admission_fee' => '',
                        'tuition_fee' => '',
                        'term_fee' => '',
                        'other_fee' => '',
                        'smart_class_fee' => '',
                        'computer_fee' => '',
                        'sports_fee' => '',
                        'insurance_fee' => '',
                        'exam_fee' => '',
                        'enroll_fee' =>''
                    );
                    $this->db->where('school_id', $this->input->post('edit_id'));
                    $this->db->update('tbl_standard', $updatefee_data);
                    $updateremfee= array(
                        'remaining_fee' => NULL,
                    );
                    $this->db->where('school_id', $this->input->post('edit_id'));
                    $this->db->update('tbl_student', $updateremfee);
                }
                try {
                    $this->db->trans_start(FALSE);
                    $this->db->where('id', $this->input->post('edit_id'));
                    $this->db->update('tbl_school', $updated_data);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() == FALSE) {
                        $this->load->view('error');
                        return false;
                    }
                    $this->session->set_flashdata('response', "Record Updated Successfully.");
                    redirect('schools/');
                } catch (Exception $e) {
                    $this->session->set_flashdata('Error', $e->getMessage());
                }
            }
        }

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {

            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);

            $id = $this->input->get('id');

            if ($id) {
                $_SESSION['school_editid'] = $id;
            } else {
                $id = $_SESSION['school_editid'];
            }

            $updated_data = $row = array();

            $row['data'] = $this->School_model->update_school($id);

            $this->db->select('*');
            $query=$this->db->get('tbl_fee_structure');
            $row['feedata']=$query->result();

            $this->load->view('Schools/edit', $row);
            $this->load->view('footer');
        } else if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $this->session->set_flashdata('response_error', "Please login as super admin to access page");
            redirect('home/');
        }else{
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }

    }

    /**
     * @Desc : for delete school
     */
    public function delete_school_details()
    {
        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {

            $id = $this->input->post('id');

            $result = $this->School_model->check_delete($id);
            if ($result > 0) {
                echo 'In this school no. of students are ' . $result . ' so this school not be deleted';
            } else {

                $this->db->where('school_id', $id);
                $query = $this->db->delete('tbl_admin');

                $this->db->where('school_id', $id);
                $fee_query = $this->db->delete('tbl_standard');

                $this->db->where('school_id', $id);
                $std_query = $this->db->delete('tbl_standard_management');

                if ($query && $fee_query && $std_query) {
                    $this->db->where('id', $id);
                    $query_delete = $this->db->delete('tbl_school');
                    echo 'Data Deleted';
                } else {
                    echo "Data not deleted";
                }


            }
        } else if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $this->session->set_flashdata('response_error', "Please login as super admin to access page");
            redirect('home/');
        }else{
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }

    /**
     * @Desc : for check email id exists
     */
    public function check_emailId_exists()
    {
        $data = array();
        $email_id = $this->input->post('email');
        $data = $this->School_model->email_id_exists($email_id);
        if ($data != null) {
            echo 'Already exists';
        } else {
            echo 'Available';
        }
    }
}