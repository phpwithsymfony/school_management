<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csv_import extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');

        $this->load->database();
        $this->load->model('csv_import_model');
        $this->load->library('Csvimport');
    }

    public function index()
    {

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);

            $student['standard'] = $this->csv_import_model->get_std_value();

            $student['school'] = $this->csv_import_model->get_school_value();

            $this->load->view('upload_csv', $student);
            $this->load->view('footer');
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }

    }

    public function import()
    {
        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {
            $file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"]);
            //for same name delete
            $name_data = array();
            foreach ($file_data as $student) {
                $name['firstname'] = $student['firstname'];
                $name['Surname'] = $student['Surname'];
                $name['fathername'] = $student['fathername'];
                array_push($name_data, $name);
            }
            $result = array_map("unserialize", array_unique(array_map("serialize", $name_data)));
            $dupes = array_diff_key($name_data, $result);

            foreach ($dupes as $id => $item) {
                $key = $id;
                $array_unique = $file_data[$key];
                unset($file_data[$key]);
            }

            $this->db->select('firstname,surname,father_name');
            $student_query = $this->db->get('tbl_student');
            $student_result = $student_query->result();

            if ($student_result) {
                foreach ($student_result as $database_data) {
                    foreach ($file_data as $k => $sheet_data) {
                        if ($database_data->firstname == $sheet_data['firstname'] && $database_data->surname == $sheet_data['Surname'] && $database_data->father_name == $sheet_data['fathername']) {
                            unset($file_data[$k]);
                        }
                    }
                }
            }
            $i = 0;
            foreach ($file_data as $row) {
                //for get value of school id
                $school = $row["school"];
                $this->db->select('id');
                $this->db->where('name', $school);
                $results_query = $this->db->get('tbl_school');
                $results['school_id'] = $results_query->result();
                $school_id = $results['school_id'][0]->id;
                $school_data = array();
                if ($school_id == null) {
                    $school_data = array(
                        'name' => $row["school"]
                    );
                    $this->db->insert("tbl_school", $school_data);
                    $this->db->select('id');
                    $array = array('name' => $school);
                    $school_query = $this->db->get_where('tbl_school', $array);
                    $result_school['school'] = $school_query->result();
                    $school_id = $result_school['school'][0]->id;
                } else {
                    $school_id = $results['school_id'][0]->id;
                }

                $standard = $row["standard"];
                $division = $row["division"];
                $this->db->select('standard');
                $array = array('standard' => $standard, 'school_id' => $school_id);
                $query = $this->db->get_where('tbl_standard_management', $array);
                $result = $query->result();
                $std_data = array();
                if (!$result) {
                    $std_data = array(
                        'school_id' => $school_id,
                        'standard' => $standard
                    );
                    $this->db->insert("tbl_standard_management", $std_data);
                    $this->db->select('id');
                    $array = array('standard' => $standard, 'school_id' => $school_id);
                    $std_query = $this->db->get_where('tbl_standard_management', $array);
                    $result_std['std'] = $std_query->result();
                    $standard_id = $result_std['std'][0]->id;
                } else {
                    $this->db->select('id');
                    $array = array('standard' => $standard, 'school_id' => $school_id);
                    $query_std = $this->db->get_where('tbl_standard_management', $array);
                    $result['standard'] = $query_std->result();
                    $standard_id = $result['standard'][0]->id;
                }

                $this->db->select('division');
                $array = array('standard' => $standard, 'school_id' => $school_id);
                $div_query = $this->db->get_where('tbl_standard_management', $array);
                $div_result = $div_query->result();
                $last_divs = array_column($div_result, 'division');
                $div_value = $last_divs[0];
                $check_last = strpos(',', $div_value);

                $last_divison = explode(",", $div_value);
                $check_div = array_search($division, $last_divison);
                $div_data = array();

                if ($check_div == false) {

                    $this->db->select('division');
                    $array = array('standard' => $standard, 'school_id' => $school_id);
                    $query_div = $this->db->get_where('tbl_standard_management', $array);
                    $result_div['division'] = $query_div->result();
                    $div = $result_div['division'][0]->division;
                    $f_division = $div . ',' . $division;
                    $insert = substr($f_division, 1);

                    $div_data = array(
                        'division' => $div . ',' . $division
                    );

                    $array = array('standard' => $standard, 'school_id' => $school_id);
                    $this->db->where($array);
                    $this->db->update("tbl_standard_management", $div_data);
                }
                $date = $row["birthdate"];
                $res = explode("-", $date);
                $birthdate = $res[2] . "-" . $res[1] . "-" . $res[0];
                $i++;
                $data[] = array(
                    // 'id' => $i,
                    'school_id' => $school_id,
                    'standard_id' => $standard_id,
                    'gr_number' => $row["G.R. Number"],
                    'divison' => $row["division"],
                    'medium' => $row["medium"],
                    'stream' => $row["stream"],
                    'section' => $row["section"],
                    'admission_year' => $row["admission year"],
                    'firstname' => $row["firstname"],
                    'surname' => $row["Surname"],
                    'father_name' => $row["fathername"],
                    'mother_name' => $row["mothername"],
                    'email' => $row["email"],
                    'contact_no' => $row["contact no."],
                    'alter_contact_no' => $row["alter contact no."],
                    'address' => $row["address"],
                    'gender' => $row["gender"],
                    'birthdate' => $birthdate,
                    'category' => $row["category(general/RTE)"],
                    'caste' => $row["caste"],
                    'remaining_fee' => NULL,
                    'admission_date' => $row["admission date"],
                    'is_deleted' => '0',
                    'created_by' => $_SESSION['role'],
                    'updated_by' => Null,
                    'created' => date("Y-m-d H:i:s"),
                    'updated' => date("Y-m-d H:i:s")
                );
            }
            $this->csv_import_model->insert($data);
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }

    }

    public function clear_all_student()
    {
        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {
            $fee_all = $this->db->empty_table('tbl_fee_details');
            $std_all = $this->db->empty_table('tbl_standard');

            $delete_all = $this->db->empty_table('tbl_student');
            $standard_all = $this->db->empty_table('tbl_standard_management');
            if ($delete_all && $standard_all) {
                $this->session->set_flashdata('response', "Clear All Student Data Deleted Successfully.");
                redirect('csv_import/');
            } else {
                $this->session->set_flashdata('response', "Not Clear All Student Data Deleted.");
                redirect('csv_import/');
            }
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }

    public function clear_student_standard()
    {
        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {
            $standard = $this->input->post('clear_student_standard');
            $school = $this->input->post('clear_student_school');

            $this->db->where('standard_mgt_id', $standard);
            $this->db->where('school_id', $school);
            $std_fee_query = $this->db->delete('tbl_standard');
            $this->db->where('id', $standard);
            $this->db->where('school_id', $school);
            $std_query = $this->db->delete('tbl_standard_management');
            $this->db->where('standard_id', $standard);
            $this->db->where('school_id', $school);
            $delete_query = $this->db->delete('tbl_student');
            if ($delete_query && $std_fee_query && $std_query) {
                echo "Clear Student Data Deleted Successfully";
            } else {
                echo "Not Clear Student Data Deleted";
            }
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }

    }
}