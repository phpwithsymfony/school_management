<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    //index page
    public function index()
    {

        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Home_model');

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {

            $data['school'] = $this->Home_model->school_data();
            $data['student'] = $this->Home_model->student_data();
            $data['paid_fee'] = $this->Home_model->paid_fee_data();
            $data['daily_paid_fee'] = $this->Home_model->daily_paid_fee();
            $data['pending_fee'] = $this->Home_model->pending_fee_data();
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);
            $this->load->view('home', $data);
            $this->load->view('footer');

        } else if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {

            $data['student'] = $this->Home_model->student_data();
            $data['paid_fee'] = $this->Home_model->paid_fee_data();
            $data['daily_paid_fee'] = $this->Home_model->daily_paid_fee();
            $data['pending_fee'] = $this->Home_model->pending_fee_data();
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);
            $this->load->view('home', $data);
            $this->load->view('footer');

        } else {
            redirect('admin/login');
        }
    }

}

?>