<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit_profile extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        error_reporting(E_ALL & ~E_NOTICE);
        $this->load->helper(array('url', 'form'));
    }

    /**
     * @Desc : fetch admin details for update
     */
    function fetch_admin_details()
    {
        $output = array();
        $this->load->model("Admin_model");
        $data = $this->Admin_model->fetch_admin_details_manageprofile($_POST["user_id"]);
        foreach ($data as $row) {
            $output['update_uname_admin'] = $row->username;
            $output['update_fname_admin'] = $row->firstname;
            $output['update_lname_admin'] = $row->lastname;
            $output['update_email_admin'] = $row->email;
            $output['update_gender_admin'] = $row->gender;
        }

        echo json_encode($output);
    }

    /**
     * @Desc : for edit admin profile
     */
    function edit_profile()
    {

        if ($_POST["btn_action"] == "Update") {
            $updated_data = array(
                'username' =>$this->input->post('update_uname_admin'),
                'firstname' => $this->input->post('update_fname_admin'),
                'lastname' => $this->input->post('update_lname_admin'),
                'email' => $this->input->post('update_email'),
                'gender' => $this->input->post('update_gender_admin')
            );
            $email = $this->input->post('update_admin_email');
            $username = $this->input->post('update_admin_username');
            $this->db->select('id');
            $this->db->where('username', $username);
            $email_query = $this->db->get('tbl_admin');
            $email_result = $email_query->result();
            $school_id = array_column($email_result, 'id');

            if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {
                $this->db->where('id', $this->input->post('user_id'));
                $query = $this->db->update('tbl_admin', $updated_data);
            } else {
                $this->db->where_in('id', $school_id);
                $query = $this->db->update('tbl_admin', $updated_data);
            }
            $this->load->model('Admin_model');
            $_SESSION['name'] = $this->input->post('update_fname_admin');
            $_SESSION['email'] = $this->input->post('update_email');
            echo 'Data Updated';

        }
    }

    /**
     * @Desc : check old password is correct or not
     */
    public function check_old_password()
    {

        $oldpassword = md5($_POST['oldpassword']);

        $this->db->SELECT('*');
        $array = array('password' => $oldpassword, 'id' => $_SESSION['user_id']);
        $query = $this->db->get_where('tbl_admin', $array);

        $user = $query->num_rows();
        if ($user > 0) {
            echo "true";
        } else {
            echo "false";
        }
    }

    /**
     * @Desc : for change password
     */
    public function change_password()
    {

        $id = $this->input->post('change_id');
        $password = md5($this->input->post('new_password'));

        $email = $this->input->post('change_email');
        $this->db->select('school_id');
        $this->db->where('email', $email);
        $email_query = $this->db->get('tbl_admin');
        $email_result = $email_query->result();
        $school_id = array_column($email_result, 'school_id');

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {
            $value = array('password' => $password);
            $this->db->where('id', $id);
            $this->db->update('tbl_admin', $value);
        } else {
            $value = array('password' => $password);
            $this->db->where('username', $email);
//            $this->db->where_in('school_id', $school_id);
            $this->db->update('tbl_admin', $value);
        }
        echo 'Your Password Changed';
    }

}