<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->library('form_validation');
        $this->load->database();
        date_default_timezone_set('Asia/Kolkata');
        $this->load->model('Setting_model');
    }

    /**
     * @Desc : show all schools list
     */
    public function index()
    {

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'super_admin') {
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);

            $this->load->model("Setting_model");
            $settings['data'] = $this->Setting_model->fetch_settings();

            $this->load->view('Settings/index',$settings);
            $this->load->view('footer');
        }else if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $this->session->set_flashdata('response_error', "Please login as super admin to access page");
            redirect('home/');
        }else{
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }


    public function update_settings()
    {
        $masterpassword = $this->input->post('masterpassword');
        $master_pass_id = $this->input->post('master_pass_id');

        $update_settings = [
            'masterpassword'    => $masterpassword,
            'modified_date'     => date("Y-m-d H:i:s")
        ];

        try {
            $this->db->trans_start(FALSE);
            $this->db->where("id", $master_pass_id);
            $this->db->update("tbl_setting", $update_settings);
            $this->db->trans_complete();

            if ($this->db->trans_status() == FALSE) {
                throw new Exception('Database error! Please try again..');
            }
            $this->session->set_flashdata('response', "Record Updated Successfully.");
            redirect('settings');
        } catch (Exception $e) {
            $this->load->view('error');
            return false;
        }
    }
    
    
}