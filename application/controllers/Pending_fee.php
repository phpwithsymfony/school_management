<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pending_fee extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->model("Manage_standard_model");
        $this->load->model("Student_model");
        $this->load->model("Pending_fee_model");
        $this->load->model('Take_fee_model');
        $this->load->model("Academic_year_model");
        $this->load->library('pdf');
    }

    /**
     * @Desc : for  student list
     */
    public function index()
    {
        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            $url_name['url'] = $this->uri->segment(1) ? $this->uri->segment(1) : 'home';
            $this->load->view('header', $url_name);

            $standard['std'] = $this->Manage_standard_model->fetch_std();

            $standard['school'] = $this->Student_model->fetch_school();

            $standard['year'] = $this->Academic_year_model->getAllYears();

            $this->load->view('Pending_fee/index', $standard);
            $this->load->view('Students/view_fee');
            $this->load->view('footer');
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }
    }

    /**
     * @Desc : for  student list
     */
    public function student_list()
    {

        $list = $this->Pending_fee_model->get_datatables();

        $data = array();
        foreach ($list as $rows) {
            $row = array();
            $row[] = $rows->gr_number;
            $row[] = $rows->firstname . ' ' . $rows->father_name . ' ' . $rows->surname;
            $row[] = $rows->email;
            $row[] = $rows->contact_no;
            $row[] = $rows->standard . ' ' . $rows->divison;
            $row[] = $rows->medium;
            $row[] = $rows->remaining_fee;
            $this->db->select('id');
            $this->db->where('gr_number',$rows->gr_number);
            $this->db->where('firstname',$rows->firstname);
            $this->db->where('father_name',$rows->father_name);
            $this->db->where('surname',$rows->surname);
            $this->db->where('academic_year',$rows->academic_year);
            $query=$this->db->get('tbl_student');
            $result=$query->result();
            $row[] = '<button type="button" data-toggle="modal" data-target="#feeModal" name="view" id="' . $result[0]->id  . '" class="btn btn-info btn-sm pending_view_fee">View Fee</button>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Pending_fee_model->count_all(),
            "recordsFiltered" => $this->Pending_fee_model->count_filtered(),
            "data" => $data,
        );

        echo json_encode($output);
    }
    /**
     * @Desc : pdf of all standard pending fee list
     */
//    public function allstdpdf()
//    {
//        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
//            //get school name
//            $school_id = $_SESSION['local_schoolid'];
//            $this->db->select('name');
//            $this->db->where('id',$school_id);
//            $this->db->from('tbl_school');
//            $query = $this->db->get();
//            $school_name = $query->result();
//            $list['school_name'] = $school_name[0]->name;
//
//            //get all standrad list
//            $list['standard_data'] = $this->Pending_fee_model->get_all_standard_list();
//
//            //get all standrad rem fee
////            $list['standard_remaining_fee'] = $this->Pending_fee_model->get_all_std_rem_fee();
//            $this->load->view('Pending_fee/allStdPendingfeePDF',$list);
//
//            $html = $this->output->get_output();
//
//            $this->pdf->generate($html, 'pendingfee');
//
//        }else{
//            $response['message'] = "Please login to access page";
//            $this->load->view('Auth/login', $response);
//        }
//    }
    /**
     * @Desc : pdf of pending fee list
     */
    public function convertpdf()
    {
        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            
            $list['student_data'] = $this->Pending_fee_model->get_student_list();
            foreach ($list['student_data'] as $row => $field) {
                $this->db->select('*');
                $this->db->where('gr_number',$field->gr_number);
                $this->db->where('firstname',$field->firstname);
                $this->db->where('father_name',$field->father_name);
                $this->db->where('surname',$field->surname);
                $this->db->where('standard_id',$field->standard_id);
                $this->db->where('academic_year',$field->academic_year);
                $query=$this->db->get('tbl_student');
                $result=$query->result();

                foreach($result as $student_id){
                    $this->db->select('*');
                    $this->db->where('student_id',$student_id->id);
                    $this->db->where('fee_year',$field->academic_year);
                    $paid_fee_student = $this->db->get('tbl_fee_details');
                    if($paid_fee_student->num_rows() > 0){
                        $dataArrayIndex = count($paid_fee_student->result());
                        foreach($paid_fee_student->result() as $fee_details){
                            $i = 0;
                            foreach($paid_fee_student->result() as $fee_Data){
                                $i++;
                                $field->paid_fee[$i] = $fee_Data->paid_fee;
                                $field->fee_month[$i] = $fee_Data->fee_month;
                                $field->discount[$i] = $fee_Data->discount;
                                $field->concession[$i] = $fee_Data->concession;
                            }

                        }
                    }else{
                        $field->paid_fee = '';
                        $field->fee_month = '';
                        $field->discount = '';
                        $field->concession = '';
                    }

                }
            }

            if($this->input->post('hide_std')){
                $standard=$this->input->post('hide_std');
                $list['standard']= $standard;
            }else{
                $list['standard']='';
            }
            if($this->input->post('hide_div')){
                $division=$this->input->post('hide_div');
                $list['division']= $division;
            }else{
                $list['division']='';
            }

            $results = array();

            foreach ($list['student_data'] as $row => $field) {

                //check if "gr_number" is already set and store the index value it exist within
                $existing_index = NULL;
                foreach ($results as $result_row => $result_field) {

                    if ($result_field['firstname'] == $field->firstname && $result_field['surname'] == $field->surname) {
                        $existing_index = $result_row;
                        break;
                    }
                }
                if (isset($existing_index)) {
                    //the "gr_number" already exist, so add to the existing gr_number
                    $dataArrayIndex = count($results[$existing_index]['dataArray']);
                    $results[$existing_index]['dataArray'][$dataArrayIndex]['paid_fee'] = $field->paid_fee;
                    $results[$existing_index]['dataArray'][$dataArrayIndex]['fee_month'] = $field->fee_month;
                    $results[$existing_index]['dataArray'][$dataArrayIndex]['discount'] = $field->discount;
                    $results[$existing_index]['dataArray'][$dataArrayIndex]['concession'] = $field->concession;
                } else {
                    //the "gr_number" does not exist, create it
                    $results_index = count($results);
                    $results[$results_index]['standard_id'] = $field->standard_id;
                    $results[$results_index]['admission_year'] = $field->admission_year;
                    $results[$results_index]['school_id'] = $field->school_id;
                    $results[$results_index]['gr_number'] = $field->gr_number;
                    $results[$results_index]['firstname'] = $field->firstname;
                    $results[$results_index]['email'] = $field->email;
                    $results[$results_index]['contact_no'] = $field->contact_no;
                    $results[$results_index]['standard'] = $field->standard;
                    $results[$results_index]['divison'] = $field->divison;
                    $results[$results_index]['remaining_fee'] = $field->remaining_fee;
                    $results[$results_index]['father_name'] = $field->father_name;
                    $results[$results_index]['surname'] = $field->surname;
                    $results[$results_index]['dataArray']['paid_fee'] = $field->paid_fee;
                    $results[$results_index]['dataArray']['fee_month'] = $field->fee_month;
                    $results[$results_index]['dataArray']['discount'] = $field->discount;
                    $results[$results_index]['dataArray']['concession'] = $field->concession;
                }
            }

            $remdata=array();
            $remmonth=array();
            $fee_year = '';
            if($this->input->post('hide_feeyear')){
                $fee_year = $this->input->post('hide_feeyear');
            }
            if($fee_year == ''){
                $fee_year = '2';
            }

            if($this->input->post('hide_feemonth')){
                foreach ($results as $row_result){

                    $standard_id=$row_result['standard_id'];
                    $admission_year=$row_result['admission_year'];
                    $school_id=$row_result['school_id'];

                    $this->db->select('feestructure_flag');
                    $this->db->where('id',$school_id);
                    $fee_query=$this->db->get('tbl_school');
                    $fee_result=$fee_query->result();

                    $this->db->select('standard');
                    $this->db->where('id',$standard_id);
                    $standard_query=$this->db->get('tbl_standard_management');
                    $standard_result=$standard_query->result();
                    $school_fee['fee'] = $this->Student_model->remaining_fee($standard_id, $school_id,$fee_year);

                    $current_year = date("Y");
                    $data=$row_result['dataArray']['fee_month'];
                    $lastmonth = '';
                    if($row_result['dataArray']['fee_month'] != '') {
                        /*Check array start*/
                        if(is_array($data)){
                            $lastdata = count($data);
                        }else{
                            $lastdata = count(array($data));
                        }
                        /*end*/

                        if ($lastdata != 0) {
                            $lastpaidmonth = $data[$lastdata];
                        }
                        if (strpos($lastpaidmonth, '-') !== false) {
                            $month = explode(' - ', $lastpaidmonth);
                            $lastmonth = $month[1];
                        } else {
                            $lastmonth = $lastpaidmonth;
                        }
                    }
                    $search_month=$this->input->post('hide_feemonth');
                    $monthNames = array("June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May");
                    $search_Index = array_search($search_month, $monthNames);
                    if($search_Index){
                        if($lastmonth != 'May'){
                            $search_month_index=array_search($search_month, $monthNames)+1;
                            $lastmonth_index=array_search($lastmonth, $monthNames)+1;
                        }else{
                            $search_month_index=array_search($search_month, $monthNames);
                            $lastmonth_index=array_search($lastmonth, $monthNames);
                        }

                    }else{
                        $monthNames = array("first", "second", "third", "fourth");
                        if($lastmonth != 'fourth'){
                            $search_month_index = (array_search($search_month, $monthNames))+1;
                            $lastmonth_index = (array_search($lastmonth, $monthNames))+1;
                        }else{
                            $search_month_index = (array_search($search_month, $monthNames));
                            $lastmonth_index = (array_search($lastmonth, $monthNames));
                        }
                    }
                    if($lastmonth == ''){
                        $rem_month=$monthNames[0].' - '.$search_month;
                    }else if($lastmonth_index){
                        $rem_month=$monthNames[$lastmonth_index].' - '.$search_month;
                    }
                    array_push($remmonth,$rem_month);
                    if($fee_result[0]->feestructure_flag == '1'){
                        if (count($school_fee['fee']) > 0) {
                            if ($admission_year == $current_year || $standard_result[0]->standard == '1' || $standard_result[0]->standard == 'I' || $standard_result[0]->standard == '9') {
                                if($search_month == "December" || $search_month == "May"){
                                    $totalFee = $school_fee['fee']['0']->admission_fee + ($school_fee['fee']['0']->tuition_fee) * $search_month_index + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->smart_class_fee + $school_fee['fee']['0']->	computer_fee + $school_fee['fee']['0']->sports_fee + $school_fee['fee']['0']->insurance_fee + $school_fee['fee']['0']->exam_fee+ $school_fee['fee']['0']->enroll_fee;
                                }else{
                                    $totalFee = $school_fee['fee']['0']->admission_fee + ($school_fee['fee']['0']->tuition_fee) * $search_month_index + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->smart_class_fee + $school_fee['fee']['0']->	computer_fee + $school_fee['fee']['0']->sports_fee + $school_fee['fee']['0']->insurance_fee + $school_fee['fee']['0']->exam_fee+ $school_fee['fee']['0']->enroll_fee;
                                }
                            } else {
                                if($search_month == "December" || $search_month == "May"){
                                    $totalFee = ($school_fee['fee']['0']->tuition_fee) * $search_month_index + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->smart_class_fee + $school_fee['fee']['0']->	computer_fee + $school_fee['fee']['0']->sports_fee + $school_fee['fee']['0']->insurance_fee + $school_fee['fee']['0']->exam_fee+ $school_fee['fee']['0']->enroll_fee;
                                }else{
                                    $totalFee = ($school_fee['fee']['0']->tuition_fee) * $search_month_index + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->smart_class_fee + $school_fee['fee']['0']->	computer_fee + $school_fee['fee']['0']->sports_fee + $school_fee['fee']['0']->insurance_fee + $school_fee['fee']['0']->exam_fee+ $school_fee['fee']['0']->enroll_fee;
                                }
                            }
                        } else {
                            $totalFee = NULL;
                        }
                    }else if($fee_result[0]->feestructure_flag == '2'){
                        if (count($school_fee['fee']) > 0) {
                            if ($admission_year == $current_year || $standard_result[0]->standard == '1' || $standard_result[0]->standard == 'I' || $standard_result[0]->standard == '9') {
                                if($search_month == "December" || $search_month == "May"){
                                    $totalFee = $school_fee['fee']['0']->admission_fee + ($school_fee['fee']['0']->tuition_fee) * $search_month_index + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->term_fee  + $school_fee['fee']['0']->other_fee;
                                }else{
                                    $totalFee = $school_fee['fee']['0']->admission_fee + ($school_fee['fee']['0']->tuition_fee) * $search_month_index + $school_fee['fee']['0']->term_fee  + $school_fee['fee']['0']->other_fee;
                                }
                            } else {
                                if($search_month == "December" || $search_month == "May"){
                                    $totalFee = ($school_fee['fee']['0']->tuition_fee) * $search_month_index + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->other_fee;
                                }else{
                                    $totalFee = ($school_fee['fee']['0']->tuition_fee) * $search_month_index + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->other_fee;
                                }
                            }

                        } else {
                            $totalFee = NULL;
                        }
                    }else if($fee_result[0]->feestructure_flag == '4'){
                        if($fee_year != '1'){
                            if (count($school_fee['fee']) > 0) {
                                $totalFee = ($school_fee['fee']['0']->tuition_fee / 3 )*$search_month_index;
                            } else {
                                $totalFee = NULL;
                            }
                        }else{
                            if (count($school_fee['fee']) > 0) {
                                $totalFee = ($school_fee['fee']['0']->tuition_fee / 4 )*$search_month_index;
                            } else {
                                $totalFee = NULL;
                            }
                        }

                    }else{
                        if (count($school_fee['fee']) > 0) {
                            if ($admission_year == $current_year ||  $standard_result[0]->standard == '1' || $standard_result[0]->standard == 'I' || $standard_result[0]->standard == '9') {
                                if($search_month == "December" || $search_month == "May"){
                                    $totalFee = $school_fee['fee']['0']->admission_fee + ($school_fee['fee']['0']->tuition_fee) * $search_month_index + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->other_fee + $school_fee['fee']['0']->other_fee;
                                }else{
                                    $totalFee = $school_fee['fee']['0']->admission_fee + ($school_fee['fee']['0']->tuition_fee) * $search_month_index + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->other_fee;
                                }
                            } else {
                                if($search_month == "December" || $search_month == "May"){
                                    $totalFee = ($school_fee['fee']['0']->tuition_fee) * $search_month_index + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->other_fee + $school_fee['fee']['0']->other_fee;
                                }else{
                                    $totalFee = ($school_fee['fee']['0']->tuition_fee) * $search_month_index + $school_fee['fee']['0']->term_fee + $school_fee['fee']['0']->other_fee;
                                }
                            }

                        } else {
                            $school_fee = NULL;
                        }
                    }
                    $array=$row_result['dataArray'];
                    $paidFee=array();
                    if($row_result['dataArray']['paid_fee'] != ''){
                        $totalpaidData = array();
                        $i=0;
                        foreach ($row_result['dataArray']['paid_fee'] as $rows) {
                            $i++;
                            $totalpaidData=(int)$row_result['dataArray']['paid_fee'][$i]  + (int)$row_result['dataArray']['discount'][$i] + (int)$row_result['dataArray']['concession'][$i];
                            array_push($paidFee,$totalpaidData);
                        }
                    }

                    $Total_paid=array_sum($paidFee);
                    $results_index = count($results);
                    $remainingFee = $totalFee-$Total_paid;
                    array_push($remdata,$remainingFee);

                }
            }else{
                foreach ($results as $row_result){
                    $data=$row_result['dataArray']['fee_month'];
                    $lastpaidmonth = '';
                    $lastmonth = '';
                    if($row_result['dataArray']['fee_month'] != ''){
                        $lastdata=count($data);
                        if($lastdata != 0){
                            $lastpaidmonth = $data[$lastdata];
                        }
                        if (strpos($lastpaidmonth, '-') !== false) {
                            $month=explode(' - ',$lastpaidmonth);
                            $lastmonth=$month[1];
                        }else{
                            $lastmonth=$lastpaidmonth;
                        }
                        $monthNames = array("June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May");
                        $search_Index = array_search($lastmonth, $monthNames);
                        if($search_Index){
                            $search_month_index=array_search($lastmonth, $monthNames) + 1;
                        }else{
                            if($fee_year != '1'){
                                $monthNames = array("first", "second", "third");
                                $search_month_index = (array_search($lastmonth, $monthNames));
                            }else{
                                $monthNames = array("first", "second", "third", "fourth");
                                $search_month_index = (array_search($lastmonth, $monthNames));
                            }
                        }
                    }
                    $standard_id=$row_result['standard_id'];
                    $admission_year=$row_result['admission_year'];
                    $school_id=$row_result['school_id'];

                    $this->db->select('feestructure_flag');
                    $this->db->where('id',$school_id);
                    $fee_query=$this->db->get('tbl_school');
                    $fee_result=$fee_query->result();

                    if($lastmonth == ''){
                        if($fee_result[0]->feestructure_flag == '4'){
                            $rem_month='first - third';
                        }else{
                            $rem_month='June - May';
                        }
                    }else if($search_month_index){
                      $rem_month=$monthNames[$search_month_index].' - '.$monthNames[sizeof($monthNames) - 1];
                    }
                  

                    array_push($remmonth,$rem_month);

                    $remainingFee=$row_result['remaining_fee'];
                    array_push($remdata,$remainingFee);
                }
            }
            $list['remaining'] = $remdata;
            $list['total_rem_fee']= array_sum($list['remaining']);

            $list['remaining_month'] = $remmonth;
            $list['data'] = $results;
            if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'admin') {
                if (isset($_SESSION['local_schoolid'])) {
                    $this->db->select('*');
                    $this->db->where('id', $_SESSION['local_schoolid']);
                    $school_query = $this->db->get('tbl_school');
                    $list['school_name'] = $school_query->result();
                } else {
                    $this->db->select('*');
                    $this->db->where('id', $_SESSION['school_id']);
                    $school_query = $this->db->get('tbl_school');
                    $list['school_name'] = $school_query->result();
                }
            }
            /*Check Fee Year Start*/
                $requestYear = $this->input->post('hide_request_year');
                if(!empty($requestYear)){
                    $list['request_year'] = 'Year : '.$this->Academic_year_model->getYear($this->input->post('hide_request_year'));
                }else{
                    $list['request_year'] = 'Date : '.date("d-m-Y");
                }
            /*End*/

            $this->load->view('Pending_fee/pendingfeePDF', $list);

            $html = $this->output->get_output();

            $this->pdf->generate($html, 'pendingfee');
        } else {
            $response['message'] = "Please login to access page";
            $this->load->view('Auth/login', $response);
        }

    }

    /**
     * @Desc : for view fee details
     */
    public function view_fee()
    {
        $output = array();

        $gr_number = $this->input->post('gr_number');

        $student_data = $this->Pending_fee_model->fetch_student_data($gr_number);

        $result = $this->Pending_fee_model->fetch_student_fee($gr_number);

        $result_month = $this->Take_fee_model->get_last_month($gr_number);

        if ($result_month != '') {
            $last_fee_period = $result_month->fee_month;

            if (strstr($last_fee_period, '- ')) {
                $last_fee_month = trim(substr($last_fee_period, strrpos($last_fee_period, '- ') + 1));
            } else {
                $last_fee_month = $result_month->fee_month;
            }
            if($last_fee_month == 'May'){
                $student_data[0]->remaining_fee = 0;
            }
        }
        $fee_details = array();
        for ($i = 0; $i < count($result); $i++) {
            $fee_details[$i]["paid_fee"] = number_format($result[$i]['paid_fee']);
            $fee_details[$i]["fee_month"] = $result[$i]['fee_month'];
            $fee_details[$i]["payment_date"] = date("d-m-Y", strtotime($result[$i]['payment_date']));
        }
        $output['student_data'] = $student_data;
        $output['fee_details'] = $fee_details;
        echo json_encode($output);
    }

}