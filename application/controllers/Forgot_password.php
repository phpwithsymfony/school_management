<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot_password extends CI_Controller
{

    public function __construct()
    {
        ob_start();
        parent::__construct();
        error_reporting(E_ALL & ~E_NOTICE);
        $this->load->helper(array('url', 'form'));
        $this->load->database();
        $this->load->library('mail');

    }

    /**
     * @Desc : forgot password
     */
    public function index()
    {

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            redirect('home/');
        } else {
            $this->load->library('form_validation');
            if ($this->input->post('forgot_pwd')) {
                $email = $this->input->post('forgot_email');

                $this->db->SELECT('*');
                $array = array('email' => $email);
                $query = $this->db->get_where('tbl_admin', $array);

                $user = $query->num_rows();
                if ($user > 0) {
                    $str = "0123456789qwertyuiopasdfghjklzxcvbnm";
                    $str = str_shuffle($str);
                    $str = substr($str, 0, 10);
                    $value = array('token' => $str);
                    $this->db->where('email', $this->input->post('forgot_email'));
                    $this->db->update('tbl_admin', $value);


                    $data = array('token' => 'forgot_password/reset_password?token=' . $str);
                    $mail_body = $this->load->view('Forgot_password/mail_body', $data, TRUE);
                    $email = $this->input->post('forgot_email');

                    if ($this->mail->send($email, $mail_body)) {
                        redirect("admin/login");
                    } else {
                        $response['message'] = "ERROR: Unable to send reset link.";
                    }
                } else {
                    $response['message'] = "No such account exists in database";
                }
            }
            $this->load->view('Forgot_password/index', $response);
        }

    }

    /**
     * @Desc : for reset password
     */
    public function reset_password()
    {

        $token = $this->input->get('token');

        $this->db->select('*');
        $this->db->where('token', $token);
        $query = $this->db->get('tbl_admin');
        $row['data'] = $query->result();

        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true) {
            redirect('home/');
        } else {
            $this->load->view('Forgot_password/reset_password', $row);
        }

        if ($this->input->post('reset_pwd')) {
            $new_token = $this->input->post('token');
            $updated_data = array();
            $updated_data = array(
                'password' => md5($this->input->post('reset_password')),
            );
            $this->db->where("token", $new_token);
            $this->db->update("tbl_admin", $updated_data);
            redirect("admin/login");
        }
    }
}

?>