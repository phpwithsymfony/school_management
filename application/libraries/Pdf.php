<?php
ini_set('memory_limit','-1');
ini_set('post_max_size','120M');
ini_set('max_execution_time', 600);
    defined('BASEPATH') OR exit('No direct script access allowed');
    define('DOMPDF_ENABLE_AUTOLOAD', false);
    require_once("./web/assets/vendor/dompdf/dompdf/dompdf_config.inc.php");
class Pdf {

      public function generate($html, $filename='', $paper='A4',$stream=TRUE, $orientation = "potrait")
      {
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . $filename . "\"");
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper($paper, $orientation);
        $dompdf->render();
        if ($stream) {
            ob_end_clean();
            $dompdf->stream($filename.".pdf", array("Attachment" => 0));
            redirect(base_url());
            exit();
        } else {
            return $dompdf->output();
            exit();
        }
      }
    }