<?php

class Admin_model extends CI_Model
{
    var $table = 'tbl_admin';
    var $select_column = array('tbl_admin.id', 'tbl_school.school_code', 'tbl_school.name', 'tbl_admin.firstname', 'tbl_admin.lastname', 'tbl_admin.middlename', 'tbl_admin.email', 'tbl_admin.contact_no', 'tbl_admin.city');
    var $column_order = array('tbl_admin.id', 'tbl_school.school_code', 'tbl_school.name', 'tbl_admin.firstname', 'tbl_admin.lastname', 'tbl_admin.middlename', 'tbl_admin.email', 'tbl_admin.contact_no', 'tbl_admin.city');
    var $column_search = array('tbl_admin.id', 'tbl_school.school_code', 'tbl_school.name', 'tbl_admin.firstname', 'tbl_admin.lastname', 'tbl_admin.middlename', 'tbl_admin.email', 'tbl_admin.contact_no', 'tbl_admin.city');
    var $order = array('tbl_school.school_code' => 'asc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {

        //add search
        if ($this->input->post('school_id')) {
            $this->db->where('tbl_school.school_code', $this->input->post('school_id'));
        }
        if ($this->input->post('school_name')) {
            $this->db->where('tbl_school.name', $this->input->post('school_name'));
        }
        if ($this->input->post('firstname')) {
            $name = $this->input->post('firstname');
            $names = explode(" ", $name);
            $this->db->where_in('tbl_admin.firstname', $names);
            $this->db->or_where_in('tbl_admin.middlename', $names);
            $this->db->or_where_in('tbl_admin.lastname', $names);
        }
        if ($this->input->post('contact_no')) {
            $this->db->where('tbl_admin.contact_no', $this->input->post('contact_no'));
        }
        if ($this->input->post('email')) {
            $this->db->where('tbl_admin.email', $this->input->post('email'));
        }
        if ($this->input->post('city')) {
            $this->db->where('tbl_admin.city', $this->input->post('city'));
        }
        $this->db->select($this->select_column);
        $this->db->where('role', 'admin');
        $this->db->from($this->table);
        $this->db->join('tbl_school', 'tbl_school.id=tbl_admin.school_id', 'left');
        $i = 0;

        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    //for fetch admin details in nav_bar
    function fetch_admin_details_manageprofile($user_id)
    {
        $this->db->select('*');
        $this->db->where("id", $user_id);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    //for update admin details in nav_bar
    function update_admin_details_manageprofile($school_id, $updated_data)
    {
        $this->db->where_in('school_id', $school_id);
        $query = $this->db->update($this->table, $updated_data);
        return $query->result();
    }

    //for get school id for add admin
    function get_school_id()
    {
        $this->db->select('*');
        $query = $this->db->get('tbl_school');
        return $query->result();
    }

    //for get school name as per school id
    function fetch_school_name($school_id)
    {
        $this->db->select('name,id');
        $this->db->where('school_code', $school_id);
        $query = $this->db->get('tbl_school');
        return $query->result();
    }

    //for update admin
    function update_admin($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    //for school id exists or not
    function school_id_exists($school_id)
    {
        $this->db->select('id');
        $this->db->where('school_code', $school_id);
        $query = $this->db->get('tbl_school');
        $result = $query->result();

        $this->db->select('school_id');
        $this->db->where('school_id', $result[0]->id);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    //for school id exists or not
    function email_id_exists($email_id, $old_email)
    {
        if ($old_email) {
            $this->db->select('school_id');
            $this->db->where('username', $old_email);
            $school_query = $this->db->get($this->table);
            $school_result = $school_query->result();
            $school_ids = array_column($school_result, 'school_id');

            $this->db->select('school_id');
            $all_school_query = $this->db->get($this->table);
            $all_school_result = $all_school_query->result();
            $all_school_ids = array_column($all_school_result, 'school_id');

            $school_id = array_diff($all_school_ids, $school_ids);

            $this->db->select('username');
            $this->db->where_in('school_id', $school_id);
            $this->db->where('username', $email_id);
            $query = $this->db->get($this->table);
            return $query->result();
        } else {
            $this->db->select('username');
            $this->db->where('username', $email_id);
            $query = $this->db->get($this->table);
            return $query->result();
        }

    }
}

?>