<?php 

class Academic_year_model extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

	public  function getYear($id){
		$this->db->select('year');
        $this->db->where('id',$id);
        $academic_year_query=$this->db->get('tbl_academic_year');
        $academic_year_query=$academic_year_query->result();
        return $academic_year_query[0]->year;
	}

    public function getAllYears(){
        $academic_year_query=$this->db->get('tbl_academic_year');
        $academic_year_query=$academic_year_query->result();
        return $academic_year_query;
    }
}