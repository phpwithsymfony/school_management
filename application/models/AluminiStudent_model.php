<?php
class AluminiStudent_model extends CI_Model
{
    var $table = "tbl_student";

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function make_query()
    {

        if ($_SESSION['role'] == 'super_admin') {
            $select_column = array("tbl_student.id", "tbl_student.gr_number", "tbl_student.firstname", "tbl_student.father_name", "tbl_student.surname", "tbl_student.email", "tbl_student.contact_no", "tbl_standard_management.standard", "tbl_student.divison", "tbl_student.medium", "tbl_student.category", "tbl_school.name");
            $column_search = array("tbl_student.id", "tbl_student.gr_number", "tbl_student.firstname", "tbl_student.father_name", "tbl_student.surname", "tbl_student.email", "tbl_student.contact_no", "tbl_standard_management.standard", "tbl_student.divison", "tbl_student.medium", "tbl_student.category", "tbl_school.name");
            $column_order = array("tbl_student.id", "tbl_student.gr_number", "tbl_student.firstname", "tbl_student.father_name", "tbl_student.surname", "tbl_student.email", "tbl_student.contact_no", "tbl_standard_management.standard", "tbl_student.divison", "tbl_student.medium", "tbl_student.category", "tbl_school.name");
        } else {
            $select_column = array("tbl_student.id", "tbl_student.gr_number", "tbl_student.firstname", "tbl_student.father_name", "tbl_student.surname", "tbl_student.email", "tbl_student.contact_no", "tbl_standard_management.standard", "tbl_student.divison", "tbl_student.medium", "tbl_student.category");
            $column_search = array("tbl_student.id", "tbl_student.gr_number", "tbl_student.firstname", "tbl_student.father_name", "tbl_student.surname", "tbl_student.email", "tbl_student.contact_no", "tbl_standard_management.standard", "tbl_student.divison", "tbl_student.medium", "tbl_student.category");
            $column_order = array("tbl_student.id", "tbl_student.gr_number", "tbl_student.firstname", "tbl_student.father_name", "tbl_student.surname", "tbl_student.email", "tbl_student.contact_no", "tbl_standard_management.standard", "tbl_student.divison", "tbl_student.medium", "tbl_student.category");
        }
        if (isset($_SESSION['local_schoolid'])) {
            $this->db->select('id');
            $this->db->where('id', $_SESSION['local_schoolid']);
            $school_query = $this->db->get('tbl_school');
            $result_school = $school_query->result();
            $school_ids = array_column($result_school, 'id');
        } else {
            $this->db->select('id');
            $this->db->where('id', $_SESSION['school_id']);
            $school_query = $this->db->get('tbl_school');
            $result_school = $school_query->result();
            $school_ids = array_column($result_school, 'id');
        }
        if ($_SESSION['role'] == 'admin') {
            $this->db->where_in('tbl_student.school_id', $school_ids);
        }
        //add search
        if ($this->input->post('alumini_school_name')) {
            $this->db->where('tbl_student.school_id', $this->input->post('alumini_school_name'));
        }
        if ($this->input->post('alumini_student_std')) {
            $this->db->where('tbl_standard_management.standard', $this->input->post('alumini_student_std'));
        }
        if ($this->input->post('alumini_student_div')) {
            $this->db->where('tbl_student.divison', $this->input->post('alumini_student_div'));
        }
        if ($this->input->post('alumini_student_medium')) {
            $this->db->like('tbl_student.medium', $this->input->post('alumini_student_medium'));
        }
        if ($this->input->post('alumini_student_school')) {
            $this->db->like('tbl_school.id', $this->input->post('alumini_student_school'));
        }
        $this->db->select($select_column);
        $this->db->where('tbl_student.is_deleted', '1', TRUE);
        $this->db->from($this->table);
        $this->db->join('tbl_school', 'tbl_school.id=tbl_student.school_id', 'left');
        $this->db->join('tbl_standard_management', 'tbl_standard_management.id=tbl_student.standard_id', 'left');

        $i = 0;

        foreach ($column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if (isset($_POST['order'])) {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function make_datatables()
    {
        $this->make_query();

        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function get_filtered_data()
    {
        $this->make_query();
        $query = $this->db->get();

        return $query->num_rows();
    }

    public function get_all_data()
    {
        $this->db->from($this->table);

        return $this->db->count_all_results();
    }

    public function get_student_list()
    {
        if (isset($_SESSION['local_schoolid'])) {
            $this->db->select('id');
            $this->db->where('id', $_SESSION['local_schoolid']);
            $school_query = $this->db->get('tbl_school');
            $result_school = $school_query->result();
            $school_ids = array_column($result_school, 'id');
        } else {
            $this->db->select('id');
            $this->db->where('id', $_SESSION['school_id']);
            $school_query = $this->db->get('tbl_school');
            $result_school = $school_query->result();
            $school_ids = array_column($result_school, 'id');
        }
        if ($_SESSION['role'] == 'admin') {
            $this->db->where_in('tbl_student.school_id', $school_ids);
        }
        //add search
        if ($this->input->post('alumini_hidden_student_std')) {
            $this->db->where('tbl_standard_management.standard', $this->input->post('alumini_hidden_student_std'));
        }
        if ($this->input->post('alumini_hidden_student_div')) {
            $this->db->where('tbl_student.divison', $this->input->post('alumini_hidden_student_div'));
        }
        if ($this->input->post('alumini_hidden_student_medium')) {
            $this->db->like('tbl_student.medium', $this->input->post('alumini_hidden_student_medium'));
        }
        if ($this->input->post('alumini_hidden_student_school')) {
            $this->db->like('tbl_school.id', $this->input->post('alumini_hidden_student_school'));
        }
        $this->db->select('*');
        $this->db->where('tbl_student.is_deleted','1',TRUE);
        $this->db->from($this->table);
        $this->db->join('tbl_school', 'tbl_school.id=tbl_student.school_id', 'left');
        $this->db->join('tbl_standard_management', 'tbl_standard_management.id=tbl_student.standard_id', 'left');
        $query=$this->db->get();
        return $query->result();
    }
}