<?php

class Pending_fee_model extends CI_Model
{
    var $table = 'tbl_student';
    var $select_column = array('tbl_student.id', 'tbl_student.gr_number', 'tbl_student.firstname', 'tbl_student.father_name', 'tbl_student.surname', 'tbl_student.email', 'tbl_student.contact_no', 'tbl_standard_management.standard', 'tbl_student.divison', 'tbl_student.medium', 'tbl_student.remaining_fee');
    var $column_order = array('tbl_student.id', 'tbl_student.gr_number', 'tbl_student.firstname', 'tbl_student.father_name', 'tbl_student.surname', 'tbl_student.email', 'tbl_student.contact_no', 'tbl_standard_management.standard', 'tbl_student.divison', 'tbl_student.medium', 'tbl_student.remaining_fee');
    var $column_search = array('tbl_student.id', 'tbl_student.gr_number', 'tbl_student.firstname', 'tbl_student.father_name', 'tbl_student.surname', 'tbl_student.email', 'tbl_student.contact_no', 'tbl_standard_management.standard', 'tbl_student.divison', 'tbl_student.medium', 'tbl_student.remaining_fee');
    var $order = array('tbl_student.id' => 'asc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function _get_datatables_query()
    {
        $monthNames = array("June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May");
        $monthValues = array("6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5");

        $query = "SELECT fee1.* FROM tbl_fee_details fee1 
	LEFT JOIN tbl_fee_details fee2 ON (fee1.student_id = fee2.student_id AND fee1.id < fee2.id)
	INNER JOIN tbl_student as student ON (fee1.`student_id` = student.id) 
WHERE fee2.id IS NULL AND student.is_deleted = '0';";
        $student_fee_data = $this->db->query($query);
        $result = $student_fee_data->result();
        $paid_student = array();
        foreach ($student_fee_data->result_array() as $paid_row) {
            $paid_student[] = $paid_row['student_id'];
        }

        $this->db->select('id');
        if ($this->input->post('pendingFeeYear')) {
            $this->db->like('tbl_student.academic_year', $this->input->post('pendingFeeYear'));
        }
        $this->db->where('tbl_student.category !=', 'RTE', TRUE);
        $this->db->where('tbl_student.is_deleted','0',TRUE);
        $this->db->where('tbl_student.remaining_fee !=', '0', TRUE);
        $student = $this->db->get('tbl_student');
        $student_array = $student->result();
        $total = array();
        foreach ($student->result_array() as $row) {
            $total[] = $row['id'];
        }

        $pending_data = array();
        $unpaid_student = array_diff($total, $paid_student);

        $current = date("y");
        $previous = date("Y",strtotime("-1 year"));
        $previous_year = $previous.'-'.$current;

        foreach ($result as $fee_month) {
            $fee_period = $fee_month->fee_month;
            $fee_year = $fee_month->fee_year;

            $this->db->select('year');
            $this->db->where('id',$fee_year);
            $student = $this->db->get('tbl_academic_year');
            $student_feeyear = $student->result();

            $period_array = explode(" - ", $fee_month->fee_month);
            $last_month = end($period_array);

            if($student_feeyear[0]->year == $previous_year){
                $last_month_index = array_search($last_month, $monthNames);
                if($last_month_index){
                    if($last_month != 'May'){
                        $GRnumber = $fee_month->student_id;
                        $number = array_push($pending_data, $GRnumber);
                    }
                }else{
                    if($fee_year != '1'){
                        if($last_month != 'third'){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_data, $GRnumber);
                        }
                    }else{
                        if($last_month != 'fourth'){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_data, $GRnumber);
                        }
                    }
                }
            }else{
                $last_month_index = array_search($last_month, $monthNames);
                if($last_month_index){
                    $month = date('F');
                    $current_month_index = array_search($month, $monthNames);
                    if ($last_month_index < $current_month_index) {
                        $GRnumber = $fee_month->student_id;
                        $number = array_push($pending_data, $GRnumber);
                    }
                }else{
                    if($fee_year != '1'){
                        $month = date('F');
                        $current_month_index = array_search($month, $monthNames);
                        if($last_month = 'first' && $current_month_index > 3){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_data, $GRnumber);
                        }elseif ($last_month = 'second' && $current_month_index > 7){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_data, $GRnumber);
                        }
                    }else{
                        $month = date('F');
                        $current_month_index = array_search($month, $monthNames);
                        if($last_month = 'first' && $current_month_index > 2){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_data, $GRnumber);
                        }elseif ($last_month = 'second' && $current_month_index > 5){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_data, $GRnumber);
                        }elseif ($last_month = 'third' && $current_month_index > 8){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_data, $GRnumber);
                        }
                    }
                }
            }

        }
        if($_SESSION['user_name'] != 'superadmin'){
            $result_school = array();
            $this->db->select('school_id');
            $array = array('username' => $_SESSION['user_name']);
            $this->db->where($array);
            $query = $this->db->get('tbl_admin');
            $result_school = $query->result();
            $array=json_decode(json_encode($result_school), true);
            $school_ids = array_column($array, 'school_id');

            $this->db->where_in('tbl_student.school_id', $school_ids);
        }

        //add search
        if ($this->input->post('school_name')) {
            $this->db->where('tbl_student.school_id', $this->input->post('school_name'));
        }
        if ($this->input->post('student_std')) {
            $this->db->where('tbl_standard_management.standard', $this->input->post('student_std'));
        }
        if ($this->input->post('student_div')) {
            $this->db->where('tbl_student.divison', $this->input->post('student_div'));
        }
        if ($this->input->post('pendingfee_medium')) {
            $this->db->like('tbl_student.medium', $this->input->post('pendingfee_medium'));
        }
        if ($this->input->post('pendingfee_school')) {
            $this->db->like('tbl_student.school_id', $this->input->post('pendingfee_school'));
        }
        if ($this->input->post('pendingFeeYear')) {
            $this->db->like('tbl_student.academic_year', $this->input->post('pendingFeeYear'));
        }
        if ($this->input->post('pendingfee_month')) {
            $feedata = array();
            $pending_feedata = array();
            foreach ($result as $fee_month) {
                $fee_period = $fee_month->fee_month;
                $fee_year = $fee_month->fee_year;


                $period_array = explode(" - ", $fee_month->fee_month);
                $last_month = end($period_array);

                $last_month_index = array_search($last_month, $monthNames);

                if($last_month_index){
                    $month = $this->input->post('pendingfee_month');
                    $current_month_index = array_search($month, $monthNames);
                    if (($last_month_index <= $current_month_index) && $last_month != 'May') {
                        $GRnumber = $fee_month->student_id;
                        $number = array_push($pending_feedata, $GRnumber);
                    }
                }else{
                    if($fee_year != '1'){
                        $month = $this->input->post('pendingfee_month');
                        $installment = array("first", "second", "third");
                        $current_installment_index = array_search($month, $installment);
                        $last_installment_index = array_search($last_month, $installment);
                        $current_month_index = array_search($month, $installment);
                        if($last_month != 'third'){
                            if($last_month = 'first' && $current_month_index > 3){
                                $GRnumber = $fee_month->student_id;
                                $number = array_push($pending_feedata, $GRnumber);
                            }elseif ($last_month = 'second' && $current_month_index > 7){
                                $GRnumber = $fee_month->student_id;
                                $number = array_push($pending_feedata, $GRnumber);
                            }
                        }


                    }else{
                        $month = $this->input->post('pendingfee_month');
                        $installment = array("first", "second", "third", "fourth");
                        $current_installment_index = array_search($month, $installment);
                        $last_installment_index = array_search($last_month, $installment);
                        if(($last_installment_index <= $current_installment_index) && $last_month != 'fourth'){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_feedata, $GRnumber);
                        }
                    }
                }

            }
            $finalfeedata = array_merge($pending_feedata,$unpaid_student);
            $this->db->where_in('tbl_student.id', $finalfeedata);
        }

        $finaldata = array_merge($pending_data,$unpaid_student);
        $this->db->from('tbl_student');

        if ($finaldata != null && $this->input->post('pendingfee_month')==null) {
            $this->db->where_in('tbl_student.id', $finaldata);
        }
        $this->db->join('tbl_standard_management', 'tbl_standard_management.id=tbl_student.standard_id');

        $i = 0;

        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    //for get all standrad list
//    public function get_all_standard_list(){
//        $school_id = $_SESSION['local_schoolid'];
//        $this->db->select('id,standard,division');
//        $this->db->where('school_id',$school_id);
//        $this->db->order_by('standard');
//        $this->db->from('tbl_standard_management');
//        $query = $this->db->get();
//        $result = $query->result();
//        return $result;
//    }

    //for pending fee pdf
    public function get_allstd_pendingfee()
    {
        $school_id = $_SESSION['local_schoolid'];

        $this->db->select('id,standard,division');
        $this->db->where('school_id',$school_id);
        $this->db->from('tbl_standard_management');
        $query = $this->db->get();
        $result = $query->result();
        echo '<pre>';
        print_r($result);
        die();
        $this->db->select('*');
        $this->db->where('school_id',$school_id);
        $this->db->where('fee_year','2');
        $this->db->from('tbl_fee_details');
        $feedetails_query = $this->db->get();
        $currentYearFee = $feedetails_query->result();

        $this->db->select('*');
        $this->db->where('school_id',$school_id);
        $this->db->where('fee_year','1');
        $this->db->from('tbl_fee_details');
        $feedetails_query = $this->db->get();
        $previousYearFee = $feedetails_query->result();

        $previouspaidFee = array();
        foreach ($previousYearFee as $paidfee){
            array_push($previouspaidFee,$paidfee->paid_fee);
        }
        $totalPrevious = array_sum($previouspaidFee);

        $currentpaidFee = array();

        foreach ($currentYearFee as $currentpaidfee) {
            array_push($currentpaidFee, $currentpaidfee->paid_fee);
        }
        $totalCurrent = array_sum($currentpaidFee);
        print_r($totalCurrent);
        print_r($totalPrevious);
        die();
    }
    //for pending fee pdf
    public function get_student_list()
    {
        $monthNames = array("June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May");
        $monthValues = array("6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5");

        $query = "SELECT fee1.* FROM tbl_fee_details fee1 
	LEFT JOIN tbl_fee_details fee2 ON (fee1.student_id = fee2.student_id AND fee1.id < fee2.id)
	INNER JOIN tbl_student as student ON (fee1.`student_id` = student.id) 
WHERE fee2.id IS NULL AND student.is_deleted = '0';";
        $student_fee_data = $this->db->query($query);
        $result = $student_fee_data->result();
        foreach ($student_fee_data->result_array() as $paid_row) {
            $paid_student[] = $paid_row['student_id'];
        }

        $this->db->select('id');
        if ($this->input->post('pendingFeeYear')) {
            $this->db->like('tbl_student.academic_year', $this->input->post('pendingFeeYear'));
        }
        $this->db->where('tbl_student.category !=', 'RTE', TRUE);
        $this->db->where('tbl_student.is_deleted','0',TRUE);
        $this->db->where('tbl_student.remaining_fee !=', '0', TRUE);
        $student = $this->db->get('tbl_student');
        $student_array = $student->result();
        $total = array();
        foreach ($student->result_array() as $row) {
            $total[] = $row['id'];
        }

        $pending_data = array();
        $unpaid_student = array_diff($total, $paid_student);

        $current = date("y");
        $previous = date("Y",strtotime("-1 year"));
        $previous_year = $previous.'-'.$current;

        foreach ($result as $fee_month) {
            $fee_period = $fee_month->fee_month;
            $fee_year = $fee_month->fee_year;

            $this->db->select('year');
            $this->db->where('id',$fee_year);
            $student = $this->db->get('tbl_academic_year');
            $student_feeyear = $student->result();

            $period_array = explode(" - ", $fee_month->fee_month);
            $last_month = end($period_array);

            if($student_feeyear[0]->year == $previous_year){
                $last_month_index = array_search($last_month, $monthNames);
                if($last_month_index){
                    if($last_month != 'May'){
                        $GRnumber = $fee_month->student_id;
                        $number = array_push($pending_data, $GRnumber);
                    }
                }else{
                    if($fee_year != '1'){
                        if($last_month != 'third'){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_data, $GRnumber);
                        }
                    }else{
                        if($last_month != 'fourth'){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_data, $GRnumber);
                        }
                    }

                }
            }else{
                $last_month_index = array_search($last_month, $monthNames);
                if($last_month_index){
                    $month = date('F');
                    $current_month_index = array_search($month, $monthNames);
                    if ($last_month_index < $current_month_index) {
                        $GRnumber = $fee_month->student_id;
                        $number = array_push($pending_data, $GRnumber);
                    }
                }else{
                    if($fee_year != '1'){
                        $month = date('F');
                        $current_month_index = array_search($month, $monthNames);
                        if($last_month = 'first' && $current_month_index > 3){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_data, $GRnumber);
                        }elseif ($last_month = 'second' && $current_month_index > 7){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_data, $GRnumber);
                        }
                    }else{
                        $month = date('F');
                        $current_month_index = array_search($month, $monthNames);
                        if($last_month = 'first' && $current_month_index > 2){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_data, $GRnumber);
                        }elseif ($last_month = 'second' && $current_month_index > 5){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_data, $GRnumber);
                        }elseif ($last_month = 'third' && $current_month_index > 8){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_data, $GRnumber);
                        }
                    }

                }
            }

        }
        $result_school = array();
        $this->db->select('school_id');
        $array = array('username' => $_SESSION['user_name']);
        $this->db->where($array);
        $query = $this->db->get('tbl_admin');
        $result_school = $query->result();
        $array=json_decode(json_encode($result_school), true);
        $school_ids = array_column($array, 'school_id');

        if ($_SESSION['role'] == 'admin') {
            $this->db->where('tbl_student.school_id', $_SESSION['local_schoolid']);
        }

        if ($this->input->post('hide_std')) {
            $this->db->where('tbl_standard_management.standard', $this->input->post('hide_std'));
        }
        if ($this->input->post('hide_div')) {
            $this->db->where('tbl_student.divison', $this->input->post('hide_div'));
        }
        if ($this->input->post('hide_medium')) {
            $this->db->like('tbl_student.medium', $this->input->post('hide_medium'));
        }
        if ($this->input->post('hide_school')) {
            $this->db->like('tbl_student.school_id', $this->input->post('hide_school'));
        }
        if ($this->input->post('hide_feeyear')) {
            $this->db->like('tbl_student.academic_year', $this->input->post('hide_feeyear'));
        }
        if ($this->input->post('hide_feemonth')) {
            $feedata = array();
            $pending_feedata = array();
            foreach ($result as $fee_month) {
                $fee_period = $fee_month->fee_month;
                $fee_year = $fee_month->fee_year;


                $period_array = explode(" - ", $fee_month->fee_month);
                $last_month = end($period_array);

                $last_month_index = array_search($last_month, $monthNames);

                if($last_month_index){
                    $month = $this->input->post('hide_feemonth');
                    $current_month_index = array_search($month, $monthNames);
                    if (($last_month_index <= $current_month_index) && $last_month != 'May') {
                        $GRnumber = $fee_month->student_id;
                        $number = array_push($pending_feedata, $GRnumber);
                    }
                }else{
                    if($fee_year != '1'){
                        $month = $this->input->post('hide_feemonth');
                        $installment = array("first", "second", "third");
                        $current_installment_index = array_search($month, $installment);
                        $last_installment_index = array_search($last_month, $installment);
                        $current_month_index = array_search($month, $installment);
                        if($last_month != 'third'){
                            if($last_month = 'first' && $current_month_index > 3){
                                $GRnumber = $fee_month->student_id;
                                $number = array_push($pending_feedata, $GRnumber);
                            }elseif ($last_month = 'second' && $current_month_index > 7){
                                $GRnumber = $fee_month->student_id;
                                $number = array_push($pending_feedata, $GRnumber);
                            }
                        }
                    }else{
                        $month = $this->input->post('hide_feemonth');
                        $installment = array("first", "second", "third", "fourth");
                        $current_installment_index = array_search($month, $installment);
                        $last_installment_index = array_search($last_month, $installment);
                        if(($last_installment_index <= $current_installment_index) && $last_month != 'fourth'){
                            $GRnumber = $fee_month->student_id;
                            $number = array_push($pending_feedata, $GRnumber);
                        }
                    }
                }

            }
            $finalfeedata = array_merge($pending_feedata,$unpaid_student);
            $this->db->where_in('tbl_student.id', $finalfeedata);
        }
        $finaldata = array_merge($pending_data,$unpaid_student);
        $this->db->from('tbl_student');
        if ($finaldata != null && $this->input->post('hide_feemonth')==null) {
            $this->db->where_in('tbl_student.id', $finaldata);
        }
        $this->db->join('tbl_standard_management', 'tbl_standard_management.id=tbl_student.standard_id','left');

        $query = $this->db->get();

        return $query->result();
    }

    public function fetch_student_data($gr_number)
    {
        $this->db->select('*');
        $this->db->where('tbl_student.id', $gr_number);
        $this->db->from('tbl_student');
        $this->db->join('tbl_standard_management', 'tbl_standard_management.id=tbl_student.standard_id', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    public function fetch_student_fee($gr_number)
    {
        $this->db->distinct();
        $this->db->select('*');
        $this->db->where('student_id', $gr_number);
        $query = $this->db->get('tbl_fee_details');
        return $query->result_array();
    }
}