<?php
// hide errors
ini_set('display_errors', '0');
// but log them
error_reporting(e_all | e_strict);

class Home_model extends CI_Model
{
    //fetch total student
    public function student_data()
    {
        $this->db->select('*');

        if ($_SESSION['role'] == 'admin') {
            if (isset($_SESSION['local_schoolid'])) {
                $this->db->where('tbl_student.school_id', $_SESSION['local_schoolid']);
            } else {
                $this->db->where('tbl_student.school_id', $_SESSION['school_id']);;
            }
        }
        $this->db->where('tbl_student.is_deleted','0',TRUE);
        $query = $this->db->get('tbl_student');
        return $query->num_rows();
    }

    //fetch total school
    public function school_data()
    {

        $this->db->select('*');

        $query = $this->db->get('tbl_school');
        return $query->num_rows();
    }

    //fetch paid fee data
    public function paid_fee_data()
    {

        if ($_SESSION['role'] == 'admin') {
            if (isset($_SESSION['local_schoolid'])) {
                $this->db->where('tbl_student.school_id', $_SESSION['local_schoolid']);
            } else {
                $this->db->where('tbl_student.school_id', $_SESSION['school_id']);;
            }
        }

        $this->db->select('*');
        $this->db->where('tbl_student.is_deleted','0',TRUE);
        $this->db->from('tbl_fee_details');
        $this->db->join('tbl_student', 'tbl_student.id=tbl_fee_details.student_id');
        $query = $this->db->get();
        return $query->num_rows();

    }

    //today paid fee data
    public function daily_paid_fee()
    {
        if ($_SESSION['role'] == 'admin') {
            if (isset($_SESSION['local_schoolid'])) {
                $this->db->where('tbl_student.school_id', $_SESSION['local_schoolid']);
            } else {
                $this->db->where('tbl_student.school_id', $_SESSION['school_id']);;
            }
        }

        $this->db->select('*');

        $startdate = date("Y-m-d H:i:s");
        $dateTime = new DateTime($startdate);
        $today_date = $dateTime->format('Y-m-d');
        $this->db->where('tbl_student.is_deleted','0',TRUE);
        $this->db->where("DATE_FORMAT(tbl_fee_details.payment_date,'%Y-%m-%d') = '$today_date'", NULL, FALSE);
        $this->db->from('tbl_fee_details');
        $this->db->join('tbl_student', 'tbl_student.id=tbl_fee_details.student_id');
        $query = $this->db->get();
        return $query->num_rows();
    }

    //for pending fee
    public function pending_fee_data()
    {
        $monthNames = array("June", "July", "August", "September", "October", "November", "December", "January", "February", "March", "April", "May");
        $monthValues = array("6", "7", "8", "9", "10", "11", "12", "1", "2", "3", "4", "5");

        $query = "SELECT fee1.* FROM tbl_fee_details fee1 LEFT JOIN tbl_fee_details fee2 ON (fee1.student_id = fee2.student_id AND fee1.id < fee2.id) WHERE fee2.id IS NULL";
        $student_fee_data = $this->db->query($query);
        $result = $student_fee_data->result();

        $this->db->select('id');
        $student = $this->db->get('tbl_student');
        $student_array = $student->result();
        $total = array();
        foreach ($student->result_array() as $row) {
            $total[] = $row['id'];
        }

        $this->db->select('student_id');
        $paid_fee_student = $this->db->get('tbl_fee_details');
        $paid_student = array();
        foreach ($paid_fee_student->result_array() as $row) {
            $paid_student[] = $row['student_id'];
        }

        $data = array();
        $data = array_diff($total, $paid_student);
        foreach ($result as $fee_month) {
            $fee_period = $fee_month->fee_month;
            $period_array = explode(" - ", $fee_month->fee_month);
            $last_month = end($period_array);
            $last_month_index = array_search($last_month, $monthNames);

            $month = date('F');
            $current_month_index = array_search($month, $monthNames);

            if ($last_month_index < $current_month_index) {
                $GRnumber = $fee_month->student_id;
                $number = array_push($data, $GRnumber);

            }

        }
        if ($_SESSION['role'] == 'admin') {
            if (isset($_SESSION['local_schoolid'])) {
                $this->db->where('tbl_student.school_id', $_SESSION['local_schoolid']);
            } else {
                $this->db->where('tbl_student.school_id', $_SESSION['school_id']);;
            }
        }

        if ($data != null) {
            $this->db->where_in('tbl_student.id', $data);
        }
        $this->db->where('tbl_student.is_deleted','0',TRUE);
        $this->db->where('tbl_student.category !=', 'RTE', TRUE);
        $this->db->from('tbl_student');
        $this->db->join('tbl_fee_details', 'tbl_fee_details.student_id=tbl_student.id', 'left');
        $this->db->join('tbl_standard_management', 'tbl_standard_management.id=tbl_student.standard_id');
        $query = $this->db->get();
        return $query->num_rows();
    }
}

?>