<?php

class Fee_model extends CI_Model
{
    var $table = 'tbl_student';
    var $select_column = array('tbl_student.id as student_id', 'tbl_student.gr_number', 'tbl_student.firstname', 'tbl_student.father_name', 'tbl_student.surname', 'tbl_student.email', 'tbl_student.contact_no', 'tbl_standard_management.standard', 'tbl_student.divison', 'tbl_student.category', 'tbl_student.remaining_fee');
    var $column_order = array('tbl_student.gr_number', 'tbl_student.firstname', 'tbl_student.email', 'tbl_student.contact_no', 'tbl_standard_management.standard', 'tbl_student.category');
    var $column_search = array('tbl_student.gr_number', 'tbl_student.firstname', 'tbl_student.email', 'tbl_student.contact_no', 'tbl_standard_management.standard', 'tbl_student.category');
    var $order = array('tbl_student.gr_number' => 'asc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {
        $this->db->select('*');
        $query_academic=$this->db->get('tbl_academic_year');
        $result_academic=$query_academic->last_row();
        $last_fee_year = $result_academic->id;
        if (isset($_SESSION['local_schoolid'])) {
            $this->db->select('id');
            $this->db->where('id', $_SESSION['local_schoolid']);
            $school_query = $this->db->get('tbl_school');
            $result_school = $school_query->result();
            $array=json_decode(json_encode($result_school), true);
            $school_ids = array_column($array, 'id');
        } else {
            $this->db->select('id');
            $this->db->where('id', $_SESSION['school_id']);
            $school_query = $this->db->get('tbl_school');
            $result_school = $school_query->result();
            $array=json_decode(json_encode($result_school), true);
            $school_ids = array_column($array, 'id');
        }
        if ($_SESSION['role'] == 'admin') {
            $this->db->where_in('tbl_student.school_id', $school_ids);
        }

        //add search
        if ($this->input->post('school_name')) {
            $this->db->where('tbl_student.school_id', $this->input->post('school_name'));
        }
        if ($this->input->post('search_grnumber')) {
            $this->db->like('tbl_student.gr_number', $this->input->post('search_grnumber'));
        }
        if ($this->input->post('search_name')) {
            $this->db->like('tbl_student.firstname', $this->input->post('search_name'));
        }
        if ($this->input->post('search_email')) {
            $this->db->like('tbl_student.email', $this->input->post('search_email'));
        }
        if ($this->input->post('search_contact_no')) {
            $this->db->like('tbl_student.contact_no', $this->input->post('search_contact_no'));
        }
        if ($this->input->post('student_std')) {
            $this->db->where('tbl_standard_management.standard', $this->input->post('student_std'));
        }
        if ($this->input->post('student_div')) {
            $this->db->where('tbl_student.divison', $this->input->post('student_div'));
        }
        if ($this->input->post('student_academic')) {
            $this->db->like('tbl_student.academic_year', $this->input->post('student_academic'));
        }
        if ($this->input->post('search_student_school')) {
            $this->db->like('tbl_school.id', $this->input->post('search_student_school'));
        }
        $this->db->select($this->select_column);
        $this->db->where('tbl_student.is_deleted','0',TRUE);
        if($this->input->post('student_academic') == ''){

            $this->db->where('tbl_student.academic_year',$last_fee_year,TRUE);
        }
        $this->db->from($this->table);
        $this->db->join('tbl_standard_management', 'tbl_standard_management.id=tbl_student.standard_id', 'left');
        $this->db->join('tbl_school', 'tbl_school.id=tbl_student.school_id', 'left');
        $i = 0;

        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function fetch_fee_data($gr_number)
    {
        $this->db->select('*');
        $this->db->where('tbl_student.id', $gr_number);
        $this->db->from('tbl_student');
        $this->db->join('tbl_standard_management', 'tbl_standard_management.id=tbl_student.standard_id', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    public function fetch_student_fee($id)
    {
        $this->db->distinct();
        $this->db->select('*');
        $this->db->where('student_id', $id);
        $query = $this->db->get('tbl_fee_details');
        return $query->result_array();
    }

    public function getStudentTotalFeeDetails($student_id=null) {

        $this->db->select('tbl_student.id,tbl_student.firstname,tbl_student.surname,tbl_school.name AS school_name,tbl_school.feestructure_flag,tbl_student.admission_year,tbl_standard_management.division,tbl_standard_management.standard,tbl_standard.admission_fee,tbl_standard.tuition_fee,tbl_standard.term_fee,tbl_standard.other_fee,tbl_standard.smart_class_fee,tbl_standard.computer_fee,tbl_standard.sports_fee,tbl_standard.insurance_fee,tbl_standard.exam_fee,tbl_standard.enroll_fee');
        $this->db->where("fee_year LIKE '%-3'", NULL, FALSE);
        if(!is_null($student_id)) {
            $this->db->where('tbl_student.id', $student_id);
        }
        $this->db->from('tbl_student');
        $this->db->join('tbl_school', 'tbl_school.id=tbl_student.school_id');
        $this->db->join('tbl_standard_management', 'tbl_student.standard_id=tbl_standard_management.id');
        $this->db->join('tbl_standard', 'tbl_standard_management.id=tbl_standard.standard_mgt_id');
        $query = $this->db->get();
        //print($this->db->last_query());exit;
        return $query->result_array();
    }

    public function getStudentPaidFeeDetails($student_id=null) {
        $sql = "select SUM(paid_fee) as total_fee,SUM(discount) as total_discount,SUM(concession) as total_concession,stu.`remaining_fee` from tbl_fee_details as fee
	INNER JOIN tbl_student as stu ON (fee.`student_id` = stu.`id`) where stu.id = '$student_id'
 group by student_id having remaining_fee > 0 ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function updateStudentRemainingFeeDetails($remaining_fee,$student_id=null) {
        
        $this->db->set('remaining_fee', $remaining_fee);
        $this->db->where('id', $student_id);
        $this->db->update('tbl_student');
        print($this->db->last_query());
        echo "<br/><br/>";
    }

}
