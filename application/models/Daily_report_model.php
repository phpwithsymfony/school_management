<?php

class Daily_report_model extends CI_Model
{
    var $table = 'tbl_student';
    var $select_column = array('tbl_student.id as student_id', 'tbl_student.gr_number', 'tbl_student.firstname', 'tbl_student.father_name', 'tbl_student.surname', 'tbl_student.email', 'tbl_student.contact_no', 'tbl_standard_management.standard', 'tbl_student.divison', 'tbl_student.medium', 'tbl_fee_details.receipt_no', 'tbl_fee_details.id','tbl_fee_details.payment_date');

    var $column_order = array('tbl_student.gr_number', 'tbl_student.firstname', 'tbl_student.father_name', 'tbl_student.surname', 'tbl_student.email', 'tbl_student.contact_no', 'tbl_standard_management.standard', 'tbl_student.divison', 'tbl_student.medium');

    var $column_search = array('tbl_student.gr_number', 'tbl_student.firstname', 'tbl_student.father_name', 'tbl_student.surname', 'tbl_student.email', 'tbl_student.contact_no', 'tbl_standard_management.standard', 'tbl_student.divison', 'tbl_student.medium');
    var $order = array('tbl_student.gr_number' => 'asc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {

        if (isset($_SESSION['local_schoolid'])) {
            $this->db->select('id');
            $this->db->where('id', $_SESSION['local_schoolid']);
            $school_query = $this->db->get('tbl_school');
            $result_school = $school_query->result();
            $school_ids = array_column($result_school, 'id');
        } else {
            $this->db->select('id');
            $this->db->where('id', $_SESSION['school_id']);
            $school_query = $this->db->get('tbl_school');
            $result_school = $school_query->result();
            $school_ids = array_column($result_school, 'id');
        }
        if ($_SESSION['role'] == 'admin') {
            $this->db->where_in('tbl_student.school_id', $school_ids);
        }
        //add search
        if ($this->input->post('dailyreport_school_name')) {
            $this->db->where('tbl_student.school_id', $this->input->post('dailyreport_school_name'));
        }
        if ($this->input->post('dailyreport_student_std')) {
            $this->db->where('tbl_standard_management.standard', $this->input->post('dailyreport_student_std'));
        }
        if ($this->input->post('dailyreport_student_div')) {
            $this->db->where('tbl_student.divison', $this->input->post('dailyreport_student_div'));
        }
        if ($this->input->post('dailyreport_student_school')) {
            $this->db->like('tbl_school.id', $this->input->post('dailyreport_student_school'));
        }
        if ($this->input->post('dailyreport_receipt_no')) {
            $this->db->like('tbl_fee_details.receipt_no', $this->input->post('dailyreport_receipt_no'));
        }
        if ($this->input->post('dailyreport_academic')) {
            $this->db->like('tbl_student.academic_year', $this->input->post('dailyreport_academic'));
        }
        if ($this->input->post('dailyreport_start_date') && $this->input->post('dailyreport_end_date')) {
            $startdate = $this->input->post('dailyreport_start_date');
            $enddate = $this->input->post('dailyreport_end_date');
            $dateTime = new DateTime($startdate);
            $start_date = $dateTime->format('Y/m/d');
            $enddateTime = new DateTime($enddate);
            $end_date = $enddateTime->format('Y/m/d');
            $data = array(
                'date1' => $start_date,
                'date2' => $end_date
            );
             $this->db->where("tbl_fee_details.payment_date BETWEEN " . "'" . $data['date1'] . "'" . " AND " . "'" . $data['date2'] . "'");
        } else {
            if($this->input->post('dailyreport_start_date')){
                $startdate = $this->input->post('dailyreport_start_date');
                $dateTime = new DateTime($startdate);
                $start_date = $dateTime->format('Y-m-d');
                $this->db->where("DATE_FORMAT(tbl_fee_details.payment_date,'%Y-%m-%d') = '$start_date'", NULL, FALSE);
            }
            if($this->input->post('dailyreport_end_date')){
                $enddate = $this->input->post('dailyreport_end_date');
                $dateTime = new DateTime($enddate);
                $end_date = $dateTime->format('Y-m-d');
                $this->db->where("DATE_FORMAT(tbl_fee_details.payment_date,'%Y-%m-%d') = '$end_date'", NULL, FALSE);
            }
        }

        if(!$this->input->post('dailyreport_start_date') && !$this->input->post('dailyreport_end_date')){
            $currentdate = date("Y-m-d H:i:s");
            $currentdateTime = new DateTime($currentdate);
            $today_date = $currentdateTime->format('Y-m-d');
            $this->db->where("DATE_FORMAT(tbl_fee_details.payment_date,'%Y-%m-%d') = '$today_date'", NULL, FALSE);
        }

        $this->db->select($this->select_column);
        $this->db->where('tbl_student.is_deleted','0',TRUE);
        $this->db->from($this->table);
        $this->db->join('tbl_fee_details', 'tbl_fee_details.student_id = tbl_student.id', 'left');
        $this->db->join('tbl_standard_management', 'tbl_standard_management.id=tbl_student.standard_id', 'left');
        $this->db->join('tbl_school', 'tbl_school.id=tbl_student.school_id', 'left');
        $i = 0;

        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }


    public function fetch_payment_date()
    {
        $this->db->select('tbl_fee_details.student_id,tbl_fee_details.payment_date,tbl_student.school_id');
        $this->db->from('tbl_fee_details');
        $this->db->join('tbl_student', 'tbl_student.id=tbl_fee_details.student_id','left');
       // $this->db->group_by('tbl_fee_details.payment_date');
        if ($_SESSION['role'] == 'admin' && isset($_SESSION['local_schoolid'])) {
            $this->db->where('tbl_student.school_id', $_SESSION['local_schoolid']);
        }
        if ($_SESSION['role'] == 'admin' && !isset($_SESSION['local_schoolid'])) {
            $this->db->where('tbl_student.school_id', $_SESSION['school_id']);
        }

        $query = $this->db->get();
        $resultsByGroup = array();
        $dateArraykey = array();


        if(!empty($query->result_array())){
            foreach($query->result_array() as $row) {
                if($row['payment_date'] != '0000-00-00'){
                 $resultsByGroup[$row['payment_date']][] = $row['payment_date'];
                }
            }
            $dateArraykey = array_keys($resultsByGroup);    
        }
        return  $dateArraykey;
    }

    //for fetch all fee details for receipt
    public function fee_details($id)
    {
        $this->db->distinct();
        $this->db->select('*');
        $this->db->where('student_id', $id);
        $query = $this->db->get('tbl_fee_details');
        return $query->last_row();
    }
    //for fetch all fee details for report
    public function report_fee_details($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('tbl_fee_details');
        return $query->result();
    }
    //for get student list for receipt
    public function student_details($id)
    {
        $this->db->select('*');
        $this->db->where('tbl_student.id', $id);

        $this->db->from('tbl_student');
        $this->db->join('tbl_standard_management', 'tbl_standard_management.id=tbl_student.standard_id', 'left');

        $query = $this->db->get();
        return $query->result();
    }
    //for get student list for report
    public function report_student_details($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $student_query = $this->db->get('tbl_fee_details');
        $result=$student_query->result();

        $this->db->select('*');
        $this->db->where('tbl_student.id', $result[0]->student_id);

        $this->db->from('tbl_student');
        $this->db->join('tbl_standard_management', 'tbl_standard_management.id=tbl_student.standard_id', 'left');

        $query = $this->db->get();
        return $query->result();
    }
    public function get_paidfee_list()
    {
        if (isset($_SESSION['local_schoolid'])) {
            $this->db->select('id');
            $this->db->where('id', $_SESSION['local_schoolid']);
            $school_query = $this->db->get('tbl_school');
            $result_school = $school_query->result();
            $school_ids = array_column($result_school, 'id');
        } else {
            $this->db->select('id');
            $this->db->where('id', $_SESSION['school_id']);
            $school_query = $this->db->get('tbl_school');
            $result_school = $school_query->result();
            $school_ids = array_column($result_school, 'id');
        }
        if ($_SESSION['role'] == 'admin') {
            $this->db->where_in('tbl_student.school_id', $school_ids);
        }

        if ($this->input->post('dailyreport_hidden_std')) {
            $this->db->where('tbl_standard_management.standard', $this->input->post('dailyreport_hidden_std'));
        }
        if ($this->input->post('dailyreport_hidden_div')) {
            $this->db->where('tbl_student.divison', $this->input->post('dailyreport_hidden_div'));
        }
        if ($this->input->post('dailyreport_hidden_school')) {
            $this->db->like('tbl_student.school_id', $this->input->post('dailyreport_hidden_school'));
        }
        if ($this->input->post('dailyreport_hidden_receipt_no')) {
            $this->db->like('tbl_fee_details.receipt_no', $this->input->post('dailyreport_hidden_receipt_no'));
        }
        if ($this->input->post('dailyreport_hidden_academic')) {
            $this->db->like('tbl_student.academic_year', $this->input->post('dailyreport_hidden_academic'));
        }
        if ($this->input->post('dailyreport_hidden_startdate') && $this->input->post('dailyreport_hidden_enddate')) {
            $startdate = $this->input->post('dailyreport_hidden_startdate');
            $enddate = $this->input->post('dailyreport_hidden_enddate');
            $dateTime = new DateTime($startdate);
            $start_date = $dateTime->format('Y/m/d');
            $enddateTime = new DateTime($enddate);
            $end_date = $enddateTime->format('Y/m/d');
            $data = array(
                'date1' => $start_date,
                'date2' => $end_date
            );
            $this->db->where("tbl_fee_details.payment_date BETWEEN " . "'" . $data['date1'] . "'" . " AND " . "'" . $data['date2'] . "'");
        }else{
            if($this->input->post('dailyreport_hidden_startdate')){
                $startdate = $this->input->post('dailyreport_hidden_startdate');
                $dateTime = new DateTime($startdate);
                $start_date = $dateTime->format('Y-m-d');
                $this->db->where("DATE_FORMAT(tbl_fee_details.payment_date,'%Y-%m-%d') = '$start_date'", NULL, FALSE);
            }
            if($this->input->post('dailyreport_hidden_enddate')){
                $enddate = $this->input->post('dailyreport_hidden_enddate');
                $dateTime = new DateTime($enddate);
                $end_date = $dateTime->format('Y-m-d');
                $this->db->where("DATE_FORMAT(tbl_fee_details.payment_date,'%Y-%m-%d') = '$end_date'", NULL, FALSE);
            }
        }

        if(!$this->input->post('dailyreport_hidden_startdate') && !$this->input->post('dailyreport_hidden_enddate')){
            $currentdate = date("Y-m-d H:i:s");
            $currentdateTime = new DateTime($currentdate);
            $today_date = $currentdateTime->format('Y-m-d');
            $this->db->where("DATE_FORMAT(tbl_fee_details.payment_date,'%Y-%m-%d') = '$today_date'", NULL, FALSE);
        }
        $this->db->select('*');
        $this->db->where('tbl_student.is_deleted','0',TRUE);
        $this->db->from('tbl_fee_details');
        $this->db->join('tbl_student', 'tbl_student.id = tbl_fee_details.student_id', 'left');
        $this->db->join('tbl_standard_management', 'tbl_standard_management.id=tbl_student.standard_id', 'left');
        $this->db->order_by("tbl_fee_details.receipt_no","asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function fee_part($id)
    {
        $this->db->select('standard_id');
        $this->db->where('id', $id);
        $query = $this->db->get('tbl_student');
        $result = $query->result();

        $standard = $result[0]->standard_id;
        $this->db->select('*');
        $this->db->where('standard_mgt_id', $standard);
        $query = $this->db->get('tbl_standard');
        return $query->result();
    }
    public function report_fee_part($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $student_query = $this->db->get('tbl_fee_details');
        $result=$student_query->result();

        $this->db->select('standard_id');
        $this->db->where('id', $result[0]->student_id);
        $query = $this->db->get('tbl_student');
        $result = $query->result();

        $standard = $result[0]->standard_id;
        $this->db->select('*');
        $this->db->where('standard_mgt_id', $standard);
        $query = $this->db->get('tbl_standard');
        return $query->result();
    }
}