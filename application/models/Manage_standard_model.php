<?php

class Manage_standard_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function fetch_std()
    {
        if (isset($_SESSION['local_schoolid'])) {
            $result = array();
            $this->db->select('school_id');
            $this->db->where('school_id', $_SESSION['local_schoolid']);
            $query = $this->db->get('tbl_admin');
            $result = $query->result();
        } else {
            $result = array();
            $this->db->select('school_id');
            $this->db->where('id', $_SESSION['user_id']);
            $query = $this->db->get('tbl_admin');
            $result = $query->result();
        }


        if ($_SESSION['role'] == 'admin') {
            $this->db->where('school_id', $result[0]->school_id);
        }
        $this->db->select('standard');
        $this->db->from('tbl_standard_management');
        $this->db->group_by('standard');
        $query = $this->db->get();

        return $query->result();
    }

    public function fetch_div($standard)
    {
        $this->db->select('division');
        $this->db->where('standard', $standard);
        $this->db->from('tbl_standard_management');
        $query = $this->db->get();
        return $query->result();
    }

    var $table = "tbl_standard_management";

    var $order = array('tbl_standard_management.standard' => 'asc');

    function get_query()
    {
        if ($_SESSION['role'] == 'super_admin') {
            $column_select = array('tbl_standard_management.id', 'tbl_school.name', 'tbl_standard_management.standard', 'tbl_standard_management.division');
            $column_search = array('tbl_school.name', 'tbl_standard_management.standard', 'tbl_standard_management.division');
            $column_order = array('tbl_school.name', 'tbl_standard_management.standard', 'tbl_standard_management.division');
        } else {
            $column_select = array('tbl_standard_management.id', 'tbl_standard_management.standard', 'tbl_standard_management.division');
            $column_search = array('tbl_standard_management.standard', 'tbl_standard_management.division');
            $column_order = array('tbl_standard_management.standard', 'tbl_standard_management.division');
        }

        $result_school = array();
        $this->db->select('school_id');
        $array = array('email' => $_SESSION['email']);
        $this->db->where($array);
        $query = $this->db->get('tbl_admin');
        $result_school = $query->result();
        $array=json_decode(json_encode($result_school), true);
        $school_ids = array_column($array, 'school_id');

        if ($_SESSION['role'] == 'admin') {
            $this->db->where_in('tbl_standard_management.school_id', $school_ids);
        }

        if ($this->input->post('school_name')) {
            $this->db->where('tbl_standard_management.school_id', $this->input->post('school_name'));
        }
        if ($this->input->post('student_std')) {
            $this->db->where('tbl_standard_management.standard', $this->input->post('student_std'));
        }
        if ($this->input->post('std_school')) {
            $this->db->where('tbl_standard_management.school_id', $this->input->post('std_school'));
        }

        $this->db->select($column_select);
        $this->db->from($this->table);
        $this->db->join('tbl_school', 'tbl_school.id=tbl_standard_management.school_id');
        $i = 0;

        foreach ($column_search as $item) {

            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->get_query();
        if ($_POST["length"] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function get_filtered_data()
    {
        $this->get_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_all_data()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function fetch_std_data($id,$year)
    {
        $this->db->select('tbl_standard_management.id,tbl_standard_management.standard,tbl_standard_management.division,tbl_standard_management.school_id,tbl_standard.admission_fee,tbl_standard.tuition_fee,tbl_standard.term_fee,tbl_standard.other_fee,tbl_standard.smart_class_fee,tbl_standard.sports_fee,tbl_standard.exam_fee,tbl_standard.computer_fee,tbl_standard.insurance_fee,tbl_standard.enroll_fee');
        $this->db->where('tbl_standard_management.id', $id);
        $this->db->where('tbl_standard.fee_year', $year);
        $this->db->from('tbl_standard_management');
        $this->db->join('tbl_standard', 'tbl_standard.standard_mgt_id=tbl_standard_management.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function fetch_std_fee_data($id)
    {
        $this->db->select('id,standard,division,school_id');
        $this->db->where('id', $id);
        $query = $this->db->get('tbl_standard_management');
        return $query->result();
    }

    public function check_for_delete($id, $divId)
    {
        $this->db->select('id,school_id');
        $this->db->where('tbl_standard_management.id', $id);
        $query = $this->db->get('tbl_standard_management');
        $data = $query->result();

        $standard = $data[0]->id;
        $school = $data[0]->school_id;

        $this->db->select('*');
        $array = array('standard_id' => $standard, 'divison' => $divId, 'school_id' => $school);
        $query = $this->db->get_where('tbl_student', $array);
        return $query->num_rows();

    }

    public function check_delete($id)
    {
        $this->db->select('standard,school_id,id');
        $this->db->where('id', $id);
        $query = $this->db->get('tbl_standard_management');
        $data = $query->result();

        $standard = $data[0]->id;
        $school = $data[0]->school_id;

        $this->db->select('*');
        $array = array('standard_id' => $standard, 'school_id' => $school,'is_deleted' => '0');
        $query = $this->db->get_where('tbl_student', $array);
        return $query->num_rows();

    }

    public function check_for_div($id)
    {
        $this->db->select('standard');
        $this->db->where('id', $id);
        $query = $this->db->get('tbl_standard_management');
        $data = $query->result();

        $standard = $data[0]->standard;
        $this->db->select('divison');
        $array = array('standard' => $standard);
        $query = $this->db->get_where('standard_management', $array);
        $div = $query->result();
        return $div[0]->divison;
    }
}