<?php

class Setting_model extends CI_Model
{
    var $table = 'tbl_setting';
    var $column_order = array('id', 'masterpassword');
    var $column_search = array('id', 'masterpassword');
    var $order = array('id' => 'asc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function fetch_settings()
    {
        $this->db->select('*');
        $query = $this->db->get('tbl_setting');
        return $query->result();
    }
}

?>