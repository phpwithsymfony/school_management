<?php

class Student_model extends CI_Model
{
    var $table = "tbl_student";
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function make_query()
    {

        if ($_SESSION['role'] == 'super_admin') {
            $select_column = array("tbl_student.id", "tbl_student.gr_number", "tbl_student.firstname", "tbl_student.father_name", "tbl_student.surname", "tbl_student.email", "tbl_student.contact_no", "tbl_standard_management.standard", "tbl_student.divison", "tbl_student.medium", "tbl_student.category", "tbl_school.name");
            $column_search = array("tbl_student.id", "tbl_student.gr_number", "tbl_student.firstname", "tbl_student.father_name", "tbl_student.surname", "tbl_student.email", "tbl_student.contact_no", "tbl_standard_management.standard", "tbl_student.divison", "tbl_student.medium", "tbl_student.category", "tbl_school.name");
            $column_order = array("tbl_student.id", "tbl_student.gr_number", "tbl_student.firstname", "tbl_student.father_name", "tbl_student.surname", "tbl_student.email", "tbl_student.contact_no", "tbl_standard_management.standard", "tbl_student.divison", "tbl_student.medium", "tbl_student.category", "tbl_school.name");
        } else {
            $select_column = array("tbl_student.id", "tbl_student.gr_number", "tbl_student.firstname", "tbl_student.father_name", "tbl_student.surname", "tbl_student.email", "tbl_student.contact_no", "tbl_standard_management.standard", "tbl_student.divison", "tbl_student.medium", "tbl_student.category");
            $column_search = array("tbl_student.id", "tbl_student.gr_number", "tbl_student.firstname", "tbl_student.father_name", "tbl_student.surname", "tbl_student.email", "tbl_student.contact_no", "tbl_standard_management.standard", "tbl_student.divison", "tbl_student.medium", "tbl_student.category");
            $column_order = array("tbl_student.id", "tbl_student.gr_number", "tbl_student.firstname", "tbl_student.father_name", "tbl_student.surname", "tbl_student.email", "tbl_student.contact_no", "tbl_standard_management.standard", "tbl_student.divison", "tbl_student.medium", "tbl_student.category");
        }
        if (isset($_SESSION['local_schoolid'])) {
            $this->db->select('id');
            $this->db->where('id', $_SESSION['local_schoolid']);
            $school_query = $this->db->get('tbl_school');
            $result_school = $school_query->result();
            $array=json_decode(json_encode($result_school), true);
            $school_ids = array_column($array, 'id');
        } else {
            $this->db->select('id');
            $this->db->where('id', $_SESSION['school_id']);
            $school_query = $this->db->get('tbl_school');
            $result_school = $school_query->result();
            $array=json_decode(json_encode($result_school), true);
            $school_ids = array_column($array, 'id');
        }

        if ($_SESSION['role'] == 'admin') {
            $this->db->where_in('tbl_student.school_id', $school_ids);
        }
        //add search
        if ($this->input->post('student_std')) {
            $this->db->where('tbl_standard_management.standard', $this->input->post('student_std'));
        }
        if ($this->input->post('student_div')) {
            $this->db->where('tbl_student.divison', $this->input->post('student_div'));
        }
        if ($this->input->post('student_medium')) {
            $this->db->like('tbl_student.medium', $this->input->post('student_medium'));
        }
        if ($this->input->post('student_school')) {
            $this->db->like('tbl_school.id', $this->input->post('student_school'));
        }
        if ($this->input->post('student_transferstd')) {
            $this->db->where('tbl_standard_management.standard', $this->input->post('student_std'));
        }
        if ($this->input->post('student_transferdiv')) {
            $this->db->where('tbl_student.divison', $this->input->post('student_div'));
        }
        if ($this->input->post('student_transfermedium')) {
            $this->db->like('tbl_student.medium', $this->input->post('student_medium'));
        }
        if ($this->input->post('student_academic')) {
            $this->db->like('tbl_student.academic_year', $this->input->post('student_academic'));
        }
        if ($this->input->post('student_transferschool')) {
            $this->db->like('tbl_school.id', $this->input->post('student_school'));
        }
        $this->db->select($select_column);
        $this->db->where('tbl_student.is_deleted','0',TRUE);
        $this->db->from($this->table);
        $this->db->join('tbl_school', 'tbl_school.id=tbl_student.school_id', 'left');
        $this->db->join('tbl_standard_management', 'tbl_standard_management.id=tbl_student.standard_id', 'left');

//        $i = 0;
//
//        foreach ($column_search as $item) {
//            if ($_POST['search']['value']) {
//                if ($i === 0) {
//                    $this->db->group_start();
//                    $this->db->like($item, $_POST['search']['value']);
//                } else {
//                    $this->db->or_like($item, $_POST['search']['value']);
//                }
//                if (count($column_search) - 1 == $i)
//                    $this->db->group_end();
//            }
//            $i++;
//        }
//        if (isset($_POST['order'])) {
//            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
//        } else if (isset($this->order)) {
//            $order = $this->order;
//            $this->db->order_by(key($order), $order[key($order)]);
//        }
    }

    function make_datatables()
    {
        $this->make_query();

//        if ($_POST['length'] != -1) {
//            $this->db->limit($_POST['length'], $_POST['start']);
//        }

        $query = $this->db->get();
        return $query->result();
    }

    public function get_filtered_data()
    {
        $this->make_query();
        $query = $this->db->get();

        return $query->num_rows();
    }

    public function get_all_data()
    {
        $this->db->from($this->table);

        return $this->db->count_all_results();
    }

    //for addstudent
    public function insert($last_grno, $standard, $division, $medium, $stream, $section, $admission_year, $name, $surname, $father_name, $mother_name, $email, $contact_no, $add_altercontact, $address, $gender, $birthdate, $category, $rem, $school_id, $caste, $admission_date, $created, $created_by,$academic_year,$birthplace)
    {
        $this->db->set('gr_number', $last_grno);
        $this->db->set('standard_id', $standard);
        $this->db->set('divison', $division);
        $this->db->set('medium', $medium);
        $this->db->set('stream', $stream);
        $this->db->set('section', $section);
        $this->db->set('admission_year', $admission_year);
        $this->db->set('firstname', $name);
        $this->db->set('surname', $surname);
        $this->db->set('father_name', $father_name);
        $this->db->set('mother_name', $mother_name);
        $this->db->set('email', $email);
        $this->db->set('contact_no', $contact_no);
        $this->db->set('alter_contact_no', $add_altercontact);
        $this->db->set('address', $address);
        $this->db->set('gender', $gender);
        $this->db->set('birthdate', $birthdate);
        $this->db->set('category', $category);
        $this->db->set('remaining_fee', $rem);
        $this->db->set('academic_year', $academic_year);
        $this->db->set('school_id', $school_id);
        $this->db->set('caste', $caste);
        $this->db->set('birthplace', $birthplace);
        $this->db->set('admission_date', $admission_date);
        $this->db->set('is_deleted', '0');
        $this->db->set('created', $created);
        $this->db->set('created_by', $created_by);
        $query = $this->db->insert('tbl_student');
        return $query;
    }

    //for update student details
    public function update_student($id)
    {
        $this->db->SELECT('*');
        $student_id = array('id' => $id);
        $query = $this->db->get_where('tbl_student', $student_id);
        return $query->result();
    }

    //insert remaining fee
    public function remaining_fee($std, $school_id, $fee_year='')
    {
        $this->db->SELECT('admission_fee,tuition_fee,term_fee,other_fee,smart_class_fee,insurance_fee,exam_fee,sports_fee,computer_fee,enroll_fee');
        if($fee_year){
            $standard = array('standard_mgt_id' => $std, 'school_id' => $school_id,'fee_year' => $school_id.'-'.$fee_year);
        }else{
            $standard = array('standard_mgt_id' => $std, 'school_id' => $school_id);
        }
        $query = $this->db->get_where('tbl_standard', $standard);

        return $query->result();
    }

    //for search student
    public function fetch_div_data($standard)
    {
        $this->db->SELECT('division,id');
        if ($_SESSION['role'] == 'admin') {
            $schoolId = $_SESSION['local_schoolid'] ? $_SESSION['local_schoolid'] : $_SESSION['school_id'];
            $this->db->where('standard', $standard);
            $this->db->where('school_id', $schoolId);
        } else {
            $this->db->where('standard', $standard);
        }
        $query = $this->db->get('tbl_standard_management');
        return $query->result();
    }

    //for add student
    public function fetch_div_add($standard, $school_id)
    {
        $this->db->select('division,id');

        if ($_SESSION['role'] == 'admin') {
            $schoolId = $_SESSION['local_schoolid'] ? $_SESSION['local_schoolid'] : $_SESSION['school_id'];
            $this->db->where('standard', $standard);
            $this->db->where('school_id', $schoolId);
        } else {
            $this->db->where('school_id', $school_id);
            $this->db->where('standard', $standard);
        }

        $query = $this->db->get('tbl_standard_management');
        return $query->result();
    }

    public function fetch_school_data()
    {
        $this->db->select('school_id');
        $this->db->where('id', $_SESSION['user_id']);
        $query = $this->db->get('tbl_admin');
        $result = $query->result();

        $this->db->select('id,school_code,name');
        $this->db->where('id', $result[0]->school_id);
        $query = $this->db->get('tbl_school');
        return $query->result();
    }

    //fetch school name for search
    public function fetch_school()
    {
        $this->db->select('id,name,city');
        $query = $this->db->get('tbl_school');
        return $query->result();
    }

    //fetch std value
    public function get_std_value($school_id)
    {

        $this->db->select('id');
        $this->db->where('school_code', $school_id);
        $query = $this->db->get('tbl_school');
        $result = $query->result();
        $schoolId = $result[0]->id;

        $this->db->select('standard');
        $this->db->where('school_id', $schoolId);
        $query = $this->db->get('tbl_standard_management');
        return $query->result();
    }

    //for get school id for add admin
    function get_school_id()
    {
        $this->db->select('school_id');
        $this->db->group_by('school_id');
        $this->db->from('tbl_standard_management');
        $query = $this->db->get();
        $array = $query->result();
        $school_idarr = array();
        foreach ($array as $schoolid) {
            $id = $schoolid->school_id;
            array_push($school_idarr, $id);
        }

        $this->db->select('school_code,city,name');
        $this->db->where_in('id', $school_idarr);
        $query = $this->db->get('tbl_school');
        return $query->result();
    }

    //for check email
    public function email_id_exists($email_id)
    {
        $this->db->select('*');
        $array = array('email' => $email_id);
        $query = $this->db->get_where('tbl_student', $array);
        return $query->num_rows();
    }

    //for check gr_number
    public function gr_number_exists($gr_number)
    {
        $this->db->select('*');
        $array = array('gr_number' => $gr_number);
        $query = $this->db->get_where('tbl_student', $array);
        return $query->num_rows();
    }

    //for student pdf
    public function get_student_list()
    {
        if (isset($_SESSION['user_logged']) && $_SESSION['user_logged'] == true && $_SESSION['role'] == 'admin') {
            if (isset($_SESSION['local_schoolid'])) {
                $this->db->select('*');
                $this->db->where('id', $_SESSION['local_schoolid']);
                $school_query = $this->db->get('tbl_school');
                $result_school = $school_query->result();
                $school_ids = array_column($result_school, 'id');
            } else {
                $this->db->select('*');
                $this->db->where('id', $_SESSION['school_id']);
                $school_query = $this->db->get('tbl_school');
                $result_school = $school_query->result();
                $school_ids = array_column($result_school, 'id');
            }
        }
        if ($_SESSION['role'] == 'admin') {
            $this->db->where_in('tbl_student.school_id', $school_ids);
        }
        //add search
        if ($this->input->post('hidden_student_std')) {
            $this->db->where('tbl_standard_management.standard', $this->input->post('hidden_student_std'));
        }
        if ($this->input->post('hidden_student_div')) {
            $this->db->where('tbl_student.divison', $this->input->post('hidden_student_div'));
        }
        if ($this->input->post('hidden_student_medium')) {
            $this->db->where('tbl_student.medium', $this->input->post('hidden_student_medium'));
        }
        if ($this->input->post('hidden_student_school')) {
            $this->db->where('tbl_school.id', $this->input->post('hidden_student_school'));
        }
        if ($this->input->post('hidden_student_academic')) {
            $this->db->where('tbl_student.academic_year', $this->input->post('hidden_student_academic'));
        }
        $this->db->select('*');
        $this->db->where('tbl_student.is_deleted','0',TRUE);
        $this->db->from($this->table);
        $this->db->join('tbl_school', 'tbl_school.id=tbl_student.school_id', 'left');
        $this->db->join('tbl_standard_management', 'tbl_standard_management.id=tbl_student.standard_id', 'left');
        $this->db->order_by('surname','ASC');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result();
    }

    //edit all student standard
    public function edit_standard_all($student_array,$new_std,$new_school)
    {

        foreach ($student_array as $student_id){
            $this->db->select('*');
            $this->db->where('tbl_student.id',$student_id);
            $this->db->from('tbl_student');
            $query = $this->db->get();

            $created = date("Y-m-d H:i:s");
            $created_by = $_SESSION['role'];

            $this->db->select('*');
            $query_academic=$this->db->get('tbl_academic_year');
            $result_academic=$query_academic->last_row();
            $last_fee_year = $result_academic->id;

            $array = $query->result();
            foreach ($array as $student) {
                $this->db->set('gr_number', $student->gr_number);
                $this->db->set('standard_id', $new_std);
                $this->db->set('divison', $student->divison);
                $this->db->set('medium', $student->medium);
                $this->db->set('stream', $student->stream);
                $this->db->set('section', $student->section);
                $this->db->set('admission_year', $student->admission_year);
                $this->db->set('firstname', $student->firstname);
                $this->db->set('surname', $student->surname);
                $this->db->set('father_name', $student->father_name);
                $this->db->set('mother_name', $student->mother_name);
                $this->db->set('email', $student->email);
                $this->db->set('contact_no', $student->contact_no);
                $this->db->set('alter_contact_no', $student->alter_contact_no);
                $this->db->set('address', $student->address);
                $this->db->set('gender', $student->gender);
                $this->db->set('birthdate', $student->birthdate);
                $this->db->set('category', $student->category);
                $this->db->set('academic_year', $last_fee_year);
                $this->db->set('school_id', $new_school);
                $this->db->set('caste', $student->caste);
                $this->db->set('birthplace', $student->birthplace);
                $this->db->set('admission_date', $student->admission_date);
                $this->db->set('is_deleted', '0');
                $this->db->set('created', $created);
                $this->db->set('created_by', $created_by);


                $new_admission_date = $student->admission_date;
                $category = $student->category;

                $parts = explode('-', $new_admission_date);
                $admission_y = $parts[0];

                $current_year = date("Y");

                $this->db->SELECT('admission_fee,tuition_fee,term_fee,other_fee,smart_class_fee,insurance_fee,exam_fee,sports_fee,computer_fee,enroll_fee');
                $standard = array('standard_mgt_id' => $new_std, 'school_id' => $new_school, 'fee_year' => $new_school.'-2');
                $query = $this->db->get_where('tbl_standard', $standard);

                $rem_fee['fee'] = $query->result();


                $this->db->select('feestructure_flag');
                $this->db->where('id',$new_school);
                $fee_query=$this->db->get('tbl_school');
                $fee_result=$fee_query->result();

                $this->db->select('standard');
                $this->db->where('id',$new_std);
                $standard_query=$this->db->get('tbl_standard_management');
                $standard_result=$standard_query->result();

                if ($category == 'General') {
                    if($fee_result[0]->feestructure_flag == '1'){
                        if (count($rem_fee['fee']) > 0) {
                            if ($admission_y == $current_year || $standard_result[0]->standard == '1' || $standard_result[0]->standard == 'I' || $standard_result[0]->standard == '9') {
                                $rem = $rem_fee['fee']['0']->admission_fee + ($rem_fee['fee']['0']->tuition_fee) * 12 + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->smart_class_fee + $rem_fee['fee']['0']->	computer_fee + $rem_fee['fee']['0']->sports_fee + $rem_fee['fee']['0']->insurance_fee + $rem_fee['fee']['0']->exam_fee+ $rem_fee['fee']['0']->enroll_fee;
                            } else {
                                $rem = ($rem_fee['fee']['0']->tuition_fee) * 12 + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->smart_class_fee + $rem_fee['fee']['0']->	computer_fee + $rem_fee['fee']['0']->sports_fee + $rem_fee['fee']['0']->insurance_fee + $rem_fee['fee']['0']->exam_fee+ $rem_fee['fee']['0']->enroll_fee;
                            }
                        } else {
                            $rem = NULL;
                        }
                    }else if($fee_result[0]->feestructure_flag == '2'){
                        if (count($rem_fee['fee']) > 0) {
                            if ($admission_y == $current_year || $standard_result[0]->standard == '1' || $standard_result[0]->standard == 'I' || $standard_result[0]->standard == '9') {
                                $rem = $rem_fee['fee']['0']->admission_fee + ($rem_fee['fee']['0']->tuition_fee) * 12 + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->term_fee  + $rem_fee['fee']['0']->other_fee;
                            } else {
                                $rem = ($rem_fee['fee']['0']->tuition_fee) * 12 + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->other_fee;
                            }

                        } else {
                            $rem = NULL;
                        }
                    }else if($fee_result[0]->feestructure_flag == '4'){
                        if (count($rem_fee['fee']) > 0) {
                            $rem = $rem_fee['fee']['0']->tuition_fee;

                        } else {
                            $rem = NULL;
                        }
                    }else{
                        if (count($rem_fee['fee']) > 0) {
                            if ($admission_y == $current_year ||  $standard_result[0]->standard == '1' || $standard_result[0]->standard == 'I' || $standard_result[0]->standard == '9') {
                                $rem = $rem_fee['fee']['0']->admission_fee + ($rem_fee['fee']['0']->tuition_fee) * 12 + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->other_fee + $rem_fee['fee']['0']->other_fee;
                            } else {
                                $rem = ($rem_fee['fee']['0']->tuition_fee) * 12 + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->term_fee + $rem_fee['fee']['0']->other_fee + $rem_fee['fee']['0']->other_fee;
                            }

                        } else {
                            $rem = NULL;
                        }
                    }
                } else {
                    $rem = '0';
                }
                $this->db->set('remaining_fee', $rem);
                $query = $this->db->insert('tbl_student');
            }
        }
        return $query;
    }
}
