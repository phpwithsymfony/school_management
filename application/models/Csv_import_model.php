<?php

class Csv_import_model extends CI_Model
{
    function select()
    {
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('tbl_student');
        return $query;
    }

    function insert($data)
    {
        $insert = $this->db->insert_batch('tbl_student', $data);
        if ($insert) {
            $update = "UPDATE `tbl_standard_management` SET `division` = TRIM(LEADING ',' FROM `division`)";
            $this->db->query($update);
        }
    }

    public function get_std_value()
    {
        $this->db->select('standard,id');
        $query = $this->db->get('tbl_standard_management');
        return $query->result();
    }

    public function get_school_value()
    {
        $this->db->select('name,id');
        $query = $this->db->get('tbl_school');
        return $query->result();
    }
}