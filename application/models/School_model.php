<?php

class School_model extends CI_Model
{
    var $table = 'tbl_school';
    var $column_order = array('id', 'school_code', 'name', 'email', 'contact', 'city', 'state');
    var $column_search = array('id', 'school_code', 'name', 'email', 'contact', 'city', 'state');
    var $order = array('school_code' => 'asc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {

        //add search
        if ($this->input->post('school_id')) {
            $this->db->like('school_code', $this->input->post('school_id'));
        }
        if ($this->input->post('school_name')) {
            $this->db->like('name', $this->input->post('school_name'));
        }
        if ($this->input->post('school_contact')) {
            $this->db->like('contact', $this->input->post('school_contact'));
        }
        if ($this->input->post('school_email')) {
            $this->db->like('email', $this->input->post('school_email'));
        }
        if ($this->input->post('school_city')) {
            $this->db->like('city', $this->input->post('school_city'));
        }
        if ($this->input->post('school_state')) {
            $this->db->like('state', $this->input->post('school_state'));
        }
        $this->db->from($this->table);
        $i = 0;

        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    //for update school details
    public function update_school($id)
    {
        $this->db->SELECT('*');
        $school_id = array('id' => $id);
        $query = $this->db->get_where('tbl_school', $school_id);
        return $query->result();
    }

    //for school id exists or not
    function school_id_exists($school_id)
    {
        $this->db->select('school_code');
        $this->db->where('school_code', $school_id);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    //check for delete
    public function check_delete($id)
    {
        $this->db->select('*');
        $array = array('school_id' => $id);
        $query = $this->db->get_where('tbl_student', $array);
        return $query->num_rows();

    }

    //check for email
    public function email_id_exists($email_id)
    {
        $this->db->select('*');
        $array = array('email' => $email_id);
        $query = $this->db->get_where('tbl_school', $array);
        return $query->num_rows();
    }
}

?>