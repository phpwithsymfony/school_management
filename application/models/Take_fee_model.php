<?php

class Take_fee_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    //for get student data
    public function take_fee($id,$student_fee_year='')
    {

        $this->db->select('tbl_student.*,tbl_standard_management.standard,tbl_standard.admission_fee,tbl_standard.tuition_fee,tbl_standard.term_fee,tbl_standard.other_fee,tbl_standard.smart_class_fee,tbl_standard.computer_fee,tbl_standard.sports_fee,tbl_standard.insurance_fee,tbl_standard.exam_fee,tbl_standard.enroll_fee');

        $this->db->from('tbl_student');
        $this->db->join('tbl_standard_management', 'tbl_standard_management.id=tbl_student.standard_id', 'left');

        $this->db->join('tbl_standard', 'tbl_standard.standard_mgt_id=tbl_student.standard_id', 'left');

        if(isset($student_fee_year) && $student_fee_year!=""){
            $this->db->where('tbl_standard.fee_year', $student_fee_year);
        }

        $this->db->where('tbl_student.id', $id);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }

    //for get remaining fee
    public function remaining_fee($id,$fee_year='')
    {
        $this->db->select('remaining_fee');
        $this->db->where('id', $id);
        if($fee_year){
            $this->db->where('academic_year', $fee_year);
        }
        $this->db->from('tbl_student');
        $query = $this->db->get();
        return $query->result();
    }
    //for get last pending fee for edit fee
    public function get_pending_fee($id)
    {
        $this->db->select('student_id');
        $this->db->where('id', $id);
        $s_query = $this->db->get('tbl_fee_details');
        $s_result = $s_query->result();
        $student_id=$s_result[0]->student_id;

        $this->db->select('*');
        $this->db->where('student_id',$student_id);
        $get_query=$this->db->get('tbl_fee_details');
        $get_result=$get_query->num_rows();

        if($get_result==1){
            return '';
        }else{
            $query="select *from tbl_fee_details where student_id = $student_id order by id DESC LIMIT 1,1";
            $pending_fee=$this->db->query($query);
            return $pending_fee->result();
        }

    }
    //for get last fee month
    public function get_last_month($id)
    {
        $this->db->select('*');
        $array = array('student_id' => $id);
        $query = $this->db->get_where('tbl_fee_details', $array);
        return $query->last_row();
    }
    public function edit_get_last_month($fee_id){
        $this->db->select('*');
        $array = array('id' => $fee_id);
        $query = $this->db->get_where('tbl_fee_details', $array);
        return $query->last_row();
    }
    //for paid one month

    public function paid_amount($id)
    {
        $this->db->select('*');
        $array = array('student_id' => $id);
        $query = $this->db->get_where('tbl_fee_details', $array);
        return $query->last_row();
    }
}